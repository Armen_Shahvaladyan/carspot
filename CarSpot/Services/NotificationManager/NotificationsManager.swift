//
//  NotificationsManager.swift
//  RAN Oil
//
//  Created by Sergey Charchoghlyan on 10.07.21.
//

import Firebase
import FirebaseMessaging
import UserNotifications

class PushNotificationManager: NSObject, MessagingDelegate, UNUserNotificationCenterDelegate {
    
    override init() {
        super.init()
    }
    private let deviceProvider = Provider<DeviceEndPoint>()
    
    func registerForPushNotifications() {
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.delegate = self
        notificationCenter.getNotificationSettings { settings in
            switch settings.authorizationStatus {
            case .authorized:
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                    self.updateFirebasePushTokenIfNeeded()
                }
            case .notDetermined:
                self.requestNotificationAuthorization(completion: { (error) in
                    if error == nil {
                        DispatchQueue.main.async {
                            UIApplication.shared.registerForRemoteNotifications()
                            self.updateFirebasePushTokenIfNeeded()
                        }
                    } else {
                        print("Denied")
                    }
                })
            case .denied:
                print("Denied")
            default :
                break
            }
        }
    }
    
    private func requestNotificationAuthorization(completion: @escaping (Error?) -> Void) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (success, error) in
            if success && error != nil {
                completion(nil)
            } else {
                completion(error)
            }
        }
    }

    private func updateFirebasePushTokenIfNeeded() {
        if let token = Messaging.messaging().fcmToken {
            KeychainWrapper.deviceCode = token
            deviceProvider.request(target: .add(token), type: Empty.self) { _,_  in }
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        updateFirebasePushTokenIfNeeded()
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
}
