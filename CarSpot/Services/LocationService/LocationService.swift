//
//  LocationManager.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.10.21.
//

import Foundation
import CoreLocation

public enum ServiceState {
    case available
    case notDetermined
    case denied
    case restricted
    case disabled
}
 
protocol LocationServiceDelegate: AnyObject {
    func locationService(_ service: LocationService, didFailWithError error: Error)
    func locationService(_ service: LocationService, didUpdateLocations locations: [CLLocation])
    func locationService(_ service: LocationService, didChange state: ServiceState)
}

final class LocationService: NSObject {
    
    //MARK: - Lifecycle
    override init() {
        super.init()
        locationManager.delegate = self
//        locationManager.startUpdatingLocation()
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.requestLocation()
//        } else {
//            locationManager.requestWhenInUseAuthorization()
//        }
    }
    
    //MARK: - Properties
    weak var delegate: LocationServiceDelegate?
    private let locationManager = CLLocationManager()
    
    var state: ServiceState {
        guard CLLocationManager.locationServicesEnabled() else {
            return .disabled
        }
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            return .notDetermined
        case .denied:
            return .denied
        case .restricted:
            return .restricted
        default:
            return .available
        }
    }
    
    var distanceFilter: CLLocationDistance {
        set {
            locationManager.distanceFilter = newValue
        }
        get {
            locationManager.distanceFilter
        }
    }
    
    //MARK: - Public API
    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
    
    func startUpdatingLocation() {
        locationManager.startUpdatingLocation()
    }
    
    func requestWhenInUseAuthorization() {
        locationManager.requestWhenInUseAuthorization()
        startUpdatingLocation()
    }
}

extension LocationService: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        delegate?.locationService(self, didChange: state)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        delegate?.locationService(self, didUpdateLocations: locations)
        
//        guard let location = locations.first else { return }
//        mapView.animate(toLocation: location.coordinate)
//        GooglePlacesRequestHelpers.getPlaceAddress(location: location.coordinate) { address in
//            if self.viewModel.status?.state != .find {
//                self.updateAddress(address, location.coordinate.latitude, location.coordinate.longitude)
//            }
//        }
//        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        delegate?.locationService(self, didFailWithError: error)
    }
}
