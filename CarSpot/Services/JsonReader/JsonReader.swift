//
//  JsonReader.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.05.21.
//

import Foundation

enum JsonName {
    case country
    
    //MARK: - Properties
    var name: String {
        switch self {
        case .country:
            return "phone_code"
        }
    }
}

struct JsonReader<T> where T: Decodable {
    
    //MARK: - Public API
    func read(with jsonName: JsonName) -> ([T]?, Error?) {
        if let url = Bundle.main.url(forResource: jsonName.name, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let models = try decoder.decode([T].self, from: data)
                return (models, nil)
            } catch {
                return (nil, error)
            }
        }
        return (nil, nil)
    }
}
