//
//  KeychainWrapper.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 01.06.21.
//

import Foundation
import KeychainAccess

struct KeychainWrapper {
    static private let keychain = Keychain()
    
    static var token: String? {
        get {
            do {
                return try keychain.get(KeychainWrapperKeys.token.rawValue)
            } catch {
                return nil
            }
        }
        set {
            do {
                if let newValue = newValue {
                    try? keychain.set(newValue, key: KeychainWrapperKeys.token.rawValue)
                } else {
                    try? keychain.remove(KeychainWrapperKeys.token.rawValue)
                }
            }
        }
        
    }
    
    static var deviceCode: String? {
        get {
            do {
                return try keychain.get(KeychainWrapperKeys.deviceCode.rawValue)
            } catch {
                return nil
            }
        }
        set {
            do {
                if let newValue = newValue {
                    try? keychain.set(newValue, key: KeychainWrapperKeys.deviceCode.rawValue)
                } else {
                    try? keychain.remove(KeychainWrapperKeys.deviceCode.rawValue)
                }
            }
        }
    }
}

private extension KeychainWrapper {
    enum KeychainWrapperKeys: String {
        case token
        case deviceCode
    }
}
