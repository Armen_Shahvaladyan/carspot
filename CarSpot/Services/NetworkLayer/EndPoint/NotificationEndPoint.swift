//
//  NotificationEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.07.21.
//

import Moya

enum NotificationEndPoint {
    case notifications(Int, Int)
    case delete(String)
    case off(NotificationOption)
    case settings
}

extension NotificationEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .notifications:
            return "api/Notification/GetAll"
        case .delete:
            return "api/Notification/Delete"
        case .off:
            return "api/Notification/Off"
        case .settings:
            return "api/Notification/GetSettings"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .notifications, .delete, .off:
            return .post
        case .settings:
            return .get
        }
    }
    
    var parameters: Parameter {
        switch self {
        case let .notifications(skip, take):
            return ["skip": skip,
                    "take": take]
        case .delete(let id):
            return ["id": id]
        case .off(let option):
            return ["notificationOffType": option.rawValue]
        case .settings:
            return [:]
        }
    }
    
    var task: Task {
        switch self {
        case .settings:
            return .requestPlain
        case .notifications, .delete, .off:
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        }
    }
}
