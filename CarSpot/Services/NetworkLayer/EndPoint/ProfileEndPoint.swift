//
//  ProfileEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 21.07.21.
//

import Moya

enum ProfileEndPoint {
    case info
    case edit(User)
    case uploadPhoto(File)
    case logOut(String)
    case coins
}

extension ProfileEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .info:
            return "api/Account/Get"
        case .edit:
            return "api/Account/Edit"
        case .uploadPhoto:
            return "api/Account/UploadProfilePicture"
        case .logOut:
            return "api/Account/SignOut"
        case .coins:
            return "api/User/GetCoins"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .info, .coins:
            return .get
        case .edit, .uploadPhoto, .logOut:
            return .post
        }
    }
    
    var parameters: Parameter {
        switch self {
        case .info, .uploadPhoto, .coins:
            return [:]
        case .edit(let user):
            return user.mapToJSON()
        case .logOut(let deviceId):
            return ["deviceId": deviceId]
        }
    }
    
    var task: Task {
        switch self {
        case .info, .coins:
            return .requestPlain
        case .edit, .logOut:
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        case let .uploadPhoto(file):
            return .uploadMultipart([file.multipartBody])
        }
    }
}
