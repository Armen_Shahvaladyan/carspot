//
//  PaymentMethodEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 22.07.21.
//

import Moya

enum PaymentMethodEndPoint {
    case cards
    case add(String, String, String, String)
    case delete(String)
    case changeDefault(String)
}

extension PaymentMethodEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .cards:
            return "api/Payment/GetAttachedCards"
        case .add:
            return "api/Payment/AttachCard"
        case .delete:
            return "api/Payment/DeleteAttachedCard"
        case .changeDefault:
            return "api/Payment/DefaultCard"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .cards:
            return .get
        case .add, .delete, .changeDefault:
            return .post
        }
    }
    
    var parameters: Parameter {
        switch self {
        case .cards:
            return [:]
        case .delete(let id):
            return ["id": id]
        case .changeDefault(let id):
            return ["cardId": id]
        case let .add(number, date, code, name):
            return ["cardNumber": number.replacingOccurrences(of: " ", with: ""),
                    "expirationDate": date,
                    "cardCode": code,
                    "ownerName": name]
        }
    }
    
    var task: Task {
        switch self {
        case .cards:
            return .requestPlain
        case .add, .delete, .changeDefault:
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        }
    }
}
