//
//  AuthEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 15.07.21.
//

import Moya

enum AuthEndPoint {
    case sendCode(String)
    case verify(String, String)
    case add(User)
}

extension AuthEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .sendCode:
            return "api/Account/SendCode"
        case .verify:
            return "api/Account/Verify"
        case .add:
            return "api/Account/Add"
        }
    }
    
    var method: Moya.Method {
        .post
    }
    
    var parameters: Parameter {
        switch self {
        case .sendCode(let phoneNumber):
            return ["phoneNumber": phoneNumber]
        case let .verify(phoneNumber, code):
            return ["phoneNumber": phoneNumber, "code": code]
        case .add(let user):
            return user.mapToJSON()
        }
    }
    
    var task: Task {
        .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
    }
}
