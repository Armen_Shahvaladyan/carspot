//
//  HelpEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 27.07.21.
//

import Moya

enum HelpEndPoint {
    case info(String)
}

extension HelpEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .info:
            return "api/Help/GetAll"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .info:
            return .post
        }
    }
    
    var parameters: Parameter {
        switch self {
        case .info(let search):
            return search.isEmpty ? [:] : ["search": search]
        }
    }
    
    var task: Task {
        switch self {
        case .info:
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        }
    }
}
