//
//  SuggestParkingEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 26.08.21.
//

import Moya

enum SuggestParkingEndPoint {
    case stop
    case add(Double, Double, String, Double, StreetOption)
    case start(Spot, Double)
    case defaultData
}

extension SuggestParkingEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .add:
            return "api/SuggestParking/Add"
        case .stop:
            return "api/SuggestParking/Stop"
        case .start:
            return "api/SuggestParking/Start"
        case .defaultData:
            return "api/SuggestParking/GetDefaultData"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .add, .stop, .start:
            return .post
        case .defaultData:
            return .get
        }
    }
    
    var parameters: Parameter {
        switch self {
        case .stop, .defaultData:
            return [:]
        case let .add(lat, lng, address, amount, parkingType):
            return ["lat": lat,
                    "lng": lng,
                    "address": address,
                    "amount": amount,
                    "parkingType": parkingType.value]
        case let .start(spot, amount):
            return ["lat": spot.lat,
                    "lng": spot.lng,
                    "spotId": spot.id,
                    "authorId": spot.authorId,
                    "amount": amount,
                    "saleSpotId": spot.saleSpotId]
        }
    }
    
    var task: Task {
        switch self {
        case .defaultData:
            return .requestPlain
        case .add, .stop, .start:
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        }
    }
}
