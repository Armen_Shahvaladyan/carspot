//
//  StateEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 23.09.21.
//

import Moya

enum StateEndPoint {
    case parking
    case subscription
}

extension StateEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .parking:
            return "api/State/Parking"
        case .subscription:
            return "api/State/Subscription"
        }
    }
    
    var method: Moya.Method {
        .get
    }
}
