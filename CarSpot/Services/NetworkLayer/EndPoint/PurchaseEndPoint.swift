//
//  PurchaseEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 26.07.21.
//

import Moya

enum PurchaseEndPoint {
    case purchases(Int, Int)
}

extension PurchaseEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .purchases:
            return "api/Coupon/GetByUser"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .purchases:
            return .post
        }
    }
    
    var parameters: Parameter {
        switch self {
        case let .purchases(skip, take):
            return ["skip": skip,
                    "take": take]
        }
    }
    
    var task: Task {
        switch self {
        case .purchases:
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        }
    }
}
