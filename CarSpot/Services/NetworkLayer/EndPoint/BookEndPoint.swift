//
//  BookEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 08.09.21.
//

import Moya

enum BookEndPoint {
    case add(String)
    case accept(String)
    case reject(String)
}

extension BookEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .add:
            return "api/Book/Add"
        case .accept:
            return "api/Book/Accept"
        case .reject:
            return "api/Book/Reject"
        }
    }
    
    var method: Moya.Method {
        .post
    }
    
    var parameters: Parameter {
        switch self {
        case .add(let id):
            return ["spotId": id]
        case .accept(let id):
            return ["spotId": id]
        case .reject(let id):
            return ["spotId": id]
        }
    }
    
    var task: Task {
        .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
    }
}
