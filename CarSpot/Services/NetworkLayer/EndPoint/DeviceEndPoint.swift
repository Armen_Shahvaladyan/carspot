//
//  DevicceEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.09.21.
//

import Moya

enum DeviceEndPoint {
    case add(String)
}

extension DeviceEndPoint: MultiTargetType {
    var path: String {
        "api/Device/Add"
    }
    
    var method: Moya.Method {
        .post
    }
    
    var parameters: Parameter {
        switch self {
        case .add(let pushToken):
            return ["token": pushToken,
                    "deviceId": UIDevice.current.identifierForVendor?.uuidString ?? "",
                    "osType": 2]
        }
    }
    
    var task: Task {
        .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
    }
}
