//
//  TransactionEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 30.07.21.
//

import Moya

enum TransactionEndPoint {
    case transactions(TransactionFilter, Int, Int)
}

extension TransactionEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .transactions:
            return "api/Transaction/GetAll"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .transactions:
            return .post
        }
    }
    
    var parameters: Parameter {
        switch self {
        case let .transactions(filter, skip, take):
            var params = filter.map()
            params["skip"] = skip
            params["take"] = take
            return params
        }
    }
    
    var task: Task {
        switch self {
        case .transactions:
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        }
    }
}
