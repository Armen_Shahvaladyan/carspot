//
//  FindParkiingEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.08.21.
//

import Moya

enum FindParkiingEndPoint {
    case stop
    case find(Double, Double, String)
    case hide(String)
    case defaultData
}

extension FindParkiingEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .find:
            return "api/FindParkiing/Start"
        case .stop:
            return "api/FindParkiing/Stop"
        case .hide:
            return "api/FindParkiing/Hide"
        case .defaultData:
            return "api/FindParkiing/GetDefaultData"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .find, .stop, .hide:
            return .post
        case .defaultData:
            return .get
        }
    }
    
    var parameters: Parameter {
        switch self {
        case .stop, .defaultData:
            return [:]
        case let .find(lat, lng, address):
            return ["lat": lat,
                    "lng": lng,
                    "address": address]
        case .hide(let id):
            return ["spotId": id]
        }
    }
    
    var task: Task {
        switch self {
        case .defaultData:
            return .requestPlain
        case .find, .stop, .hide:
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        }
    }
}
