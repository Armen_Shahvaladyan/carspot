//
//  UserEndPoint.swift
//  LEDrivers
//
//  Created by Local Express on 8/13/20.
//  Copyright © 2020 Local Express. All rights reserved.
//

import Moya

enum CarEndPoint {
    case brands
    case color
}

extension CarEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .brands:
            return "api/Car/GetAll"
        case .color:
            return "api/Color/GetAll"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Task {
        .requestPlain
    }
}
