//
//  CouponEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 16.08.21.
//

import Moya

enum CouponEndPoint {
    case all(Int, Int)
    case buy(String)
}

extension CouponEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .all:
            return "api/Coupon/GetAll"
        case .buy:
            return "api/Coupon/Buy"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .all, .buy:
            return .post
        }
    }
    
    var parameters: Parameter {
        switch self {
        case let .all(skip, take):
            return ["skip": skip,
                    "take": take]
        case .buy(let id):
            return ["couponId": id]
        }
    }
    
    var task: Task {
        switch self {
        case .all, .buy:
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        }
    }
}
