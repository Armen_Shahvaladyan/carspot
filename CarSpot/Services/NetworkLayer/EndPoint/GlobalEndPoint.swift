//
//  GlobalEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 15.07.21.
//

import Moya

enum GlobalEndPoint {
    case all
    case rewards
}

extension GlobalEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .all:
            return "api/AppGlobal/GetAll"
        case .rewards:
            return "api/AppGlobal/GetRewards"
        }
    }
    
    var method: Moya.Method {
        .get
    }
    
    var task: Task {
        .requestPlain
    }
}
