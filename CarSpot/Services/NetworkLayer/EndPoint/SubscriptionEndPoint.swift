//
//  SubscriptionPlanEndPoin.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.08.21.
//

import Moya

enum SubscriptionEndPoint {
    case state
    case buyCoins(Double, String)
    case freeTrial
    case plans
    case subscribe(String, String?, String?, String?)
    case checkPromoCode(String)
}

extension SubscriptionEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .buyCoins:
            return "api/SubscriptionPlan/BuyCoins"
        case .state:
            return "api/State/Subscription"
        case .freeTrial, .subscribe:
            return "api/SubscriptionPlan/Subscribe"
        case .plans:
            return "api/SubscriptionPlan/GetAll"
        case .checkPromoCode:
            return "api/SubscriptionPlan/CheckPromoCodeAndGet"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .state, .plans, .checkPromoCode:
            return .get
        case .buyCoins, .freeTrial, .subscribe:
            return .post
        }
    }
    
    var parameters: Parameter {
        switch self {
        case .state, .freeTrial, .plans:
            return [:]
        case .checkPromoCode(let code):
            return ["promoCode": code]
        case let .subscribe(planId, couponId, promoCode, cardId):
            var param = ["subscriptionPlanId": planId]
            
            if let couponId = couponId {
                param["couponId"] = couponId
            }
            
            if let promoCode = promoCode {
                param["promoCode"] = promoCode
            }
            
            if let cardId = cardId {
                param["cardId"] = cardId
            }
            
            return param
        case let .buyCoins(coins, id):
            return ["coins": coins, "cardId": id]
        }
    }
    
    var task: Task {
        switch self {
        case .checkPromoCode:
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        case .state, .plans:
            return .requestPlain
        case .buyCoins, .freeTrial, .subscribe:
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        }
    }
}
