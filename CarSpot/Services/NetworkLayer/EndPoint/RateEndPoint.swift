//
//  RateEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.09.21.
//

import Moya

enum RateEndPoint {
    case tip(String, Int)
    case add(String, Int, String?)
}

extension RateEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .tip:
            return "api/Tip/Add"
        case .add:
            return "api/Rating/Add"
        }
    }
    
    var method: Moya.Method {
        .post
    }
    
    var parameters: Parameter {
        switch self {
        case let .tip(id, coins):
            return ["saleSpotId": id,
                    "coins": coins]
        case let.add(id, rating, reason):
            var param: [String: Any] = ["saleSpotId": id,
                                        "rating": rating]
            if let reason = reason {
                param["reason"] = reason
            }
            return param
        }
    }
    
    var task: Task {
        .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
    }
}
