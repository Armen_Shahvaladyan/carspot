//
//  AddressEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 24.07.21.
//

import Moya

enum AddressEndPoint {
    case addresses
    case add(Double, Double, String, String, String, Int)
    case delete(String)
}

extension AddressEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .add:
            return "api/Address/Add"
        case .addresses:
            return "api/Address/GetAll"
        case .delete:
            return "api/Address/Delete"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .add, .delete:
            return .post
        case .addresses:
            return .get
        }
    }
    
    var parameters: Parameter {
        switch self {
        case .addresses:
            return [:]
        case let .add(lat, lng, address, name, comment, type):
            return ["lat": lat,
                    "lng": lng,
                    "addressText": address,
                    "name": name,
                    "addressType": type,
                    "comment": comment]
        case .delete(let id):
            return ["id": id]
        }
    }
    
    var task: Task {
        switch self {
        case .addresses:
            return .requestPlain
        case .add, .delete:
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        }
    }
}
