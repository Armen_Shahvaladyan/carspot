//
//  LocationEndPoint.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 30.09.21.
//

import Moya

enum LocationEndPoint {
    case share(Double, Double)
}

extension LocationEndPoint: MultiTargetType {
    var path: String {
        "api/Location/Share"
    }
    
    var method: Moya.Method {
        .post
    }
    
    var parameters: Parameter {
        switch self {
        case .share(let lat, let lng):
            return ["lat": lat, "lng": lng]
        }
    }
    
    var task: Task {
        .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
    }
}
