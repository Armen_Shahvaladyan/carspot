//
//  SignalRService.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 24.08.21.
//

import Foundation
import SwiftSignalRClient

class SignalRService {
    
    //MARK: - Properties
    var handleFindParking: (([Spot]) -> Void)?
    var handleSuggestParking: (([Spot]) -> Void)?
    var handleBookSpot: ((ParkingStatus?) -> Void)?
    var handleShareLocation: (() -> Void)?
    var handleBookReject: (() -> Void)?
    var handleBookFinish: (() -> Void)?
    
    private var connection: HubConnection!
    
    //MARK: - Lifecycle
    init(url: URL) {
        connection = HubConnectionBuilder(url: url).withLegacyHttpConnection()
            .withAutoReconnect()
            .withHttpConnectionOptions(configureHttpOptions: { option in
                option.accessTokenProvider = {
                    KeychainWrapper.token
                }
            }).withLogging(minLogLevel: .debug).build()
        
        connection.delegate = self
        
        connection.on(method: "findParking") { message in
            if let spots = try? message.getArgument(type: [Spot].self) {
                self.handleFindParking?(spots)
            } else {
                self.handleFindParking?([])
            }
        }
        
        connection.on(method: "book") { message in
            if let status = try? message.getArgument(type: ParkingStatus.self) {
                self.handleBookSpot?(status)
            } else {
                self.handleBookSpot?(nil)
            }
        }
        
        connection.on(method: "suggestParking") { message in
            if let spots = try? message.getArgument(type: [Spot].self) {
                self.handleSuggestParking?(spots)
            } else {
                self.handleSuggestParking?([])
            }
        }
        
        connection.on(method: "shareLocation") { message in
            print("--------", message)
            self.handleShareLocation?()
        }
        
        connection.on(method: "bookReject") { message in
            self.handleBookReject?()
        }
        
        connection.on(method: "bookFinish") { message in
            self.handleBookFinish?()
        }
        
        connection.start()
    }

    //MARK: - Private API
    private func handleMessage(_ message: String, from user: String) {
        print(message, user)
    }
}

extension SignalRService: HubConnectionDelegate {
    func connectionDidOpen(hubConnection: HubConnection) {
        print(#function)
    }
    
    func connectionDidFailToOpen(error: Error) {
        print(#function)
    }
    
    func connectionDidClose(error: Error?) {
        print(#function)
    }
}
