//
//  GooglePlacesRequestService.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 26.07.21.
//

import GooglePlaces

class GooglePlacesRequestHelpers {
    
    static func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            Thread.onMainThread {
                completion(json)
            }
        })
        
        task.resume()
    }
    
    static func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0) })
            }
        )
    }
    
    static func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: [ "placeid": id, "key": apiKey ],
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    static func getPlaceAddress(location: CLLocationCoordinate2D, completion: @escaping (String?) -> Void) {
        let latlng = String(location.latitude) + "," + String(location.longitude)
        doRequest("https://maps.googleapis.com/maps/api/geocode/json", params: [ "latlng": latlng, "key": Constant.googlePlaceAPIKey], completion: {
            let array = $0.value(forKey: "results") as? [[String: Any]]
            
            if let place = array?.first {
                completion(place["formatted_address"] as? String)
            } else {
                completion(nil)
            }
        })
    }
}
