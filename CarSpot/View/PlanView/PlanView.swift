//
//  PlanView.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 07.08.21.
//

import UIKit

class PlanView: UIView, NibView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - Public API
    func configure(with title: String) {
        titleLabel.text = title
    }
}
