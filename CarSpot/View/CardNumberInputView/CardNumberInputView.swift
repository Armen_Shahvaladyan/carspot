//
//  CardNumberInputView.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 12.06.21.
//

import UIKit
import SkyFloatingLabelTextField

protocol CreditCardEditingViewDelegate: AnyObject {
    func creditCardEditingViewDidOpenScan(_ view: CreditCardEditingView)
    func creditCardEditingView(_ view: CreditCardEditingView, didChange text: String)
}

@IBDesignable
class CreditCardEditingView: UIView {
    // MARK: Parameters
    weak var delegate: CreditCardEditingViewDelegate?
    
    enum InputType: Int {
        case cardNumber = 0
        case cardHolder
        case expDate
        case cvvNumder
    }
    
    private var leadingConstrinat: NSLayoutConstraint!
    private var trailingConstrinat: NSLayoutConstraint!
    
    private(set) var leftPadding: CGFloat = 16
    private(set) var rightPadding: CGFloat = 16
    
    private var containerView: CornerShadowView = {
        let v = CornerShadowView()
        v.addCorners([.topLeft, .topRight])
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    private(set) var textField: SkyFloatingLabelTextField = {
        let tf = SkyFloatingLabelTextField()
        tf.textColor = UIColor("2E2E33")
        tf.lineHeight = 0
        tf.lineColor = .clear
        tf.selectedLineColor = .clear
        tf.titleColor = UIColor("737380", alpha: 0.8)
        tf.placeholderColor = UIColor("2E2E33")
        tf.selectedTitleColor = UIColor("#FD6632")
        tf.titleFont = UIFont.SF(.display, weight: .regular, size: 12)
        tf.placeholderFont = UIFont.SF(.display, weight: .regular, size: 16)
        tf.titleFormatter = { $0 }
        tf.tintColor = UIColor("#FD6632")
        tf.addTarget(self, action: #selector(formatTextField), for: .editingChanged)
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    private(set) var imageView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.contentMode = .scaleAspectFit
        iv.isUserInteractionEnabled = true
//        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    var type: InputType = .cardHolder {
        didSet {
            switch type {
            case .cardNumber:
                imageView.image = #imageLiteral(resourceName: "ic_scan")
                textField.keyboardType = .numberPad
            case .cardHolder:
                imageView.isHidden = true
                textField.keyboardType = .default
                textField.autocorrectionType = .no
            case .expDate:
                let pickerView = UIPickerView()
                pickerView.delegate = self
                pickerView.dataSource = self
                textField.inputView = pickerView
                imageView.isHidden = true
            case .cvvNumder:
                imageView.isHidden = true
                textField.keyboardType = .numberPad
                textField.isSecureTextEntry = true
            }
        }
    }
    
    private(set) var lineView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor("C9C9CE")
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    @IBInspectable var placeholder: String = "hj" {
        didSet {
            self.textField.placeholder = placeholder
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var text: String = "hj" {
        didSet {
            self.textField.text = text
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var textColor: UIColor = .black {
        didSet {
            self.textField.textColor = textColor
            self.setNeedsDisplay()
        }
    }

    var leftMargin: CGFloat = 16 {
        didSet {
            leadingConstrinat.constant = leftMargin
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    
    var rightMargin: CGFloat = 16 {
        didSet {
            trailingConstrinat.constant = rightMargin
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    
    //MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.viewInit()
        self.improvePerformance()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.viewInit()
        self.improvePerformance()
    }
    
    override func becomeFirstResponder() -> Bool {
        return textField.becomeFirstResponder()
    }
    
    override func resignFirstResponder() -> Bool {
        return textField.resignFirstResponder()
    }
    
    //MARK: - Private API
    private func improvePerformance() {
        /// Cache the view into a bitmap instead of redrawing the stars each time
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        isOpaque = true
    }
    
    private func viewInit() {
        containerViewInit()
        subViewsInit()
    }
    
    @objc func formatTextField(_ sender: UITextField) {
        switch type {
        case .cardNumber:
            let rawText = textField.text?.components(separatedBy: " ").joined() ?? ""
            var newText = String(rawText.prefix(16))
            
            let spaceIndex = [12, 8, 4]
            
            for index in spaceIndex {
                guard newText.count >= index + 1 else { continue }
                newText.insert(" ", at: String.Index(encodedOffset: index))
            }
            
            setText(newText)
        case .cardHolder:
            setText(textField.text)
        case .expDate:
            break
        case .cvvNumder:
            let rawText = textField.text ?? ""
            let newText = String(rawText.prefix(4))
            setText(newText)
        }
        
        delegate?.creditCardEditingView(self, didChange: textField.text ?? "")
    }
    
    func setText(_ text: String?) {
        if textField.text != text {
            textField.text = text
        }
    }
}

private extension CreditCardEditingView {
    func containerViewInit() {
        self.addSubview(containerView)
        
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }
    
    func subViewsInit() {
        containerView.addSubview(lineView)
        
        lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        lineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        lineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        lineView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.addArrangedSubview(textField)
        stackView.addArrangedSubview(imageView)
        
        containerView.addSubview(stackView)
        stackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: leftPadding).isActive = true
        stackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -rightPadding).isActive = true
        stackView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        
        imageView.widthAnchor.constraint(equalToConstant: 24).isActive = true
        
        textField.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(openScan))
        imageView.addGestureRecognizer(tap)
    }
    
    @objc func openScan() {
        delegate?.creditCardEditingViewDidOpenScan(self)
    }
}

extension CreditCardEditingView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        lineView.backgroundColor = UIColor("#FD6632")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        lineView.backgroundColor = UIColor("C9C9CE")
    }
}

extension CreditCardEditingView: UIPickerViewDataSource, UIPickerViewDelegate {
    enum ExpDate: Int, CaseIterable {
        case month
        case year
        var data: [String] {
            switch self {
            case .month:
                return (1...12).map({ String(format: "%02d", arguments: [$0]) })
            case .year:
                let year = Calendar.current.component(.year, from: Date())
                return (year...year + 20).map(String.init)
            }
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return ExpDate.allCases.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ExpDate(rawValue: component)?.data.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ExpDate(rawValue: component)?.data[row] ?? ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let month = ExpDate.month.data[pickerView.selectedRow(inComponent: ExpDate.month.rawValue)]
        let year = ExpDate.year.data[pickerView.selectedRow(inComponent: ExpDate.year.rawValue)]
        setText("\(month)/\(year)")
        delegate?.creditCardEditingView(self, didChange: "\(month)/\(year.suffix(2))")
    }
}
