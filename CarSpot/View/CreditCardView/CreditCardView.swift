//
//  CreditCardView.swift
//  CardView
//
//  Created by Armen Shahvaladyan on 12.06.21.
//

import UIKit

protocol CreditCardViewDelegate: AnyObject {
    func creditCardViewDidSelectNumber(_ view: CreditCardView)
    func creditCardViewDidSelectHolder(_ view: CreditCardView)
    func creditCardViewDidSelectDate(_ view: CreditCardView)
}

class CreditCardView: UIView {
    
    @IBOutlet weak var cardContainerView: UIView!
    @IBOutlet weak var cardFrontView: UIView!
    
    @IBOutlet weak var cardBrandImageView: UIImageView!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cardHolderLabel: UILabel!
    @IBOutlet weak var expDateLabel: UILabel!
    
    @IBOutlet weak var cardNumberButton: UIButton!
    @IBOutlet weak var cardHolderButton: UIButton!
    @IBOutlet weak var expDateButton: UIButton!
    
    @IBOutlet weak var cardNumberFakeLabel: UILabel!
    @IBOutlet weak var nameSurnameLabel: UILabel!
    @IBOutlet weak var expDateFakeLabel: UILabel!
        
    weak var delegate: CreditCardViewDelegate?
    
    private(set) var cvvNumber: String = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    func updateCardNumber(with text: String) {
        cardNumberFakeLabel.textColor = text.count == 0 ? UIColor("959BBF") : .clear
        cardNumberLabel.isHidden = text.count <= 0
        cardNumberLabel.text = text
        if text.hasPrefix("4") {
            cardBrandImageView.image = #imageLiteral(resourceName: "VISA")
        } else if text.hasPrefix("5") || text.hasPrefix("2") {
            cardBrandImageView.image = #imageLiteral(resourceName: "MASTERCARD")
        } else if text.hasPrefix("3") {
            cardBrandImageView.image = #imageLiteral(resourceName: "AMERICAN_EXPRESS")
        } else {
            cardBrandImageView.image = nil
        }
    }
    
    func updateCardHolder(with text: String) {
        nameSurnameLabel.textColor = text.count == 0 ? UIColor("959BBF") : .clear
        cardHolderLabel.isHidden = text.count <= 0
        cardHolderLabel.text = text.uppercased()
    }
    
    func updateCardExpDate(with text: String) {
        expDateFakeLabel.textColor = text.count == 0 ? UIColor("959BBF") : .clear
        expDateLabel.isHidden = text.count <= 0
        expDateLabel.text = text
    }

    func updateCardCvvNumber(with text: String) {
        cvvNumber = text
    }
    
    @IBAction func tapCardNumberButton(_ sender: UIButton) {
        delegate?.creditCardViewDidSelectNumber(self)
    }
    
    @IBAction func tapCardHolderButton(_ sender: UIButton) {
        delegate?.creditCardViewDidSelectHolder(self)
    }
    
    @IBAction func tapExpDateButton(_ sender: UIButton) {
        delegate?.creditCardViewDidSelectDate(self)
    }
    
}

private extension CreditCardView {
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    func initNibView() {
        guard let view = CreditCardView.nib
            .instantiate(withOwner: self, options: nil)
            .compactMap({ $0 as? UIView })
            .first else {
                return
        }
        
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    func initView() {
        initNibView()

        backgroundColor = .clear
    }
}
