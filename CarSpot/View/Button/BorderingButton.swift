//
//  BorderingButton.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 23.05.21.
//

import UIKit

class BorderingButton: UIButton {
    
}

extension BorderingButton: Bordering {
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
}
