//
//  MainButton.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.05.21.
//

import UIKit

@objc
enum ButtonState: Int {
    case `default`
    case disable
    case load
    case secondary
}

@IBDesignable
class Button: UIControl {
    
    //MARK:  - Properties
    private var containerView: CornerShadowView = {
        let v = CornerShadowView()
        v.setupButtonConfigure()
        v.isUserInteractionEnabled = false
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    private var gradientrView: GradientView = {
        let gv = GradientView()
        gv.roundness = 16
        gv.startColor = UIColor("#FF884D")
        gv.endColor = UIColor("#FD6632")
        gv.isHorizontal = true
        gv.isUserInteractionEnabled = false
        gv.translatesAutoresizingMaskIntoConstraints = false
        return gv
    }()
    
    private lazy var titleLabel: UILabel = {
        let tl = UILabel()
        tl.text = title
        tl.textColor = titleColor
        tl.textAlignment = .center
        tl.font = font
        tl.isUserInteractionEnabled = false
        tl.translatesAutoresizingMaskIntoConstraints = false
        return tl
    }()
    
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView(style: .medium)
        ai.color = .gray
        ai.hidesWhenStopped = true
        ai.translatesAutoresizingMaskIntoConstraints = false
        return ai
    }()
    
    var buttonState: ButtonState = .default {
        didSet {
            switch buttonState {
            case .default:
                cofigureDefaultState()
            case .disable:
                cofigureDisableState()
            case .load:
                cofigureLoadState()
            case .secondary:
                cofigureSecondaryState()
            }
        }
    }
    
    @IBInspectable var title: String = "Next" {
        didSet {
            self.titleLabel.text = title
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var font: UIFont = UIFont.SF(.display, weight: .regular, size: 16) {
        didSet {
            self.titleLabel.font = font
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var titleColor: UIColor = .white {
        didSet {
            self.titleLabel.textColor = titleColor
            self.setNeedsDisplay()
        }
    }
    
    //MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
        improvePerformance()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
        improvePerformance()
    }
    
    //MARK: - Private API
    private func improvePerformance() {
        /// Cache the view into a bitmap instead of redrawing the stars each time
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        isOpaque = true
    }
}

extension Button {
    private func commonInit() {
        self.addSubview(containerView)
        
        backgroundColor = .clear
        isEnabled = true
        
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        containerView.addSubview(gradientrView)
        
        gradientrView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        gradientrView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        gradientrView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        gradientrView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        
        containerView.addSubview(titleLabel)
        
        titleLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 16).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -16).isActive = true
        
        containerView.addSubview(activityIndicatorView)
        
        activityIndicatorView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        activityIndicatorView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
    }
    
    private func cofigureDefaultState() {
        isEnabled = true
        titleLabel.isHidden = false
        gradientrView.isHidden = false
        titleColor = .white
        let conf = RoundedShadowConfigure(shadowSide: [], blur: 0, spread: 0, shadowColor: .clear,
                                          fillColor: .clear,
                                          corners: [.allCorners], radius: 16)
        containerView.roundedShadowConf = conf
        containerView.layoutRoundedShadow(on: containerView.layer, conf: conf)
        containerView.layer.borderWidth = 0
        containerView.layer.borderColor = UIColor.clear.cgColor
        activityIndicatorView.stopAnimating()
    }
    
    private func cofigureDisableState() {
        isEnabled = false
        titleLabel.isHidden = false
        gradientrView.isHidden = true
        titleColor = UIColor("868686")
        let conf = RoundedShadowConfigure(shadowSide: [], blur: 0, spread: 0, shadowColor: .clear,
                                          fillColor: UIColor("D2D2D6"),
                                          corners: [.allCorners], radius: 16)
        containerView.roundedShadowConf = conf
        containerView.layoutRoundedShadow(on: containerView.layer, conf: conf)
        containerView.layer.borderWidth = 0
        containerView.layer.borderColor = UIColor.clear.cgColor
        activityIndicatorView.stopAnimating()
    }
    
    private func cofigureLoadState() {
        isEnabled = false
        titleLabel.isHidden = true
        activityIndicatorView.startAnimating()
    }
    
    private func cofigureSecondaryState() {
        isEnabled = true
        titleColor = UIColor("#FD6632")
        titleLabel.isHidden = false
        gradientrView.isHidden = true
        let conf = RoundedShadowConfigure(shadowSide: [], blur: 0, spread: 0, shadowColor: .clear, fillColor: .white, corners: [.allCorners], radius: 16)
        containerView.roundedShadowConf = conf
        containerView.layoutRoundedShadow(on: containerView.layer, conf: conf)
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = UIColor("E8E8EA").cgColor
        containerView.layer.cornerRadius = 16
        containerView.layer.masksToBounds = true
        activityIndicatorView.stopAnimating()
    }
}
