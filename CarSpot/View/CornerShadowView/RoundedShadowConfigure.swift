//
//  RoundedShadowConfigure.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 22.05.21.
//

import UIKit

struct RoundedShadowConfigure {
    var shadowSide: ShadowSide
    let blur: CGFloat
    let spread: CGFloat
    let shadowColor: UIColor?
    let fillColor: UIColor? //default backgroundColor
    let corners: UIRectCorner
    let radius: CGFloat
    
    init(fillColor: UIColor? = nil, corners: UIRectCorner = [], radius: CGFloat = 0) {
        self.shadowSide = []
        self.blur = 0
        self.spread = 0
        self.shadowColor = .clear
        self.fillColor = fillColor
        self.corners = corners
        self.radius = radius
    }
    
    init(shadowSide: ShadowSide, blur: CGFloat, spread: CGFloat, shadowColor: UIColor?, fillColor: UIColor? = nil, corners: UIRectCorner, radius: CGFloat) {
        if shadowSide == .all {
            self.shadowSide = [.left, .right, .top, .bottom]
        } else {
            self.shadowSide = shadowSide
        }
        self.blur = blur
        self.spread = spread
        self.shadowColor = shadowColor
        self.fillColor = fillColor
        self.corners = corners
        self.radius = radius
    }
}

struct ShadowSide : OptionSet {
    internal let rawValue: UInt
    
    public init(rawValue: UInt) {
        self.rawValue = rawValue
    }
    
    public static var  top : ShadowSide {
        return .init(rawValue: 1 << 0)
    }
    public static var  left : ShadowSide {
        return .init(rawValue: 1 << 1)
    }
    public static var  right : ShadowSide {
        return .init(rawValue: 1 << 2)
    }
    public static var  bottom : ShadowSide {
        return .init(rawValue: 1 << 3)
    }
    
    public static var  all : ShadowSide {
        return .init(rawValue: 1 << 4)
    }
}

protocol RoundedShadowStyling {
    var roundedShadowConf: RoundedShadowConfigure? {set get}
    func layoutRoundedShadow(on layer: CALayer, conf: RoundedShadowConfigure)
    func removeRoundedShadow(from layer: CALayer)
}

extension RoundedShadowStyling where Self : UIView {
    
    func layoutRoundedShadow(on layer: CALayer, conf: RoundedShadowConfigure) {
        layoutView(layer: layer, conf: conf)
    }
    
    func removeRoundedShadow(from layer: CALayer) {
        if let shadowLayer = shadowLayer() {
            shadowLayer.removeFromSuperlayer()
        }
    }
    
    private func layoutView(layer: CALayer, conf: RoundedShadowConfigure) {
        if let shadowLayer = shadowLayer() {
            update(shadowLayer, conf: conf)
        } else {
            createLayer(on: layer, conf: conf)
        }
    }
    
    private func createLayer(on layer: CALayer, conf: RoundedShadowConfigure) {
        let shadowLayer = CAShapeLayer()
        shadowLayer.name = "__RoundedShadowStyling__"
        
        let maskRect = self.maskRect(conf: conf)
        
        shadowLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: conf.corners, cornerRadii: CGSize(width: conf.radius, height: conf.radius)).cgPath
        shadowLayer.fillColor = conf.fillColor?.cgColor ?? backgroundColor?.cgColor
        
        shadowLayer.shadowColor = conf.shadowColor?.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        shadowLayer.shadowOpacity = 1
        shadowLayer.shadowRadius =  conf.blur / 2.0
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(rect: maskRect).cgPath
        shadowLayer.mask = maskLayer
        
        layer.insertSublayer(shadowLayer, at: 0)
        
        layer.backgroundColor = UIColor.clear.cgColor
        layer.shadowColor = UIColor.clear.cgColor
        layer.shadowRadius = conf.radius
    }
    
    private func update(_ shadowLayer: CAShapeLayer, conf: RoundedShadowConfigure) {
        guard let layer = shadowLayer.superlayer else {
            return
        }
        shadowLayer.frame = layer.bounds
        let maskRect = self.maskRect(conf: conf)
        shadowLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: conf.corners, cornerRadii: CGSize(width: conf.radius, height: conf.radius)).cgPath
        shadowLayer.shadowPath = shadowLayer.path
        if let fillColor = conf.fillColor?.cgColor {
            shadowLayer.fillColor = fillColor
        }
        
        shadowLayer.shadowColor = conf.shadowColor?.cgColor
        shadowLayer.shadowRadius =  conf.blur / 2.0
        
        if let mask = shadowLayer.mask as? CAShapeLayer {
            mask.path = UIBezierPath(rect: maskRect).cgPath
        } else {
            let maskLayer = CAShapeLayer()
            maskLayer.path = UIBezierPath(rect: maskRect).cgPath
            shadowLayer.mask = maskLayer
        }
        layer.backgroundColor = UIColor.clear.cgColor
        layer.shadowColor = UIColor.clear.cgColor
        layer.shadowRadius = conf.radius
    }
    
    private func shadowLayer() -> CAShapeLayer? {
        return layer.sublayers?.first(where: { $0.name == "__RoundedShadowStyling__"}) as? CAShapeLayer
    }
    
    private func maskRect(conf: RoundedShadowConfigure) -> CGRect {
        var maskRect = bounds
        
        if conf.shadowSide.contains(.top) {
            maskRect.origin.y -= conf.spread
            maskRect.size.height += conf.spread
        }
        
        if conf.shadowSide.contains(.left) {
            maskRect.origin.x -= conf.spread
            maskRect.size.width += conf.spread
        }
        
        if conf.shadowSide.contains(.right) {
            maskRect.size.width += conf.spread
        }
        
        if conf.shadowSide.contains(.bottom) {
            maskRect.size.height += conf.spread
        }
        
        return maskRect
    }
}
