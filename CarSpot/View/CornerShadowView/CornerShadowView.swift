//
//  CornerShadowView.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 22.05.21.
//

import UIKit

class CornerShadowView: UIView, RoundedShadowStyling {
    //MARK - Properties
    var roundedShadowConf: RoundedShadowConfigure?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let conf = roundedShadowConf {
            self.layoutRoundedShadow(on: self.layer, conf: conf)
        }
    }
    
    func setupOnboardingConfigure() {
        self.roundedShadowConf = RoundedShadowConfigure(shadowSide: [],
                                                        blur: 0,
                                                        spread: 0,
                                                        shadowColor: .clear,
                                                        fillColor: .white,
                                                        corners: [.bottomLeft, .bottomRight],
                                                        radius: 20)
    }
    
    func addCorners(_ corners: UIRectCorner) {
        self.roundedShadowConf = RoundedShadowConfigure(shadowSide: [],
                                                        blur: 0,
                                                        spread: 0,
                                                        shadowColor: .clear,
                                                        fillColor: UIColor("737380", alpha: 0.08),
                                                        corners: corners,
                                                        radius: 16)
    }
    
    func setupButtonConfigure() {
        self.roundedShadowConf = RoundedShadowConfigure(shadowSide: [],
                                                        blur: 0,
                                                        spread: 0,
                                                        shadowColor: .clear,
                                                        fillColor: .white,
                                                        corners: [.allCorners],
                                                        radius: 16)
    }
    
    func setupOTPConfigure() {
        self.roundedShadowConf = RoundedShadowConfigure(shadowSide: [],
                                                        blur: 0,
                                                        spread: 0,
                                                        shadowColor: .clear,
                                                        fillColor: UIColor("737380", alpha: 0.08),
                                                        corners: [.topLeft, .topRight],
                                                        radius: 16)
    }
    
    func setupFilterUnselectedConfigure() {
        self.roundedShadowConf = RoundedShadowConfigure(shadowSide: [],
                                                        blur: 0,
                                                        spread: 0,
                                                        shadowColor: .clear,
                                                        fillColor: UIColor("737380", alpha: 0.08),
                                                        corners: .allCorners,
                                                        radius: 16)
    }
    
    func setupFilterSelectedConfigure(shadowSide: ShadowSide = .all) {
        self.roundedShadowConf = RoundedShadowConfigure(shadowSide: shadowSide,
                                                        blur: 4,
                                                        spread: 4,
                                                        shadowColor: UIColor("B2B2B2"),
                                                        fillColor: .white,
                                                        corners: .allCorners,
                                                        radius: 15)
    }
}
