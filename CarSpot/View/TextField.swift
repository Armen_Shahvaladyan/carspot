//
//  TextField.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.09.21.
//

import UIKit

class TextField: UITextField {

    let padding = UIEdgeInsets(top: 15, left: 24, bottom: 15, right: 24)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
