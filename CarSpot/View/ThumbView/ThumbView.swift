//
//  ThumbView.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 01.07.21.
//

import UIKit

class ThumbView: ShadowView, NibView {

    //MARK: - Properties
    var color: UIColor = .clear
    
    //MARK: - Lifecycle
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        layer.addSublayer(drawShapeLayer(with: bounds.height / 2))
        layer.addSublayer(drawShapeLayer(with: bounds.height / 5))
    }
    
    //MARK: - Private API
    private func drawShapeLayer(with radius: CGFloat) -> CAShapeLayer {
        let circlePath = UIBezierPath(arcCenter: center,
                                      radius: radius,
                                      startAngle: CGFloat(0),
                                      endAngle: CGFloat(Double.pi * 2),
                                      clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
            
        // Change the fill color
        shapeLayer.fillColor = color.cgColor
        // You can change the stroke color
        shapeLayer.strokeColor = UIColor.white.cgColor
        // You can change the line width
        shapeLayer.lineWidth = 2
        return shapeLayer
    }
}
