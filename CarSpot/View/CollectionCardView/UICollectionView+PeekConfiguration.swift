//
//  UICollectionView+PeekConfiguration.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 11.07.21.
//

import UIKit

extension UICollectionView {
    public func configureForPeekingDelegate(scrollDirection: UICollectionView.ScrollDirection = .horizontal) {
        self.decelerationRate = .fast
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
        self.isPagingEnabled = false
        //Keeping this to support older versions
        let layout = collectionViewLayout as! UICollectionViewFlowLayout
        layout.scrollDirection = scrollDirection
    }
}
