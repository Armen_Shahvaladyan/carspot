//
//  Axis.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 11.07.21.
//

import Foundation

/// Defines the possible cases of a collection view axis.
enum Axis {

    /// Represents the main axis in the direction of paging
    case main

    /// Represents the cross axis perpendicular to the direction of paging
    case cross
}
