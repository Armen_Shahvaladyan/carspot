//
//  CGPoint+Axis.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 11.07.21.
//

import UIKit

extension CGPoint {
    func attribute(axis: Axis, scrollDirection: UICollectionView.ScrollDirection) -> CGFloat {
        switch (axis, scrollDirection) {
        case (.main, .horizontal), (.cross, .vertical):
            return x
        case (.main, .vertical), (.cross, .horizontal):
            return y
        default:
            assertionFailure("Not implemented")
            return 0
        }
    }
}
