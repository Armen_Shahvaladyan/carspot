//
//  CollectionViewPeekingBehavior.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 11.07.21.
//

import UIKit

extension UICollectionView {
    func configureForPeekingBehavior(behavior: CollectionViewPeekingBehavior) {
        collectionViewLayout = behavior.layout
        decelerationRate = .fast
    }
}

/// Defines a peeking behavior for the collection view. This class will hold all logic and dependencies to make the paging work
class CollectionViewPeekingBehavior {

    /// The collection view layout that allows cells to peek
    var layout: CollectionViewCellPeekingLayout

    /// The scrolling paging behavior
    var paging = CollectionViewPaging()

    /// The space between cells
    var cellSpacing: CGFloat

    /// The peeking of the cells
    var cellPeekWidth: CGFloat

    /// The minimum number of items that can be scrolled. Setting this value to nil will not add any constraints.
    ///
    /// This field is useful in cases where you have multiple items showing at the same time. Setting this value to the number of items to show will ensure that scrolling will always show a page with new items.
    var minimumItemsToScroll: Int?

    /// The maximum number of items that can be scrolled. Setting this value to nil will not add any constraint
    ///
    /// The default implementation is to allow scrolling depending on the target page and the velocity.
    var maximumItemsToScroll: Int?

    /// The number of items to be shown in each page
    var numberOfItemsToShow: Int

    /// The direction of scrolling of the collection view
    var scrollDirection: UICollectionView.ScrollDirection

    var velocityThreshold: CGFloat

    /// Total number of items to be shown
    var numberOfItems: Int {
        return layout.collectionView?.numberOfItems(inSection: 0) ?? 0
    }

    /// Returns the current index of the left most item
    var currentIndex: Int {
        return paging.currentIndex
    }

    init(cellSpacing: CGFloat = 20, cellPeekWidth: CGFloat = 20, minimumItemsToScroll: Int? = nil, maximumItemsToScroll: Int? = nil, numberOfItemsToShow: Int = 1, scrollDirection: UICollectionView.ScrollDirection = .horizontal, velocityThreshold: CGFloat = 0.2) {
        self.cellSpacing = cellSpacing
        self.cellPeekWidth = cellPeekWidth
        self.minimumItemsToScroll = minimumItemsToScroll
        self.maximumItemsToScroll = maximumItemsToScroll
        self.numberOfItemsToShow = numberOfItemsToShow
        self.scrollDirection = scrollDirection
        layout = CollectionViewCellPeekingLayout(scrollDirection: scrollDirection)
        self.velocityThreshold = velocityThreshold
        layout.dataSource = self
        paging.dataSource = self
    }

    /// Scrolls to an item at a specific index with or without animation
    func scrollToItem(at index: Int, animated: Bool) {
        layout.collectionView?.setContentOffset(layout.startingPointForItem(index: index), animated: animated)
        paging.setIndex(index)
    }

    /// Required function to be called when the `scrollViewWillEndDragging` `UICollectionViewDelegate` function is called
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        paging.collectionViewWillEndDragging(scrollDirection: scrollDirection, withVelocity: velocity, targetContentOffset: targetContentOffset)
    }
}

extension CollectionViewPeekingBehavior: CollectionViewCellPeekingLayoutDataSource {
    func cellPeekingLayoutPeekingLength(_ layout: CollectionViewCellPeekingLayout) -> CGFloat {
        return cellPeekWidth
    }

    func cellPeekingLayoutSpacingLength(_ layout: CollectionViewCellPeekingLayout) -> CGFloat {
        return cellSpacing
    }

    func cellPeekingLayoutNumberOfItemsToShow(_ layout: CollectionViewCellPeekingLayout) -> Int {
        return numberOfItemsToShow
    }
}

extension CollectionViewPeekingBehavior: CollectionViewPagingDataSource {
    func collectionViewPagingVelocityThreshold(_ collectionViewPaging: CollectionViewPaging) -> CGFloat {
        return velocityThreshold
    }

    func collectionViewNumberOfItems(_ collectionViewPaging: CollectionViewPaging) -> Int {
        return numberOfItems
    }

    func collectionViewPaging(_ collectionViewPaging: CollectionViewPaging, offsetForItemAtIndex index: Int) -> CGFloat {
        return layout.startingPointForItem(index: index).attribute(axis: .main, scrollDirection: scrollDirection)
    }

    func collectionViewPaging(_ collectionViewPaging: CollectionViewPaging, indexForItemAtOffset offset: CGFloat) -> Int {
        let safeOffset = min(max(0, offset), layout.contentLength(axis: .main))
        let point: CGPoint
        switch (scrollDirection) {
        case .horizontal:
            point = CGPoint(x: safeOffset, y: 0)
        case .vertical:
            point = CGPoint(x: 0, y: safeOffset)
        default:
            assertionFailure("Not implemented")
            return .zero
        }
        return layout.indexForItemAtPoint(point: point)
    }

    func collectionViewPagingMinimumItemsToScroll(_ collectionViewPaging: CollectionViewPaging) -> Int? {
        return minimumItemsToScroll
    }

    func collectionViewPagingMaximumItemsToScroll(_ collectionViewPaging: CollectionViewPaging) -> Int? {
        return maximumItemsToScroll
    }
}
