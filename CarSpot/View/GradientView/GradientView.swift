//
//  GradientView.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 23.05.21.
//

import UIKit

@IBDesignable class GradientView: UIView {
    //MARK: - Properties
    @IBInspectable var startColor: UIColor = UIColor.white {
        didSet {
            updateUI()
        }
    }

    @IBInspectable var endColor: UIColor = .black {
        didSet {
            updateUI()
        }
    }

    @IBInspectable var isHorizontal: Bool = false {
        didSet {
            updateUI()
        }
    }
    
    @IBInspectable var isProportional: Bool = false {
        didSet {
            updateUI()
        }
    }

    @IBInspectable var roundness: CGFloat = 0.0 {
        didSet {
            updateUI()
        }
    }

    private var gradientlayer = CAGradientLayer()

    //MARK: - Lifecycle
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        updateFrame()
    }

    //MARK: - Private API
    private func setupUI() {
        gradientlayer = CAGradientLayer()
        updateUI()
        layer.addSublayer(gradientlayer)
    }

    private func updateFrame() {
        gradientlayer.frame = bounds
        layer.cornerRadius = roundness
    }

    private func updateUI() {
        gradientlayer.colors = [startColor.cgColor, endColor.cgColor]
        if isHorizontal {
            gradientlayer.startPoint = CGPoint(x: 0, y: 0)
            gradientlayer.endPoint = CGPoint(x: 1, y: 0)
        } else {
            gradientlayer.startPoint = CGPoint(x: 0, y: 0)
            gradientlayer.endPoint = CGPoint(x: 0, y: 1)
        }

        layer.cornerRadius = roundness
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor?.cgColor ?? tintColor.cgColor
        layer.masksToBounds = true
        updateFrame()
    }
}

extension GradientView: Bordering {
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
}
