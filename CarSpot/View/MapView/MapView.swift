//
//  MapView.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 09.07.21.
//

import UIKit
import GoogleMaps

class MapView: GMSMapView {
    
    //MARK: - Properties
    var myLocationButton: Bool {
        set {
            settings.myLocationButton = newValue
        }
        get {
            return settings.myLocationButton
        }
    }
    
    //MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    //MARK: - Public API
    func changePosition(with lat: Double, lng: Double) {
        let camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: lat, longitude: lng), zoom: 17)
        self.animate(to: camera)
    }
    
    func addMark(with lat: Double, lng: Double) {
        let marker: GMSMarker = GMSMarker()
        let view = UIImageView(image: #imageLiteral(resourceName: "ic_radio"))
        view.tintColor = .appOrange
        marker.iconView = view
        marker.appearAnimation = .pop
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        DispatchQueue.main.async {
            marker.map = self
        }
    }
    
    func disableDragging() {
        settings.scrollGestures = false
        settings.zoomGestures = false
        settings.rotateGestures = false
    }
    
    func enableDragging() {
        settings.scrollGestures = true
        settings.zoomGestures = true
        settings.rotateGestures = true
    }
    
    //MARK: - Private API
    private func commonInit() {
        do {
            if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
                mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else{
                print("unable to find style.json")
            }
        } catch {
            print("One or more of the map styles failed to load.\(error)")
        }
        
        isMyLocationEnabled = true
        settings.myLocationButton = true
    }
}
