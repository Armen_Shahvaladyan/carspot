//
//  SelectedViewWithIcon.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.05.21.
//

import UIKit
import SkyFloatingLabelTextField

enum ViewState {
    case `default`
    case disable
}

protocol SelectedViewWithIconDelegate: AnyObject {
    func selectedViewWithIcon(_ view: SelectedViewWithIcon, didSelect text: String)
}

@IBDesignable
class SelectedViewWithIcon: UIView {
    // MARK: Parameters
    weak var delegate: SelectedViewWithIconDelegate?
    
    private var leadingConstrinat: NSLayoutConstraint!
    private var trailingConstrinat: NSLayoutConstraint!
    
    private(set) var leftPadding: CGFloat = 16
    private(set) var rightPadding: CGFloat = 16
    
    private var containerView: CornerShadowView = {
        let v = CornerShadowView()
        v.addCorners([.topLeft, .topRight])
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    private(set) var textField: SkyFloatingLabelTextField = {
        let tf = SkyFloatingLabelTextField()
        tf.textColor = UIColor("2E2E33")
        tf.lineHeight = 0
        tf.lineColor = .clear
        tf.selectedLineColor = .clear
        tf.titleColor = UIColor("737380", alpha: 0.8)
        tf.placeholderColor = UIColor("2E2E33")
        tf.selectedTitleColor = UIColor("737380", alpha: 0.8)
        tf.titleFont = UIFont.SF(.display, weight: .regular, size: 12)
        tf.placeholderFont = UIFont.SF(.display, weight: .regular, size: 16)
        tf.titleFormatter = { $0 }
        tf.isUserInteractionEnabled = false
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    private(set) var iconImageView: UIImageView = {
        let imgView = UIImageView(image: #imageLiteral(resourceName: "ic_down"))
        imgView.isUserInteractionEnabled = true
        imgView.contentMode = .center
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    private(set) var lineView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor("C9C9CE")
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    @IBInspectable var placeholder: String = "hj" {
        didSet {
            self.textField.placeholder = placeholder
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var text: String = "hj" {
        didSet {
            self.textField.text = text
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var textColor: UIColor = .black {
        didSet {
            self.textField.textColor = textColor
            self.setNeedsDisplay()
        }
    }

    @IBInspectable var image: UIImage? = UIImage() {
        didSet {
            self.iconImageView.image = image
        }
    }
    
    var leftMargin: CGFloat = 16 {
        didSet {
            leadingConstrinat.constant = leftMargin
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    
    var rightMargin: CGFloat = 16 {
        didSet {
            trailingConstrinat.constant = rightMargin
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    
    var state: ViewState = .default {
        didSet {
            switch state {
            case .default:
                cofigureDefaultState()
            case .disable:
                cofigureDisableState()
            }
        }
    }
    
    //MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.viewInit()
        self.improvePerformance()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.viewInit()
        self.improvePerformance()
    }
    
    //MARK: - Private API
    private func improvePerformance() {
        /// Cache the view into a bitmap instead of redrawing the stars each time
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        isOpaque = true
    }
    
    private func viewInit() {
        containerViewInit()
        subViewsInit()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        containerView.addGestureRecognizer(tapGesture)
    }
    
    @objc private func handleTap() {
        delegate?.selectedViewWithIcon(self, didSelect: textField.text ?? "")
    }
}

extension SelectedViewWithIcon {
    private func containerViewInit() {
        self.addSubview(containerView)
        
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }
    
    private func subViewsInit() {
        
        backgroundColor = .clear
        
        containerView.addSubview(iconImageView)
        
        iconImageView.widthAnchor.constraint(equalToConstant: 24).isActive = true
        iconImageView.heightAnchor.constraint(equalToConstant: 24).isActive = true
        iconImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        iconImageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -rightPadding).isActive = true
        
        containerView.addSubview(textField)
        
        textField.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 8).isActive = true
        textField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: leftPadding).isActive = true
        textField.trailingAnchor.constraint(equalTo: iconImageView.leadingAnchor, constant: -rightPadding).isActive = true
        textField.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -14).isActive = true
        
        containerView.addSubview(lineView)
        
        lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        lineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        lineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        lineView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
    }
}

extension SelectedViewWithIcon {
    private func cofigureDefaultState() {
        self.alpha = 1
    }
    
    private func cofigureDisableState() {
        self.alpha = 0.5
    }
}
