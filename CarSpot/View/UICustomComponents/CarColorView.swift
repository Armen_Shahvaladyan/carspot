//
//  CarColorView.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.06.21.
//

import SkyFloatingLabelTextField

protocol CarColorViewDelegate: AnyObject {
    func carColorView(_ view: CarColorView, didSelect text: String)
}

@IBDesignable
class CarColorView: UIView {
    // MARK: Parameters
    weak var delegate: CarColorViewDelegate?
    
    private var containerView: CornerShadowView = {
        let v = CornerShadowView()
        v.addCorners([.topLeft, .topRight])
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    private(set) var textField: SkyFloatingLabelTextField = {
        let tf = SkyFloatingLabelTextField()
        tf.textColor = UIColor("2E2E33")
        tf.lineHeight = 0
        tf.lineColor = .clear
        tf.selectedLineColor = .clear
        tf.titleColor = UIColor("737380", alpha: 0.8)
        tf.placeholderColor = UIColor("2E2E33")
        tf.selectedTitleColor = UIColor("737380", alpha: 0.8)
        tf.titleFont = UIFont.SF(.display, weight: .regular, size: 12)
        tf.placeholderFont = UIFont.SF(.display, weight: .regular, size: 16)
        tf.titleFormatter = { $0 }
        tf.isUserInteractionEnabled = false
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    private(set) var iconImageView: UIImageView = {
        let imgView = UIImageView(image: #imageLiteral(resourceName: "ic_down"))
        imgView.contentMode = .center
        imgView.isUserInteractionEnabled = true
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    private(set) var lineView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor("C9C9CE")
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    private(set) var colorView: UIView = {
        let v = UIView()
        v.isHidden = true
        v.backgroundColor = .clear
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    @IBInspectable var placeholder: String = "hj" {
        didSet {
            self.textField.placeholder = placeholder
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var text: String = "hj" {
        didSet {
            self.textField.text = text
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var textColor: UIColor = .black {
        didSet {
            self.textField.textColor = textColor
            self.setNeedsDisplay()
        }
    }

    @IBInspectable var image: UIImage? = UIImage() {
        didSet {
            self.iconImageView.image = image
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var color: UIColor = .clear {
        didSet {
            self.colorView.isHidden = color == .clear
            self.colorView.backgroundColor = color
            self.layoutSubviews()
        }
    }
    
    //MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.viewInit()
        self.improvePerformance()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.viewInit()
        self.improvePerformance()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    
        colorView.layer.masksToBounds = true
        colorView.layer.cornerRadius = colorView.bounds.height / 2
    }
    
    //MARK: - Private API
    private func improvePerformance() {
        /// Cache the view into a bitmap instead of redrawing the stars each time
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        isOpaque = true
    }
    
    private func viewInit() {
        containerViewInit()
        subViewsInit()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        containerView.addGestureRecognizer(tapGesture)
    }
    
    @objc private func handleTap() {
        delegate?.carColorView(self, didSelect: textField.text ?? "")
    }
}

extension CarColorView {
    private func containerViewInit() {
        
        backgroundColor = .clear
        
        self.addSubview(containerView)
        
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }
    
    private func subViewsInit() {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.spacing = 8
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.addArrangedSubview(colorView)
        stackView.addArrangedSubview(textField)
        stackView.addArrangedSubview(iconImageView)
        
        containerView.addSubview(stackView)
        
        colorView.widthAnchor.constraint(equalToConstant: 16).isActive = true
        colorView.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        iconImageView.widthAnchor.constraint(equalToConstant: 24).isActive = true
        iconImageView.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        stackView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 16).isActive = true
        stackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -16).isActive = true
        
        containerView.addSubview(lineView)
        
        lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        lineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        lineView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        lineView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
    }
}
