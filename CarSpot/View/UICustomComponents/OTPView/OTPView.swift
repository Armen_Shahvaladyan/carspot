//
//  OTPView.swift
//  OTPView
//
//  Created by datt on 13/11/19.
//  Copyright © 2019 datt. All rights reserved.
//

import UIKit

protocol OTPViewDelegate {
    func otpView(_ view: OTPView, didAdd text: String, at position: Int)
    func otpView(_ view: OTPView, didRemove text: String, at position: Int)
    func otpView(_ view: OTPView, didChange position: Int)
    func otpViewBecomeFirstResponder()
    func otpViewResignFirstResponder()
}

@IBDesignable class OTPView: UIView {
    
    /** The number of textField that will be put in the OTPView */
    @IBInspectable var count: Int = 4
    
    /** Spaceing between textField in the OTPView */
    @IBInspectable var spacing: CGFloat = 16
    
    /** Text color for the textField */
    @IBInspectable var textColorTextField: UIColor = UIColor.black
    
    /** Background color for the textField */
    @IBInspectable var backGroundColorTextField: UIColor = UIColor.clear
    
    /** Corner radius for the TextField */
    @IBInspectable var cornerRadiusTextField: CGFloat = 0.0
    
    /** Tint/cursor color for the TextField */
    @IBInspectable var tintColorTextField: UIColor = UIColor.systemBlue
    
    /** Dismiss keyboard with enter last character*/
    @IBInspectable var dismissOnLastEntry: Bool = false
    
    /** Secure Text Entry*/
    @IBInspectable var isSecureTextEntry: Bool = false
    
    /** Hide cursor*/
    @IBInspectable var isCursorHidden: Bool = false
    
    /** Dark keyboard*/
    @IBInspectable var isDarkKeyboard: Bool = false
    
    var textEdgeInsets : UIEdgeInsets?
    var editingTextEdgeInsets : UIEdgeInsets?
    
    var delegate : OTPViewDelegate?
    var keyboardType: UIKeyboardType = .numberPad
    
    var text : String? {
        get {
            return textFields.compactMap { $0.text }.joined()
        } set {
            textFields.forEach { $0.text = nil }
            for i in 0 ..< textFields.count {
                if i < (newValue?.count ?? 0) {
                    if let txt = newValue?[i..<(i + 1)],
                       let code = Int(txt) {
                        textFields[i].text = String(code)
                    }
                }
            }
        }
    }
    
    private let _tag = 1000
    private var textFields: [OTPTextField] = []
    private var lineViews: [UIView] = []
    
    private lazy var stackVeiw: UIStackView = {
        let sv = UIStackView()
        sv.alignment = .fill
        sv.distribution = .fillEqually
        sv.spacing = spacing
        sv.translatesAutoresizingMaskIntoConstraints = false
        
        return sv
    }()
    
    /** Override coder init, for IB/XIB compatibility */
    #if !TARGET_INTERFACE_BUILDER
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialization()
    }
    
    /** Override common init, for manual allocation */
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initialization()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        self.initialization()
    }
    #endif
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        initialization()
    }
    
    private func initialization() {
        if textFields.count != 0 { return }
        
        backgroundColor = .clear
        addSubview(stackVeiw)
        
        stackVeiw.topAnchor.constraint(equalTo: topAnchor).isActive = true
        stackVeiw.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        stackVeiw.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        stackVeiw.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        for i in 1 ... count {
            let cornerShadowView = CornerShadowView()
            cornerShadowView.setupOTPConfigure()
            cornerShadowView.translatesAutoresizingMaskIntoConstraints = false
            
            let lineView = UIView()
            lineView.backgroundColor = UIColor("C9C9CE")
            lineView.translatesAutoresizingMaskIntoConstraints = false
            
            cornerShadowView.addSubview(lineView)
            
            lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
            lineView.leadingAnchor.constraint(equalTo: cornerShadowView.leadingAnchor).isActive = true
            lineView.trailingAnchor.constraint(equalTo: cornerShadowView.trailingAnchor).isActive = true
            lineView.bottomAnchor.constraint(equalTo: cornerShadowView.bottomAnchor).isActive = true
            
            let textField = createTextField(with: i * _tag, lineView: lineView)
            
            stackVeiw.addArrangedSubview(cornerShadowView)
            textFields.append(textField)
            lineViews.append(lineView)
            
            cornerShadowView.addSubview(textField)
            
            textField.topAnchor.constraint(equalTo: cornerShadowView.topAnchor).isActive = true
            textField.leadingAnchor.constraint(equalTo: cornerShadowView.leadingAnchor).isActive = true
            textField.trailingAnchor.constraint(equalTo: cornerShadowView.trailingAnchor).isActive = true
            textField.bottomAnchor.constraint(equalTo: cornerShadowView.bottomAnchor).isActive = true
        }
    }
    
    override func becomeFirstResponder() -> Bool {
        if isCursorHidden {
            for i in 0 ..< textFields.count {
                if textFields[i].text?.count == 0 {
                    _ = textFields[i].becomeFirstResponder()
                    break
                } else if (textFields.count - 1) == i {
                    _ = textFields[i].becomeFirstResponder()
                    break
                }
            }
        } else {
            _ = textFields.first?.becomeFirstResponder()
        }
        delegate?.otpViewBecomeFirstResponder()
        return super.becomeFirstResponder()
    }
    
    override func resignFirstResponder() -> Bool {
        textFields.forEach { _ = $0.resignFirstResponder() }
        delegate?.otpViewResignFirstResponder()
        
        return super.resignFirstResponder()
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        _ = self.becomeFirstResponder()
    }
    
    func validate() -> Bool {
        var isValid = true
        textFields.forEach { (textField) in
            if Int(textField.text ?? "") == nil {
                isValid = false
            }
        }
        return isValid
    }
    
    private func createTextField(with tag: Int, lineView: UIView) -> OTPTextField {
        let textField = OTPTextField()
        textField.delegate = self
        textField._delegate = self
        textField.otpView = self
        textField.lineView = lineView
        textField.borderStyle = .none
        textField.tag = tag
        textField.tintColor = tintColorTextField
        textField.backgroundColor = backGroundColorTextField
        textField.isSecureTextEntry = isSecureTextEntry
        textField.font = .SF(.display, weight: .regular, size: 16)
        textField.keyboardAppearance = isDarkKeyboard ? .dark : .default
        textField.textColor = textColorTextField
        textField.textAlignment = .center
        textField.keyboardType = keyboardType
        
        if isCursorHidden { textField.tintColor = .clear }
        
        if #available(iOS 12.0, *) {
            textField.textContentType = .oneTimeCode
        }
        
        if isCursorHidden {
            let tapView = UIView(frame: self.bounds)
            tapView.backgroundColor = .clear
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            tapView.addGestureRecognizer(tap)
            self.addSubview(tapView)
        }
        
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        return textField
    }
}

extension OTPView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.otpView(self, didChange: textField.tag / _tag - 1)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.trimmingCharacters(in: CharacterSet.whitespaces).count != 0 {
            textField.text = string
            if textField.tag < count * _tag {
                let next = textField.superview?.superview?.viewWithTag((textField.tag / _tag + 1) * _tag)
                next?.becomeFirstResponder()
            } else if textField.tag == count * _tag && dismissOnLastEntry {
                textField.resignFirstResponder()
            }
        } else if string.count == 0 { // is backspace
            textField.text = ""
        }
        delegate?.otpView(self, didAdd: text ?? "", at: textField.tag / _tag - 1)
        return false
    }
}

extension OTPView: OTPTextFieldDelegate {
    func otpTextFieldDidSelectDelete(_ textField: OTPTextField) {
        if textField.tag > _tag,
           let next = textField.superview?.superview?.viewWithTag((textField.tag / _tag - 1) * _tag) as? UITextField {
            next.text = ""
            next.becomeFirstResponder()
            delegate?.otpView(self, didRemove: text ?? "", at: next.tag / _tag - 1)
        }
    }
}

extension OTPViewDelegate {
    func otpView(_ view: OTPView, didAdd text: String, at position: Int) { }
    func otpView(_ view: OTPView, didRemove text: String, at position: Int) { }
    func otpView(_ view: OTPView, didChange position: Int) { }
    func otpViewBecomeFirstResponder() { }
    func otpViewResignFirstResponder() { }
}
