//
//  OTPTextField.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 30.05.21.
//

import UIKit

protocol OTPTextFieldDelegate: AnyObject {
    func otpTextFieldDidSelectDelete(_ textField : OTPTextField)
}

class OTPTextField: UITextField {
    
    //MARK: - Properties
    weak var _delegate : OTPTextFieldDelegate?
    weak var otpView : OTPView!
    var lineView: UIView!
    
    //MARK: - Lifecycle
    override func deleteBackward() {
        super.deleteBackward()
        
        _delegate?.otpTextFieldDidSelectDelete(self)
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        false
    }
    
    override func becomeFirstResponder() -> Bool {
        lineView.backgroundColor = UIColor("#FD6632")
        
        return super.becomeFirstResponder()
    }
    
    override func resignFirstResponder() -> Bool {
        lineView.backgroundColor = UIColor("C9C9CE")
        
        return super.resignFirstResponder()
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        bounds.inset(by: otpView.textEdgeInsets ?? UIEdgeInsets.zero)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        bounds.inset(by: otpView.editingTextEdgeInsets ?? UIEdgeInsets.zero)
    }
}
