//
//  PanContainerView.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.05.21.
//

import UIKit

public protocol PanContainerViewDelegate: AnyObject {
    func panContainerViewFrameChanged(_ frame: CGRect)
}

public class PanContainerView: UIView {
    
    private var observerAdded: Bool = false
    public weak var delegate: PanContainerViewDelegate?

    init(presentedView: UIView, frame: CGRect) {
        super.init(frame: frame)
        addSubview(presentedView)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        guard observerAdded else { return }
        removeObserver(self, forKeyPath: #keyPath(UIView.frame))
        removeObserver(self, forKeyPath: #keyPath(UIView.center))
    }

    override public func didMoveToSuperview() {
        super.didMoveToSuperview()
        guard !observerAdded else { return }
        observerAdded = true
        addObserver(self, forKeyPath: #keyPath(UIView.frame), options: [.new, .old], context: nil)
        addObserver(self, forKeyPath: #keyPath(UIView.center), options: [.new, .old], context: nil)

    }

    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        switch keyPath {
        case #keyPath(UIView.frame), #keyPath(UIView.center):
            delegate?.panContainerViewFrameChanged(self.frame)
        default:
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }

}

extension UIView {
    var panContainerView: PanContainerView? {
        return subviews.first(where: { view -> Bool in
            view is PanContainerView
        }) as? PanContainerView
    }
}
