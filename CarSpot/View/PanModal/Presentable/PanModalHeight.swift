//
//  PanModalHeight.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.05.21.
//

import UIKit

public enum PanModalHeight: Equatable {
    case maxHeight
    case maxHeightWithTopInset(CGFloat)
    case contentHeight(CGFloat)
    case contentHeightIgnoringSafeArea(CGFloat)
    case intrinsicHeight
}
