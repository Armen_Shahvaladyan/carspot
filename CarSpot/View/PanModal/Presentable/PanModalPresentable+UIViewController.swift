//
//  PanModalPresentable+UIViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.05.21.
//

import UIKit

public extension PanModalPresentable where Self: UIViewController {

    typealias AnimationBlockType = () -> Void
    typealias AnimationCompletionType = (Bool) -> Void

    typealias LayoutType = UIViewController & PanModalPresentable

    func panModalTransition(to state: PanModalPresentationController.PresentationState) {
        presentedVC?.transition(to: state)
    }

    func panModalSetNeedsLayoutUpdate() {
        presentedVC?.setNeedsLayoutUpdate()
    }

    func panModalPerformUpdates(_ updates: () -> Void) {
        presentedVC?.performUpdates(updates)
    }

    func panModalAnimate(_ animationBlock: @escaping AnimationBlockType, _ completion: AnimationCompletionType? = nil) {
        PanModalAnimator.animate(animationBlock, config: self, completion)
    }
}
