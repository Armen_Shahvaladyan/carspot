//
//  PanModalPresenter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.05.21.
//

import UIKit

protocol PanModalPresenter: AnyObject {

    var isPanModalPresented: Bool { get }

    func presentPanModal(_ viewControllerToPresent: PanModalPresentable.LayoutType,
                         sourceView: UIView?,
                         sourceRect: CGRect,
                         completion: (() -> Void)?)

}
