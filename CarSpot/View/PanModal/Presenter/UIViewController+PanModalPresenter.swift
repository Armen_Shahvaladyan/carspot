//
//  UIViewController+PanModalPresenter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.05.21.
//

import UIKit

extension UIViewController: PanModalPresenter {

    public var isPanModalPresented: Bool {
        return (transitioningDelegate as? PanModalPresentationDelegate) != nil
    }

    public func presentPanModal(_ viewControllerToPresent: PanModalPresentable.LayoutType,
                                sourceView: UIView? = nil,
                                sourceRect: CGRect = .zero,
                                completion: (() -> Void)? = nil) {
        if UIDevice.current.userInterfaceIdiom == .pad {
            viewControllerToPresent.modalPresentationStyle = .popover
            viewControllerToPresent.popoverPresentationController?.sourceRect = sourceRect
            viewControllerToPresent.popoverPresentationController?.sourceView = sourceView ?? view
            viewControllerToPresent.popoverPresentationController?.delegate = PanModalPresentationDelegate.default
        } else {
            viewControllerToPresent.modalPresentationStyle = .custom
            viewControllerToPresent.modalPresentationCapturesStatusBarAppearance = true
            viewControllerToPresent.transitioningDelegate = PanModalPresentationDelegate.default
        }

        present(viewControllerToPresent, animated: true, completion: completion)
    }
    
    final func presentBottomSheet(_ controller: PanModalPresentable.LayoutType) {
        controller.modalPresentationStyle = .custom
        controller.modalPresentationCapturesStatusBarAppearance = true
        controller.transitioningDelegate = PanModalPresentationDelegate.default
        present(controller, animated: true, completion: nil)
    }
}
