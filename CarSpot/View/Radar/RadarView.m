//
//  RadarView.m
//  CarSpot
//
//  Created by Armen Shahvaladyan on 13.07.21.
//

#import "RadarView.h"
#import "AngleGradientLayer.h"

@implementation RadarView

+ (Class)layerClass {
    return [AngleGradientLayer class];
}

- (id)initWithFrame:(CGRect)frame {
    if (!(self = [super initWithFrame:frame]))
        return nil;
    
    self.backgroundColor = [UIColor clearColor];
    [self setOpaque:NO];

    NSArray *colors = @[
                    (id)[UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 0].CGColor,
                    (id)[UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 0].CGColor,
                    (id)[UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 0].CGColor,
                    (id)[UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 0].CGColor,
                    (id)[UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 0].CGColor,
                    (id)[UIColor colorWithRed: 255 / 255.0 green: 103 / 255.0 blue: 0 alpha: 1].CGColor
                    ];
    
    AngleGradientLayer *l = (AngleGradientLayer *)self.layer;
    l.colors = colors;
    l.contentsScale = [UIScreen mainScreen].scale; // Retina
    l.cornerRadius = CGRectGetWidth(self.bounds) / 2;
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    
    int radius = 21;
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, frame.size.width, frame.size.height) cornerRadius:0];
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0 * radius, 2.0 * radius) cornerRadius:radius];
    [path appendPath:circlePath];
    [path setUsesEvenOddFillRule:YES];
    
    maskLayer.path = path.CGPath;
    maskLayer.fillRule = kCAFillRuleEvenOdd;
    
    l.mask = maskLayer;
    
    self.clipsToBounds = YES;
    self.userInteractionEnabled = NO;
    return self;
}

- (void)spin {
    CABasicAnimation *spin = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    spin.duration = 3;
    spin.toValue = [NSNumber numberWithFloat:-M_PI];
    spin.cumulative = YES;
    spin.removedOnCompletion = NO;
    spin.repeatCount = MAXFLOAT;
    self.alpha = 0.68;
    self.hidden = NO;
    [self.layer addAnimation:spin forKey:@"spinRadarView"];
}

- (void)stopSpin {
    [UIView animateWithDuration:0.3
                     animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self.layer removeAllAnimations];
        self.hidden = YES;
    }];
}

@end
