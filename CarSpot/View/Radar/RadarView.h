//
//  RadarView.h
//  CarSpot
//
//  Created by Armen Shahvaladyan on 13.07.21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RadarView : UIView

- (void)spin;
- (void)stopSpin;

@end

NS_ASSUME_NONNULL_END
