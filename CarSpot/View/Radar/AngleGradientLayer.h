//
//  AngleGradientLayer.h
//  CarSpot
//
//  Created by Armen Shahvaladyan on 13.07.21.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface AngleGradientLayer : CALayer

@property(copy) NSArray *colors;
@property(copy) NSArray *locations;

@end

NS_ASSUME_NONNULL_END
