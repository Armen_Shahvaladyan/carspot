//
//  Localizable.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.10.21.
//

import Foundation

protocol Localizable {
    var tableName: String { get }
    var localized: String { get }
}

extension Localizable where Self: RawRepresentable, Self.RawValue == String {
    
    var localized: String {
        return rawValue.localized(tableName: tableName)
    }
}
