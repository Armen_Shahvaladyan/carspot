//
//  Action.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.10.21.
//

import Foundation

enum Action: String, Localizable {
    case stop
    case find
    case next
        
    var tableName: String { return "ActionElementStrings" }
}
