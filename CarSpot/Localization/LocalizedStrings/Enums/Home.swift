//
//  Home.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.10.21.
//

import Foundation

enum Home: String, Localizable {
    case findLoaderTitle
    case suggestLoaderTitle
        
    var tableName: String { return "HomeElementStrings" }
}
