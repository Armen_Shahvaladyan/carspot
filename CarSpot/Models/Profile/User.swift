//
//  User.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 09.06.21.
//

import UIKit

class User: Codable {
    
    //MARK: - Properties
    var id: String
    var name: String
    var lastName: String
    var phoneNumber: String
    var email: String
    var carBrand: CarBrand
    var carColor: CarColor
    var carNumber: String
    var imagePath: String?
    var color: String
    var brand: String
    var model: String
    var colorHex: String
    var colorId: Int
    var image: UIImage?
    
    enum CodingKeys: String, CodingKey {
        case id, lastName, email, phoneNumber, color, brand, colorId, model
        case name          = "firstName"
        case imagePath     = "profilePicturePath"
        case carNumber     = "number"
        case colorHex      = "colorHexCode"
    }
    
    //MARK: - Lifecycle
    required init(from decoder: Decoder) throws {
        let container           = try decoder.container(keyedBy: CodingKeys.self)
        id                      = try container.decode(String.self, forKey: .id)
        name                    = try container.decode(String.self, forKey: .name)
        lastName                = try container.decode(String.self, forKey: .lastName)
        phoneNumber             = try container.decode(String.self, forKey: .phoneNumber)
        email                   = try container.decode(String.self, forKey: .email)
        carNumber               = try container.decode(String.self, forKey: .carNumber)
        imagePath               = try? container.decode(String.self, forKey: .imagePath)
        color                   = try container.decode(String.self, forKey: .color)
        brand                   = try container.decode(String.self, forKey: .brand)
        model                   = try container.decode(String.self, forKey: .model)
        colorHex                = try container.decode(String.self, forKey: .colorHex)
        colorId                 = try container.decode(Int.self, forKey: .colorId)
        
        carBrand = CarBrand(brand: brand, model: model)
        carColor = CarColor(id: colorId, name: color, hexCode: colorHex)
    }
    
    init(name: String, lastName: String, email: String, carNumber: String, carBrand: CarBrand, carColor: CarColor) {
        self.name = name
        self.lastName = lastName
        self.email = email
        self.carNumber = carNumber
        self.carBrand = carBrand
        self.carColor = carColor
        
        self.id = ""
        self.phoneNumber = ""
        self.color = ""
        self.brand = ""
        self.model = ""
        self.colorHex = ""
        self.colorId = 0
    }
    
    //MARK: - Public API
    func mapToJSON() -> [String: Any] {
        var data: [String: Any] = [:]
        
        data["firstName"] = name
        data["lastName"] = lastName
        data["email"] = email
        data["colorId"] = carColor.id
        data["brand"] = carBrand.name
        data["number"] = carNumber
        
        if let model = carBrand.model {
            data["model"] = model.name
        }
            
        return data
    }
}

extension User {
    var fullName: String {
        name + " " + lastName
    }
}
