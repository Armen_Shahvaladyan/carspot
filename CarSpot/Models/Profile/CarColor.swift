//
//  CarColor.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 03.06.21.
//

import UIKit

class CarColor: NSObject, Codable {
    
    //MARK: - Properties
    let id: Int
    let name: String
    let hexCode: String

    enum CodingKeys: String, CodingKey {
        case id, hexCode
        case name  = "colorName"
    }
    
    //MARK: - Lifecycle
    init(id: Int, name: String, hexCode: String) {
        self.id = id
        self.name = name
        self.hexCode = hexCode
    }
}

extension CarColor {
    var color: UIColor {
        return UIColor(hexCode)
    }
}
