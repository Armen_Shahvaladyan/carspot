//
//  CarModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 02.06.21.
//

import Foundation

class CarModel: NSObject, Codable {
    
    //MARK: - Properties
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case name = "model"
    }
    
    //MARK: - Lifecycle
    init(name: String) {
        self.name = name
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name          = try container.decode(String.self, forKey: .name)
    }
}
