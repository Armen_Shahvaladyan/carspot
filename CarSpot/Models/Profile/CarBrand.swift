//
//  CarBrand.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 01.06.21.
//

import Foundation

class CarBrand: NSObject, Codable {
    
    //MARK: - Properties
    let name: String
    let models: [CarModel]
    var model: CarModel?
    
    enum CodingKeys: String, CodingKey {
        case models
        case name = "brand"
    }
    
    //MARK: - Lifecycle
    init(brand: String, model: String) {
        self.name = brand
        self.models = []
        self.model = CarModel(name: model)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name          = try container.decode(String.self, forKey: .name)
        models        = try container.decode([CarModel].self, forKey: .models)
    }
}
