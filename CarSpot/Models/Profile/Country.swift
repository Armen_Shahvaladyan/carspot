//
//  Country.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.05.21.
//

import Foundation

class Country: NSObject, Codable {
    
    //MARK: - Properties
    let name: String
    let dialCode: String
    let code: String
}
