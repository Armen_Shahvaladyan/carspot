//
//  PlaceDetails.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 26.07.21.
//

import CoreLocation

class PlaceDetails {
    
    //MARK: - Properties
    let formattedAddress: String
    var name: String? = nil
    var streetNumber: String? = nil
    var route: String? = nil
    var postalCode: String? = nil
    var country: String? = nil
    var countryCode: String? = nil
    var placeId: String? = nil
    var locality: String? = nil
    var subLocality: String? = nil
    var administrativeArea: String? = nil
    var administrativeAreaCode: String? = nil
    var subAdministrativeArea: String? = nil
    var coordinate: CLLocationCoordinate2D? = nil
    
    //MARK: - Lifecycle
    init?(json: [String: Any]) {
        guard let result = json["result"] as? [String: Any],
            let formattedAddress = result["formatted_address"] as? String
            else { return nil }
        
        self.formattedAddress = formattedAddress
        self.name = result["name"] as? String
        
        if let addressComponents = result["address_components"] as? [[String: Any]] {
            streetNumber = get("street_number", from: addressComponents, ofType: .short)
            route = get("route", from: addressComponents, ofType: .short)
            postalCode = get("postal_code", from: addressComponents, ofType: .long)
            country = get("country", from: addressComponents, ofType: .long)
            countryCode = get("country", from: addressComponents, ofType: .short)
            
            locality = get("locality", from: addressComponents, ofType: .long)
            subLocality = get("sublocality", from: addressComponents, ofType: .long)
            administrativeArea = get("administrative_area_level_1", from: addressComponents, ofType: .long)
            administrativeAreaCode = get("administrative_area_level_1", from: addressComponents, ofType: .short)
            subAdministrativeArea = get("administrative_area_level_2", from: addressComponents, ofType: .long)
        }
        
        if let placeId = result["place_id"] as? String {
            self.placeId = placeId
        }
        
        if let geometry = result["geometry"] as? [String: Any],
            let location = geometry["location"] as? [String: Any],
            let latitude = location["lat"] as? CLLocationDegrees,
            let longitude = location["lng"] as? CLLocationDegrees {
            coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
    }
}

private extension PlaceDetails {
    
    enum ComponentType: String {
        case short = "short_name"
        case long = "long_name"
    }
    
    func get(_ component: String, from array: [[String: Any]], ofType: ComponentType) -> String? {
        return (array.first { ($0["types"] as? [String])?.contains(component) == true })?[ofType.rawValue] as? String
    }
}
