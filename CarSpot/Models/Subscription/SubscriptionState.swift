//
//  SubscriptionState.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 09.08.21.
//

import Foundation

struct SubscriptionState: Codable {
    
    //MARK: - Properties
    let coins: Double
    let status: SubscriptionStatus
    
    enum CodingKeys: String, CodingKey {
        case coins
        case status = "subscriptionStatus"
    }
    
    //MARK: - Lifecycle
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        coins         = try container.decode(Double.self, forKey: .coins)
        status        = try container.decode(SubscriptionStatus.self, forKey: .status)
    }
}

enum SubscriptionStatus: Int, Codable {
    case idle = 1
    case payedSubscription
    case freeTrial
}
