//
//  SubscriptionPlan.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 09.08.21.
//

import Foundation

class SubscriptionPlan: Codable {
    
    //MARK: - Properties
    let id: String
    let name: String
    let price: Double
    let coins: Double
    var selected: Bool
    let imagePath: String?
    let planTypeName: String
    let creditQuantity: Double
    
    enum CodingKeys: String, CodingKey {
        case id, name, price, coins, planTypeName, creditQuantity
        case imagePath = "filePath"
        case selected = "isSelecte"
    }
    
    //MARK: - Lifecycle
    required init(from decoder: Decoder) throws {
        let container  = try decoder.container(keyedBy: CodingKeys.self)
        id             = try container.decode(String.self, forKey: .id)
        name           = try container.decode(String.self, forKey: .name)
        price          = try container.decode(Double.self, forKey: .price)
        coins          = try container.decode(Double.self, forKey: .coins)
        imagePath      = try? container.decode(String.self, forKey: .imagePath)
        selected       = try container.decode(Bool.self, forKey: .selected)
        planTypeName   = try container.decode(String.self, forKey: .planTypeName)
        creditQuantity = try container.decode(Double.self, forKey: .creditQuantity)
    }
}

extension SubscriptionPlan {
    var fullPath: String? {
        if let path = imagePath {
            return Environment.live.baseURL + path
        }
        return nil
    }
}
