//
//  Purchase.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 26.07.21.
//

import Foundation

struct Purchase: Codable {
    
    //MARK: - Properties
    let id: String
    let buyingDate: String
    let filePath: String?
    let couponType: Int
    let title: String
    let description: String
    let priceCoins: Double
    let promoCode: String?
    let percentDiscount: String?
    
    //MARK: - Lifecycle
    init(from decoder: Decoder) throws {
        let container       = try decoder.container(keyedBy: CodingKeys.self)
        id                  = try container.decode(String.self, forKey: .id)
        buyingDate          = try container.decode(String.self, forKey: .buyingDate)
        couponType          = try container.decode(Int.self, forKey: .couponType)
        title               = try container.decode(String.self, forKey: .title)
        description         = try container.decode(String.self, forKey: .description)
        priceCoins          = try container.decode(Double.self, forKey: .priceCoins)
        filePath            = try? container.decode(String.self, forKey: .filePath)
        promoCode           = try? container.decode(String.self, forKey: .promoCode)
        percentDiscount     = try? container.decode(String.self, forKey: .percentDiscount)
    }
}
