//
//  Notification.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.07.21.
//

import Foundation

struct Notification: Codable {
    
    //MARK: - Properties
    let id: String
    let title: String?
    let coins: Int?
    let description: String?
    let distance: String?
    let duration: String?
    
    //MARK: - Lifecycle
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id            = try container.decode(String.self, forKey: .id)
        title         = try? container.decode(String.self, forKey: .title)
        coins         = try? container.decode(Int.self, forKey: .coins)
        distance      = try? container.decode(String.self, forKey: .distance)
        duration      = try? container.decode(String.self, forKey: .duration)
        description   = try? container.decode(String.self, forKey: .description)
    }
}
