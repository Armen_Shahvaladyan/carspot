//
//  Reward.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 16.08.21.
//

import Foundation

struct Reward: Codable {
    
    //MARK: - Properties
    let id: String
    let title: String
    let filePath:String?
    let couponType: CouponType
    let description: String?
    let priceCoins: Double
    let promoCode: String?
    let percentDiscount: Double?
    
    //MARK: - Lifecycle
    init(from decoder: Decoder) throws {
        let container   = try decoder.container(keyedBy: CodingKeys.self)
        id              = try container.decode(String.self, forKey: .id)
        title           = try container.decode(String.self, forKey: .title)
        filePath        = try? container.decode(String.self, forKey: .filePath)
        couponType      = try container.decode(CouponType.self, forKey: .couponType)
        description     = try? container.decode(String.self, forKey: .description)
        promoCode       = try? container.decode(String.self, forKey: .promoCode)
        priceCoins      = try container.decode(Double.self, forKey: .priceCoins)
        percentDiscount = try? container.decode(Double.self, forKey: .percentDiscount)
    }
}

enum CouponType: Int, Codable {
    case a = 1
    case b
    case c
}

extension Reward {
    var fullPath: String? {
        if let path = filePath {
            return Environment.live.baseURL + path
        }
        return nil
    }
}
