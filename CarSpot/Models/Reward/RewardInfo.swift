//
//  RewardInfo.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 16.08.21.
//

import Foundation

struct RewardInfo: Codable {
    
    //MARK: - Properties
    let question: String?
    let answer: String?
    
    //MARK: - Lifecycle
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        question      = try? container.decode(String.self, forKey: .question)
        answer        = try? container.decode(String.self, forKey: .answer)
    }
}
