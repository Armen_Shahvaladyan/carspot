//
//  Coin.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 04.08.21.
//

import Foundation

struct Coin: Codable {
    
    //MARK: - Properties
    let count: Double
    let activeDate: String?
    
    enum CodingKeys: String, CodingKey {
        case count = "coins"
        case activeDate = "coinsUpdateDate"
    }
    
    //MARK: - Lifecycle
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        count         = try container.decode(Double.self, forKey: .count)
        activeDate    = try? container.decode(String.self, forKey: .activeDate)
    }
}

struct UserCard: Codable {
    
    //MARK: - Properties
    let success: Bool
    
    enum CodingKeys: String, CodingKey {
        case success = "userCard"
    }
    
    //MARK: - Lifecycle
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        success       = try container.decode(Bool.self, forKey: .success)
    }
}
