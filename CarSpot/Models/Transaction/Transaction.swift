//
//  fetchTransaction.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 30.07.21.
//

import Foundation

struct Transaction: Codable {
    
    //MARK: - Properties
    let id: String
    let address: String
    let amount: Int
    let coins: Int
    let coinsType: CoinType
    let createdDate: String
}
