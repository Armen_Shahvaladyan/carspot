//
//  TransactionFilter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 30.07.21.
//

import Foundation
import SwiftMoment

struct TransactionFilter {
    
    //MARK: - Properties
    var coinsType: CoinType = .all
    var coinsFrom: Int = 10
    var coinsTo: Int = 5000
    var period: CalendarSelection?
    
    var hasFilter: Bool {
        return coinsType != .all || coinsFrom != 10 || coinsTo != 5000 || period != nil
    }
    
    //MARK: - Public API
    mutating func reset() {
        coinsType = .all
        coinsFrom = 10
        coinsTo = 5000
        period = nil
    }
    
    func map() -> [String: Any] {
        var param: [String: Any] = [:]
        
        switch coinsType {
        case .earned, .spent:
            param["coinsType"] = coinsType.rawValue
        default:
            param["coinsType"] = nil
        }
        
        if coinsFrom != 10 {
            param["coinsFrom"] = coinsFrom
        }
        
        if coinsTo != 5000 {
            param["coinsTo"] = coinsTo
        }
        
        if let period = period {
            switch period {
            case .dayRange(let range):
                let start = range.lowerBound
                let end = range.upperBound
                
                if let startDate = Calendar.current.date(from: start.components),
                   let endDate = Calendar.current.date(from: end.components) {
                    param["peroidFrom"] = moment(startDate).format("yyyy-MM-dd'T'HH:mm:ssX")
                    param["peroidTo"] = moment(endDate).format("yyyy-MM-dd'T'HH:mm:ssX")
                }
            default:
                param["peroidFrom"] = nil
                param["peroidTo"] = nil
            }
        } else {
            param["peroidFrom"] = nil
            param["peroidTo"] = nil
        }

        return param
    }
}

enum CoinType: Int, Codable {
    case all    = 0
    case earned = 1
    case spent  = 2
    
    var title: String {
        switch self {
        case .all:
            return "All"
        case .earned:
            return "Earned"
        case .spent:
            return "Spent"
        }
    }
    
    var name: String {
        switch self {
        case .all:
            return ""
        case .earned:
            return "Offer spot"
        case .spent:
            return "Find spot"
        }
    }
    
    var image: UIImage? {
        switch self {
        case .all:
            return nil
        case .earned:
            return #imageLiteral(resourceName: "ic_up_arrow")
        case .spent:
            return #imageLiteral(resourceName: "ic_down_arrow")
        }
    }
    
    var color: UIColor {
        switch self {
        case .all:
            return .clear
        case .earned:
            return UIColor("#00B377")
        case .spent:
            return UIColor("#FF1A40")
        }
    }
}
