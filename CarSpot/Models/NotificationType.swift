//
//  NotificationType.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.07.21.
//

import Foundation

struct NotificationType: Codable {
    
    //MARK: - Properties
    let option: NotificationOption
    
    enum CodingKeys: String, CodingKey {
        case option = "notificationOffType"
    }
    
    //MARK: - Lifecycle
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        option        = try container.decode(NotificationOption.self, forKey: .option)
    }
}
