//
//  ParkingState.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 23.09.21.
//

import Foundation
import CoreLocation

struct ParkingStatus: Codable {
    
    //MARK: - Properties
    let state: ParkingState
    var carInfo: CarInfo?
    var conversationId: String?
    var spotId: String?
    var saleSpotId: String?
    var lat: Double?
    var lng: Double?
    var firstName: String?
    var lastName: String?
    var rating: Double?
    var picturePath: String?
    var address: String?
    var amount: Double?
    var distanse: String?
    var time: String?
    var authorId: String?
    var participantId: String?
    var spotType: SpotType?
    
    enum CodingKeys: String, CodingKey {
        case state              = "parkingStateStatus"
        case firstName          = "userFirstName"
        case lastName           = "userLastName"
        case picturePath        = "userProfilePicturePath"
        case rating             = "userRating"
        case spotType           = "userSpotType"
        case spotId, saleSpotId, lat, lng, address, amount, distanse, time, authorId, participantId, conversationId, carInfo
    }
    
    //MARK: - Lifecycle
    init(from decoder: Decoder) throws {
        let container   = try decoder.container(keyedBy: CodingKeys.self)
        conversationId  = try? container.decode(String.self, forKey: .conversationId)
        spotId          = try? container.decode(String.self, forKey: .spotId)
        saleSpotId      = try? container.decode(String.self, forKey: .saleSpotId)
        lat             = try? container.decode(Double.self, forKey: .lat)
        lng             = try? container.decode(Double.self, forKey: .lng)
        firstName       = try? container.decode(String.self, forKey: .firstName)
        lastName        = try? container.decode(String.self, forKey: .lastName)
        rating          = try? container.decode(Double.self, forKey: .rating)
        picturePath     = try? container.decode(String.self, forKey: .picturePath)
        address         = try? container.decode(String.self, forKey: .address)
        amount          = try? container.decode(Double.self, forKey: .amount)
        distanse        = try? container.decode(String.self, forKey: .distanse)
        time            = try? container.decode(String.self, forKey: .time)
        authorId        = try? container.decode(String.self, forKey: .authorId)
        participantId   = try? container.decode(String.self, forKey: .participantId)
        carInfo         = try? container.decode(CarInfo.self, forKey: .carInfo)
        spotType        = try? container.decode(SpotType.self, forKey: .spotType)
        state           = (try? container.decode(ParkingState.self, forKey: .state)) ?? .none
    }
}

struct CarInfo: Codable {
    let number: String
    let colorName: String
    let hexCode: String
    let brand: String
    let model: String
}

enum ParkingState: Int, Codable {
    case none = 1
    case find
    case suggest
    case book
    case finished
}

enum SpotType: Int, Codable {
    case find = 1
    case suggest
}

extension ParkingStatus {
    var location: CLLocation? {
        guard let lat = lat,
              let lng = lng else { return nil }
        return CLLocation(latitude: lat, longitude: lng)
    }
}
