//
//  Address.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 25.07.21.
//

import Foundation

struct Address: Codable {
    
    //MARK: - Properties
    let id: String
    let lat: Double
    let lng: Double
    let address: String
    let name: String
    let comment: String?
    let type: AddressType
    
    enum CodingKeys: String, CodingKey {
        case id, lat, lng, name, comment
        case address = "addressText"
        case type    = "addressType"
    }
    
    //MARK: - Lifecycle
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id            = try container.decode(String.self, forKey: .id)
        lat           = try container.decode(Double.self, forKey: .lat)
        lng           = try container.decode(Double.self, forKey: .lng)
        name          = try container.decode(String.self, forKey: .name)
        comment       = try? container.decode(String.self, forKey: .comment)
        address       = try container.decode(String.self, forKey: .address)
        type          = try container.decode(AddressType.self, forKey: .type)
    }
}

enum AddressType: Int, Codable {
    case home  = 1
    case work  = 2
    case other = 3
    
    var title: String {
        switch self {
        case .home:
            return "Home"
        case .work:
            return "Work"
        case .other:
            return "Other"
        }
    }
    
    var image: UIImage {
        switch self {
        case .home:
            return #imageLiteral(resourceName: "ic_home")
        case .work:
            return #imageLiteral(resourceName: "ic_work")
        case .other:
            return #imageLiteral(resourceName: "ic_star")
        }
    }
}
