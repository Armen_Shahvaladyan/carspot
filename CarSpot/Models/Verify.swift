//
//  Verify.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 16.07.21.
//

import Foundation

struct Verify: Codable {
    
    //MARK: - Properties
    let token: String
    let expirationDate: String
    let authenticate: Bool
    
    enum CodingKeys: String, CodingKey {
        case expirationDate
        case token        = "accessToken"
        case authenticate = "isAuthenticate"
    }
}
