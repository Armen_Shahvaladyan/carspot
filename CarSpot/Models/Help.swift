//
//  Help.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 27.07.21.
//

import Foundation

struct Help: Codable {
    
    //MARK: - Properties
    let id: Int
    let title: String?
    let items: [HelpItem]?
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case items = "helps"
    }
    
    //MARK: - Lifecycle
    init(from decoder: Decoder) throws {
        let container       = try decoder.container(keyedBy: CodingKeys.self)
        id                  = try container.decode(Int.self, forKey: .id)
        title               = try? container.decode(String.self, forKey: .title)
        items               = try? container.decode([HelpItem].self, forKey: .items)
    }
}

struct HelpItem: Codable {
    
    //MARK: - Properties
    let id: Int
    let subTitle: String?
    let description: String?
    
    //MARK: - Lifecycle
    init(from decoder: Decoder) throws {
        let container       = try decoder.container(keyedBy: CodingKeys.self)
        id                  = try container.decode(Int.self, forKey: .id)
        subTitle            = try? container.decode(String.self, forKey: .subTitle)
        description         = try? container.decode(String.self, forKey: .description)
    }
}
