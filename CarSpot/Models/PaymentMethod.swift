//
//  PaymentMethod.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 23.07.21.
//

import Foundation

enum CardType: String, Codable {
    case visa = "Visa"
    case masterCard = "MasterCard"
    case unknown = ""
    
    var image: UIImage? {
        switch self {
        case .visa:
            return #imageLiteral(resourceName: "VISA")
        case .masterCard:
            return #imageLiteral(resourceName: "MASTERCARD")
        case .unknown:
            return nil
        }
    }
}

struct PaymentMethod: Codable {
    
    //MARK: - Properties
    let id: String
    let lastDight: String
    let date: String
    let cardType: CardType
    let name: String
    let isDefault: Bool
    
    enum CodingKeys: String, CodingKey {
        case id, cardType, isDefault
        case lastDight        = "cardNumberLastDight"
        case date = "expirationDate"
        case name = "ownerName"
    }
    
    //MARK: - Lifecycle
    init(from decoder: Decoder) throws {
        let container           = try decoder.container(keyedBy: CodingKeys.self)
        id                      = try container.decode(String.self, forKey: .id)
        name                    = try container.decode(String.self, forKey: .name)
        date                    = try container.decode(String.self, forKey: .date)
        lastDight               = try container.decode(String.self, forKey: .lastDight)
        isDefault               = try container.decode(Bool.self, forKey: .isDefault)
        cardType                = (try? container.decode(CardType.self, forKey: .cardType)) ?? .unknown
    }
}

extension PaymentMethod {
    var number: String {
        "**** **** **** \(lastDight)"
    }
}
