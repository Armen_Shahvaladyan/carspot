//
//  AppGlobal.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 15.07.21.
//

import Foundation

struct AppGlobal: Codable {
    
    //MARK: - Properties
    let privacyPolicy: String
    let aboutApp: String
    let aboutAsFacebook: String
    let aboutAsInstagram: String
    let aboutAsEmail: String
    let aboutAsPhoneNumber: String
    let termsAndConditions: String
    let aboutSubscription: [String]
    let faq: [FAQ]
}

struct FAQ: Codable {
    
    //MARK: - Properties
    let id: Int
    let value: String
    let appGlobalType: Int
}
