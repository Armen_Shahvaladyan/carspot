//
//  Spot.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.08.21.
//

import Foundation
import CoreLocation

class Spot: Codable {
    
    //MARK: - Properties
    let id: String
    let saleSpotId: String
    let participantId: String
    let lat: Double
    let lng: Double
    let address: String
    var isSuggested: Bool
    let firstName: String
    let lastName: String
    let rating: Double
    let picturePath: String?
    var amount: Double
    let distanse: String
    let time: String
    let authorId: String
    
    enum CodingKeys: String, CodingKey {
        case id          = "spotId"
        case firstName   = "userFirstName"
        case lastName    = "userLastName"
        case rating      = "userRating"
        case picturePath = "userProfilePicturePath"
        case saleSpotId, lat, lng, address, amount, distanse, time, authorId, participantId, isSuggested
    }
    
    //MARK: - Lifecycle
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id            = try container.decode(String.self, forKey: .id)
        saleSpotId    = try container.decode(String.self, forKey: .saleSpotId)
        participantId = try container.decode(String.self, forKey: .participantId)
        lat           = try container.decode(Double.self, forKey: .lat)
        lng           = try container.decode(Double.self, forKey: .lng)
        address       = try container.decode(String.self, forKey: .address)
        isSuggested   = (try? container.decode(Bool.self, forKey: .isSuggested)) ?? false
        firstName     = try container.decode(String.self, forKey: .firstName)
        lastName      = try container.decode(String.self, forKey: .lastName)
        rating        = try container.decode(Double.self, forKey: .rating)
        picturePath   = try? container.decode(String.self, forKey: .picturePath)
        amount        = try container.decode(Double.self, forKey: .amount)
        distanse      = try container.decode(String.self, forKey: .distanse)
        time          = try container.decode(String.self, forKey: .time)
        authorId      = try container.decode(String.self, forKey: .authorId)
    }
}

extension Spot {
    var fullName: String {
        firstName + " " + lastName
    }
    
    var rate: String {
        rating.rounded(to: 3).removeZerosFromEnd()
    }
    
    var position: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: lat, longitude: lng)
    }
}
