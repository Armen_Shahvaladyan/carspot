//
//  OfferSpot.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 26.08.21.
//

import Foundation

struct Offer: Codable {
    
    //MARK: - Properties
    let id: String
    let spots: [Spot]
    
    enum CodingKeys: String, CodingKey {
        case spots
        case id = "saleSpotId"
    }
    
    //MARK: - Lifecycle
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id            = try container.decode(String.self, forKey: .id)
        spots         = try container.decode([Spot].self, forKey: .spots)
    }
}
