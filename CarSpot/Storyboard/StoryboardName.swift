//
//  StoryboardName.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 21.05.21.
//

import Foundation

enum StoryboardName: String {
    case onboarding     = "Onboarding"
    case auth           = "Auth"
    case profile        = "Profile"
    case leftMenu       = "LeftMenu"
    case home           = "Home"
    case userProfile    = "UserProfile"
    case payment        = "Payment"
    case purchases      = "Purchases"
    case addresses      = "Addresses"
    case contactUs      = "ContactUs"
    case notification   = "Notification"
    case transactions   = "Transactions"
    case rewards        = "Rewards"
    case help           = "Help"
    case aboutApp       = "AboutApp"
    case start          = "Start"
}
