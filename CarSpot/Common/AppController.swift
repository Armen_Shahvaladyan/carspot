//
//  AppController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 01.06.21.
//

import UIKit

struct AppController {
    
}

extension AppController {
    /// Check user signIn and have token or not
    static var hasToken: Bool {
        return KeychainWrapper.token != nil
    }
    
    static func removeToken() {
        KeychainWrapper.token = nil
    }
    
    static func dismissAnyAlertControllerIfPresent() {
        guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first,
              var topVC = window.rootViewController?.presentedViewController else { return }
        while topVC.presentedViewController != nil  {
            topVC = topVC.presentedViewController!
        }
        if topVC.isKind(of: UIAlertController.self) {
            topVC.dismiss(animated: false, completion: nil)
        }
    }
    
    static func logout() {
        KeychainWrapper.token = nil
        guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else { return }
        Launcher.perform(segue: .auth, window: window)
    }
}
