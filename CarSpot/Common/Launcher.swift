//
//  Launcher.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 21.05.21.
//

import UIKit

struct Launcher {
    enum RootScreen {
        case onboarding
        case auth
        case main
        case start
        case spotDetails(ParkingStatus?)
    }
    
    static func perform(segue: RootScreen, window: UIWindow) {
        switch segue {
        case .onboarding:
            let onboardingViewController = OnboardingRouter.createOnboardingViewController()
            window.rootViewController = onboardingViewController
            window.makeKeyAndVisible()
        case .auth:
            let phoneNumberViewController = PhoneNumberRouter.createPhoneNumberViewController()
            let navigationController = NavigationController(rootViewController: phoneNumberViewController)
            window.rootViewController = navigationController
            window.layer.fadeTransaction(duration: 0.4)
            window.makeKeyAndVisible()
        case .main:
            let vc = HostViewController()
            window.rootViewController = vc
            window.layer.fadeTransaction(duration: 0.4)
            window.makeKeyAndVisible()
        case .start:
            let vc = StartRouter.createStartViewController()
            window.rootViewController = vc
            window.makeKeyAndVisible()
        case let .spotDetails(status):
            let vc = SpotDetailsContainerRouter.createSpotDetailsContainerViewController(with: status)
            let nc = NavigationController(rootViewController: vc)
            window.rootViewController = nc
            window.layer.fadeTransaction(duration: 0.4)
            window.makeKeyAndVisible()
        }
    }
}
