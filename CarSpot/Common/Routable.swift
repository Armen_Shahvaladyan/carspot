//
//  Routable.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 21.05.21.
//

import Foundation

protocol Routable {
    associatedtype SegueType
    associatedtype SourceType
    func perform(_ segue: SegueType, from source: SourceType)
}
