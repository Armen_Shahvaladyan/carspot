//
//  Errorable.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 01.06.21.
//

import Foundation

protocol Errorable: Error {
    var title: String { get }
    var message: String { get }
}

extension Errorable {
    var title: String {
        return "Attention!"
    }
}
