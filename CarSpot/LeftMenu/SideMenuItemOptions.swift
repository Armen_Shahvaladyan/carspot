//
//  SideMenuItemOptions.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.06.21.
//

import UIKit

/**
 Defines the parameters for the shadow that is shown under to content controller when the side menu is open.
 */
struct SideMenuItemOptions {
    
    struct Shadow {
        var color: UIColor? = UIColor.black
        var opacity: CGFloat = 0.3
        var offset: CGSize = CGSize(width: -5, height: 5)
        var radius: CGFloat = 3
        
        init() { }
        
        init(color: UIColor? = UIColor.black,
             opacity: CGFloat = 0.3,
             offset: CGSize = CGSize(width: -5, height: 5),
             radius: CGFloat = 3) {
            self.color = color
            self.opacity = opacity
            self.offset = offset
            self.radius = radius
        }
    }
    
    var shadow = Shadow()
    
    var cornerRadius: CGFloat = 0.0 {
        willSet {
            if newValue < 0 {
                preconditionFailure("Invalid `cornerRadius` value: \(newValue). Must be non-negative")
            }
        }
    }
    
    init() { }
}
