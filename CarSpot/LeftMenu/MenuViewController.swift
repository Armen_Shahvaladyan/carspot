//
//  MenuViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.06.21.
//

import UIKit

class MenuViewController: UIViewController {
    
    weak var menuContainerViewController: MenuContainerViewController?
    var navigationMenuTransitionDelegate: MenuTransitioningDelegate?
    
    @objc func handleTap(recognizer: UIGestureRecognizer) {
        menuContainerViewController?.hideSideMenu()
    }
}
