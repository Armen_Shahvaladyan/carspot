//
//  SideMenuItemContent.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.06.21.
//

import UIKit

/**
 The protocol to indicate item of side menu. Every menu item should adopt this protocol.
 */
protocol SideMenuItemContent {

    /**
     Shows left side menu.
     */
    func showSideMenu()
}

/**
 The extension of SideMenuItemContent protocol implementing `showSideMenu()` method for UIViewController class.
 */
extension SideMenuItemContent where Self: UIViewController {
    
    func showSideMenu() {
        if let menuContainerViewController = parent as? MenuContainerViewController {
            menuContainerViewController.showSideMenu()
        } else if let navController = parent as? UINavigationController,
                  let menuContainerViewController = navController.parent as? MenuContainerViewController {
            menuContainerViewController.showSideMenu()
        }
    }
}
