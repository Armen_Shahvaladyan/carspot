//
//  Int+Extension.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 03.08.21.
//

import Foundation

extension Int {
    var doubleValue: Double {
        return Double(self)
    }
}
