//
//  Bundle+Extension.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.07.21.
//

import Foundation

extension Bundle {
    var displayName: String? {
        return infoDictionary?["CFBundleName"] as? String
    }
    
    var bundleVersion: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
    
    var versionReleaseNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    var releaseVersionNumberPretty: String {
        return "\(versionReleaseNumber ?? "1.0").\(bundleVersion ?? "1")"
    }
}
