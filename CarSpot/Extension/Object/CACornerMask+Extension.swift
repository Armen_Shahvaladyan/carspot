//
//  CACornerMask+Extension.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 26.06.21.
//

import UIKit

extension CACornerMask {
    static var topLeftTopRight: CACornerMask {
        [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    static var bottomLeftBottomRight: CACornerMask {
        [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
    static var leftSide: CACornerMask {
        [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
    
    static var rightSide: CACornerMask {
        [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
    }
    
    static var all: CACornerMask {
        [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
}
