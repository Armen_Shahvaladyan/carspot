//
//  Thread+Extension.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 01.06.21.
//

import Foundation

extension Thread {
    class func onMainThread(_ completion: @escaping (()-> Void)) {
        if Thread.current.isMainThread {
            completion()
            return
        } else {
            DispatchQueue.main.async {
                completion()
            }
        }
    }
}
