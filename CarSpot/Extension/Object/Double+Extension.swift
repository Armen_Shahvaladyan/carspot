//
//  Double+Extension.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 03.08.21.
//

import Foundation

extension Double {
    var intValue: Int {
        return Int(self)
    }
    
    func removeZerosFromEnd() -> String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 16
        return String(formatter.string(from: number) ?? "0")
    }
    
    func rounded(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
