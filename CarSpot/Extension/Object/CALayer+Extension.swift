//
//  CALayer+Extension.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 22.05.21.
//

import UIKit

extension CALayer {
    func fadeTransaction(duration: CFTimeInterval) {
        let transition = CATransition()
        transition.duration = duration
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        transition.type = .fade
        add(transition, forKey: nil)
    }
    
    func presentTransaction(from: CATransitionSubtype, type: CATransitionType = .moveIn, duration: CFTimeInterval = 0.5) {
        let transition = CATransition()
        transition.duration = duration
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        transition.type = type
        transition.subtype = from
        add(transition, forKey: nil)
    }
}
