//
//  UserDefaults+Extension.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 21.05.21.
//

import Foundation

extension UserDefaults {
    static var seenOnboarding: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: "hasSeenOnboarding")
            UserDefaults.standard.synchronize()
        }
        get {
            if let seenOnboarding = UserDefaults.standard.object(forKey: "hasSeenOnboarding") as? Bool {
                return seenOnboarding
            }
            return false
        }
    }
}
