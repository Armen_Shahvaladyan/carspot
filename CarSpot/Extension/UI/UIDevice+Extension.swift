//
//  UIDevice+Extension.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 30.09.21.
//

import UIKit

extension UIDevice {
    var hasSafeArea: Bool {
        guard #available(iOS 11.0, *),
              let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first,
              let windowScene = window.windowScene else { return false }
        if windowScene.interfaceOrientation.isPortrait {
            return window.safeAreaInsets.top >= 44
        } else {
            return window.safeAreaInsets.left > 0 || window.safeAreaInsets.right > 0
        }
    }
}
