//
//  UIImageView+Extension.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 22.07.21.
//

import UIKit
import SDWebImage

extension UIImageView {
    func setImage(path: String?, _ placeHolder: UIImage?) {
        if let path = path, !path.isEmpty {
            sd_imageIndicator = SDWebImageActivityIndicator.gray
            sd_setImage(with: URL(string: path), placeholderImage: nil, options: []) { (image, error, cacheType, url) in
                if error == nil {
                    self.sd_imageIndicator = .none
                    self.image = image
                } else {
                    self.image = placeHolder
                }
            }
        } else {
            image = placeHolder
        }
    }
    
    func setImage(path: String?, _ placeHolder: UIImage?, completion: @escaping (UIImage?) -> Void) {
        if let path = path, !path.isEmpty {
            sd_imageIndicator = SDWebImageActivityIndicator.gray
            sd_setImage(with: URL(string: path), placeholderImage: nil, options: []) { (image, error, cacheType, url) in
                if error == nil {
                    self.sd_imageIndicator = .none
                    self.image = image
                    completion(image)
                } else {
                    self.image = placeHolder
                    completion(nil)
                }
            }
        } else {
            image = placeHolder
            completion(nil)
        }
    }
}
