//
//  UIViewController+Extension.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 01.06.21.
//

import UIKit

extension UIViewController {
    func showAlertForLocation(with error: Errorable, settingsAction: @escaping (() -> Void)) {
        let ac = UIAlertController(title: error.title, message: error.message, preferredStyle: .alert)
        
        let settings = UIAlertAction(title: "Settings", style: .default) { _ in
            settingsAction()
        }
        
        ac.addAction(settings)
        ac.preferredAction = settings
        
        Thread.onMainThread {
            self.present(ac, animated: true, completion:nil)
        }
    }
    
    func showAlert(with error: Errorable, settingsAction: (() -> Void)? = nil) {
        let ac = UIAlertController(title: error.title, message: error.message, preferredStyle: .alert)
        
        let actionTitle = settingsAction == nil ? "Ok" : "Settings"
        let ok = UIAlertAction(title: actionTitle, style: .default) { _ in
            settingsAction?()
        }
        
        ac.addAction(ok)
        ac.preferredAction = ok
        
        if settingsAction != nil {
            let closeAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            ac.addAction(closeAction)
        }
        
        Thread.onMainThread {
            self.present(ac, animated: true, completion:nil)
        }
    }
    
    func showAlert(with error: CSError, settingsAction: (() -> Void)? = nil) {
        let ac = UIAlertController(title: error.key, message: error.message, preferredStyle: .alert)
        
        let actionTitle = settingsAction == nil ? "Ok" : "Settings"
        let ok = UIAlertAction(title: actionTitle, style: .default) { _ in
            settingsAction?()
        }
        
        ac.addAction(ok)
        ac.preferredAction = ok
        
        if settingsAction != nil {
            let closeAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            ac.addAction(closeAction)
        }
        
        Thread.onMainThread {
            self.present(ac, animated: true, completion:nil)
        }
    }
    
    func showAlert(with title: String, message: String) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Ok", style: .default)
        
        ac.addAction(ok)
        
        Thread.onMainThread {
            self.present(ac, animated: true, completion:nil)
        }
    }
    
    func showAlertForBook(with title: String,
                          message: String,
                          noAction: @escaping () -> Void,
                          yesAction: @escaping () -> Void) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let no = UIAlertAction(title: "No", style: .default) { _ in
            noAction()
        }
        
        let yes = UIAlertAction(title: "Yes", style: .default) { _ in
            yesAction()
        }
        
        ac.addAction(no)
        ac.addAction(yes)
        ac.preferredAction = yes
        
        Thread.onMainThread {
            self.present(ac, animated: true, completion:nil)
        }
    }
}
