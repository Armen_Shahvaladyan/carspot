//
//  UIView+Extension.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 22.05.21.
//

import UIKit

extension UIView {
    func rotate(angle: Double, animated: Bool = true) {
        func rotate(angle: Double) {
            self.transform = CGAffineTransform(rotationAngle: CGFloat(angle))
        }
        
        Thread.onMainThread {
            if animated {
                UIView.animate(withDuration: 0.2,
                               animations: {
                                rotate(angle: angle)
                               })
            } else {
                rotate(angle: angle)
            }
        }
    }
    
    var safeAreaTop: CGFloat {
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            return window?.safeAreaInsets.top ?? 0
        }
        return 0
    }
    
    var safeAreaBottom: CGFloat {
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            return window?.safeAreaInsets.bottom ?? 0
        }
        return 0
    }
}
