//
//  UIViewController+Extension.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 02.06.21.
//

import UIKit

private var vSpinner: UIView?
private var activityIndicator = UIActivityIndicatorView(style: .large)
private var onViewInstance: UIView?

extension UIViewController {
    // MARK: Loading

    func showLoading(onView: UIView,
                     backColor: UIColor = .clear,
                     tag: Int = 11) {
        onViewInstance = onView
        vSpinner = UIView(frame: onView.bounds)
        vSpinner!.backgroundColor = backColor
        activityIndicator.color = UIColor.appOrange
        activityIndicator.startAnimating()

        activityIndicator.center = onView.center

        DispatchQueue.main.async {
            guard let spiner = vSpinner else { return }
            spiner.tag = tag
            spiner.addSubview(activityIndicator)
            onView.addSubview(spiner)
        }
    }

    func hideLoading(tag: Int = 11) {
        DispatchQueue.main.async {
            onViewInstance?.subviews.forEach { itemView in
                if itemView.tag == tag {
                    itemView.removeFromSuperview()
                }
            }
            activityIndicator.stopAnimating()
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}

@nonobjc extension UIViewController {
    func addChild(_ child: UIViewController, to view: UIView) {
        addChild(child)

        child.view.frame = view.bounds

        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func addChild(_ child: UIViewController, frame: CGRect? = nil) {
        addChild(child)

        if let frame = frame {
            child.view.frame = frame
        }

        view.addSubview(child.view)
        child.didMove(toParent: self)
    }

    func remove() {
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
}

extension UIViewController {
    func topMostViewController() -> UIViewController {
        
        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }
        
        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
        }
        
        return self
    }
}
