//
//  UINavigationController+Extension.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 12.06.21.
//

import UIKit

extension UINavigationController {
    func hideHairline() {
        if let hairline = findHairlineImageViewUnder(navigationBar) {
            hairline.isHidden = true
        }
    }
    
    func restoreHairline() {
        if let hairline = findHairlineImageViewUnder(navigationBar) {
            hairline.isHidden = false
        }
    }
    
    func findHairlineImageViewUnder(_ view: UIView) -> UIImageView? {
        if view is UIImageView && view.bounds.size.height <= 1.0 {
            let v = view as? UIImageView
            v?.backgroundColor = .red
            return v
        }
        for subview in view.subviews {
            if let imageView = self.findHairlineImageViewUnder(subview) {
                imageView.backgroundColor = .green
                return imageView
            }
        }
        return nil
    }
}
