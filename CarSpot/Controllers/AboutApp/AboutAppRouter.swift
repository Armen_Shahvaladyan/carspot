//
//  AboutAppRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

enum AboutAppSegue {
    case termsAndConditions
    case privacyPolicy
    case sourceLibraries
}

protocol AboutAppRoutable: Routable where SegueType == AboutAppSegue, SourceType == AboutAppViewController {

}

struct AboutAppRouter: AboutAppRoutable {
    
    func perform(_ segue: AboutAppSegue, from source: AboutAppViewController) {
        switch segue {
        case .termsAndConditions:
            let vc = AppGlobalRouter.createPrivacyPolicyViewController(with: .termsAndConditions)
            source.present(vc, animated: true)
        case .privacyPolicy:
            let vc = AppGlobalRouter.createPrivacyPolicyViewController(with: .privacyPolicy)
            source.present(vc, animated: true)
        case .sourceLibraries:
            let vc = SourceLibrariesRouter.createSourceLibrariesViewController()
            source.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension AboutAppRouter {
    static func createAboutAppViewController() -> AboutAppViewController {
        let vc = AboutAppViewController.storyboardInstance
        vc.router = AboutAppRouter()
        vc.viewModel = AboutAppViewModel()
        
        return vc
    }
}
