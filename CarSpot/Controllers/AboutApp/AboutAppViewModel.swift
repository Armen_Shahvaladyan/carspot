//
//  AboutAppViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import Foundation

class AboutAppViewModel {
   
    //MARK: - Properties
    private(set) var appGlobal: AppGlobal?
    private let globalProvider = Provider<GlobalEndPoint>()
    
    //MARK: - Public API
    func fetchGlobal(completion: @escaping (CSError?) -> Void) {
        globalProvider.request(target: .all, type: AppGlobal.self) { (appGlobal, error) in
            Thread.onMainThread {
                self.appGlobal = appGlobal
                completion(error)
            }
        }
    }
}
