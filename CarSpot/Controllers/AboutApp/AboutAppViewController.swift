//
//  AboutAppViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

class AboutAppViewController: LeftSideMainViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    
    //MARK: - Properties
    var router: AboutAppRouter!
    var viewModel: AboutAppViewModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    //MARK: - Private API
    private func setup() {
        title = "About app"
        
        if let version = Bundle.main.versionReleaseNumber {
            versionLabel.isHidden = false
            versionLabel.text = "Version \(version)"
        } else {
            versionLabel.isHidden = true
        }
        
        showLoading(onView: self.view)
        viewModel.fetchGlobal { [weak self] error in
            guard let self = self else { return }
            Thread.onMainThread {
                self.hideLoading()
                if let error = error {
                    self.showAlert(with: error)
                } else if let appGlobal = self.viewModel.appGlobal {
                    self.configureView(with: appGlobal)
                }
            }
        }
    }
    
    private func configureView(with appGlobal: AppGlobal) {
        infoLabel.text = appGlobal.aboutApp
    }
    
    //MARK: - IBActions
    @IBAction func openLibrary() {
        router.perform(.sourceLibraries, from: self)
    }
    
    @IBAction func openTermsAndConditions() {
        router.perform(.termsAndConditions, from: self)
    }
    
    @IBAction func openPrivacyPolicy() {
        router.perform(.privacyPolicy, from: self)
    }
}

extension AboutAppViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .aboutApp
}
