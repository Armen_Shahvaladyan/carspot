//
//  SourceLibrariesViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.07.21.
//

import Foundation

enum SourceLibraryType: String, CaseIterable {
    case moya = "Moya"
    case cRRefresh = "CRRefresh"
    case googleMaps = "GoogleMaps"
    case sDWebImage = "SDWebImage"
    case googlePlaces = "GooglePlaces"
    case keychainAccess = "KeychainAccess"
    case horizonCalendar = "HorizonCalendar"
    case iQKeyboardManagerSwift = "IQKeyboardManagerSwift"
    case skyFloatingLabelTextField = "SkyFloatingLabelTextField"
}

class SourceLibrariesViewModel {
    let sourceLibraries = SourceLibraryType.allCases
}
