//
//  SourceLibrariesViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.07.21.
//

import UIKit

class SourceLibrariesViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    var router: SourceLibrariesRouter!
    var viewModel: SourceLibrariesViewModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    //MARK: - Private API
    private func setup() {
        title = "Source Libraries"
        collectionView.dataSource = self
        collectionView.delegate = self
    }
}

extension SourceLibrariesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.sourceLibraries.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let type = viewModel.sourceLibraries[indexPath.item]
        let cell = SourceLibraryCell.cell(collection: collectionView, indexPath: indexPath)
        cell.configure(with: type)
        return cell
    }
}

extension SourceLibrariesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.bounds.width, height: 56)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let type = viewModel.sourceLibraries[indexPath.item]
        router.perform(.source(type), from: self)
    }
}

extension SourceLibrariesViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .aboutApp
}
