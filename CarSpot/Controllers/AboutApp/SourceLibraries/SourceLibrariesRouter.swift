//
//  SourceLibrariesRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.07.21.
//

import UIKit

enum SourceLibrariesSegue {
    case source(SourceLibraryType)
}

protocol SourceLibrariesRoutable: Routable where SegueType == SourceLibrariesSegue, SourceType == SourceLibrariesViewController {

}

struct SourceLibrariesRouter: SourceLibrariesRoutable {
    func perform(_ segue: SourceLibrariesSegue, from source: SourceLibrariesViewController) {
        switch segue {
        case .source(let type):
            print("sdsd")
//            let vc = LicenseRouter.createLicenseViewController(with: type)
//            source.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension SourceLibrariesRouter {
    static func createSourceLibrariesViewController() -> SourceLibrariesViewController {
        let vc = SourceLibrariesViewController.storyboardInstance
        vc.router = SourceLibrariesRouter()
        vc.viewModel = SourceLibrariesViewModel()
        
        return vc
    }
}
