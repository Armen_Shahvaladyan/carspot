//
//  SourceLibraryCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.07.21.
//

import UIKit

class SourceLibraryCell: CollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var nameLabel: UILabel!
    
    //MARK: - Public API
    func configure(with type: SourceLibraryType) {
        nameLabel.text = type.rawValue
    }
}
