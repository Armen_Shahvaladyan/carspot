//
//  TopupViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.08.21.
//

import UIKit

class TopupViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var amountView: EditingView!
    @IBOutlet weak var topUpButton: Button!
    @IBOutlet weak var cardView: SelectedViewWithIcon!
    @IBOutlet weak var successfullyView: UIView!
    @IBOutlet weak var coinLabel: UILabel!
    @IBOutlet weak var okButton: Button!
    
    //MARK: - Properties
    var router: TopupRouter!
    var viewModel: TopupViewModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    //MARK: - Public API
    func select(method: PaymentMethod?) {
        viewModel.selectedMethod = method
        checkTopupButtonState()
        if let method = method {
            cardView.text = "**** **** **** \(method.lastDight)"
        }
    }
    
    //MARK: - Private API
    private func setup() {
        amountView.textField.keyboardType = .decimalPad
        amountView.delegate = self
        cardView.delegate = self
        topUpButton.buttonState = .disable
        okButton.buttonState = .secondary
        successfullyView.isHidden = true
    }
    
    private func checkTopupButtonState() {
        if let amount = viewModel.amount,
           amount > 0,
           viewModel.selectedMethod != nil {
            topUpButton.buttonState = .default
        } else {
            topUpButton.buttonState = .disable
        }
    }
    
    private func startLoading() {
        topUpButton.buttonState = .load
        amountView.isUserInteractionEnabled = false
        cardView.isUserInteractionEnabled = false
    }
    
    private func stopLoading() {
        topUpButton.buttonState = .default
        amountView.isUserInteractionEnabled = true
        cardView.isUserInteractionEnabled = true
    }
    
    //MARK: - IBActions
    @IBAction func cancel() {
        router.perform(.dismiss, from: self)
    }
    
    @IBAction func topup() {
        startLoading()
        viewModel.buyCoins { [weak self] error in
            guard let self = self else { return }
            self.stopLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.coinLabel.text = "\(self.viewModel.amount ?? 0) $"
                self.successfullyView.isHidden = false
            }
        }
    }
    
    @IBAction func ok() {
        router.perform(.dismiss, from: self)
    }
}

extension TopupViewController: PanModalPresentable {
    var cornerRadius: CGFloat {
        return 32
    }

    var panScrollable: UIScrollView? {
        return scrollView
    }

    var longFormHeight: PanModalHeight {
        return .contentHeight(scrollView.contentSize.height + 56)
    }

    var anchorModalToLongForm: Bool {
        return false
    }

    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .longForm = state
        else { return }

        panModalSetNeedsLayoutUpdate()
    }
}

extension TopupViewController: EditingViewDelegate {
    func editingView(_ view: EditingView, didChange text: String) {
        viewModel.amount = text.doubleValue
        checkTopupButtonState()
    }
}

extension TopupViewController: SelectedViewWithIconDelegate {
    func selectedViewWithIcon(_ view: SelectedViewWithIcon, didSelect text: String) {
        router.perform(.cards, from: self)
    }
}

extension TopupViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .transactions
}
