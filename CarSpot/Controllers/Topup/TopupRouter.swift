//
//  TopupRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.08.21.
//

import UIKit

enum TopupSegue {
    case dismiss
    case cards
}

protocol TopupRoutable: Routable where SegueType == TopupSegue, SourceType == TopupViewController {

}

struct TopupRouter: TopupRoutable {
    
    func perform(_ segue: TopupSegue, from source: TopupViewController) {
        switch segue {
        case .dismiss:
            source.dismiss(animated: true)
        case .cards:
            let vc = PaymentMethodsRouter.createPaymentMethodsViewController(selectionMode: true)
            let nc = NavigationController(rootViewController: vc)
            
            vc.didSelectMethod = { method in
                source.select(method: method)
            }
            
            source.present(nc, animated: true)
        }
    }
}

extension TopupRouter {
    static func createTopupViewController() -> TopupViewController {
        let vc = TopupViewController.storyboardInstance
        vc.router = TopupRouter()
        vc.viewModel = TopupViewModel()
        
        return vc
    }
}
