//
//  TopupViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.08.21.
//

import Foundation

class TopupViewModel {
    
    //MARK: - Properties
    var amount: Double?
    var selectedMethod: PaymentMethod?
    
    private let provider = Provider<SubscriptionEndPoint>()
    
    //MARK: - Properties
    func buyCoins(completion: @escaping (CSError?) -> Void) {
        guard let coins = amount,
              let cardId = selectedMethod?.id else { return }
        
        provider.request(target: .buyCoins(coins, cardId),
                         type: UserCard.self) { _, error in
            Thread.onMainThread {
                completion(error)
            }
        }
    }
}
