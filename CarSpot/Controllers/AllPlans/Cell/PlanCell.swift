//
//  PlanCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 07.08.21.
//

import UIKit

class PlanCell: CollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var creditQuantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var radioImageView: UIImageView!
    
    //MARK: - Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        
        selectedView.layer.cornerRadius = 16
        selectedView.layer.masksToBounds = true
        
        radioImageView.layer.masksToBounds = true
        radioImageView.layer.cornerRadius = radioImageView.bounds.height / 2
    }
    
    //MARK: - Public API
    func configure(with plan: SubscriptionPlan) {
        nameLabel.text = plan.name
        creditQuantityLabel.text = "\(plan.creditQuantity.removeZerosFromEnd()) credits"
        priceLabel.text = "\(plan.price.removeZerosFromEnd())$ / mounth"
        imageView.isHidden = plan.imagePath == nil
        
        plan.selected ? select() : deselect()

        imageView.setImage(path: plan.fullPath, nil) { image in
            self.imageView.isHidden = image == nil
        }
    }
    
    //MARK: - Private API
    private func select() {
        radioImageView.image = #imageLiteral(resourceName: "ic_radio")
        radioImageView.layer.borderWidth = 0
        radioImageView.layer.borderColor = UIColor.clear.cgColor
        
        selectedView.layer.borderWidth = 1
        selectedView.layer.borderColor = UIColor("#F2B687").cgColor
    }
    
    private func deselect() {
        radioImageView.image = nil
        radioImageView.layer.borderWidth = 1
        radioImageView.layer.borderColor = UIColor("#82818B").cgColor
        
        selectedView.layer.borderWidth = 0
        selectedView.layer.borderColor = UIColor.clear.cgColor
    }
}
