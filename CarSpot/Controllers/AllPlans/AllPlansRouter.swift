//
//  AllPlansRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 07.08.21.
//

import UIKit

enum AllPlansSegue {
    case dismiss
    case card
    case paymentMethod
}

protocol AllPlansRoutable: Routable where SegueType == AllPlansSegue, SourceType == AllPlansViewController {

}

struct AllPlansRouter: AllPlansRoutable {
    
    func perform(_ segue: AllPlansSegue, from source: AllPlansViewController) {
        switch segue {
        case .dismiss:
            source.navigationController?.dismiss(animated: true)
        case .card:
            let vc = AddPaymentMethodRouter.createAddPaymentMethodViewController()
            
            vc.didAddCard = {
                source.subscribe()
            }
            
            source.navigationController?.pushViewController(vc, animated: true)
        case .paymentMethod:
            let vc = PaymentMethodRouter.createPaymentMethodViewController()
            
            vc.didSelectCreditCard = {
                source.subscribe()
            }
            
            vc.didSelectApplePay = {
                source.applePay()
            }
            
            source.presentPanModal(vc)
        }
    }
}

extension AllPlansRouter {
    static func createAllPlansViewController(with subscriptions: [String]?) -> AllPlansViewController {
        let vc = AllPlansViewController.storyboardInstance
        vc.router = AllPlansRouter()
        vc.viewModel = AllPlansViewModel(subscriptions: subscriptions)
        
        return vc
    }
}
