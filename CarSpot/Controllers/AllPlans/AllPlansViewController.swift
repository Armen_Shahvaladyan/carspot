//
//  AllPlansViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 07.08.21.
//

import UIKit
import PassKit

class AllPlansViewController: BaseViewController {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var promoCodeView: EditingViewWithIcon!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var stackViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Properties
    var router: AllPlansRouter!
    var viewModel: AllPlansViewModel!
    
    var authorizeValue: String?
    var authorizeDescriptor: String?
    
    private let supportedNetworks: [PKPaymentNetwork] = [.amex, .masterCard, .visa, .JCB, .discover, .electron, .vPay]
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        fetchPlans()
        hideBackItemTitle()
    }
    
    //MARK: - Private API
    func subscribe() {
        showLoading(onView: self.view)
        viewModel.subscribe { [weak self] (error, success) in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                if let success = success,
                   success {
                    self.router.perform(.dismiss, from: self)
                } else {
                    self.router.perform(.card, from: self)
                }
            }
        }
    }
    
    func applePay() {
        if let plan = viewModel.selectedSubscriptionPlan  {
            let request = PKPaymentRequest()
            request.currencyCode = "USD"
            request.countryCode = "US"
            request.merchantIdentifier = "merchant.app.carspot.app"
            request.supportedNetworks = supportedNetworks
            
            request.merchantCapabilities = PKMerchantCapability.capability3DS
            request.paymentSummaryItems.append(PKPaymentSummaryItem(label: plan.name, amount: NSDecimalNumber(value: plan.price)))

            let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
            applePayController?.delegate = self
            
            self.present(applePayController!, animated: true, completion: nil)
        } else {
            showAlert(with: "Attention!", message: "Please select a plan.")
        }
    }

    //MARK: - Private API
    private func setup() {
        title = "Subscription"
        
        promoCodeView.keyboardType = .numberPad
        promoCodeView.image = nil
        promoCodeView.delegate = self
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset.left = 11
        collectionView.contentInset.right = 11
        if let subscriptions = viewModel.subscriptions {
            configure(with: subscriptions)
        } else {
            fetchGlobal()
        }
    }
    
    private func fetchGlobal() {
        showLoading(onView: self.view)
        viewModel.fetchGlobal { [weak self] error, global in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else if let global = global {
                self.viewModel.subscriptions = global.aboutSubscription
                self.configure(with: global.aboutSubscription)
            }
        }
    }
    
    private func fetchPlans() {
        showLoading(onView: self.view)
        viewModel.fetchPlans { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else  {
                self.collectionView.reloadData()
            }
        }
    }
    
    private func configure(with subscriptions: [String]) {
        if !subscriptions.isEmpty {
            stackView.removeConstraint(stackViewHeightConstraint)
        }
        
        for subscription in subscriptions {
            let v = PlanView.fromNib()
            v.configure(with: subscription)
            stackView.addArrangedSubview(v)
        }
    }
    
    private func canMakeApplePay() -> Bool {
        return PKPaymentAuthorizationViewController.canMakePayments() &&
            PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: supportedNetworks)
    }
    
    @objc private func check(promoCode: String) {
        viewModel.check(promoCode: promoCode) { [weak self] error in
            guard let self = self else { return }
            if let _ = error {
                self.promoCodeView.image = #imageLiteral(resourceName: "ic_close")
                self.promoCodeView.iconImageView.tintColor = .black
            } else {
                self.promoCodeView.image = self.viewModel.isActivatePromoCode ? #imageLiteral(resourceName: "ic_success") : #imageLiteral(resourceName: "ic_close")
                self.promoCodeView.iconImageView.tintColor = self.viewModel.isActivatePromoCode ? .appGreen : .black
            }
            self.collectionView.reloadData()
        }
    }
    
    //MARK: - IBActions
    @IBAction func onSubscribe() {
        if viewModel.selectedSubscriptionPlan == nil {
            showAlert(with: CSError(key: "Warning!", message: "Please select plan"))
        } else {
            if canMakeApplePay() {
                router.perform(.paymentMethod, from: self)
            } else {
                subscribe()
            }
        }
    }
}

extension AllPlansViewController: PKPaymentAuthorizationViewControllerDelegate {
    func performApplePayCompletion(_ controller: PKPaymentAuthorizationViewController, alert: UIAlertController) {
        controller.dismiss(animated: true, completion: {() -> Void in
            self.present(alert, animated: false, completion: nil)
        })
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: nil)
        print("paymentAuthorizationViewControllerDidFinish called")
    }
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: (@escaping (PKPaymentAuthorizationStatus) -> Void)) {

        if payment.token.paymentData.count > 0 {
            self.authorizeValue = self.convertToString(payment.token.paymentData)
            self.authorizeDescriptor = "COMMON.APPLE.INAPP.PAYMENT"
            //Request with authorizeValue and authorizeDescriptor to backend
            controller.dismiss(animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Authorization Failed!", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            return self.performApplePayCompletion(controller, alert: alert)
        }
    }
    
    func convertToString(_ theData: Data) -> String {
        let utf8 = String(data: theData, encoding: .utf8) ?? ""
        let b64 = utf8.toBase64()
        return b64
    }
}

extension AllPlansViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.isActivatePromoCode ? viewModel.subscriptionPlansOfPromoCode.count : viewModel.subscriptionPlans.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = PlanCell.cell(collection: collectionView, indexPath: indexPath)
        let subscriptionPlan = viewModel.isActivatePromoCode ? viewModel.subscriptionPlansOfPromoCode[indexPath.item] : viewModel.subscriptionPlans[indexPath.item]
        cell.configure(with: subscriptionPlan)
        return cell
    }
}

extension AllPlansViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = 210
        let height = collectionView.bounds.size.height - 10
        return CGSize(width: width, height: height)
    }
}

extension AllPlansViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.selectedSubscriptionPlan?.selected = false
        viewModel.selectedSubscriptionPlan = viewModel.isActivatePromoCode ? viewModel.subscriptionPlansOfPromoCode[indexPath.item] : viewModel.subscriptionPlans[indexPath.item]
        viewModel.selectedSubscriptionPlan?.selected = true
        collectionView.reloadData()
    }
}

extension AllPlansViewController: EditingViewWithIconDelegate {
    func editingView(_ view: EditingViewWithIcon, didChange text: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(check(promoCode:)), object: text)
        perform(#selector(check(promoCode:)), with: text, afterDelay: 0.75)
    }
}

extension AllPlansViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .transactions
}
