//
//  AllPlansViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 07.08.21.
//

import Foundation

class AllPlansViewModel {
    
    //MARK: - Properties
    var subscriptions: [String]?
    var selectedSubscriptionPlan: SubscriptionPlan?
    var subscriptionPlans: [SubscriptionPlan] = []
    var subscriptionPlansOfPromoCode: [SubscriptionPlan] = []
    private let globalProvider = Provider<GlobalEndPoint>()
    private let subscriptionProvider = Provider<SubscriptionEndPoint>()
    
    var isActivatePromoCode: Bool {
        !subscriptionPlansOfPromoCode.isEmpty
    }
    
    //MARK: - Lifecycle
    init(subscriptions: [String]?) {
        self.subscriptions = subscriptions
    }
    
    //MARK: - Public API
    func fetchGlobal(completion: @escaping (CSError?, AppGlobal?) -> Void) {
        globalProvider.request(target: .all,
                               type: AppGlobal.self) { global, error in
            completion(error, global)
        }
    }
    
    func fetchPlans(completion: @escaping (CSError?) -> Void) {
        subscriptionProvider.request(target: .plans,
                                     type: [SubscriptionPlan].self) { plans, error in
            if let error = error {
                completion(error)
            } else if let plans = plans {
                self.subscriptionPlans = plans
                plans.forEach { plan in
                    if plan.selected {
                        self.selectedSubscriptionPlan = plan
                    }
                }
                completion(nil)
            } else {
                completion(nil)
            }
        }
    }
    
    func subscribe(completion: @escaping (CSError?, Bool?) -> Void) {
        guard let subscriptionPlan = selectedSubscriptionPlan else { return }
        subscriptionProvider.request(target: .subscribe(subscriptionPlan.id, nil, nil, nil),
                                     type: [String: Bool].self) { response, error in
            if let error = error {
                completion(error, nil)
            } else {
                if let userCard = response?["userCard"] {
                    completion(nil, userCard)
                } else {
                    completion(nil, nil)
                }
            }
        }
    }
    
    func check(promoCode: String, completion: @escaping (CSError?) -> Void) {
        subscriptionProvider.request(target: .checkPromoCode(promoCode),
                                     type: [SubscriptionPlan].self) { plans, error in
            if let error = error {
                completion(error)
            } else {
                self.subscriptionPlansOfPromoCode = plans ?? []
                if self.subscriptionPlansOfPromoCode.isEmpty {
                    self.subscriptionPlans.forEach { plan in
                        if plan.selected {
                            self.selectedSubscriptionPlan = plan
                        }
                    }
                } else {
                    self.subscriptionPlansOfPromoCode.forEach { plan in
                        if plan.selected {
                            self.selectedSubscriptionPlan = plan
                        }
                    }
                }
                completion(nil)
            }
        }
    }
}
