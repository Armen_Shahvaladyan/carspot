//
//  PaymentMethodViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 10.08.21.
//

import UIKit

class PaymentMethodViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK: - Properties
    var router: PaymentMethodRouter!
    var viewModel: PaymentMethodViewModel!
    
    var didSelectApplePay: (() -> Void)?
    var didSelectCreditCard: (() -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK: - IBActions
    @IBAction func applePay() {
        router.perform(.dismiss, from: self)
    }
    
    @IBAction func creditCard() {
        didSelectCreditCard?()
        router.perform(.dismiss, from: self)
    }
}

extension PaymentMethodViewController: PanModalPresentable {
    var cornerRadius: CGFloat {
        return 32
    }

    var panScrollable: UIScrollView? {
        return scrollView
    }

    var longFormHeight: PanModalHeight {
        return .contentHeight(scrollView.contentSize.height + 56)
    }

    var anchorModalToLongForm: Bool {
        return false
    }

    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .longForm = state
        else { return }

        panModalSetNeedsLayoutUpdate()
    }
}

extension PaymentMethodViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .transactions
}
