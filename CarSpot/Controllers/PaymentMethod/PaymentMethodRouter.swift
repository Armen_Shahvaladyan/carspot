//
//  PaymentMethodRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 10.08.21.
//

import UIKit

enum PaymentMethodSegue {
    case dismiss
}

protocol PaymentMethodRoutable: Routable where SegueType == PaymentMethodSegue, SourceType == PaymentMethodViewController {

}

struct PaymentMethodRouter: PaymentMethodRoutable {
    
    func perform(_ segue: PaymentMethodSegue, from source: PaymentMethodViewController) {
        switch segue {
        case .dismiss:
            source.dismiss(animated: true) {
                source.didSelectApplePay?()
            }
        }
    }
}

extension PaymentMethodRouter {
    static func createPaymentMethodViewController() -> PaymentMethodViewController {
        let vc = PaymentMethodViewController.storyboardInstance
        vc.router = PaymentMethodRouter()
        vc.viewModel = PaymentMethodViewModel()
        
        return vc
    }
}
