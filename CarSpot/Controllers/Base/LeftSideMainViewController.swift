//
//  LeftSideMainViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 21.07.21.
//

import UIKit

class LeftSideMainViewController: BaseViewController, SideMenuItemContent {

    override func viewDidLoad() {
        super.viewDidLoad()

        addMenuItem()
        
        didOpenMenu = {
            self.showSideMenu()
        }
    }
}
