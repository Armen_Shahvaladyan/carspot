//
//  WebComposeView.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 27.07.21.
//

import SafariServices

class WebComposeView {
    static func open(at view: UIViewController, with path: String) {
        guard let url = URL(string: path) else { return }
        
        if UIApplication.shared.canOpenURL(url) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let webViewController = WebViewRouter.createWebViewController(url: url, with: config)
            view.present(webViewController, animated: true)
        }
    }
}
