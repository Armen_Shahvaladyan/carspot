//
//  MainTabBarViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

class MainTabBarViewController: BaseViewController, SideMenuItemContent {

    override func viewDidLoad() {
        super.viewDidLoad()

        addMenuItem()
        
        didOpenMenu = {
            (self.tabBarController as? TabBarController)?.showSideMenu()
        }
    }
}
