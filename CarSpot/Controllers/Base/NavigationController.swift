//
//  NavigationController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 22.05.21.
//

import UIKit

class NavigationController: UINavigationController {
    // MARK: - LyfeCicle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributes: [NSAttributedString.Key : Any] = [.font: UIFont.SF(.text, weight: .semibold, size: 17),
                                                          .foregroundColor: UIColor.black]
        
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = .white
        appearance.titleTextAttributes = attributes
        appearance.largeTitleTextAttributes = [.font: UIFont.SF(.text, weight: .bold, size: 34),
                                               .foregroundColor: UIColor.black]
        
        UINavigationBar.appearance().tintColor = .black
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if viewControllers.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
        }
        super.pushViewController(viewController, animated: animated)
    }
    
    override func popToRootViewController(animated: Bool) -> [UIViewController]? {
        super.popToRootViewController(animated: animated)
    }
}
