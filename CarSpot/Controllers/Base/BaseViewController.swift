//
//  BaseViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 21.05.21.
//

import UIKit

class BaseViewController: UIViewController {

    //MARK: - Properties
    var didOpenMenu: (() -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        hideBackItemTitle()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    //MARK: - Public API
    func addMenuItem() {
        let menuItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu.pdf"), style: .plain, target: self, action: #selector(openMenu))
        self.navigationItem.leftBarButtonItem = menuItem
    }
    
    func hideBackItemTitle() {
        let backButton = UIBarButtonItem()
        backButton.title = ""
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    @objc func openMenu() {
        didOpenMenu?()
    }
}
