//
//  AddAddressError.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 25.07.21.
//

import Foundation

enum AddAddressError: Error {
    case address
    case name
    case type
    case unknown(String)
}

extension AddAddressError: Errorable {
    var message: String {
        switch self {
        case .address:
            return "Please fill a valid address."
        case .name:
            return "Please fill a valid name."
        case .type:
            return "Please select any type."
        case .unknown(let message):
            return message
        }
    }
}
