//
//  AddAddressViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 14.06.21.
//

import UIKit

class AddAddressViewController: BaseViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var nameView: EditingView!
    @IBOutlet weak var addressView: TitleView!
    @IBOutlet weak var commentView: EditingView!
    @IBOutlet weak var mapView: MapView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    var router: AddAddressRouter!
    var viewModel: AddAddressViewModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    //MARK: - Private API
    private func setup() {
        title = "New address"
        
        hideBackItemTitle()
        collectionView.delegate = self
        collectionView.dataSource = self
        
        addressView.text = viewModel.address
        
        mapView.settings.myLocationButton = false
        mapView.layer.cornerRadius = 32
        mapView.layer.masksToBounds = true
        mapView.layer.maskedCorners = CACornerMask.bottomLeftBottomRight
        mapView.isUserInteractionEnabled = false
        nameView.delegate = self
        commentView.delegate = self
        
        mapView.changePosition(with: viewModel.lat, lng: viewModel.lng)
        mapView.addMark(with: viewModel.lat, lng: viewModel.lng)
    }
    
    //MARK: - IBActions
    @IBAction func save() {
        if let error = viewModel.checkValidation() {
            self.showAlert(with: error)
        } else {
            showLoading(onView: self.view)
            viewModel.addAddress { [weak self] error in
                guard let self = self else { return }
                self.hideLoading()
                if let error = error {
                    self.showAlert(with: error)
                } else {
                    self.router.perform(.root, from: self)
                }
            }
        }
    }
}

extension AddAddressViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.addressTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = AddressTypeCell.cell(collection: collectionView, indexPath: indexPath)
        
        let type = viewModel.addressTypes[indexPath.item]
        let selected = viewModel.selectedType == type
        cell.configure(with: viewModel.addressTypes[indexPath.item], selected: selected)
        return cell
    }
}

extension AddAddressViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.selectType(at: indexPath.item)
        collectionView.reloadData()
    }
}

extension AddAddressViewController: EditingViewDelegate {
    func editingView(_ view: EditingView, didChange text: String) {
        if view == nameView {
            viewModel.name = text
        } else if view == commentView {
            viewModel.comment = text
        }
    }
}

extension AddAddressViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .addresses
}
