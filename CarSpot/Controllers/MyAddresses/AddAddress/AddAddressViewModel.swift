//
//  AddAddressViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 14.06.21.
//

import Foundation

class AddAddressViewModel {
    
    //MARK: - Properties
    let lat: Double
    let lng: Double
    let address: String
    let addressTypes: [AddressType] = [.home, .work, .other]
    
    var name: String = ""
    var comment: String = ""

    private(set) var selectedType: AddressType?
    private let provider = Provider<AddressEndPoint>()
    
    //MARK: - Lifecycle
    init(lat: Double, lng: Double, address: String) {
        self.lat = lat
        self.lng = lng
        self.address = address
    }
    
    //MARK: - Public API
    func selectType(at index: Int) {
        selectedType = addressTypes[index]
    }
    
    func checkValidation() -> AddAddressError? {
        if address.isEmpty {
            return .address
        } else if name.isEmpty || name.removeWhitespacesAndNewlines().isEmpty {
            return .name
        } else if selectedType == nil {
            return .type
        }
        
        return nil
    }
    
    func addAddress(completion: @escaping (CSError?) -> Void) {
        provider.request(target: .add(lat, lng, address, name, comment, selectedType?.rawValue ?? 1),
                         type: Bool.self) { _, error in
            Thread.onMainThread {
                if let error = error {
                    completion(error)
                } else {
                    completion(nil)
                }
            }
        }
    }
}
