//
//  AddAddressRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 14.06.21.
//

import UIKit

enum AddAddressSegue {
    case root
}

protocol AddAddressRoutable: Routable where SegueType == AddAddressSegue, SourceType == AddAddressViewController {
    
}

struct AddAddressRouter: AddAddressRoutable {
    func perform(_ segue: AddAddressSegue, from source: AddAddressViewController) {
        switch segue {
        case .root:
            (source.navigationController?.viewControllers.first as? MyAddressesViewController)?.fetchAddresses()
            source.navigationController?.popToRootViewController(animated: true)
        }
    }
}

extension AddAddressRouter {
    static func createAddAddressViewController(with lat: Double, lng: Double, address: String) -> AddAddressViewController {
        let vc = AddAddressViewController.storyboardInstance
        vc.router = AddAddressRouter()
        vc.viewModel = AddAddressViewModel(lat: lat, lng: lng, address: address)
        
        return vc
    }
}
