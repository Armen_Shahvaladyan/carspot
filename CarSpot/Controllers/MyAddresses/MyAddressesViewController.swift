//
//  MyAddressesViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

class MyAddressesViewController: LeftSideMainViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var emptyView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var router: MyAddressesRouter!
    var viewModel: MyAddressesViewModel!
    
    private var addAddress: UIBarButtonItem?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        fetchAddresses()
    }
    
    //MARK: - Public API
    func fetchAddresses() {
        showLoading(onView: self.view)
        viewModel.getAddresses { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.emptyView.isHidden = !self.viewModel.addresses.isEmpty
                self.tableView.reloadData()
                if !self.viewModel.addresses.isEmpty {
                    self.configureNavigationItem()
                }
            }
        }
    }
    
    //MARK: - Private API
    private func setup() {
        title = "My addresses"
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func configureNavigationItem() {
        if addAddress == nil {
            addAddress = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_plus.pdf"), style: .plain, target: self, action: #selector(add))
            addAddress!.tintColor = .appOrange
            self.navigationItem.rightBarButtonItem = addAddress
        }
    }
    
    @objc private func add() {
        router.perform(.addressChooser, from: self)
    }
    
    private func deleteRow(at indexPath: IndexPath) {
        showLoading(onView: self.view)
        viewModel.delete(at: indexPath.row) { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func addAdress() {
        add()
    }
}

extension MyAddressesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.addresses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MyAddressesCell.cell(table: tableView, indexPath: indexPath)
        cell.configure(with: viewModel.addresses[indexPath.row])
        return cell
    }
}

extension MyAddressesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title: "Delete") { (action, view, completion) in
            self.deleteRow(at: indexPath)
            completion(true)
        }
        
        deleteAction.image = #imageLiteral(resourceName: "ic_close").withTintColor(.white)
        deleteAction.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
}

extension MyAddressesViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .addresses
}
