//
//  AddressChooserMapViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 24.07.21.
//

import Foundation

class AddressChooserMapViewModel {
    
    //MARK: - Properties
    var lat: Double = 0
    var long: Double = 0
    let onlyAddress: Bool
    
    //MARK: - Properties
    init(onlyAddress: Bool) {
        self.onlyAddress = onlyAddress
    }
}
