//
//  AddressChooserMapRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 24.07.21.
//

import UIKit

enum AddressChooserMapSegue {
    case close
    case addAddress(Double, Double, String)
}

protocol AddressChooserMapRoutable: Routable where SegueType == AddressChooserMapSegue, SourceType == AddressChooserMapViewController {

}

struct AddressChooserMapRouter: AddressChooserMapRoutable {
    
    func perform(_ segue: AddressChooserMapSegue, from source: AddressChooserMapViewController) {
        switch segue {
        case .close:
            source.navigationController?.popViewController(animated: true)
        case let .addAddress(lat, lng, address):
            let vc = AddAddressRouter.createAddAddressViewController(with: lat,
                                                                     lng: lng,
                                                                     address: address)
            source.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension AddressChooserMapRouter {
    static func createAddressChooserMapViewController(onlyAddress: Bool = false) -> AddressChooserMapViewController {
        let vc = AddressChooserMapViewController.storyboardInstance
        vc.router = AddressChooserMapRouter()
        vc.viewModel = AddressChooserMapViewModel(onlyAddress: onlyAddress)
        
        return vc
    }
}
