//
//  AddressChooserMapViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 24.07.21.
//

import UIKit
import GoogleMaps

class AddressChooserMapViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var mapView: MapView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    //MARK: - Properties
    var router: AddressChooserMapRouter!
    var viewModel: AddressChooserMapViewModel!
    var didSelectAddress: ((String?, Double?, Double?) -> Void)?
    
    let locationManager = CLLocationManager()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    private func setup() {
        title = "Choose address"
        mapView.padding.bottom = 70
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    //MARK: - IBActions
    @IBAction func apply() {
        let address = addressLabel.text ?? ""
        didSelectAddress?(address, viewModel.lat, viewModel.long)
        let segue: AddressChooserMapSegue = viewModel.onlyAddress ? .close : .addAddress(viewModel.lat, viewModel.long, address)
        router.perform(segue, from: self)
    }
}

extension AddressChooserMapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        let location = position.target
        viewModel.lat = location.latitude
        viewModel.long = location.longitude
        GooglePlacesRequestHelpers.getPlaceAddress(location: location) { address in
            self.addressLabel.text = address
        }
    }
}

extension AddressChooserMapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        locationManager.stopUpdatingLocation()
        viewModel.lat = location.coordinate.latitude
        viewModel.long = location.coordinate.longitude
        
        mapView.camera = GMSCameraPosition(target: location.coordinate,
                                           zoom: 17,
                                           bearing: 0,
                                           viewingAngle: 0)
        
        manager.stopUpdatingLocation()
    }
}

extension AddressChooserMapViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .addresses
}
