//
//  MyAddressesRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

enum MyAddressesSegue {
    case map
    case addressChooser
}

protocol MyAddressesRoutable: Routable where SegueType == MyAddressesSegue, SourceType == MyAddressesViewController {

}

struct MyAddressesRouter: MyAddressesRoutable {
    
    var handleAddAddress: ((String) -> Void)?
    
    func perform(_ segue: MyAddressesSegue, from source: MyAddressesViewController) {
        switch segue {
        case .addressChooser:
            let vc = AddressChooserRouter.createAddressChooserViewController()
            
            vc.didSelectMap = {
                self.perform(.map, from: source)
            }
            
            vc.didSelectPlace = { (place, details) in
                let vc = AddAddressRouter.createAddAddressViewController(with: details.coordinate?.latitude ?? 0,
                                                                         lng: details.coordinate?.longitude ?? 0,
                                                                         address: place.mainAddress)
                source.navigationController?.pushViewController(vc, animated: true)
            }
            
            source.present(vc, animated: true)
        case .map:
            let vc = AddressChooserMapRouter.createAddressChooserMapViewController()
            source.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension MyAddressesRouter {
    static func createMyAddressesViewController() -> MyAddressesViewController {
        let vc = MyAddressesViewController.storyboardInstance
        vc.router = MyAddressesRouter()
        vc.viewModel = MyAddressesViewModel()
        
        return vc
    }
}
