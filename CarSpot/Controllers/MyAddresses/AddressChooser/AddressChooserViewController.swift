//
//  AddressChooserViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 14.06.21.
//

import UIKit
import GooglePlaces

class AddressChooserViewController: BaseViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var searchBarContainerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var router: AddressChooserRouter!
    var viewModel: AddressChooserViewModel!
    
    var didSelectMap: (() -> Void)?
    var didSelectPlace: ((Place, PlaceDetails) -> Void)?
    
    private var searchBar: SearchBar!
    private let rasterSize: CGFloat = 10.0
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        configureSearch()
    }
    
    //MARK: - Private API
    private func setup() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func configureSearch() {
        searchBar = searchBar(with: rasterSize, delegate: self)
        searchBarContainerView.addSubview(searchBar)
                
        let constraints = [
            searchBar.centerYAnchor.constraint(equalTo: searchBarContainerView.centerYAnchor),
            searchBar.leadingAnchor.constraint(equalTo: searchBarContainerView.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: searchBarContainerView.trailingAnchor),
            searchBar.bottomAnchor.constraint(equalTo: searchBarContainerView.bottomAnchor)
        ]
        searchBar.becomeFirstResponder()
        NSLayoutConstraint.activate(constraints)
    }
    
    private func searchBar(with rasterSize: CGFloat, delegate: SearchBarDelegate) -> SearchBar {
        let config = searchBarConfig(with: rasterSize)
        let bar = SearchBar(config: config)
        bar.delegate = delegate
        bar.placeholder = "Search"
        bar.updateBackgroundImage(withRadius: 16,
                                  corners: [.allCorners],
                                  color: UIColor.rgb(red: 244, green: 244, blue: 245, alpha: 1))
        return bar
    }
    
    private func searchBarConfig(with rasterSize: CGFloat) -> SearchBarConfig {
        var config: SearchBarConfig = SearchBarConfig()
        config.rasterSize = rasterSize
        config.leftViewMode = .always
        config.useCancelButton = false
        config.cancelButtonTitle = "Cancel"
        config.cancelButtonTextAttributes = [.foregroundColor: UIColor.darkGray]
        config.textContentType = UITextContentType.fullStreetAddress.rawValue
        config.textAttributes = [.foregroundColor: UIColor("#2E2E33"),
                                 .font: UIFont.SF(.text, weight: .regular, size: 17)]
        return config
    }
    
    @IBAction func openMap() {
        router.perform(.dismiss, from: self)
        didSelectMap?()
    }
}

extension AddressChooserViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = AddressChooserCell.cell(table: tableView, indexPath: indexPath)
        cell.configure(with: viewModel.places[indexPath.row])
        return cell
    }
}

extension AddressChooserViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchBar.isFirstResponder {
            searchBar.resignFirstResponder()
        }
        showLoading(onView: self.view)
        let place = viewModel.places[indexPath.row]
        viewModel.getPlaceDetails(with: place.id) { [weak self] details in
            guard let self = self,
                  let details = details,
                  let _ = details.coordinate else { return }
            self.hideLoading()
            self.router.perform(.dismiss, from: self)
            self.didSelectPlace?(place, details)
        }
    }
}

extension AddressChooserViewController: SearchBarDelegate {
    func searchBar(_ searchBar: SearchBar, textDidChange text: String) {
        if text.isEmpty {
            viewModel.clear()
            tableView.reloadData()
        } else {
            viewModel.placeAutocomplete(with: text) { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
}

extension AddressChooserViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .addresses
}
