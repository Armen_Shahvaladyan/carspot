//
//  AddressChooserRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 14.06.21.
//

import UIKit

enum AddressChooserSegue {
    case dismiss
}

protocol AddressChooserRoutable: Routable where SegueType == AddressChooserSegue, SourceType == AddressChooserViewController {

}

struct AddressChooserRouter: AddressChooserRoutable {
    
    func perform(_ segue: AddressChooserSegue, from source: AddressChooserViewController) {
        switch segue {
        case .dismiss:
            source.dismiss(animated: true)
        }
    }
}

extension AddressChooserRouter {
    static func createAddressChooserViewController() -> AddressChooserViewController {
        let vc = AddressChooserViewController.storyboardInstance
        vc.router = AddressChooserRouter()
        vc.viewModel = AddressChooserViewModel()
        
        return vc
    }
}
