//
//  File.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 14.06.21.
//

import Foundation
import GooglePlaces

class AddressChooserViewModel {
    
    //MARK: - Properties
    private var radius: Double = 0.0
    private var placeType: PlaceType = .all
    private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    private var strictBounds: Bool = false
    private(set) var places: [Place] = []
    
    //MARK: - Public API
    func clear() {
        places = []
    }
    
    func placeAutocomplete(with address: String, completion: @escaping () -> Void) {
        let parameters = parameters(with: address)
        GooglePlacesRequestHelpers.getPlaces(with: parameters) {
            self.places = $0
            completion()
        }
    }
    
    func getPlaceDetails(with id: String, completion: @escaping (PlaceDetails?) -> Void) {
        GooglePlacesRequestHelpers.getPlaceDetails(id: id, apiKey: Constant.googlePlaceAPIKey) { placeDetails in
            completion(placeDetails)
        }
    }
    
    //MARK: - Private API
    private func parameters(with address: String) -> [String: String] {
        var params = [
            "input": address,
            "types": placeType.rawValue,
            "key": Constant.googlePlaceAPIKey
        ]
        
        if CLLocationCoordinate2DIsValid(coordinate) {
            params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
            
            if radius > 0 {
                params["radius"] = "\(radius)"
            }
            
            if strictBounds {
                params["strictbounds"] = "true"
            }
        }
        
        return params
    }
}
