//
//  AddressChooserCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 14.06.21.
//

import UIKit

class AddressChooserCell: TableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    //MARK: - Public API
    func configure(with place: Place) {
        iconImageView.image = #imageLiteral(resourceName: "ic_location")
        nameLabel.text = place.mainAddress
    }
}
