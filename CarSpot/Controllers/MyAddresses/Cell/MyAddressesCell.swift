//
//  MyAddressesCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 13.06.21.
//

import UIKit

class MyAddressesCell: TableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    //MARK: - Public API
    func configure(with address: Address) {
        iconImageView.image = address.type.image
        nameLabel.text = address.name
        addressLabel.text = address.address
    }
}
