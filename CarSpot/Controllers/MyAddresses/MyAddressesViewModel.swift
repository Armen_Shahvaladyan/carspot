//
//  MyAddressesViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import Foundation

class MyAddressesViewModel {
    
    //MARK: - Properties
    var addresses: [Address] = []
    private let provider = Provider<AddressEndPoint>()
    
    //MARK: - Public API
    func getAddresses(completion: @escaping (CSError?) -> Void) {
        provider.request(target: .addresses,
                         type: [Address].self) { addresses, error in
            Thread.onMainThread {
                if let error = error {
                    completion(error)
                } else if let addresses = addresses {
                    self.addresses = addresses
                    completion(nil)
                } else {
                    completion(nil)
                }
            }
        }
    }
    
    func delete(at index: Int, completion: @escaping (CSError?) -> Void) {
        provider.request(target: .delete(addresses[index].id),
                         type: Bool.self) { success, error in
            Thread.onMainThread {
                if let error = error {
                    completion(error)
                } else {
                    self.addresses.remove(at: index)
                }
            }
        }
    }
}
