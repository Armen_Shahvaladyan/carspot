//
//  ContactUsViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import Foundation

class ContactUsViewModel {
    
    //MARK: - Properties
    private var appGlobal: AppGlobal?
    private(set) var types: [ContactUsType] = []
    private let globalProvider = Provider<GlobalEndPoint>()
    
    var phoneNumber: String? {
        return appGlobal?.aboutAsPhoneNumber.replacingOccurrences(of: " ", with: "")
    }
    
    //MARK: - Public API
    func fetchGlobal(completion: @escaping (CSError?) -> Void) {
        globalProvider.request(target: .all, type: AppGlobal.self) { (appGlobal, error) in
            Thread.onMainThread {
                self.appGlobal = appGlobal
                if let fb = appGlobal?.aboutAsFacebook,
                   let inst = appGlobal?.aboutAsInstagram,
                   !fb.isEmpty,
                   !inst.isEmpty {
                    self.types.append(.facebook(fb))
                    self.types.append(.instagram(inst))
                } else if let fb = appGlobal?.aboutAsFacebook,
                          !fb.isEmpty {
                    self.types.append(.facebook(fb))
                } else if let inst = appGlobal?.aboutAsInstagram,
                          !inst.isEmpty {
                    self.types.append(.instagram(inst))
                }
                
                completion(error)
            }
        }
    }
}

enum ContactUsType {
    case facebook(String)
    case instagram(String)
    
    var image: UIImage {
        switch self {
        case .facebook:
            return #imageLiteral(resourceName: "ic_facebook")
        case .instagram:
            return #imageLiteral(resourceName: "ic_instagram")
        }
    }
    
    var title: String {
        switch self {
        case .facebook:
            return "Facebook"
        case .instagram:
            return "Instagram"
        }
    }
    
    var subTitle: String {
        switch self {
        case .facebook:
            return "carspot"
        case .instagram:
            return "@carspot"
        }
    }
}
