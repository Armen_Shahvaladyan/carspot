//
//  ContactUsViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

class ContactUsViewController: LeftSideMainViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var router: ContactUsRouter!
    var viewModel: ContactUsViewModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    //MARK: - Private API
    private func setup() {
        title = "Contact us"
        
        tableView.delegate = self
        tableView.dataSource = self
        
        showLoading(onView: self.view)
        viewModel.fetchGlobal { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func onSelectChat(_ sender: UIButton) {
    
    }
    
    @IBAction func callUs(_ sender: UIButton) {
        guard let phoneNumber = viewModel.phoneNumber,
              let number = URL(string: "tel://" + phoneNumber) else { return }
        UIApplication.shared.open(number)
    }
}

extension ContactUsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.types.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ContactUsCell.cell(table: tableView, indexPath: indexPath)
        cell.configure(with: viewModel.types[indexPath.row])
        return cell
    }
}

extension ContactUsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch viewModel.types[indexPath.row] {
        case .facebook(let path):
            WebComposeView.open(at: self, with: path)
        case .instagram(let path):
            WebComposeView.open(at: self, with: path)
        }
    }
}

extension ContactUsViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .contactUs
}
