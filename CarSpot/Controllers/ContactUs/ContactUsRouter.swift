//
//  ContactUsRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

enum ContactUsSegue {
    
}

protocol ContactUsRoutable: Routable where SegueType == ContactUsSegue, SourceType == ContactUsViewController {

}

struct ContactUsRouter: ContactUsRoutable {
    
    func perform(_ segue: ContactUsSegue, from source: ContactUsViewController) {
        
    }
}

extension ContactUsRouter {
    static func createContactUsViewController() -> ContactUsViewController {
        let vc = ContactUsViewController.storyboardInstance
        vc.router = ContactUsRouter()
        vc.viewModel = ContactUsViewModel()
        
        return vc
    }
}
