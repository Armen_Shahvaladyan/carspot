//
//  ContactUsCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 18.06.21.
//

import UIKit

class ContactUsCell: TableViewCell {

    //MARK: - IBOutlet
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    //MARK: - Lifecycle
    func configure(with type: ContactUsType) {
        iconImageView.image = type.image
        titleLabel.text = type.title
        subTitleLabel.text = type.subTitle
    }
}
