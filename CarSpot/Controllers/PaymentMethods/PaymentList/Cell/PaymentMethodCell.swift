//
//  PaymentMethodCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 10.06.21.
//

import UIKit

class PaymentMethodCell: TableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var digitLabel: UILabel!
    
    //MARK: - Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
    
        imageContainerView.layer.cornerRadius = 16
        imageContainerView.layer.masksToBounds = true
    }
    
    //MARK: - Public API
    func configure(with method: PaymentMethod) {
        iconImageView.image = method.cardType.image
        typeLabel.text = method.cardType.rawValue
        digitLabel.text = method.number
    }
}
