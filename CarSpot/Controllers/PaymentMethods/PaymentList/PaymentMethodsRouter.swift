//
//  PaymentMethodsRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

enum PaymentMethodsSegue {
    case add
    case dismiss
    case detail(PaymentMethod, IndexPath)
}

protocol PaymentMethodsRoutable: Routable where SegueType == PaymentMethodsSegue, SourceType == PaymentMethodsViewController {

}

struct PaymentMethodsRouter: PaymentMethodsRoutable {
    
    func perform(_ segue: PaymentMethodsSegue, from source: PaymentMethodsViewController) {
        switch segue {
        case .add:
            let vc = AddPaymentMethodRouter.createAddPaymentMethodViewController()
            
            vc.didAddCard = {
                source.updateCards()
            }
            
            source.navigationController?.pushViewController(vc, animated: true)
        case .dismiss:
            source.dismiss(animated: true)
        case let .detail(paymentMethod, indexPath):
            let vc = PaymentDetailRouter.createPaymentDetailViewController(with: paymentMethod)
            
            vc.didDelete = {
                source.deleteMethod(at: indexPath.row)
            }
            
            vc.didChangeDefault = {
                source.changeDefault(at: indexPath)
            }
            
            source.presentPanModal(vc)
        }
    }
}

extension PaymentMethodsRouter {
    static func createPaymentMethodsViewController(selectionMode: Bool) -> PaymentMethodsViewController {
        let vc = PaymentMethodsViewController.storyboardInstance
        vc.router = PaymentMethodsRouter()
        vc.viewModel = PaymentMethodsViewModel(selectionMode: selectionMode)
        
        return vc
    }
}
