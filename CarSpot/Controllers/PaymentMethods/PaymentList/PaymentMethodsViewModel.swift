//
//  PaymentMethodsViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import Foundation

class PaymentMethodsViewModel {
    
    //MARK: - Properties
    let selectionMode: Bool
    var paymentMethods: [PaymentMethod] = []
    private let provider = Provider<PaymentMethodEndPoint>()
    
    //MARK: - Lifecycle
    init(selectionMode: Bool) {
        self.selectionMode = selectionMode
    }
    
    //MARK: - Public API
    func getCards(completion: @escaping (CSError?) -> Void) {
        provider.request(target: .cards,
                         type: [PaymentMethod].self) { methods, error in
            Thread.onMainThread {
                if let error = error {
                    completion(error)
                } else if let methods = methods {
                    self.paymentMethods = methods.sorted { $0.isDefault && !$1.isDefault }
                    completion(nil)
                } else {
                    completion(nil)
                }
            }
        }
    }
}
