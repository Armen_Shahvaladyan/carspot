//
//  PaymentMethodsViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

class PaymentMethodsViewController: LeftSideMainViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var emptyView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var router: PaymentMethodsRouter!
    var viewModel: PaymentMethodsViewModel!
    
    var didSelectMethod: ((PaymentMethod?) -> Void)?
    
    private var addMethod: UIBarButtonItem?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if viewModel.selectionMode {
            let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancel))
            navigationItem.leftBarButtonItem = cancel
        }
        
        viewModel.getCards { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.emptyView.isHidden = !self.viewModel.paymentMethods.isEmpty
                self.tableView.reloadData()
                if !self.viewModel.paymentMethods.isEmpty {
                    self.configureNavigationItem()
                }
            }
        }
    }
    
    //MARK: - Public API
    func updateCards() {
        emptyView.isHidden = true
        viewModel.getCards { [weak self] error in
            guard let self = self else { return }
            self.tableView.reloadData()
            self.configureNavigationItem()
        }
    }
    
    func changeDefault(at indexPath: IndexPath) {
        let newIndexPath = IndexPath(row: 0, section: 0)
        tableView.beginUpdates()
        tableView.moveRow(at: indexPath, to: newIndexPath)
        tableView.endUpdates()
    }
    
    func deleteMethod(at index: Int) {
        viewModel.paymentMethods.remove(at: index)
        
        if viewModel.paymentMethods.isEmpty {
            emptyView.isHidden = false
            addMethod = nil
            navigationItem.rightBarButtonItem = nil
        } else {
            if addMethod == nil {
                configureNavigationItem()
            }
        }
        tableView.reloadData()
    }
    
    //MARK: - Private API
    private func setup() {
        title = "Payment methods"
        
        tableView.delegate = self
        tableView.dataSource = self
        emptyView.isHidden = true
        
        showLoading(onView: self.view)
        viewModel.getCards { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.emptyView.isHidden = !self.viewModel.paymentMethods.isEmpty
                self.tableView.reloadData()
                if !self.viewModel.paymentMethods.isEmpty {
                    self.configureNavigationItem()
                }
            }
        }
    }
    
    private func configureNavigationItem() {
        if !viewModel.selectionMode {
            addMethod = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_plus.pdf"), style: .plain, target: self, action: #selector(add))
            addMethod!.tintColor = .appOrange
            navigationItem.rightBarButtonItem = addMethod!
        }
    }
    
    @objc private func add() {
        router.perform(.add, from: self)
    }
    
    @objc private func cancel() {
        router.perform(.dismiss, from: self)
    }
    
    //MARK: - IBActions
    @IBAction func addPayment() {
        add()
    }
}

extension PaymentMethodsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.paymentMethods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = PaymentMethodCell.cell(table: tableView, indexPath: indexPath)
        cell.configure(with: viewModel.paymentMethods[indexPath.row])
        return cell
    }
}

extension PaymentMethodsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel.selectionMode {
            didSelectMethod?(viewModel.paymentMethods[indexPath.row])
            router.perform(.dismiss, from: self)
        } else {
            router.perform(.detail(viewModel.paymentMethods[indexPath.row], indexPath), from: self)
        }
    }
}

extension PaymentMethodsViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .payment
}
