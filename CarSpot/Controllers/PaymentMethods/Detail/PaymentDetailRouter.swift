//
//  PaymentDetailRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 13.06.21.
//

import UIKit

enum PaymentDetailSegue {
    case dismiss
}

protocol PaymentDetailRoutable: Routable where SegueType == PaymentDetailSegue, SourceType == PaymentDetailViewController {

}

struct PaymentDetailRouter: PaymentDetailRoutable {
    
    func perform(_ segue: PaymentDetailSegue, from source: PaymentDetailViewController) {
        switch segue {
        case .dismiss:
            source.dismiss(animated: true)
        }
    }
}

extension PaymentDetailRouter {
    static func createPaymentDetailViewController(with paymentMethod: PaymentMethod) -> PaymentDetailViewController {
        let vc = PaymentDetailViewController.storyboardInstance
        vc.router = PaymentDetailRouter()
        vc.viewModel = PaymentDetailViewModel(paymentMethod: paymentMethod)
        
        return vc
    }
}
