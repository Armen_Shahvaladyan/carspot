//
//  PaymentDetailViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 13.06.21.
//

import Foundation

class PaymentDetailViewModel {
    
    //MARK: - Properties
    let paymentMethod: PaymentMethod
    private let provider = Provider<PaymentMethodEndPoint>()
    
    //MARK: - Lifeycle
    init(paymentMethod: PaymentMethod) {
        self.paymentMethod = paymentMethod
    }
    
    //MARK: - Public API
    func delete(completion: @escaping (CSError?) -> Void) {
        provider.request(target: .delete(paymentMethod.id),
                         type: Bool.self) { _, error in
            Thread.onMainThread {
                completion(error)
            }
        }
    }
    
    func changeDefault(completion: @escaping (CSError?) -> Void) {
        provider.request(target: .changeDefault(paymentMethod.id),
                         type: Bool.self) { _, error in
            Thread.onMainThread {
                completion(error)
            }
        }
    }
}
