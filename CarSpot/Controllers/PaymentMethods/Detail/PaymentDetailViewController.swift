//
//  PaymentDetailViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 13.06.21.
//

import UIKit

class PaymentDetailViewController: BaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var cardView: CreditCardView!
    @IBOutlet weak var defaultSwitch: UISwitch!
    @IBOutlet weak var deleteButton: Button!
    @IBOutlet weak var defaultLabel: UILabel!
    
    //MARK: - IBOutlets
    
    //MARK: - Properties
    var router: PaymentDetailRouter!
    var viewModel: PaymentDetailViewModel!
    
    var didDelete: (() -> Void)?
    var didChangeDefault: (() -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    //MARK: - Private API
    private func setup() {
        cardView.updateCardNumber(with: viewModel.paymentMethod.number)
        cardView.updateCardExpDate(with: viewModel.paymentMethod.date)
        cardView.updateCardHolder(with: viewModel.paymentMethod.name)
        cardView.cardBrandImageView.image = viewModel.paymentMethod.cardType.image
        defaultSwitch.setOn(viewModel.paymentMethod.isDefault, animated: false)
        deleteButton.buttonState = .secondary
        
        defaultLabel.isHidden = viewModel.paymentMethod.isDefault
        defaultSwitch.isHidden = viewModel.paymentMethod.isDefault
    }
    
    private func delete() {
        showLoading(onView: self.view)
        viewModel.delete { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.didDelete?()
                self.router.perform(.dismiss, from: self)
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func changeDefault(_ sender: UISwitch) {
        showLoading(onView: self.view)
        viewModel.changeDefault { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.didChangeDefault?()
                self.router.perform(.dismiss, from: self)
            }
        }
    }
    
    @IBAction func deleteCard() {
        if viewModel.paymentMethod.isDefault {
            showAlert(with: "Attention!",
                      message: "The “default card” cannot be deleted. Please make another card default, if you want to delete this card.")
        } else {
            delete()
        }
    }
}

extension PaymentDetailViewController: PanModalPresentable {
    var cornerRadius: CGFloat {
        return 32
    }

    var panScrollable: UIScrollView? {
        return scrollView
    }

    var shortFormHeight: PanModalHeight {
        return longFormHeight
    }

    var anchorModalToLongForm: Bool {
        return false
    }

    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .longForm = state else { return }
        panModalSetNeedsLayoutUpdate()
    }
}

extension PaymentDetailViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .payment
}
