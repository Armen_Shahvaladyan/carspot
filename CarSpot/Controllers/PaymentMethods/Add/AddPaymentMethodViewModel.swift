//
//  AddPaymentMethodViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 11.06.21.
//

import Foundation

class AddPaymentMethodViewModel {
    
    //MARK: - Properties
    var number = ""
    var date = ""
    var code = ""
    var name = ""
  
    private let provider = Provider<PaymentMethodEndPoint>()
    
    //MARK: - Public API
    func add(completion: @escaping (CSError?) -> Void) {
        provider.request(target: .add(number, date, code, name),
                         type: Bool.self) { _, error in
            Thread.onMainThread {
                completion(error)
            }
        }
    }
}
