//
//  AddPaymentMethodRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 11.06.21.
//

import UIKit

enum AddPaymentMethodSegue {
    case scan
    case back
}

protocol AddPaymentMethodRoutable: Routable where SegueType == AddPaymentMethodSegue, SourceType == AddPaymentMethodViewController {
    
}

struct AddPaymentMethodRouter: AddPaymentMethodRoutable {
    
    
    func perform(_ segue: AddPaymentMethodSegue, from source: AddPaymentMethodViewController) {
        switch segue {
        case .back:
            source.navigationController?.popViewController(animated: true)
        case .scan:
            let scannerView = CardScanner.getScanner { number in
                source.updateCard(number: number)
            }
            source.present(scannerView, animated: true)
        }
    }
}

extension AddPaymentMethodRouter {
    static func createAddPaymentMethodViewController() -> AddPaymentMethodViewController {
        let vc = AddPaymentMethodViewController.storyboardInstance
        vc.router = AddPaymentMethodRouter()
        vc.viewModel = AddPaymentMethodViewModel()
        
        return vc
    }
}
