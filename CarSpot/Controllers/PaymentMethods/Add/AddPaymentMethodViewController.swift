//
//  AddPaymentMethodViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 11.06.21.
//

import UIKit

class AddPaymentMethodViewController: BaseViewController, SideMenuItemContent {

    //MARK: - IBOutlets
    @IBOutlet weak var cardNumberInputView: CreditCardEditingView!
    @IBOutlet weak var cardHolderInputView: CreditCardEditingView!
    @IBOutlet weak var expDateInputView: CreditCardEditingView!
    @IBOutlet weak var cscNumberInputView: CreditCardEditingView!
    @IBOutlet weak var cardView: CreditCardView!
    
    //MARK: - Properties
    var router: AddPaymentMethodRouter!
    var viewModel: AddPaymentMethodViewModel!
    var didAddCard: (() -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    //MARK: - Public API
    func updateCard(number: String?) {
        if let number = number {
            viewModel.number = number
            cardNumberInputView.textField.text = number
            cardView.updateCardNumber(with: number)
        }
    }
    
    //MARK: - Private API
    private func setup() {
        title = "New payment method"
        
        cardNumberInputView.type = .cardNumber
        cardHolderInputView.type = .cardHolder
        expDateInputView.type = .expDate
        cscNumberInputView.type = .cvvNumder
        [cardNumberInputView, cardHolderInputView, expDateInputView, cscNumberInputView].forEach { $0?.delegate = self }
        cardView.delegate = self
    }
    
    //MARK: - IBActions
    @IBAction func add() {
        showLoading(onView: self.view)
        viewModel.add { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.didAddCard?()
                self.router.perform(.back, from: self)
            }
        }
    }
}

extension AddPaymentMethodViewController: CreditCardViewDelegate {
    func creditCardViewDidSelectNumber(_ view: CreditCardView) {
        if !cardNumberInputView.isFirstResponder {
            _ = cardNumberInputView.becomeFirstResponder()
        }
    }
    
    func creditCardViewDidSelectHolder(_ view: CreditCardView) {
        if !cardHolderInputView.isFirstResponder {
            _ = cardHolderInputView.becomeFirstResponder()
        }
    }
    
    func creditCardViewDidSelectDate(_ view: CreditCardView) {
        if !expDateInputView.isFirstResponder {
            _ = expDateInputView.becomeFirstResponder()
        }
    }
}

extension AddPaymentMethodViewController: CreditCardEditingViewDelegate {
    func creditCardEditingViewDidOpenScan(_ view: CreditCardEditingView) {
        self.view.endEditing(true)
        router.perform(.scan, from: self)
    }
    
    func creditCardEditingView(_ view: CreditCardEditingView, didChange text: String) {
        switch view.type {
        case .cardNumber:
            viewModel.number = text
            cardView.updateCardNumber(with: text)
        case .cardHolder:
            viewModel.name = text
            cardView.updateCardHolder(with: text)
        case .expDate:
            viewModel.date = text
            cardView.updateCardExpDate(with: text)
        case .cvvNumder:
            viewModel.code = text
            cardView.updateCardCvvNumber(with: text)
        }
    }
}

extension AddPaymentMethodViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .payment
}
