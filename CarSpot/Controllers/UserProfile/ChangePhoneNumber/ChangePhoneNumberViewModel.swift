//
//  ChangePhoneNumberViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 10.06.21.
//

import Foundation

class ChangePhoneNumberViewModel {
    
    //MARK: - Properties
    var selectedCounutry: Country?
    var phoneNumber: String?
    
    let user: User
    
    //MARK: - Properties
    init(user: User) {
        self.user = user
    }
}
