//
//  ChangeChangePhoneNumberRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 10.06.21.
//

import UIKit

enum ChangePhoneNumberSegue {
    case country(Country?)
}

protocol ChangePhoneNumberRoutable: Routable where SegueType == ChangePhoneNumberSegue, SourceType == ChangePhoneNumberViewController {

}

struct ChangePhoneNumberRouter: ChangePhoneNumberRoutable {
    
    var handleCountrySelection: ((Country) -> Void)?
    
    func perform(_ segue: ChangePhoneNumberSegue, from source: ChangePhoneNumberViewController) {
        switch segue {
        case .country(let country):
            let vc = CountryRouter.createCountryViewController(country: country)
            
            vc.didSelectCountry = { country in
                self.handleCountrySelection?(country)
            }
            
            source.presentBottomSheet(vc)
        }
    }
}

extension ChangePhoneNumberRouter {
    static func createChangePhoneNumberViewController(with user: User) -> ChangePhoneNumberViewController {
        let vc = ChangePhoneNumberViewController.storyboardInstance
        vc.router = ChangePhoneNumberRouter()
        vc.viewModel = ChangePhoneNumberViewModel(user: user)
        
        return vc
    }
}
