//
//  ChangePhoneNumberViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 10.06.21.
//

import UIKit

class ChangePhoneNumberViewController: BaseViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var continueButton: Button!
    @IBOutlet weak var countryView: SelectedViewWithIcon!
    @IBOutlet weak var phoneNumberCodeView: TitleView!
    @IBOutlet weak var phoneNumberView: EditingView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    //MARK: - Properties
    var router: ChangePhoneNumberRouter!
    var viewModel: ChangePhoneNumberViewModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        configureRouter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        if let _ = viewModel.selectedCounutry,
           let _ = viewModel.phoneNumber {
            continueButton.buttonState = .default
        } else {
            continueButton.buttonState = .disable
        }
    }
    
    //MARK: - Private API
    private func setup() {
        title = "Change phone number"
        countryView.delegate = self
        phoneNumberView.delegate = self
        phoneNumberView.textField.keyboardType = .numberPad
        phoneNumberLabel.text = viewModel.user.phoneNumber
    }
    
    private func configureRouter() {
        router.handleCountrySelection = { country in
            self.viewModel.selectedCounutry = country
            self.countryView.text = country.name
            self.phoneNumberCodeView.text = country.dialCode
            
            if self.viewModel.phoneNumber != nil &&
                self.continueButton.buttonState != .default {
                self.continueButton.buttonState = .default
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func openTermsAndCondition(_ sender: UITapGestureRecognizer) {
        
    }
    
    @IBAction func next(_ sender: Button) {
        self.view.endEditing(true)
        sender.buttonState = .load
        
//        if let phone = viewModel.phoneNumber,
//           let country = viewModel.selectedCounutry {
//            DispatchQueue.main.async {
//                self.router.perform(.verify(country, phone), from: self)
//            }
//        }
    }
}

extension ChangePhoneNumberViewController: SelectedViewWithIconDelegate {
    func selectedViewWithIcon(_ view: SelectedViewWithIcon, didSelect text: String) {
        router.perform(.country(viewModel.selectedCounutry), from: self)
    }
}

extension ChangePhoneNumberViewController: EditingViewDelegate {
    func editingView(_ view: EditingView, didChange text: String) {
        viewModel.phoneNumber = text
        if viewModel.selectedCounutry != nil {
            continueButton.buttonState = text.isEmpty ? .disable : .default
        }
    }
}

extension ChangePhoneNumberViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .userProfile
}
