//
//  EditUserProfileViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 09.06.21.
//

import UIKit

class EditUserProfileViewController: BaseViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var firstNameView: EditingView!
    @IBOutlet weak var lastNameView: EditingView!
    @IBOutlet weak var emailView: EditingView!
    @IBOutlet weak var carBrandView: SelectedViewWithIcon!
    @IBOutlet weak var carModelView: SelectedViewWithIcon!
    @IBOutlet weak var carColorView: CarColorView!
    @IBOutlet weak var carNumberView: EditingView!
    @IBOutlet weak var imageViewContainerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cameraButton: UIButton!
    
    //MARK: - Properties
    var router: EditUserProfileRouter!
    var viewModel: EditUserProfileViewModel!
    
    var didUserInfoChanged: (() -> Void)?
    
    private var imagePicker: ImagePicker!
    private var saveButtonItem: UIBarButtonItem!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        imageViewContainerView.layer.cornerRadius = 20
        imageViewContainerView.layer.masksToBounds = true
        
        cameraButton.layer.cornerRadius = 8
        cameraButton.layer.masksToBounds = true
        
        carColorView.layoutSubviews()
    }
    
    //MARK: - Public API
    func update(color: CarColor) {
        viewModel.color = color
        carColorView.text = color.name
        carColorView.color = UIColor(color.hexCode)
        saveButtonItem.isEnabled = viewModel.validateAllFields() == nil
    }
    
    func update(model: CarModel) {
        viewModel.brand?.model = model
        carModelView.text = model.name
        saveButtonItem.isEnabled = viewModel.validateAllFields() == nil
    }
    
    func update(brand: CarBrand) {
        self.carModelView.state = .default
        if brand.name != viewModel.brand?.name {
            viewModel.brand = brand
            viewModel.brand?.model = nil
            carBrandView.text = brand.name
            carModelView.text = ""
        }
        saveButtonItem.isEnabled = viewModel.validateAllFields() == nil
    }
    
    //MARK: - Private API
    private func setup() {
        title = "Edit"
        
        imagePicker = ImagePicker(presentationController: self, delegate: self)
        saveButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(save))
        saveButtonItem.tintColor = .appOrange
        self.navigationItem.rightBarButtonItem = saveButtonItem
        
        emailView.textField.keyboardType = .emailAddress
        carColorView.delegate = self
        [carBrandView, carModelView].forEach { $0?.delegate = self }
        [firstNameView, lastNameView, emailView, carNumberView].forEach { $0?.delegate = self }
        saveButtonItem.isEnabled = viewModel.validateAllFields() == nil
        
        firstNameView.text = viewModel.firstName
        lastNameView.text = viewModel.lastName
        emailView.text = viewModel.email
        carBrandView.text = viewModel.brand?.name ?? ""
        carModelView.text = viewModel.brand?.model?.name ?? ""
        carColorView.text = viewModel.color?.name ?? ""
        carColorView.color = viewModel.color?.color ?? .clear
        carNumberView.text = viewModel.number
        
        if let image = viewModel.user.image {
            imageView.image = image
        } else {
            imageView.setImage(path: viewModel.imagePath, #imageLiteral(resourceName: "ic_empty_image")) { image in
                self.viewModel.user.image = image
            }
        }
    }
    
    @objc private func save() {
        showLoading(onView: self.view)
        viewModel.save { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.didUserInfoChanged?()
                self.router.perform(.back, from: self)
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func openImagePicker(_ sender: UIButton) {
        imagePicker.present(from: sender)
    }
}

extension EditUserProfileViewController: CarColorViewDelegate {
    func carColorView(_ view: CarColorView, didSelect text: String) {
        if view == carColorView {
            openColors()
        }
    }
}

extension EditUserProfileViewController: SelectedViewWithIconDelegate {
    func selectedViewWithIcon(_ view: SelectedViewWithIcon, didSelect text: String) {
        switch view {
        case carBrandView:
            openBrands()
        case carModelView:
            if let brand = viewModel.brand {
                router.perform(.models(brand.models, brand.model), from: self)
            }
        default:
            break
        }
    }
}

extension EditUserProfileViewController: EditingViewDelegate {
    func editingView(_ view: EditingView, didChange text: String) {
        switch view {
        case firstNameView:
            viewModel.firstName = text
        case lastNameView:
            viewModel.lastName = text
        case emailView:
            viewModel.email = text
        case carNumberView:
            viewModel.number = text
        default:
            break
        }
        saveButtonItem.isEnabled = viewModel.validateAllFields() == nil
    }
}

extension EditUserProfileViewController {
    private func openBrands() {
        if viewModel.brands.isEmpty {
            showLoading(onView: self.view)
            viewModel.fetchCarBrands { [weak self] error in
                guard let self = self else { return }
                self.hideLoading()
                if let error = error {
                    self.showAlert(with: error)
                } else {
                    self.router.perform(.brands(self.viewModel.brands, self.viewModel.brand), from: self)
                }
            }
        } else {
            router.perform(.brands(viewModel.brands, viewModel.brand), from: self)
        }
    }
    
    private func openColors() {
        if viewModel.colors.isEmpty {
            showLoading(onView: self.view)
            viewModel.fetchCarColors { [weak self] error in
                guard let self = self else { return }
                self.hideLoading()
                if let error = error {
                    self.showAlert(with: error)
                } else {
                    self.router.perform(.colors(self.viewModel.colors, self.viewModel.color), from: self)
                }
            }
        } else {
            router.perform(.colors(viewModel.colors, viewModel.color), from: self)
        }
    }
    
    private func setProfilePhoto(image: UIImage) {
        if let imageData = image.jpegData(compressionQuality: 0.3) {
            imageView.image = image
            showLoading(onView: self.view)
            viewModel.setProfilePhoto(photo: imageData) { [weak self] error in
                guard let self = self else { return }
                self.hideLoading()
                if let error = error {
                    self.imageView.image = nil
                    self.showAlert(with: error)
                } else {
                    self.viewModel.user.image = image
                    self.didUserInfoChanged?()
                    self.router.perform(.back, from: self)
                }
            }
        }
    }
}

extension EditUserProfileViewController: ImagePickerDelegate {
    func imagePicker(_ imagePicker: ImagePicker, didSelect image: UIImage?) {
        guard let image = image else { return }
        setProfilePhoto(image: image)
    }
}

extension EditUserProfileViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .userProfile
}
