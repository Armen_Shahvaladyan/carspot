//
//  EditUserProfileViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 09.06.21.
//

import Foundation

class EditUserProfileViewModel {
    
    //MARK: - Properties
    let user: User
    
    var firstName = ""
    var lastName = ""
    var email = ""
    var brand: CarBrand?
    var color: CarColor?
    var number = ""
    
    var imagePath: String? {
        if let path = user.imagePath {
            return Environment.live.baseURL + path
        }
        return nil
    }
    
    private(set) var brands: [CarBrand] = []
    private(set) var models: [CarModel] = []
    private(set) var colors: [CarColor] = []
    
    private let carProvider = Provider<CarEndPoint>()
    private let profileProvider = Provider<ProfileEndPoint>()
    
    //MARK: - Lifecycle
    init(user: User) {
        self.user = user
        
        firstName = user.name
        lastName = user.lastName
        email = user.email
        brand = user.carBrand
        color = user.carColor
        number = user.carNumber
    }
    
    //MARK: - Public API
    func save(completion: @escaping (CSError?) -> Void) {
        user.name = firstName
        user.lastName = lastName
        user.email = email
        user.carNumber = number
        
        if let brand = brand {
            user.carBrand = brand
        }
        
        if let color = color {
            user.carColor = color
        }
        
        profileProvider.request(target: .edit(user),
                                type: Bool.self) { success, error in
            Thread.onMainThread {
                if let error = error {
                    completion(error)
                } else {
                    completion(nil)
                }
            }
        }
    }
    
    func setProfilePhoto(photo: Data, completion: @escaping (CSError?) -> ()) {
        let file = File(data: photo, name: "File", fileName: "File.jpg", mimeType: .image)
        profileProvider.request(target: .uploadPhoto(file),
                                type: Bool.self) { (_, error) in
            Thread.onMainThread {
                completion(error)
            }
        }
    }
    
    func validateAllFields() -> ProfileError? {
        if firstName.removeWhitespacesAndNewlines().isEmpty {
            return .firstNameEmpty
        } else if lastName.removeWhitespacesAndNewlines().isEmpty {
            return .lastNameEmpty
        } else if !email.isValidEmail() {
            return.email
        } else if brand == nil {
            return .brandEmpty
        } else if brand?.model == nil {
            return .modelsEmpty
        } else if color == nil {
            return .colorsEmpty
        }
        
        return nil
    }
    
    func fetchCarBrands(completion: @escaping (ProfileError?) -> Void) {
        carProvider.request(target: .brands, type: [CarBrand].self) { brands, error in
            Thread.onMainThread {
                if let error = error {
                    completion(.unknown(error.message))
                } else if let brands = brands {
                    self.brands = brands
                    completion(nil)
                } else {
                    completion(.brandEmpty)
                }
            }
        }
    }
    
    func fetchCarColors(completion: @escaping (ProfileError?) -> Void) {
        carProvider.request(target: .color, type: [CarColor].self) { colors, error in
            Thread.onMainThread {
                if let error = error {
                    completion(.unknown(error.message))
                } else if let colors = colors {
                    self.colors = colors
                    completion(nil)
                } else {
                    completion(.colorsEmpty)
                }
            }
        }
    }
}
