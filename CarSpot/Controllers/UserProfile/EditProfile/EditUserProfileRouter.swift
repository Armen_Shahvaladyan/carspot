//
//  EditUserProfileRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 09.06.21.
//

import UIKit

enum EditUserProfileSegue {
    case brands([CarBrand], CarBrand?)
    case models([CarModel], CarModel?)
    case colors([CarColor], CarColor?)
    case back
}

protocol EditUserProfileRoutable: Routable where SegueType == EditUserProfileSegue, SourceType == EditUserProfileViewController {
    
}

struct EditUserProfileRouter: EditUserProfileRoutable {
        
    func perform(_ segue: EditUserProfileSegue, from source: EditUserProfileViewController) {
        switch segue {
        case .back:
            source.navigationController?.popViewController(animated: true)
        case let .brands(brands, selectedBrand):
            let vc = CarBrandRouter.createCarBrandViewController(brands: brands, selectedBrand: selectedBrand)
            
            vc.didSelectCarBrand = { brand in
                source.update(brand: brand)
            }
            
            source.presentBottomSheet(vc)
        case let .models(models, selectedModel):
            let vc = CarModelRouter.createCarModelViewController(models: models, selectedModel: selectedModel)
            
            vc.didSelectCarModel = { model in
                source.update(model: model)
            }
            
            source.presentBottomSheet(vc)
        case let .colors(colors, selectedColor):
            let vc = CarColorRouter.createCarColorViewController(colors: colors, selectedColor: selectedColor)
            
            vc.didSelectCarColor = { color in
                source.update(color: color)
            }
            
            source.presentBottomSheet(vc)
        }
    }
}

extension EditUserProfileRouter {
    static func createEditUserProfileViewController(with user: User) -> EditUserProfileViewController {
        let vc = EditUserProfileViewController.storyboardInstance
        vc.router = EditUserProfileRouter()
        vc.viewModel = EditUserProfileViewModel(user: user)
        
        return vc
    }
}
