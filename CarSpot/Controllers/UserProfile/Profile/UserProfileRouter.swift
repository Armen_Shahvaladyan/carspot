//
//  UserProfileRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

enum UserProfileSegue {
    case change(User)
    case edit(User)
}

protocol UserProfileRoutable: Routable where SegueType == UserProfileSegue, SourceType == UserProfileViewController {

}

struct UserProfileRouter: UserProfileRoutable {
        
    func perform(_ segue: UserProfileSegue, from source: UserProfileViewController) {
        switch segue {
        case .edit(let user):
            let vc = EditUserProfileRouter.createEditUserProfileViewController(with: user)
            
            vc.didUserInfoChanged = {
                source.updateUserInfo()
            }
            
            source.navigationController?.pushViewController(vc, animated: true)
        case .change(let user):
            let vc = ChangePhoneNumberRouter.createChangePhoneNumberViewController(with: user)
            source.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension UserProfileRouter {
    static func createUserProfileViewController() -> UserProfileViewController {
        let vc = UserProfileViewController.storyboardInstance
        vc.router = UserProfileRouter()
        vc.viewModel = UserProfileViewModel()
        
        return vc
    }
}
