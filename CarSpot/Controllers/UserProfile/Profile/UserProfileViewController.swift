//
//  ProfileViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

class UserProfileViewController: LeftSideMainViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    //MARK: - Properties
    var router: UserProfileRouter!
    var viewModel: UserProfileViewModel!
    
    private let attributes: [NSAttributedString.Key: Any] = [.font: UIFont.SF(.display, weight: .regular, size: 16),
                                                             .foregroundColor: UIColor(white: 1, alpha: 0.7),
                                                             .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        configureNavigationItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.getInfo { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.updateUserInfo()
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        imageView.layer.cornerRadius = 16
        imageView.layer.masksToBounds = true
    }

    //MARK: - Public API
    func updateUserInfo() {
        nameLabel.text = viewModel.user?.fullName
        phoneNumberLabel.text = viewModel.user?.phoneNumber
        
        if let image = viewModel.user?.image {
            imageView.image = image
        } else {
            imageView.setImage(path: viewModel.imagePath, #imageLiteral(resourceName: "ic_empty_image")) { image in
                self.viewModel.user?.image = image
            }
        }
        
        tableView.reloadData()
    }
    
    //MARK: - Private API
    private func setup() {
        title = "My profile"
        tableView.dataSource = self
        showLoading(onView: self.view)
    }
    
    private func configureNavigationItem() {
        let attributeString = NSMutableAttributedString(string: "Change", attributes: attributes)
        changeButton.setAttributedTitle(attributeString, for: .normal)
        
        let editItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_edit.pdf"), style: .plain, target: self, action: #selector(edit))
        self.navigationItem.rightBarButtonItem = editItem
    }
    
    @objc private func edit() {
        if let user = viewModel.user {
            router.perform(.edit(user), from: self)
        }
    }
    
    private func signOut() {
        showLoading(onView: self.view)
        viewModel.logOut { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                AppController.logout()
            }
        }
    }
    
    private func alertForLogout() {
        let ac = UIAlertController(title: "Do you want to sign out?", message: nil, preferredStyle: .alert)
        
        let logout = UIAlertAction(title: "Logout", style: .default) { _ in
            self.signOut()
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        ac.addAction(cancel)
        ac.addAction(logout)
        ac.preferredAction = cancel
        self.present(ac, animated: true)
    }
    
    //MARK: - IBActions
    @IBAction func logOut() {
        alertForLogout()
    }
    
    @IBAction func changePhoneNumber() {
        if let user = viewModel.user {
            router.perform(.change(user), from: self)
        }
    }
}

extension UserProfileViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.user == nil ? 0 : viewModel.types.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = viewModel.types[indexPath.row]
        guard let user = viewModel.user else { return UITableViewCell() }
        switch type {
        case .carColor:
            let cell = UserProfileColorCell.cell(table: tableView, indexPath: indexPath)
            cell.configure(with: type, user: user)
            return cell
        default:
            let cell = UserProfileRegularCell.cell(table: tableView, indexPath: indexPath)
            cell.configure(with: type, user: user)
            return cell
        }
    }
}

extension UserProfileViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .userProfile
}
