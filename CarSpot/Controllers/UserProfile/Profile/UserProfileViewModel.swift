//
//  UserProfileViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import Foundation

class UserProfileViewModel: ProfileViewModel {
    
    //MARK: - Properties
    var user: User?
    let types: [UserSettingsType] = [.email, .carModel, .carBrand, .carColor, .carNumber]
    private let profileProvider = Provider<ProfileEndPoint>()
    
    var imagePath: String? {
        if let path = user?.imagePath {
            return Environment.live.baseURL + path
        }
        return nil
    }
    
    //MARK: - Public API
    func getInfo(completion: @escaping (CSError?) -> Void) {
        profileProvider.request(target: .info,
                                type: User.self) { user, error in
            if let error = error {
                completion(error)
            } else {
                self.user = user
                completion(nil)
            }
        }
    }
    
    func logOut(completion: @escaping (CSError?) -> Void) {
        guard let id = UIDevice.current.identifierForVendor?.uuidString else { return }
        profileProvider.request(target: .logOut(id),
                                type: Bool.self) { _, error in
            completion(error)
        }
    }
}

enum UserSettingsType {
    case name
    case lastName
    case email
    case carModel
    case carBrand
    case carColor
    case carNumber
    
    var title: String {
        switch self {
        case .name:
            return "First name"
        case .lastName:
            return "Last name"
        case .email:
            return "Email"
        case .carModel:
            return "Car model"
        case .carBrand:
            return "Car brand"
        case .carColor:
            return "Car color"
        case .carNumber:
            return "Car number"
        }
    }
}
