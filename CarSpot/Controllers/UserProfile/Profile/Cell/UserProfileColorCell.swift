//
//  UserProfileColorCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 08.06.21.
//

import UIKit

class UserProfileColorCell: TableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var valueLabel: UILabel!
    
    //MARK: - Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        
        colorView.layer.masksToBounds = true
        colorView.layer.cornerRadius = colorView.bounds.height / 2
    }
    
    //MARK: - Public API
    func configure(with type: UserSettingsType, user: User) {
        titleLabel.text = type.title
        valueLabel.text = user.carColor.name
        colorView.backgroundColor = user.carColor.color
    }
}
