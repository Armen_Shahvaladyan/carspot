//
//  UserProfileRegularCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 08.06.21.
//

import UIKit

class UserProfileRegularCell: TableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    //MARK: - Public API
    func configure(with type: UserSettingsType, user: User) {
        titleLabel.text = type.title
        
        switch type {
        case .email:
            valueLabel.text = user.email
        case .carModel:
            valueLabel.text = user.carBrand.model?.name
        case .carBrand:
            valueLabel.text = user.carBrand.name
        case .carNumber:
            valueLabel.text = user.carNumber
        default:
            break
        }
    }
}
