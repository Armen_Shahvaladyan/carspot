//
//  SampleTableCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.06.21.
//

import UIKit

class LeftMenuCell: TableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!

    //MARK: - Lifecycle
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        let color = selected ? .white : UIColor(white: 1, alpha: 0.5)
        iconImageView.tintColor = color
        titleLabel.textColor = color
    }
    
    //MARK: - Public API
    func configure(with type: MenuRowType) {
        titleLabel.text = type.title
        iconImageView.image = type.image
    }
}
