//
//  HostViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.06.21.
//

import UIKit

/**
 HostViewController is container view controller, contains menu controller and the list of relevant view controllers.

 Responsible for creating and selecting menu items content controlers.
 Has opportunity to show/hide side menu.
 */
class HostViewController: MenuContainerViewController {

    //MARK: - Properties
    var status: ParkingStatus?
    var spots: [Spot]?
    var offer: Offer?
    
    //MARK: - Lifecycle
    override var prefersStatusBarHidden: Bool {
        return false
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let screenSize: CGRect = UIScreen.main.bounds
        self.transitionOptions = TransitionOptions(duration: 0.4, visibleContentWidth: screenSize.width / 6)

        // Instantiate menu view controller by identifier
        self.menuViewController = LeftMenuRouter.createLeftMenuViewController()

        // Gather content items controllers
        self.contentViewControllers = contentControllers()

        // Select initial content controller. It's needed even if the first view controller should be selected.
        self.selectContentViewController(contentViewControllers.first!)

        self.currentItemOptions.cornerRadius = 10.0
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        // Options to customize menu transition animation.
        var options = TransitionOptions()

        // Animation duration
        options.duration = size.width < size.height ? 0.4 : 0.6

        // Part of item content remaining visible on right when menu is shown
        options.visibleContentWidth = size.width / 6
        self.transitionOptions = options
    }

    //MARK: - Private API
    private func contentControllers() -> [UIViewController] {
        let tabBarController = TabBarBuilder.tabBarViewController(spots: spots, offer: offer, status: status)
        
        let userProfileController = UserProfileRouter.createUserProfileViewController()
        let userProfileNC = NavigationController(rootViewController: userProfileController)
        
        let paymentMethodsController = PaymentMethodsRouter.createPaymentMethodsViewController(selectionMode: false)
        let paymentMethodsNC = NavigationController(rootViewController: paymentMethodsController)
        
        let myAddressesController = MyAddressesRouter.createMyAddressesViewController()
        let myAddressesNC = NavigationController(rootViewController: myAddressesController)
        
        let myPurchasesController = MyPurchasesRouter.createMyPurchasesViewController()
        let myPurchasesNC = NavigationController(rootViewController: myPurchasesController)
        
        let contactUsController = ContactUsRouter.createContactUsViewController()
        let contactUsNC = NavigationController(rootViewController: contactUsController)

        let helpController = HelpRouter.createHelpViewController()
        let helpNC = NavigationController(rootViewController: helpController)
        
        let aboutAppController = AboutAppRouter.createAboutAppViewController()
        let aboutAppNC = NavigationController(rootViewController: aboutAppController)
        
        return [tabBarController, userProfileNC, paymentMethodsNC, myAddressesNC, myPurchasesNC, contactUsNC, helpNC, aboutAppNC]
    }
}
