//
//  LeftMenuRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

enum LeftMenuSegue {
    
}

protocol LeftMenuRoutable: Routable where SegueType == LeftMenuSegue, SourceType == LeftMenuViewController {

}

struct LeftMenuRouter: LeftMenuRoutable {
    
    func perform(_ segue: LeftMenuSegue, from source: LeftMenuViewController) {
        
    }
}

extension LeftMenuRouter {
    static func createLeftMenuViewController() -> LeftMenuViewController {
        let vc = LeftMenuViewController.storyboardInstance
        vc.router = LeftMenuRouter()
        vc.viewModel = LeftMenuViewModel()
        
        return vc
    }
}
