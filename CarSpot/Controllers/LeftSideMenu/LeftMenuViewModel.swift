//
//  LeftMenuViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

class LeftMenuViewModel {
    
    //MARK: - Properties
    let sections = [MenuSection(type: .main, items: [.home, .profile, .paymentMethods, .myAddresses, .myPurchases, .contactUs, .help, .aboutApp, .termsAndConditions])]
}

struct MenuSection {
    let type: MenuSectionType
    let items: [MenuRowType]
}

enum MenuSectionType {
    case main
}

enum MenuRowType {
    case home
    case profile
    case paymentMethods
    case myAddresses
    case myPurchases
    case contactUs
    case help
    case aboutApp
    case termsAndConditions
    
    var title: String {
        switch self {
        case .home:
            return "Home"
        case .profile:
            return "Profile"
        case .paymentMethods:
            return "Payment methods"
        case .myAddresses:
            return "My addresses"
        case .myPurchases:
            return "My purchases"
        case .contactUs:
            return "Contact us"
        case .help:
            return "Help"
        case .aboutApp:
            return "About app"
        case .termsAndConditions:
            return "Terms and conditions"
        }
    }
    
    var image: UIImage? {
        switch self {
        case .home:
            return #imageLiteral(resourceName: "ic_home.pdf")
        case .profile:
            return #imageLiteral(resourceName: "ic_user.pdf")
        case .paymentMethods:
            return #imageLiteral(resourceName: "ic_card.pdf")
        case .myAddresses:
            return #imageLiteral(resourceName: "ic_location.pdf")
        case .myPurchases:
            return #imageLiteral(resourceName: "ic_purchases.pdf")
        case .contactUs:
            return #imageLiteral(resourceName: "ic_contact_us.pdf")
        case .help:
            return #imageLiteral(resourceName: "ic_help.pdf")
        case .aboutApp:
            return #imageLiteral(resourceName: "ic_about_app.pdf")
        case .termsAndConditions:
            return #imageLiteral(resourceName: "ic_terms_and_conditions.pdf")
        }
    }
}
