//
//  SampleMenuViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.06.21.
//

import UIKit

class LeftMenuViewController: MenuViewController {

    //MARK: - IBOutlets
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet weak var tableViewTrailingConstraint: NSLayoutConstraint!
    
    var router: LeftMenuRouter!
    var viewModel: LeftMenuViewModel!
    
    //MARK: - Lifecycle
    override var prefersStatusBarHidden: Bool {
        return false
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let trailing = self.menuContainerViewController?.transitionOptions.visibleContentWidth ?? 0
        tableViewTrailingConstraint.constant = trailing
        tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: UITableView.ScrollPosition.none)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.contentInset.top = max((tableView.bounds.height - tableView.contentSize.height) / 2, 0)
    }
}

extension LeftMenuViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuContainerViewController?.contentViewControllers.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = LeftMenuCell.cell(table: tableView, indexPath: indexPath)
        
        let rowType = viewModel.sections[indexPath.section].items[indexPath.row]
        cell.configure(with: rowType)
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }

        menuContainerViewController.selectContentViewController(menuContainerViewController.contentViewControllers[indexPath.row])
        menuContainerViewController.hideSideMenu()
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let v = UIView()
        v.backgroundColor = UIColor.clear
        return v
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
}

extension LeftMenuViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .leftMenu
}
