//
//  VerifyPhoneViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.05.21.
//

import UIKit

class VerifyPhoneViewController: BaseViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var nextButton: Button!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var otpView: OTPView!
    
    //MARK: - Properties
    var router: VerifyPhoneRouter!
    var viewModel: VerifyPhoneViewModel!
    
    private var counter = 60
    private var timer: Timer?
    private let attributes: [NSAttributedString.Key: Any] = [.font: UIFont.SF(.display, weight: .regular, size: 16),
                                                             .foregroundColor: UIColor("2E2E33"),
                                                             .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        startTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        _ = otpView.becomeFirstResponder()
    }
    
    deinit {
        if timer?.isValid == true {
            timer?.invalidate()
            timer = nil
        }
    }
    
    //MARK: - Private API
    private func setup() {
        otpView.delegate = self
        nextButton.buttonState = .disable
        descriptionLabel.text = "Enter the code we sent to \(viewModel.country.dialCode) \(viewModel.phone)"
        let attributeString = NSMutableAttributedString(string: "Change", attributes: attributes)
        changeButton.setAttributedTitle(attributeString, for: .normal)
    }
    
    private func startTimer() {
        counter = 60
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    
    @objc private func updateCounter() {
        if counter == 0 {
            resendButton.isUserInteractionEnabled = true
            UIView.performWithoutAnimation {
                let attributeString = NSMutableAttributedString(string: "Resend", attributes: attributes)
                self.resendButton.setAttributedTitle(attributeString, for: .normal)
                self.resendButton.layoutIfNeeded()
            }
            if let timer = timer,
               timer.isValid {
                self.timer?.invalidate()
                self.timer = nil
            }
        } else {
            UIView.performWithoutAnimation {
                let attributes: [NSAttributedString.Key: Any] = [.font: UIFont.SF(.display, weight: .regular, size: 16),
                                                                 .foregroundColor: UIColor("2E2E33")]
                let attributeString = NSMutableAttributedString(string: "Resend \(counter)", attributes: attributes)
                self.resendButton.setAttributedTitle(attributeString, for: .normal)
                self.resendButton.layoutIfNeeded()
            }
            counter -= 1
            resendButton.isUserInteractionEnabled = false
        }
    }
    
    private func freezeAllView(_ freeze: Bool) {
        nextButton.buttonState = freeze ? .load : .default
        nextButton.isUserInteractionEnabled = !freeze
        otpView.isUserInteractionEnabled = !freeze
        resendButton.isUserInteractionEnabled = !freeze
        changeButton.isUserInteractionEnabled = !freeze
    }
    
    //MARK: - IBActions
    @IBAction func changePhoneNumber() {
        router.perform(.back, from: self)
    }
    
    @IBAction func resend(_ sender: UIButton) {
        viewModel.sendPhoneNumber { [weak self] (error, code) in
            guard let self = self else { return }
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.resendButton.isUserInteractionEnabled = false
                self.startTimer()
            }
        }
    }
    
    @IBAction func next() {
        if let code = otpView.text {
            viewModel.code = code
            freezeAllView(true)
            otpView.isUserInteractionEnabled = false
            viewModel.verify { [weak self] (error, verify) in
                guard let self = self else { return }
                self.freezeAllView(false)
                if let error = error {
                    self.showAlert(with: error)
                } else if let verify = verify {
                    if !verify.authenticate {
                        self.router.perform(.profile(verify.token), from: self)
                    } else {
                        if let window = self.view.window {
                            KeychainWrapper.token = verify.token
                            Launcher.perform(segue: .main, window: window)
                        }
                    }
                }
            }
        }
    }
}

extension VerifyPhoneViewController: OTPViewDelegate {
    func otpView(_ view: OTPView, didAdd text: String, at position: Int) {
        nextButton.buttonState = text.count == 4 ? .default : .disable
    }
}

extension VerifyPhoneViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .auth
}
