//
//  VerifyPhoneViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.05.21.
//

import Foundation

class VerifyPhoneViewModel {
    
    //MARK: - Prperties
    let phone: String
    let country: Country
    var code: String
    private let authProvider = Provider<AuthEndPoint>()
    
    var phoneNumber: String {
        country.dialCode + phone
    }
    
    //MARK: - Lifecycle
    init(phone: String, country: Country, code: String) {
        self.phone = phone
        self.country = country
        self.code = code
    }
    
    //MARK: - Public API
    func sendPhoneNumber(completion: @escaping (CSError?, String?) -> Void) {
        authProvider.request(target: .sendCode(phoneNumber),
                             type: String.self) { code, error in
            Thread.onMainThread {
                if let error = error {
                    completion(error, nil)
                } else {
                    if let code = code {
                        self.code = code
                    }
                    completion(nil, code)
                }
            }
        }
    }
    
    func verify(completion: @escaping (CSError?, Verify?) -> Void) {
        authProvider.request(target: .verify(phoneNumber, code),
                             type: Verify.self) { verify, error in
            Thread.onMainThread {
                completion(error, verify)
            }
        }
    }
}
