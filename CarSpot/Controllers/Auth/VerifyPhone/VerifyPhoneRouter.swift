//
//  VerifyPhoneRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.05.21.
//

import UIKit

enum VerifyPhoneSegue {
    case back
    case profile(String)
}

protocol VerifyPhoneRoutable: Routable where SegueType == VerifyPhoneSegue, SourceType == VerifyPhoneViewController {

}

struct VerifyPhoneRouter: VerifyPhoneRoutable {
    
    func perform(_ segue: VerifyPhoneSegue, from source: VerifyPhoneViewController) {
        switch segue {
        case .back:
            source.navigationController?.popViewController(animated: true)
        case .profile(let token):
            let vc = ProfileRouter.createProfileViewController(with: token)
            source.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension VerifyPhoneRouter {
    static func createVerifyPhoneViewController(with phone: String, country: Country, code: String) -> VerifyPhoneViewController {
        let vc = VerifyPhoneViewController.storyboardInstance
        vc.router = VerifyPhoneRouter()
        vc.viewModel = VerifyPhoneViewModel(phone: phone, country: country, code: code)
        
        return vc
    }
}
