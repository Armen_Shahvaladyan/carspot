//
//  ProfileViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 01.06.21.
//

import Foundation

class ProfileViewModel {
    
    //MARK: - Properties
    var firstName = ""
    var lastName = ""
    var email = ""
    var carNumber = ""
    let token: String?
    var selectedBrand: CarBrand?
    var selectedColor: CarColor?
    
    private let carProvider = Provider<CarEndPoint>()
    private let authProvider = Provider<AuthEndPoint>()
    private(set) var brands: [CarBrand] = []
    private(set) var colors: [CarColor] = []
    
    //MARK: - Lifecycle
    init(token: String? = nil) {
        self.token = token
    }
    
    //MARK: - Public API
    func validateAllFields() -> ProfileError? {
        if firstName.removeWhitespacesAndNewlines().isEmpty {
            return .firstNameEmpty
        } else if lastName.removeWhitespacesAndNewlines().isEmpty {
            return .lastNameEmpty
        } else if !email.isValidEmail() {
            return.email
        } else if carNumber.removeWhitespacesAndNewlines().isEmpty {
            return .carNumber
        } else if selectedBrand == nil {
            return .brandEmpty
        } else if selectedBrand?.model == nil {
            return .modelsEmpty
        } else if selectedColor == nil {
            return .colorsEmpty
        }
        
        return nil
    }
    
    func fetchCarBrands(completion: @escaping (ProfileError?) -> Void) {
        carProvider.request(target: .brands, type: [CarBrand].self) { brands, error in
            if let error = error {
                completion(.unknown(error.message))
            } else if let brands = brands {
                self.brands = brands
                completion(nil)
            } else {
                completion(.brandEmpty)
            }
        }
    }
    
    func fetchCarColors(completion: @escaping (ProfileError?) -> Void) {
        carProvider.request(target: .color, type: [CarColor].self) { colors, error in
            if let error = error {
                completion(.unknown(error.message))
            } else if let colors = colors {
                self.colors = colors
                completion(nil)
            } else {
                completion(.colorsEmpty)
            }
        }
    }
    
    func addProfile(completion: @escaping (CSError?) -> Void) {
        guard let token = token,
              let brand = selectedBrand,
              let color = selectedColor,
              !firstName.isEmpty,
              !lastName.isEmpty,
              !email.isEmpty,
              !carNumber.isEmpty  else {
            return
        }
        
        let user = User(name: firstName,
                        lastName: lastName,
                        email: email,
                        carNumber: carNumber,
                        carBrand: brand,
                        carColor: color)
        
        KeychainWrapper.token = token
        
        authProvider.request(target: .add(user),
                             type: Bool.self) { success, error in
            if let error = error {
                KeychainWrapper.token = nil
                completion(error)
            } else {
                if let success = success, success {
                    completion(nil)
                } else {
                    KeychainWrapper.token = nil
                    completion(CSError(key: "", message: "The authentication is failed. Please try later."))
                }
            }
        }
    }
}
