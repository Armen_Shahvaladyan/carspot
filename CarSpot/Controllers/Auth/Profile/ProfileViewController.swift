//
//  ProfileViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 01.06.21.
//

import UIKit

class ProfileViewController: BaseViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var firstNameView: EditingView!
    @IBOutlet weak var lastNameView: EditingView!
    @IBOutlet weak var emailView: EditingView!
    @IBOutlet weak var carBrandView: SelectedViewWithIcon!
    @IBOutlet weak var carModelView: SelectedViewWithIcon!
    @IBOutlet weak var carColorView: CarColorView!
    @IBOutlet weak var carNumbersView: EditingView!
    @IBOutlet weak var nextButton: Button!
    
    //MARK: - Properties
    var router: ProfileRouter!
    var viewModel: ProfileViewModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    //MARK: - Public API
    func select(brand: CarBrand) {
        carModelView.state = .default
        if brand.name != viewModel.selectedBrand?.name {
            viewModel.selectedBrand = brand
            viewModel.selectedBrand?.model = nil
            carBrandView.text = brand.name
            carModelView.text = ""
        }
        checkNextButtonState()
    }
    
    func select(model: CarModel) {
        viewModel.selectedBrand?.model = model
        carModelView.text = model.name
        checkNextButtonState()
    }
    
    func select(color: CarColor) {
        viewModel.selectedColor = color
        carColorView.text = color.name
        carColorView.color = UIColor(color.hexCode)
        checkNextButtonState()
    }
    
    //MARK: - Private API
    private func setup() {
        carModelView.state = .disable
        nextButton.buttonState = .disable
        emailView.textField.keyboardType = .emailAddress
        carColorView.delegate = self
        [carBrandView, carModelView].forEach { $0?.delegate = self }
        [firstNameView, lastNameView, emailView, carNumbersView].forEach { $0?.delegate = self }
    }
    
    private func checkNextButtonState() {
        if viewModel.validateAllFields() == nil {
            nextButton.buttonState = .default
        } else {
            nextButton.buttonState = .disable
        }
    }
    
    //MARK: - IBActions
    @IBAction func next() {
        showLoading(onView: self.view)
        viewModel.addProfile { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.router.perform(.main, from: self)
            }
        }
    }
}

extension ProfileViewController: CarColorViewDelegate {
    func carColorView(_ view: CarColorView, didSelect text: String) {
        if view == carColorView {
            openColors()
        }
    }
}

extension ProfileViewController: SelectedViewWithIconDelegate {
    func selectedViewWithIcon(_ view: SelectedViewWithIcon, didSelect text: String) {
        switch view {
        case carBrandView:
            openBrands()
        case carModelView:
            if let brand = viewModel.selectedBrand {
                router.perform(.models(brand.models, brand.model), from: self)
            }
        default:
            break
        }
    }
}

extension ProfileViewController: EditingViewDelegate {
    func editingView(_ view: EditingView, didChange text: String) {
        switch view {
        case firstNameView:
            viewModel.firstName = text
        case lastNameView:
            viewModel.lastName = text
        case emailView:
            viewModel.email = text
        case carNumbersView:
            viewModel.carNumber = text
        default:
            break
        }
        checkNextButtonState()
    }
}

extension ProfileViewController {
    private func openBrands() {
        if viewModel.brands.isEmpty {
            showLoading(onView: self.view)
            viewModel.fetchCarBrands { [weak self] error in
                guard let self = self else { return }
                self.hideLoading()
                if let error = error {
                    self.showAlert(with: error)
                } else {
                    self.router.perform(.brands(self.viewModel.brands, self.viewModel.selectedBrand), from: self)
                }
            }
        } else {
            router.perform(.brands(viewModel.brands, viewModel.selectedBrand), from: self)
        }
    }
    
    private func openColors() {
        if viewModel.colors.isEmpty {
            showLoading(onView: self.view)
            viewModel.fetchCarColors { [weak self] error in
                guard let self = self else { return }
                self.hideLoading()
                if let error = error {
                    self.showAlert(with: error)
                } else {
                    self.router.perform(.colors(self.viewModel.colors, self.viewModel.selectedColor), from: self)
                }
            }
        } else {
            router.perform(.colors(viewModel.colors, viewModel.selectedColor), from: self)
        }
    }
}

extension ProfileViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .profile
}
