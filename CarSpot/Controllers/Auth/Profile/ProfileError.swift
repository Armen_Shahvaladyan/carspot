//
//  ProfileError.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 01.06.21.
//

import Foundation

enum ProfileError: Error {
    case firstNameEmpty
    case lastNameEmpty
    case email
    case carNumber
    case brandEmpty
    case modelsEmpty
    case colorsEmpty
    case unknown(String)
}

extension ProfileError: Errorable {
    var message: String {
        switch self {
        case .firstNameEmpty:
            return "First name is empty"
        case .lastNameEmpty:
            return "Last name is empty"
        case .email:
            return "Please fill a valid email address."
        case .carNumber:
            return "Car number is empty"
        case .brandEmpty:
            return "Brands are empty"
        case .modelsEmpty:
            return "Models are empty"
        case .colorsEmpty:
            return "Colors are empty"
        case .unknown(let message):
            return message
        }
    }
}
