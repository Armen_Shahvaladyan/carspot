//
//  CarBrandRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 02.06.21.
//

import UIKit

enum CarBrandSegue {
    case dismiss
}

protocol CarBrandRoutable: Routable where SegueType == CarBrandSegue, SourceType == CarBrandViewController {

}

struct CarBrandRouter: CarBrandRoutable {
    func perform(_ segue: CarBrandSegue, from source: CarBrandViewController) {
        switch segue {
        case .dismiss:
            source.dismiss(animated: true)
        }
    }
}

extension CarBrandRouter {
    static func createCarBrandViewController(brands: [CarBrand], selectedBrand: CarBrand?) -> CarBrandViewController {
        let vc = CarBrandViewController.storyboardInstance
        vc.router = CarBrandRouter()
        vc.viewModel = CarBrandViewModel(brands: brands, selectedBrand: selectedBrand)
        
        return vc
    }
}
