//
//  CarBrandViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 02.06.21.
//

import Foundation

class CarBrandViewModel {
    
    //MARK: - Properties
    var selectedBrand: CarBrand?
    private(set) var brands: [CarBrand] = []
    private(set) var searchBrands: [CarBrand] = []
    
    //MARK: - Lifecycle
    init(brands: [CarBrand], selectedBrand: CarBrand?) {
        self.brands = brands
        self.selectedBrand = selectedBrand
    }
    
    //MARK: - Public API
    func searchBrand(with searchText: String) {
        if searchText.isEmpty {
            searchBrands = []
        } else {
            searchBrands = brands.filter { $0.name.lowercased().hasPrefix(searchText.lowercased()) }
        }
    }
}
