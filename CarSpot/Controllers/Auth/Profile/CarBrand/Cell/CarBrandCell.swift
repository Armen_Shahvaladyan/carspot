//
//  CarBrandCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 02.06.21.
//

import UIKit

class CarBrandCell: TableViewCell {

    //MARK: - IBOutlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    
    //MARK: - Public API
    func configure(with brand: CarBrand, selected: Bool) {
        titleLabel.text = brand.name
        checkImageView.isHidden = !selected
    }
}
