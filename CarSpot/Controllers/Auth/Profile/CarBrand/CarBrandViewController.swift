//
//  CarBrandViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 02.06.21.
//

import UIKit

class CarBrandViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchBarBackgroundView: UIView!
    
    //MARK: - Properties
    var router: CarBrandRouter!
    var viewModel: CarBrandViewModel!
        
    var didSelectCarBrand: ((CarBrand) -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    
        searchBarBackgroundView.layer.cornerRadius = 16
        searchBarBackgroundView.layer.masksToBounds = true
    }
    
    //MARK: - Private API
    private func setup() {
        searchTextField.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension CarBrandViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.searchBrands.isEmpty ? viewModel.brands.count : viewModel.searchBrands.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CarBrandCell.cell(table: tableView, indexPath: indexPath)
        var brand: CarBrand!
        if viewModel.searchBrands.isEmpty {
            brand = viewModel.brands[indexPath.row]
        } else {
            brand = viewModel.searchBrands[indexPath.row]
        }
        
        cell.configure(with: brand, selected: viewModel.selectedBrand?.name == brand.name)
        return cell
    }
}

extension CarBrandViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var brand: CarBrand!
        if viewModel.searchBrands.isEmpty {
            brand = viewModel.brands[indexPath.row]
        } else {
            brand = viewModel.searchBrands[indexPath.row]
        }
        viewModel.selectedBrand = brand
        tableView.reloadData()
        
        didSelectCarBrand?(brand)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.router.perform(.dismiss, from: self)
        }
    }
}

extension CarBrandViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            var searchText = text.replacingCharacters(in: textRange, with: string)
            searchText = searchText.trimmingCharacters(in: .whitespacesAndNewlines)
            viewModel.searchBrand(with: searchText)
            tableView.reloadData()
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}

extension CarBrandViewController: PanModalPresentable {
    var cornerRadius: CGFloat {
        return 32
    }

    var panScrollable: UIScrollView? {
        return tableView
    }

    var shortFormHeight: PanModalHeight {
        return longFormHeight
    }

    var anchorModalToLongForm: Bool {
        return false
    }

    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .longForm = state
        else { return }

        panModalSetNeedsLayoutUpdate()
    }
}

extension CarBrandViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .profile
}
