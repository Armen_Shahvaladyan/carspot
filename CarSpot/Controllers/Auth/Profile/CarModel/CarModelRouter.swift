//
//  CarModelRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 02.06.21.
//

import UIKit

enum CarModelSegue {
    case dismiss
}

protocol CarModelRoutable: Routable where SegueType == CarModelSegue, SourceType == CarModelViewController {

}

struct CarModelRouter: CarModelRoutable {
    func perform(_ segue: CarModelSegue, from source: CarModelViewController) {
        switch segue {
        case .dismiss:
            source.dismiss(animated: true)
        }
    }
}

extension CarModelRouter {
    static func createCarModelViewController(models: [CarModel], selectedModel: CarModel?) -> CarModelViewController {
        let vc = CarModelViewController.storyboardInstance
        vc.router = CarModelRouter()
        vc.viewModel = CarModelViewModel(models: models, selectedModel: selectedModel)
        
        return vc
    }
}
