//
//  CarModelsViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 02.06.21.
//

import UIKit

class CarModelViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchBarBackgroundView: UIView!
    
    //MARK: - Properties
    var router: CarModelRouter!
    var viewModel: CarModelViewModel!
        
    var didSelectCarModel: ((CarModel) -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    
        searchBarBackgroundView.layer.cornerRadius = 16
        searchBarBackgroundView.layer.masksToBounds = true
    }
    
    //MARK: - Private API
    private func setup() {
        searchTextField.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension CarModelViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.searchModels.isEmpty ? viewModel.models.count : viewModel.searchModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CarModelCell.cell(table: tableView, indexPath: indexPath)
        var model: CarModel!
        if viewModel.searchModels.isEmpty {
            model = viewModel.models[indexPath.row]
        } else {
            model = viewModel.searchModels[indexPath.row]
        }
        
        cell.configure(with: model, selected: viewModel.selectedModel?.name == model.name)
        return cell
    }
}

extension CarModelViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var model: CarModel!
        if viewModel.searchModels.isEmpty {
            model = viewModel.models[indexPath.row]
        } else {
            model = viewModel.searchModels[indexPath.row]
        }
        viewModel.selectedModel = model
        tableView.reloadData()
        
        didSelectCarModel?(model)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.router.perform(.dismiss, from: self)
        }
    }
}

extension CarModelViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            var searchText = text.replacingCharacters(in: textRange, with: string)
            searchText = searchText.trimmingCharacters(in: .whitespacesAndNewlines)
            viewModel.searchModel(with: searchText)
            tableView.reloadData()
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}

extension CarModelViewController: PanModalPresentable {
    var cornerRadius: CGFloat {
        return 32
    }

    var panScrollable: UIScrollView? {
        return tableView
    }

    var shortFormHeight: PanModalHeight {
        return longFormHeight
    }

    var anchorModalToLongForm: Bool {
        return false
    }

    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .longForm = state
        else { return }

        panModalSetNeedsLayoutUpdate()
    }
}

extension CarModelViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .profile
}
