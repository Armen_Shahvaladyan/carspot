//
//  CarModelViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 02.06.21.
//

import Foundation

class CarModelViewModel {
    
    //MARK: - Properties
    var selectedModel: CarModel?
    private(set) var models: [CarModel] = []
    private(set) var searchModels: [CarModel] = []
    
    //MARK: - Lifecycle
    init(models: [CarModel], selectedModel: CarModel?) {
        self.models = models
        self.selectedModel = selectedModel
    }
    
    //MARK: - Public API
    func searchModel(with searchText: String) {
        if searchText.isEmpty {
            searchModels = []
        } else {
            searchModels = models.filter { $0.name.lowercased().hasPrefix(searchText.lowercased()) }
        }
    }
}
