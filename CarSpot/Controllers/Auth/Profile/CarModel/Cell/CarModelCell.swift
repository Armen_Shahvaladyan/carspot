//
//  CarModelCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 02.06.21.
//

import UIKit

class CarModelCell: TableViewCell {

    //MARK: - IBOutlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    
    //MARK: - Public API
    func configure(with model: CarModel, selected: Bool) {
        titleLabel.text = model.name
        checkImageView.isHidden = !selected
    }
}
