//
//  CarColorViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 03.06.21.
//

import UIKit

class CarColorViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchBarBackgroundView: UIView!
    
    //MARK: - Properties
    var router: CarColorRouter!
    var viewModel: CarColorViewModel!
        
    var didSelectCarColor: ((CarColor) -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    
        searchBarBackgroundView.layer.cornerRadius = 16
        searchBarBackgroundView.layer.masksToBounds = true
    }
    
    //MARK: - Private API
    private func setup() {
        searchTextField.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension CarColorViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.searchColors.isEmpty ? viewModel.colors.count : viewModel.searchColors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CarColorCell.cell(table: tableView, indexPath: indexPath)
        var color: CarColor!
        if viewModel.searchColors.isEmpty {
            color = viewModel.colors[indexPath.row]
        } else {
            color = viewModel.searchColors[indexPath.row]
        }
        
        cell.configure(with: color, selected: viewModel.selectedColor?.name == color.name)
        return cell
    }
}

extension CarColorViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var color: CarColor!
        if viewModel.searchColors.isEmpty {
            color = viewModel.colors[indexPath.row]
        } else {
            color = viewModel.searchColors[indexPath.row]
        }
        viewModel.selectedColor = color
        tableView.reloadData()
        
        didSelectCarColor?(color)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.router.perform(.dismiss, from: self)
        }
    }
}

extension CarColorViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            var searchText = text.replacingCharacters(in: textRange, with: string)
            searchText = searchText.trimmingCharacters(in: .whitespacesAndNewlines)
            viewModel.searchColor(with: searchText)
            tableView.reloadData()
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}

extension CarColorViewController: PanModalPresentable {
    var cornerRadius: CGFloat {
        return 32
    }

    var panScrollable: UIScrollView? {
        return tableView
    }

    var shortFormHeight: PanModalHeight {
        return longFormHeight
    }

    var anchorModalToLongForm: Bool {
        return false
    }

    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .longForm = state
        else { return }

        panModalSetNeedsLayoutUpdate()
    }
}

extension CarColorViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .profile
}
