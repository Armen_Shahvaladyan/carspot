//
//  CarColorCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 03.06.21.
//

import UIKit

class CarColorCell: TableViewCell {

    //MARK: - IBOutlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var colorView: UIView!
    
    //MARK: - Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        
        colorView.layer.masksToBounds = true
        colorView.layer.cornerRadius = colorView.bounds.height / 2
    }
    
    //MARK: - Public API
    func configure(with color: CarColor, selected: Bool) {
        titleLabel.text = color.name
        checkImageView.isHidden = !selected
        colorView.backgroundColor = UIColor(color.hexCode)
        
        if color.hexCode.lowercased().contains("ffffff") {
            colorView.layer.borderWidth = 1
            colorView.layer.borderColor = UIColor("#E8E8E8").cgColor
        } else {
            colorView.layer.borderWidth = 0
            colorView.layer.borderColor = UIColor.clear.cgColor
        }
    }
}
