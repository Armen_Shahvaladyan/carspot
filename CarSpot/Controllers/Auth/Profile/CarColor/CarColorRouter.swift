//
//  CarColorRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 03.06.21.
//

import UIKit

enum CarColorSegue {
    case dismiss
}

protocol CarColorRoutable: Routable where SegueType == CarColorSegue, SourceType == CarColorViewController {

}

struct CarColorRouter: CarColorRoutable {
    func perform(_ segue: CarColorSegue, from source: CarColorViewController) {
        switch segue {
        case .dismiss:
            source.dismiss(animated: true)
        }
    }
}

extension CarColorRouter {
    static func createCarColorViewController(colors: [CarColor], selectedColor: CarColor?) -> CarColorViewController {
        let vc = CarColorViewController.storyboardInstance
        vc.router = CarColorRouter()
        vc.viewModel = CarColorViewModel(colors: colors, selectedColor: selectedColor)
        
        return vc
    }
}
