//
//  CarColorViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 03.06.21.
//

import Foundation

class CarColorViewModel {
    
    //MARK: - Properties
    var selectedColor: CarColor?
    private(set) var colors: [CarColor] = []
    private(set) var searchColors: [CarColor] = []
    
    //MARK: - Lifecycle
    init(colors: [CarColor], selectedColor: CarColor?) {
        self.colors = colors
        self.selectedColor = selectedColor
    }
    
    //MARK: - Public API
    func searchColor(with searchText: String) {
        if searchText.isEmpty {
            searchColors = []
        } else {
            searchColors = colors.filter { $0.name.lowercased().hasPrefix(searchText.lowercased()) }
        }
    }
}
