//
//  ProfileRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 01.06.21.
//

import UIKit

enum Profileegue {
    case brands([CarBrand], CarBrand?)
    case models([CarModel], CarModel?)
    case colors([CarColor], CarColor?)
    case main
}

protocol ProfileRoutable: Routable where SegueType == Profileegue, SourceType == ProfileViewController {

}

class ProfileRouter: ProfileRoutable {
        
    func perform(_ segue: Profileegue, from source: ProfileViewController) {
        switch segue {
        case let .brands(brands, selectedBrand):
            let vc = CarBrandRouter.createCarBrandViewController(brands: brands, selectedBrand: selectedBrand)
            
            vc.didSelectCarBrand = { brand in
                source.select(brand: brand)
            }
            
            source.presentBottomSheet(vc)
        case let .models(models, selectedModel):
            let vc = CarModelRouter.createCarModelViewController(models: models, selectedModel: selectedModel)
            
            vc.didSelectCarModel = { model in
                source.select(model: model)
            }
            
            source.presentBottomSheet(vc)
        case let .colors(colors, selectedColor):
            let vc = CarColorRouter.createCarColorViewController(colors: colors, selectedColor: selectedColor)
            
            vc.didSelectCarColor = { color in
                source.select(color: color)
            }
            
            source.presentBottomSheet(vc)
        case .main:
            guard let window = source.view.window else { return }
            Launcher.perform(segue: .main, window: window)
        }
    }
}

extension ProfileRouter {
    static func createProfileViewController(with token: String? = nil) -> ProfileViewController {
        let vc = ProfileViewController.storyboardInstance
        vc.router = ProfileRouter()
        vc.viewModel = ProfileViewModel(token: token)
        
        return vc
    }
}
