//
//  PhoneNumberViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 22.05.21.
//

import Foundation

class PhoneNumberViewModel {
    
    //MARK: - Properties
    var selectedCounutry: Country?
    var phoneNumber: String?
    private let authProvider = Provider<AuthEndPoint>()
    
    //MARK: - Public API
    func sendPhoneNumber(completion: @escaping (CSError?, String?) -> Void) {
        guard let country = selectedCounutry,
              let phoneNumber = phoneNumber else {
            completion(nil, nil)
            return
        }
        authProvider.request(target: .sendCode(country.dialCode + phoneNumber),
                             type: String.self) { code, error in
            Thread.onMainThread {
                if let error = error {
                    completion(error, nil)
                } else {
                    completion(nil, code)
                }
            }
        }
    }
}
