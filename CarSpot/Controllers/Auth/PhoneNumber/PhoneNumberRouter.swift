//
//  PhoneNumberRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 22.05.21.
//

import UIKit

enum PhoneNumberSegue {
    case country(Country?)
    case verify(Country, String, String)
    case termsAndConditions
}

protocol PhoneNumberRoutable: Routable where SegueType == PhoneNumberSegue, SourceType == PhoneNumberViewController {

}

struct PhoneNumberRouter: PhoneNumberRoutable {
        
    func perform(_ segue: PhoneNumberSegue, from source: PhoneNumberViewController) {
        switch segue {
        case .country(let country):
            let vc = CountryRouter.createCountryViewController(country: country)
            
            vc.didSelectCountry = { country in
                source.handleSelection(of: country)
            }
            
            source.presentBottomSheet(vc)
        case let .verify(country, phone, code):
            let vc = VerifyPhoneRouter.createVerifyPhoneViewController(with: phone, country: country, code: code)
            source.navigationController?.pushViewController(vc, animated: true)
        case .termsAndConditions:
            let vc = AppGlobalRouter.createPrivacyPolicyViewController(with: .termsAndConditions)
            source.present(vc, animated: true)
        }
    }
}

extension PhoneNumberRouter {
    static func createPhoneNumberViewController() -> PhoneNumberViewController {
        let vc = PhoneNumberViewController.storyboardInstance
        vc.router = PhoneNumberRouter()
        vc.viewModel = PhoneNumberViewModel()
        
        return vc
    }
}
