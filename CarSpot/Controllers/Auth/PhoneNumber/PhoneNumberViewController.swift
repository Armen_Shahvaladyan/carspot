//
//  PhoneNumberViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 22.05.21.
//

import UIKit

class PhoneNumberViewController: BaseViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var nextButton: Button!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var countryView: SelectedViewWithIcon!
    @IBOutlet weak var phoneNumberCodeView: TitleView!
    @IBOutlet weak var phoneNumberView: EditingView!
    
    //MARK: - Properties
    var router: PhoneNumberRouter!
    var viewModel: PhoneNumberViewModel!
    
    private let terms = "terms and conditions"
    private let desc = "By tapping phone number, you agree to our "
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        if let _ = viewModel.selectedCounutry, let _ = viewModel.phoneNumber {
            nextButton.buttonState = .default
        } else {
            nextButton.buttonState = .disable
        }
    }
    
    //MARK: - Public API
    func handleSelection(of country: Country) {
        viewModel.selectedCounutry = country
        countryView.text = country.name
        phoneNumberCodeView.text = country.dialCode
        
        if viewModel.phoneNumber != nil &&
            nextButton.buttonState != .default {
            nextButton.buttonState = .default
        }
    }
    
    //MARK: - Private API
    private func setup() {
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        countryView.delegate = self
        phoneNumberView.delegate = self
        nextButton.buttonState = .disable
        phoneNumberView.textField.keyboardType = .numberPad
        
        let font = UIFont.SF(.display, weight: .regular, size: 16)
        let fullText = desc + terms
        let attributedString = NSMutableAttributedString(fullString: fullText,
                                                         fullStringColor: UIColor("2E2E33", alpha: 0.5),
                                                         fullStringFont: font,
                                                         subString: terms,
                                                         subStringColor: UIColor("2E2E33"),
                                                         subStringFont: font,
                                                         underline: true)
        descriptionLabel.attributedText = attributedString
        
        descriptionLabel.isUserInteractionEnabled = true
        descriptionLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapLabel(gesture:))))
    }
    
    @objc func tapLabel(gesture: UITapGestureRecognizer) {
        let range = ((desc + terms) as NSString).range(of: terms)
        
        if gesture.didTapAttributedTextIn(label: descriptionLabel, inRange: range) {
            router.perform(.termsAndConditions, from: self)
        }
    }
    
    private func freezeAllView(_ freeze: Bool) {
        nextButton.buttonState = freeze ? .load : .default
        nextButton.isUserInteractionEnabled = !freeze
        countryView.isUserInteractionEnabled = !freeze
        phoneNumberView.isUserInteractionEnabled = !freeze
    }
    
    //MARK: - IBActions
    @IBAction func next(_ sender: Button) {
        self.view.endEditing(true)
        
        guard let phone = viewModel.phoneNumber,
              let country = viewModel.selectedCounutry else { return }
        freezeAllView(true)
        viewModel.sendPhoneNumber { [weak self] (error, code) in
            guard let self = self else { return }
            self.freezeAllView(false)
            if let error = error {
                self.showAlert(with: error)
            } else {
                if let code = code {
                    self.router.perform(.verify(country, phone, code), from: self)
                }
            }
        }
    }
}

extension PhoneNumberViewController: SelectedViewWithIconDelegate {
    func selectedViewWithIcon(_ view: SelectedViewWithIcon, didSelect text: String) {
        router.perform(.country(viewModel.selectedCounutry), from: self)
    }
}

extension PhoneNumberViewController: EditingViewDelegate {
    func editingView(_ view: EditingView, didChange text: String) {
        if viewModel.selectedCounutry != nil {
            nextButton.buttonState = text.isEmpty ? .disable : .default
        }
        viewModel.phoneNumber = text
    }
}

extension PhoneNumberViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .auth
}
