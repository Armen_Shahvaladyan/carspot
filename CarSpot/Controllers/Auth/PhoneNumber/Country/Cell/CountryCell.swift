//
//  CountryCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.05.21.
//

import UIKit

class CountryCell: TableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    
    //MARK: - Public API
    func configure(with country: Country, selected: Bool) {
        titleLabel.text = country.name
        checkImageView.isHidden = !selected
    }
}
