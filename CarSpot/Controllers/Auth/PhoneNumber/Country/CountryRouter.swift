//
//  CountryRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.05.21.
//


import UIKit

enum CountrySegue {
    case dismiss
}

protocol CountryRoutable: Routable where SegueType == CountrySegue, SourceType == CountryViewController {

}

struct CountryRouter: CountryRoutable {
    func perform(_ segue: CountrySegue, from source: CountryViewController) {
        switch segue {
        case .dismiss:
            source.dismiss(animated: true)
        }
    }
}

extension CountryRouter {
    static func createCountryViewController(country: Country?) -> CountryViewController {
        let vc = CountryViewController.storyboardInstance
        vc.router = CountryRouter()
        vc.viewModel = CountryViewModel(country: country)
        
        return vc
    }
}
