//
//  CountryViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.05.21.
//

import Foundation

class CountryViewModel {
    
    //MARK: - Properties
    var selectedCountry: Country?
    let jsonReader = JsonReader<Country>()
    private var countries: [Country] = []
    private(set) var sections: [(letter: Character, countries: [Country])] = []
    
    //MARK: - Lifecycle
    init(country: Country?) {
        self.selectedCountry = country
    }
    
    //MARK: - Public API
    func getCountries(completion: (Error?) -> Void) {
        let result = jsonReader.read(with: .country)
        if let error = result.1 {
            completion(error)
        } else if let countries = result.0 {
            self.countries = countries
            calculateSections(with: countries)
            completion(nil)
        } else {
            completion(nil)
        }
    }
    
    func calculateSections(with countries: [Country]) {
        sections = Dictionary(grouping: countries) { (country) -> Character in
            return country.name.first!
        }.map { (key: Character, value: [Country]) -> (letter: Character, countries: [Country]) in
            (letter: key, countries: value)
        }.sorted { (left, right) -> Bool in
            left.letter < right.letter
        }
    }
    
    func searchCountry(with searchText: String) {
        if searchText.isEmpty {
            calculateSections(with: countries)
        } else {
            let countries = self.countries.filter { $0.name.lowercased().hasPrefix(searchText.lowercased()) }
            calculateSections(with: countries)
        }
    }
}
