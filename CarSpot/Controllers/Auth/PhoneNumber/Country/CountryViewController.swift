//
//  CountryViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.05.21.
//

import UIKit

class CountryViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchBarBackgroundView: UIView!
    
    //MARK: - Properties
    var router: CountryRouter!
    var viewModel: CountryViewModel!
        
    var didSelectCountry: ((Country) -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    
        searchBarBackgroundView.layer.cornerRadius = 16
        searchBarBackgroundView.layer.masksToBounds = true
    }
    
    //MARK: - Private API
    private func setup() {
        viewModel.getCountries { [weak self] error in
            if let error = error {
                print(error.localizedDescription)
            } else {
                self?.tableView.reloadData()
            }
        }
        
        searchTextField.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "CountryHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: CountryHeaderView.reuseIdentifier)
    }
}

extension CountryViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.sections[section].countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CountryCell.cell(table: tableView, indexPath: indexPath)
        let country = viewModel.sections[indexPath.section].countries[indexPath.row]
        cell.configure(with: country, selected: viewModel.selectedCountry?.name == country.name)
        return cell
    }
}

extension CountryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        32
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let country = viewModel.sections[indexPath.section].countries[indexPath.row]
        viewModel.selectedCountry = country
        tableView.reloadData()
        
        didSelectCountry?(country)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.router.perform(.dismiss, from: self)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CountryHeaderView.reuseIdentifier) as? CountryHeaderView
        let section = viewModel.sections[section].letter
        headerView?.configure(with: String(section))
        return headerView
    }
}

extension CountryViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let searchText = text.replacingCharacters(in: textRange, with: string)
            viewModel.searchCountry(with: searchText)
            tableView.reloadData()
        }
        return true
    }
}

extension CountryViewController: PanModalPresentable {
    var cornerRadius: CGFloat {
        return 32
    }

    var panScrollable: UIScrollView? {
        return tableView
    }

    var shortFormHeight: PanModalHeight {
        return longFormHeight
    }

    var anchorModalToLongForm: Bool {
        return false
    }

    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .longForm = state
        else { return }

        panModalSetNeedsLayoutUpdate()
    }
}

extension CountryViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .auth
}
