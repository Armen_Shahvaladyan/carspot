//
//  CountryHeaderView.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.05.21.
//

import UIKit

class CountryHeaderView: UITableViewHeaderFooterView {
    
    //MARK: - Properties
    @IBOutlet weak var titleLabel: UILabel!
    
    static let reuseIdentifier = "UITableViewHeaderFooterView"
    
    //MARK: - Lifecycle
    func configure(with section: String) {
        titleLabel.text = section
    }
}
