//
//  PrivacyPolicyRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 15.07.21.
//

import UIKit

enum PrivacyPolicySegue {
    
}

protocol PrivacyPolicyRoutable: Routable where SegueType == PrivacyPolicySegue, SourceType == AppGlobalViewController {

}

struct AppGlobalRouter: PrivacyPolicyRoutable {
        
    func perform(_ segue: PrivacyPolicySegue, from source: AppGlobalViewController) {
        
    }
}

extension AppGlobalRouter {
    static func createPrivacyPolicyViewController(with type: AppGlobalType) -> AppGlobalViewController {
        let vc = AppGlobalViewController.storyboardInstance
        vc.router = AppGlobalRouter()
        vc.viewModel = AppGlobalModel(type: type)
        
        return vc
    }
}
