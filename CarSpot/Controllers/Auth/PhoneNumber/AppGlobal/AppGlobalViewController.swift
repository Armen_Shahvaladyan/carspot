//
//  PrivacyPolicyViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 15.07.21.
//

import UIKit

class AppGlobalViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!

    //MARK: - Properties
    var router: AppGlobalRouter!
    var viewModel: AppGlobalModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    //MARK: - Private API
    private func setup() {
        titleLabel.text = viewModel.type.title
        self.showLoading(onView: self.view)
        
        viewModel.fetchGlobal { [weak self] (error, global) in
            guard let self = self else { return }
            Thread.onMainThread {
                if let error = error {
                    self.showAlert(with: error)
                } else if let global = global {
                    self.hideLoading()
                    self.configureViews(with: global)
                }
            }
        }
    }
    
    private func configureViews(with appGlobal: AppGlobal) {
        switch viewModel.type {
        case .termsAndConditions:
            descriptionTextView.text = appGlobal.termsAndConditions
        case .privacyPolicy:
            descriptionTextView.text = appGlobal.privacyPolicy
        }
    }
}

extension AppGlobalViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .auth
}
