//
//  PrivacyPolicyViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 15.07.21.
//

import Foundation

class AppGlobalModel {
    
    //MARK: - Properties
    let type: AppGlobalType
    private let globalProvider = Provider<GlobalEndPoint>()
    
    //MARK: - Lifecycle
    init(type: AppGlobalType) {
        self.type = type
    }
    
    //MARK: - Public API
    func fetchGlobal(completion: @escaping (CSError?, AppGlobal?) -> Void) {
        globalProvider.request(target: .all, type: AppGlobal.self) { appGlobal, error in
            Thread.onMainThread {
                completion(error, appGlobal)
            }
        }
    }
}

enum AppGlobalType {
    case termsAndConditions
    case privacyPolicy
    
    var title: String {
        switch self {
        case .termsAndConditions:
            return "Terms and conditions"
        case .privacyPolicy:
            return "Privacy policy"
        }
    }
}
