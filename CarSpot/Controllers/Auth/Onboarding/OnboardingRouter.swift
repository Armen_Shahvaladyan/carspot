//
//  OnboardingRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 21.05.21.
//

import UIKit

enum OnboardingSegue {
    case auth
}

protocol OnboardingRoutable: Routable where SegueType == OnboardingSegue, SourceType == OnboardingViewController {

}

struct OnboardingRouter: OnboardingRoutable {
    func perform(_ segue: OnboardingSegue, from source: OnboardingViewController) {
        switch segue {
        case .auth:
            guard let window = source.view.window else { return }
            Launcher.perform(segue: .auth, window: window)
        }
    }
}

extension OnboardingRouter {
    static func createOnboardingViewController() -> OnboardingViewController {
        let vc = OnboardingViewController.storyboardInstance
        vc.router = OnboardingRouter()
        vc.viewModel = OnboardingViewModel()
        
        return vc
    }
}
