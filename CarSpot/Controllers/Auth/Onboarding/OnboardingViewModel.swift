//
//  OnboardingViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 21.05.21.
//

import Foundation

class OnboardingViewModel {
    
    //MARK: - Public API
    func finish() {
        UserDefaults.seenOnboarding = true
    }
}
