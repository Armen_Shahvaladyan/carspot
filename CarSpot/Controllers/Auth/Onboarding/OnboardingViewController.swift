//
//  OnboardingViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 21.05.21.
//

import UIKit

class OnboardingViewController: BaseViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: PageControl!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: GradientButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var previousButtonTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButtonWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var containerView: CornerShadowView! {
        didSet {
            containerView.setupOnboardingConfigure()
        }
    }
    
    //MARK: - Properties
    var router: OnboardingRouter!
    var viewModel: OnboardingViewModel!
    
    private let buttonWidth: CGFloat = 56
    private let buttonsPading: CGFloat = 16
    private var currentPage = 0
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
             
    }
    
    //MARK: - Private API
    private func finish() {
        viewModel.finish()
        router.perform(.auth, from: self)
    }
    
    @IBAction func previous() {
        currentPage -= 1
        pageControl.setPage(currentPage)
        skipButton.isHidden = false
        
        if currentPage < 2 && nextButtonWidthConstraint.constant != buttonWidth {
            nextButton.setImage(#imageLiteral(resourceName: "ic_right"), for: .normal)
            nextButton.setTitle(nil, for: .normal)
            nextButtonWidthConstraint.constant = buttonWidth
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
        if currentPage == 0 {
            previousButtonTrailingConstraint.constant = -buttonWidth
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
        var contentOffset = scrollView.contentOffset
        contentOffset.x =  CGFloat(currentPage) * scrollView.bounds.width
        scrollView.setContentOffset(contentOffset, animated: true)
    }
    
    @IBAction func next(_ sender: UIButton) {
        if currentPage == 2 {
            finish()
        }
        
        currentPage += 1
        skipButton.isHidden = currentPage == 2
        pageControl.setPage(currentPage)
        
        if currentPage >= 1 && previousButtonTrailingConstraint.constant == -buttonWidth {
            previousButtonTrailingConstraint.constant = buttonsPading
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
        if currentPage == 2 {
            nextButton.setImage(nil, for: .normal)
            nextButton.setTitle("Start", for: .normal)
            nextButtonWidthConstraint.constant = 109
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
        var contentOffset = scrollView.contentOffset
        contentOffset.x =  CGFloat(currentPage) * scrollView.bounds.width
        scrollView.setContentOffset(contentOffset, animated: true)
    }
    
    @IBAction func skip() {
        finish()
    }
}

extension OnboardingViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .onboarding
}
