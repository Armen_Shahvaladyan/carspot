//
//  MyPurchasesError.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 26.07.21.
//

import Foundation

enum MyPurchasesError: Error {
    case empty
    case unknown(String)
}

extension MyPurchasesError: Errorable {
    var message: String {
        switch self {
        case .empty:
            return ""
        case .unknown(let message):
            return message
        }
    }
}

extension MyPurchasesError: Equatable { }
