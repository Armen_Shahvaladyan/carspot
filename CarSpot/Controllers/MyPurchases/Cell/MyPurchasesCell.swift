//
//  MyPurchasesCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 12.06.21.
//

import UIKit

class MyPurchasesCell: TableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
       
        containerView.layer.cornerRadius = 16
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = UIColor.rgb(red: 243, green: 243, blue: 243, alpha: 1).cgColor
        containerView.layer.masksToBounds = true

        imageContainerView.layer.cornerRadius = 13
        imageContainerView.layer.masksToBounds = true
    }
    
    //MARK: - Public API
    func configure(with purchase: Purchase) {
        titleLabel.text = purchase.title
        subTitleLabel.text = purchase.description
    }
}
