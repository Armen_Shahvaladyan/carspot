//
//  MyPurchasesViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import Foundation

class MyPurchasesViewModel {
    
    //MARK: - Properties
    var purchases: [Purchase] = []
    private var pageNumber = 0
    private let provider = Provider<PurchaseEndPoint>()
    private let take = 40
    
    //MARK: - Public API
    func clean() {
        pageNumber = 0
        purchases = []
    }
    
    func incrementPage() {
        pageNumber += take
    }
    
    func fetchPurchases(completion: @escaping (MyPurchasesError?) -> Void) {
        let skip = pageNumber == 0 ? 0 : pageNumber + take
        provider.request(target: .purchases(skip, take),
                         type: [Purchase].self,
                         path: .models) { purchases, error in
            Thread.onMainThread {
                if let error = error {
                    completion(.unknown(error.message))
                } else if let purchases = purchases {
                    if purchases.isEmpty {
                        completion(.empty)
                    } else {
                        self.purchases += purchases
                        completion(nil)
                    }
                } else {
                    completion(.empty)
                }
            }
        }
    }
}
