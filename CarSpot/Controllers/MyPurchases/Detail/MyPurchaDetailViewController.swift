//
//  MyPurchaseDetailViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 13.06.21.
//

import UIKit

class MyPurchasDetailViewController: BaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var purchaseTitleLabel: UILabel!
    @IBOutlet weak var purchaseSubTitleLabel: UILabel!
    
    //MARK: - IBOutlets
    
    //MARK: - Properties
    var router: MyPurchasDetailRouter!
    var viewModel: MyPurchasDetailViewModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        imageContainerView.layer.cornerRadius = 13
        imageContainerView.layer.masksToBounds = true
    }
    
    //MARK: - Private API
    private func setup() {
        purchaseTitleLabel.text = viewModel.purchase.title
        purchaseSubTitleLabel.text = viewModel.purchase.description
    }
}

extension MyPurchasDetailViewController: PanModalPresentable {
    var cornerRadius: CGFloat {
        return 32
    }

    var panScrollable: UIScrollView? {
        return scrollView
    }

    var longFormHeight: PanModalHeight {
        return .contentHeight(scrollView.contentSize.height + 50)
    }

    var anchorModalToLongForm: Bool {
        return false
    }

    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .longForm = state
        else { return }

        panModalSetNeedsLayoutUpdate()
    }
}

extension MyPurchasDetailViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .purchases
}
