//
//  MyPurchaseDetailRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 13.06.21.
//

import UIKit

enum MyPurchasDetailSegue {
    
}

protocol MyPurchasDetailRoutable: Routable where SegueType == MyPurchasDetailSegue, SourceType == MyPurchasDetailViewController {

}

struct MyPurchasDetailRouter: MyPurchasDetailRoutable {
    
    func perform(_ segue: MyPurchasDetailSegue, from source: MyPurchasDetailViewController) {
        
    }
}

extension MyPurchasDetailRouter {
    static func createMyPurchaseDetailViewController(with purchase: Purchase) -> MyPurchasDetailViewController {
        let vc = MyPurchasDetailViewController.storyboardInstance
        vc.router = MyPurchasDetailRouter()
        vc.viewModel = MyPurchasDetailViewModel(purchase: purchase)
        
        return vc
    }
}
