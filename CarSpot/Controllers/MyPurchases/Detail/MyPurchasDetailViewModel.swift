//
//  MyPurchaseDetailViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 13.06.21.
//

import Foundation

class MyPurchasDetailViewModel {
    
    //MARK: - Properties
    let purchase: Purchase
    
    //MARK: - Lifecycle
    init(purchase: Purchase) {
        self.purchase = purchase
    }
}
