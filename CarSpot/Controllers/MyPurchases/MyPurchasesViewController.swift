//
//  MyPurchasesViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit
import CRRefresh

class MyPurchasesViewController: LeftSideMainViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var emptyView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var router: MyPurchasesRouter!
    var viewModel: MyPurchasesViewModel!
    
    lazy var refreshControl: UIRefreshControl = {
        let rc = UIRefreshControl()
        rc.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return rc
    }()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        setupTableView()
    }
    
    //MARK: - Private API
    private func setup() {
        title = "My purchases"
        
        showLoading(onView: self.view)
        viewModel.fetchPurchases { [weak self] error in
            guard let self = self else { return }
            Thread.onMainThread {
                self.hideLoading()
                if let error = error {
                    if error == .empty {
                        self.emptyView.isHidden = false
                    } else {
                        self.showAlert(with: error)
                    }
                } else {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset.top = 20
        tableView.refreshControl = refreshControl
        
        tableView.cr.addFootRefresh { [weak self] in
            guard let self = self else { return }
            self.viewModel.incrementPage()
            self.viewModel.fetchPurchases { [weak self] error in
                guard let self = self else { return }
                if let error = error {
                    if error == .empty {
                        self.tableView.cr.noticeNoMoreData()
                        self.tableView.reloadData()
                    } else {
                        self.showAlert(with: error)
                    }
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.tableView.cr.endLoadingMore()
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    @objc private func refresh() {
        viewModel.clean()
        tableView.cr.resetNoMore()
        
        viewModel.fetchPurchases { [weak self] error in
            guard let self = self else { return }
            Thread.onMainThread {
                if self.refreshControl.isRefreshing {
                    self.refreshControl.endRefreshing()
                }
                if let error = error,
                   error != .empty {
                    self.showAlert(with: error)
                } else {
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension MyPurchasesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.purchases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MyPurchasesCell.cell(table: tableView, indexPath: indexPath)
        cell.configure(with: viewModel.purchases[indexPath.row])
        return cell
    }
}

extension MyPurchasesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        router.perform(.detail(viewModel.purchases[indexPath.row]), from: self)
    }
}

extension MyPurchasesViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .purchases
}
