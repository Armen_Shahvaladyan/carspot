//
//  MyPurchasesRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

enum MyPurchasesSegue {
    case detail(Purchase)
}

protocol MyPurchasesRoutable: Routable where SegueType == MyPurchasesSegue, SourceType == MyPurchasesViewController {

}

struct MyPurchasesRouter: MyPurchasesRoutable {
    
    func perform(_ segue: MyPurchasesSegue, from source: MyPurchasesViewController) {
        switch segue {
        case .detail(let purchase):
            let vc = MyPurchasDetailRouter.createMyPurchaseDetailViewController(with: purchase)
            source.presentPanModal(vc)
        }
    }
}

extension MyPurchasesRouter {
    static func createMyPurchasesViewController() -> MyPurchasesViewController {
        let vc = MyPurchasesViewController.storyboardInstance
        vc.router = MyPurchasesRouter()
        vc.viewModel = MyPurchasesViewModel()
        
        return vc
    }
}
