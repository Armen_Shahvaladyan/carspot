//
//  HelpDetailViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 27.07.21.
//

import Foundation

class HelpDetailViewModel {
    
    //MARK: - Properties
    let title: String
    let item: HelpItem
    
    //MARK: - Lifecycle
    init(title: String, item: HelpItem) {
        self.title = title
        self.item = item
    }
}
