//
//  HelpDetailViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 27.07.21.
//

import UIKit

class HelpDetailViewController: BaseViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    //MARK: - Properties
    var router: HelpDetailRouter!
    var viewModel: HelpDetailViewModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    //MARK: - Private API
    private func setup() {
        hideBackItemTitle()
        
        titleLabel.text = viewModel.title
        descriptionTextView.text = viewModel.item.description
    }
}

extension HelpDetailViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .help
}
