//
//  HelpDetailDetailRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 27.07.21.
//

import UIKit

enum HelpDetailSegue {
    
}

protocol HelpDetailRoutable: Routable where SegueType == HelpDetailSegue, SourceType == HelpDetailViewController {

}

struct HelpDetailRouter: HelpDetailRoutable {
    
    func perform(_ segue: HelpDetailSegue, from source: HelpDetailViewController) {
        
    }
}

extension HelpDetailRouter {
    static func createHelpDetailViewController(with title: String, item: HelpItem) -> HelpDetailViewController {
        let vc = HelpDetailViewController.storyboardInstance
        vc.router = HelpDetailRouter()
        vc.viewModel = HelpDetailViewModel(title: title, item: item)
        
        return vc
    }
}
