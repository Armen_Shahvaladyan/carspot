//
//  HelpViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import Foundation

class HelpViewModel {
    
    //MARK: - Properties
    private(set) var helps: [Help] = []
    private let provider = Provider<HelpEndPoint>()
    
    //MARK: - Public API
    func fetchInfo(with search: String, completion: @escaping (CSError?) -> Void) {
        provider.request(target: .info(search),
                         type: [Help].self) { helps, error in
            Thread.onMainThread {
                if let error = error {
                    completion(error)
                } else if let helps = helps {
                    self.helps = helps
                    completion(nil)
                } else {
                    completion(nil)
                }
            }
        }
    }
}
