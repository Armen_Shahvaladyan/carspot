//
//  HelpViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

class HelpViewController: LeftSideMainViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var router: HelpRouter!
    var viewModel: HelpViewModel!
    
    private var searchBar: SearchBar!
    private let rasterSize: CGFloat = 10.0
    private var loadFinished = false
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureSearch()
        setup()
    }
    
    //MARK: - Private API
    private func setup() {
        title = "Help"
        
        tableView.delegate = self
        tableView.dataSource = self
        HelpHeaderView.register(table: tableView)
        search()
    }
    
    @objc private func search() {
        showLoading(onView: self.view)
        viewModel.fetchInfo(with: searchBar.text ?? "") { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.loadFinished = true
                self.tableView.reloadData()
            }
        }
    }
    
    private func configureSearch() {
        searchBar = searchBar(with: rasterSize, delegate: self)
        searchContainerView.addSubview(searchBar)
                
        let constraints = [
            searchBar.topAnchor.constraint(equalTo: searchContainerView.topAnchor),
            searchBar.leadingAnchor.constraint(equalTo: searchContainerView.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: searchContainerView.trailingAnchor),
            searchBar.bottomAnchor.constraint(equalTo: searchContainerView.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func searchBar(with rasterSize: CGFloat, delegate: SearchBarDelegate) -> SearchBar {
        let config = searchBarConfig(with: rasterSize)
        let bar = SearchBar(config: config)
        bar.delegate = delegate
        bar.placeholder = "How can we help?"
        bar.updateBackgroundImage(withRadius: 16,
                                  corners: [.allCorners],
                                  color: UIColor.rgb(red: 243, green: 243, blue: 245, alpha: 1))
        return bar
    }
    
    private func searchBarConfig(with rasterSize: CGFloat) -> SearchBarConfig {
        var config: SearchBarConfig = SearchBarConfig()
        config.rasterSize = rasterSize
        config.leftViewMode = .always
        config.useCancelButton = false
        config.cancelButtonTitle = "Cancel"
        config.cancelButtonTextAttributes = [.foregroundColor: UIColor.darkGray]
        config.textContentType = UITextContentType.fullStreetAddress.rawValue
        config.textAttributes = [.foregroundColor: UIColor("#2E2E33"),
                                 .font: UIFont.SF(.text, weight: .regular, size: 17)]
        return config
    }
}

extension HelpViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.helps.isEmpty ? loadFinished ? 1 : 0 : viewModel.helps.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.helps.isEmpty {
            return loadFinished ? 1 : 0
        } else if let items = viewModel.helps[section].items,
                  items.count > 0 {
            return items.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModel.helps.isEmpty {
            let cell = HelpEmptyCell.cell(table: tableView, indexPath: indexPath)
            cell.configure(with: searchBar.text ?? "")
            return cell
        } else {
            let cell = HelpCell.cell(table: tableView, indexPath: indexPath)
            if let items = viewModel.helps[indexPath.section].items {
                cell.configure(with: items[indexPath.row])
            }
            return cell
        }
    }
}

extension HelpViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        viewModel.helps.isEmpty ? 400 : 56
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if viewModel.helps.isEmpty { return nil }
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: HelpHeaderView.reuseIdentifier) as? HelpHeaderView
        header?.configure(with: viewModel.helps[section])
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        viewModel.helps.isEmpty ? 0.1 : 69
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel.helps.isEmpty { return }
        if let title = viewModel.helps[indexPath.section].title,
           let item = viewModel.helps[indexPath.section].items?[indexPath.row] {
            router.perform(.detail(title, item), from: self)
        }
    }
}

extension HelpViewController: SearchBarDelegate {
    func searchBar(_ searchBar: SearchBar, textDidChange text: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(search), object: nil)
        perform(#selector(search), with: nil, afterDelay: 0.75)
    }
}

extension HelpViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .help
}
