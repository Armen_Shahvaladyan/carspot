//
//  HelpHeaderView.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 27.07.21.
//

import UIKit

class HelpHeaderView: SectionTableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - Properties
    static let reuseIdentifier = "HelpHeaderView"
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.backgroundColor = .white
    }
    
    //MARK: - Public APi
    func configure(with help: Help) {
        titleLabel.text = help.title
    }
}
