//
//  HelpEmptyCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 27.07.21.
//

import UIKit

class HelpEmptyCell: TableViewCell {

    //MARK: - IBOutlet
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - Public API
    func configure(with text: String) {
        titleLabel.text = "No results found for \"\(text)\""
    }
}
