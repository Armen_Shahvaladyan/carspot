//
//  HelpCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 27.07.21.
//

import UIKit

class HelpCell: TableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - Public API
    func configure(with helpItem: HelpItem) {
        titleLabel.text = helpItem.subTitle
    }
}
