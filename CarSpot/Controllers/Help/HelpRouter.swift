//
//  HelpRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

enum HelpSegue {
    case detail(String, HelpItem)
}

protocol HelpRoutable: Routable where SegueType == HelpSegue, SourceType == HelpViewController {

}

struct HelpRouter: HelpRoutable {
    
    func perform(_ segue: HelpSegue, from source: HelpViewController) {
        switch segue {
        case let .detail(title, item):
            let vc = HelpDetailRouter.createHelpDetailViewController(with: title, item: item)
            source.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension HelpRouter {
    static func createHelpViewController() -> HelpViewController {
        let vc = HelpViewController.storyboardInstance
        vc.router = HelpRouter()
        vc.viewModel = HelpViewModel()
        
        return vc
    }
}
