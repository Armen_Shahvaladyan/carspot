//
//  TabBarController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

class TabBarController: UITabBarController, SideMenuItemContent {
    //MARK: - Properties
    var viewModel: TabBarViewModel!
    
    lazy var statusBarStyle: UIStatusBarStyle = {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .default
        }
    }()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
//        configureNavigationBar()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        statusBarStyle
    }
    
    //MARK: - Private API
    private func setup() {
        
        tabBar.tintColor = .appOrange
        tabBar.barTintColor = .white
        tabBar.unselectedItemTintColor = UIColor.rgb(red: 143, green: 143, blue: 153, alpha: 1)
        let attributes = [NSAttributedString.Key.font: UIFont.SF(.text, weight: .medium, size: 10)]
        UITabBarItem.appearance().setTitleTextAttributes(attributes, for: .normal)
        
        if #available(iOS 15.0, *) {
            let appearance = UITabBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = .white
            tabBar.standardAppearance = appearance
            tabBar.scrollEdgeAppearance = tabBar.standardAppearance
        }
    }
}
