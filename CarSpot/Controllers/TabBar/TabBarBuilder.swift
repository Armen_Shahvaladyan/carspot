//
//  TabBarBuilder.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

class TabBarBuilder {
    static func tabBarViewController(spots: [ Spot]?,
                                     offer: Offer?,
                                     status: ParkingStatus?) -> UITabBarController {
        let viewModel = TabBarViewModel()
        let tabBarController = TabBarController()
        
        tabBarController.viewModel = viewModel
        
        let homeViewController = HomeRouter.createHomeViewController(with: spots, offer: offer, status: status)
        let homeNavigationController = NavigationController(rootViewController: homeViewController)

        let transactionsViewController = TransactionsRouter.createTransactionsViewController()
        let transactionsNavigationController = NavigationController(rootViewController: transactionsViewController)

        let rewardsViewController = RewardsRouter.createRewardsViewController()
        let cartNavigationController = NavigationController(rootViewController: rewardsViewController)

        let notificationsController = NotificationsRouter.createNotificationsViewController()
        let notificationsNavigationController = NavigationController(rootViewController: notificationsController)
        
        let viewControllers = [homeNavigationController, transactionsNavigationController, cartNavigationController, notificationsNavigationController]
        tabBarController.viewControllers = viewControllers
        tabBarController.selectedIndex = 0
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            tabBarController.tabBar.itemPositioning = .centered
            tabBarController.tabBar.itemWidth = UIScreen.main.bounds.width / CGFloat(viewControllers.count)
            tabBarController.tabBar.itemSpacing = 0
        }
        
        return tabBarController
    }
}
