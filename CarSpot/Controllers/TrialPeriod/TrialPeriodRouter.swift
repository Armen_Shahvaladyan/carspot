//
//  TrialPeriodRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.08.21.
//

import UIKit

enum TrialPeriodSegue {
    case dismiss
    case allPlans([String]?)
}

protocol TrialPeriodRoutable: Routable where SegueType == TrialPeriodSegue, SourceType == TrialPeriodViewController {

}

struct TrialPeriodRouter: TrialPeriodRoutable {
    
    func perform(_ segue: TrialPeriodSegue, from source: TrialPeriodViewController) {
        switch segue {
        case .dismiss:
            source.dismiss(animated: true)
        case .allPlans(let subscriptions):
            let vc = AllPlansRouter.createAllPlansViewController(with: subscriptions)
            source.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension TrialPeriodRouter {
    static func createTrialPeriodViewController() -> TrialPeriodViewController {
        let vc = TrialPeriodViewController.storyboardInstance
        vc.router = TrialPeriodRouter()
        vc.viewModel = TrialPeriodViewModel()
        
        return vc
    }
}
