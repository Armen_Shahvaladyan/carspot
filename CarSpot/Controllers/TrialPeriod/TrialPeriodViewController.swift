//
//  TrialPeriodViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.08.21.
//

import UIKit

class TrialPeriodViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var freeTrialButton: Button!
    @IBOutlet weak var stackViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Properties
    var router: TrialPeriodRouter!
    var viewModel: TrialPeriodViewModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        setupNavigation()
        fetchState()
    }
    
    //MARK: - Private API
    private func setup() {
        freeTrialButton.isHidden = true
        freeTrialButton.buttonState = .secondary
        showLoading(onView: self.view)
        viewModel.fetchGlobal { [weak self] error, global in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else if let global = global {
                self.viewModel.subscriptions = global.aboutSubscription
                self.configure(with: global.aboutSubscription)
            }
        }
    }
    
    private func setupNavigation() {
        let closeItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_close"), style: .plain, target: self, action: #selector(close))
        self.navigationItem.leftBarButtonItem = closeItem
    }
    
    private func fetchState() {
        viewModel.fetchState { [weak self] error, state in
            guard let self = self else { return }
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.freeTrialButton.isHidden = state?.status != .idle
            }
        }
    }
    
    private func configure(with subscriptions: [String]) {
        if !subscriptions.isEmpty {
            stackView.removeConstraint(stackViewHeightConstraint)
        }
        
        for subscription in subscriptions {
            let v = PlanView.fromNib()
            v.configure(with: subscription)
            stackView.addArrangedSubview(v)
        }
    }
   
    @objc private func close() {
        router.perform(.dismiss, from: self)
    }
    
    //MARK: - IBActions
    @IBAction func allPlans() {
        router.perform(.allPlans(viewModel.subscriptions), from: self)
    }
    
    @IBAction func freeTrial() {
        showLoading(onView: self.view)
        viewModel.freeTrial { [weak self] error, success in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.router.perform(.dismiss, from: self)
            }
        }
    }
}

extension TrialPeriodViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .transactions
}
