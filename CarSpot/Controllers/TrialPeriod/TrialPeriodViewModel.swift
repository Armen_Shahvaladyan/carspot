//
//  TrialPeriodViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.08.21.
//

import Foundation

class TrialPeriodViewModel {
    
    //MARK: - Properties
    var subscriptions: [String]?
    private let globalProvider = Provider<GlobalEndPoint>()
    private let subscriptionProvider = Provider<SubscriptionEndPoint>()
    
    //MARK: - Public API
    func fetchGlobal(completion: @escaping (CSError?, AppGlobal?) -> Void) {
        globalProvider.request(target: .all,
                         type: AppGlobal.self) { global, error in
            completion(error, global)
        }
    }
    
    func fetchState(completion: @escaping (CSError?, SubscriptionState?) -> Void) {
        subscriptionProvider.request(target: .state,
                                     type: SubscriptionState.self) { state, error in
            completion(error, state)
        }
    }
    
    func freeTrial(completion: @escaping (CSError?, Bool) -> Void) {
        subscriptionProvider.request(target: .freeTrial,
                                     type: Bool.self) { success, error in
            completion(error, error == nil)
        }
    }
}
