//
//  WebViewType.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 27.07.21.
//

import Foundation

class WebViewModel {
    
    //MARK: - Properties
    let url: URL
    
    //MARK: - Lifecycle
    init(url: URL) {
        self.url = url
    }
}
