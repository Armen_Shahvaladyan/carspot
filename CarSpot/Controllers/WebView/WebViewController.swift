//
//  WebViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 27.07.21.
//

import SafariServices

class WebViewController: SFSafariViewController {
    
    //MARK: - Properties
    var router: WebViewRouter!
    let viewModel: WebViewModel
    
    init(with viewModel: WebViewModel, configuration: SFSafariViewController.Configuration) {
        self.viewModel = viewModel
        super.init(url: viewModel.url, configuration: configuration)
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        preferredBarTintColor = .black
        preferredControlTintColor = .white
    }
}
