//
//  WebViewRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 27.07.21.
//

import SafariServices

enum WebViewSegue {
    
}

protocol WebViewRoutable: Routable where SegueType == WebViewSegue, SourceType == WebViewController {

}

struct WebViewRouter: WebViewRoutable {
    func perform(_ segue: WebViewSegue, from source: WebViewController) {
        
    }
}

extension WebViewRouter {
    static func createWebViewController(url: URL, with configuration: SFSafariViewController.Configuration) -> WebViewController {
        let viewModel = WebViewModel(url: url)
        
        let vc = WebViewController(with: viewModel, configuration: configuration)
        vc.router = WebViewRouter()
        
        return vc
    }
}
