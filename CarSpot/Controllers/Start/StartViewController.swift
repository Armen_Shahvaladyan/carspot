//
//  StartViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 23.09.21.
//

import UIKit

class StartViewController: BaseViewController {

    //MARK: - Properties
    var router: StartRouter!
    var viewModel: StartViewModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        checkState()
    }
    
    //MARK: - Private API
    private func checkState() {
        viewModel.checkState { [weak self] (status, error) in
            guard let self = self else { return }
            if let error = error {
                self.showAlert(with: error)
            } else {
                if let status = status,
                   let window = self.view.window {
                    switch status.state {
                    case .find:
                        self.getFindDefault(with: status)
                    case .suggest:
                        self.getSuggestDefault(with: status)
                    case .book:
                        Launcher.perform(segue: .spotDetails(status), window: window)
                    case .finished:
                        print("asdfv")
//                        Launcher.perform(segue: .main(status), window: window)
                    case .none:
                        Launcher.perform(segue: .main, window: window)
                    }
                }
            }
        }
    }
    
    private func getFindDefault(with status: ParkingStatus) {
        viewModel.getFindDefault {  [weak self] (spots, error) in
            guard let self = self else { return }
            if let error = error {
                self.showAlert(with: error)
            } else if let spots = spots {
                self.router.perform(.findToHome(spots, status), from: self)
            }
        }
    }
    
    private func getSuggestDefault(with status: ParkingStatus) {
        viewModel.getSuggestDefault {  [weak self] (offer, error) in
            guard let self = self else { return }
            if let error = error {
                self.showAlert(with: error)
            } else if let offer = offer {
                self.router.perform(.suggestToHome(offer, status), from: self)
            }
        }
    }
}

extension StartViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .start
}
