//
//  StartRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 23.09.21.
//

import UIKit

enum StartSegue {
    case findToHome([Spot], ParkingStatus)
    case suggestToHome(Offer, ParkingStatus)
    case bookToHome(ParkingStatus)
}

protocol StartRoutable: Routable where SegueType == StartSegue, SourceType == StartViewController {

}

struct StartRouter: StartRoutable {
    
    func perform(_ segue: StartSegue, from source: StartViewController) {
        guard let window = source.view.window else { return }
        switch segue {
        case let .findToHome(spots, status):
            let vc = HostViewController()
            vc.status = status
            vc.spots = spots
            window.rootViewController = vc
            window.makeKeyAndVisible()
        case let .suggestToHome(offer, status):
            let vc = HostViewController()
            vc.status = status
            vc.offer = offer
            window.rootViewController = vc
            window.makeKeyAndVisible()
        case let .bookToHome(status):
            let vc = HostViewController()
            vc.status = status
            window.rootViewController = vc
            window.makeKeyAndVisible()
        }
    }
}

extension StartRouter {
    static func createStartViewController() -> StartViewController {
        let vc = StartViewController.storyboardInstance
        vc.router = StartRouter()
        vc.viewModel = StartViewModel()
        
        return vc
    }
}
