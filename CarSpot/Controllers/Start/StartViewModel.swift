//
//  StartViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 23.09.21.
//

import Foundation

class StartViewModel {
    //MARK: - Properties
    private let stateProvider = Provider<StateEndPoint>()
    private let findProvider = Provider<FindParkiingEndPoint>()
    private let suggestProvider = Provider<SuggestParkingEndPoint>()
    
    //MARK: - Public API
    func checkState(completion: @escaping (ParkingStatus?, CSError?) -> Void) {
        stateProvider.request(target: .parking,
                              type: ParkingStatus.self) { (status, error) in
            completion(status, error)
        }
    }
    
    func getFindDefault(completion: @escaping ([Spot]?, CSError?) -> Void) {
        findProvider.request(target: .defaultData,
                             type: [Spot].self) { (spots, error) in
            completion(spots, error)
        }
    }
    
    func getSuggestDefault(completion: @escaping (Offer?, CSError?) -> Void) {
        suggestProvider.request(target: .defaultData,
                                type: Offer.self) { (offer, error) in
            completion(offer, error)
        }
    }
}
