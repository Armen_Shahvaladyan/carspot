//
//  NotificationsViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit
import CRRefresh

class NotificationsViewController: MainTabBarViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var `switch`: UISwitch!
    
    //MARK: - Properties
    var router: NotificationsRouter!
    var viewModel: NotificationsViewModel!
    private var needLoadSettings = true
    
    lazy var refreshControl: UIRefreshControl = {
        let rc = UIRefreshControl()
        rc.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return rc
    }()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            
        if needLoadSettings {
            viewModel.fetchSettings { [weak self] (error, type) in
                guard let self = self else { return }
                if let error = error {
                    self.showAlert(with: error)
                } else {
                    self.switch.setOn(type?.option == .on, animated: false)
                }
            }
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        tabBarItem.title = "Notifications"
        tabBarItem.image = #imageLiteral(resourceName: "ic_notification").withRenderingMode(.alwaysTemplate)
    }
    
    //MARK: - Public API
    func change(option: NotificationOption?) {
        if let option = option {
            showLoading(onView: self.view)
            viewModel.off(with: option) { [weak self] error in
                guard let self = self else { return }
                self.hideLoading()
                self.needLoadSettings = true
                if let error = error {
                    self.switch.setOn(true, animated: true)
                    self.showAlert(with: error)
                } else {
                    self.switch.setOn(option == .on, animated: true)
                }
            }
        } else {
            self.switch.setOn(true, animated: true)
        }
    }
    
    //MARK: - Private API
    private func setup() {
        navigationItem.title = "Notifications"

        showLoading(onView: self.view)
        viewModel.fetchNotifications { [weak self] error in
            guard let self = self else { return }
            Thread.onMainThread {
                self.hideLoading()
                if let error = error {
                    if error == .empty {
                        self.emptyView.isHidden = false
                        self.tableView.isHidden = true
                    } else {
                        self.showAlert(with: error)
                    }
                } else {
                    self.tableView.isHidden = false
                    self.emptyView.isHidden = true
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.refreshControl = refreshControl
        
        tableView.cr.addFootRefresh { [weak self] in
            guard let self = self else { return }
            self.viewModel.incrementPage()
            self.viewModel.fetchNotifications { [weak self] error in
                guard let self = self else { return }
                if let error = error {
                    if error == .empty {
                        self.tableView.cr.noticeNoMoreData()
                        self.tableView.reloadData()
                    } else {
                        self.showAlert(with: error)
                    }
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.tableView.cr.endLoadingMore()
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    @objc private func refresh() {
        viewModel.clean()
        tableView.cr.resetNoMore()
        
        viewModel.fetchNotifications { [weak self] error in
            guard let self = self else { return }
            Thread.onMainThread {
                if self.refreshControl.isRefreshing {
                    self.refreshControl.endRefreshing()
                }
                if let error = error,
                   error != .empty {
                    self.showAlert(with: error)
                } else {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func deleteRow(at indexPath: IndexPath) {
        showLoading(onView: self.view)
        viewModel.delete(at: indexPath.row) { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func changeState(_ sender: UISwitch) {
        if sender.isOn {
            change(option: .on)
        } else {
            needLoadSettings = false
            router.perform(.option, from: self)
        }
    }
}

extension NotificationsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = NotificationCell.cell(table: tableView, indexPath: indexPath)
        cell.configure(with: viewModel.notifications[indexPath.row])
        return cell
    }
}

extension NotificationsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title: "Delete") { (action, view, completion) in
            self.deleteRow(at: indexPath)
            completion(true)
        }
        
        deleteAction.image = #imageLiteral(resourceName: "ic_close").withTintColor(.white)
        deleteAction.backgroundColor = UIColor.red
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
}

extension NotificationsViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .notification
}
