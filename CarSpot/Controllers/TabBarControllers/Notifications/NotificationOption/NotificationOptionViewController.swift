//
//  NotificationOptionViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 22.06.21.
//

import UIKit

class NotificationOptionViewController: UIViewController {

    //MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var router: NotificationOptionRouter!
    var viewModel: NotificationOptionViewModel!
    
    var didSelectOption: ((NotificationOption?) -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        didSelectOption?(viewModel.selectedOption)
    }
    
    //MARK: - Private API
    private func setup() {
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension NotificationOptionViewController: PanModalPresentable {
    var cornerRadius: CGFloat {
        return 32
    }

    var panScrollable: UIScrollView? {
        return tableView
    }

    var longFormHeight: PanModalHeight {
        return .contentHeight(tableView.contentSize.height + 50)
    }

    var anchorModalToLongForm: Bool {
        return false
    }

    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .longForm = state
        else { return }

        panModalSetNeedsLayoutUpdate()
    }
}

extension NotificationOptionViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = NotificationOptionCell.cell(table: tableView, indexPath: indexPath)
        cell.configure(with: viewModel.options[indexPath.row])
        return cell
    }
}

extension NotificationOptionViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let cell = tableView.cellForRow(at: indexPath) as? NotificationOptionCell
        cell?.cellSelected = true
        viewModel.selectOption(at: indexPath.row)
        router.perform(.dismiss, from: self)
    }
}

extension NotificationOptionViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .notification
}
