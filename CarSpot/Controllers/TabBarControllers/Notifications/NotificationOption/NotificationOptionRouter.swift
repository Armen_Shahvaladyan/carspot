//
//  NotificationOptionRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 22.06.21.
//

import UIKit

enum NotificationOptionSegue {
    case dismiss
}

protocol NotificationOptionRoutable: Routable where SegueType == NotificationOptionSegue, SourceType == NotificationOptionViewController {

}

struct NotificationOptionRouter: NotificationOptionRoutable {
    
    func perform(_ segue: NotificationOptionSegue, from source: NotificationOptionViewController) {
        switch segue {
        case .dismiss:
            source.dismiss(animated: true)
        }
    }
}

extension NotificationOptionRouter {
    static func createNotificationOptionViewController() -> NotificationOptionViewController {
        let vc = NotificationOptionViewController.storyboardInstance
        vc.router = NotificationOptionRouter()
        vc.viewModel = NotificationOptionViewModel()
        
        return vc
    }
}
