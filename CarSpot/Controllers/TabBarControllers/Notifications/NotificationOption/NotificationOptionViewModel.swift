//
//  NotificationOptionViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 22.06.21.
//

import UIKit

class NotificationOptionViewModel {
    
    //MARK: - Properties
    private(set) var selectedOption: NotificationOption?
    let options: [NotificationOption] = [.halfHour, .hour, .eightHours, .twentyFourHours, .off]
    
    //MARK: - Public API
    func selectOption(at index: Int) {
        selectedOption = options[index]
    }
}

enum NotificationOption: Int, Codable {
    case on              = 1
    case halfHour        = 2
    case hour            = 3
    case eightHours      = 4
    case twentyFourHours = 5
    case off             = 6
    
    var title: String {
        switch self {
        case .on:
            return ""
        case .halfHour:
            return "For 30 minutes"
        case .hour:
            return "For 1 hour"
        case .eightHours:
            return "For 8 hour"
        case .twentyFourHours:
            return "For 24 hour"
        case .off:
            return "Until I turn it back on"
        }
    }
    
    var image: UIImage? {
        switch self {
        case .off:
            return nil
        default:
            return #imageLiteral(resourceName: "ic_time")
        }
    }
}
