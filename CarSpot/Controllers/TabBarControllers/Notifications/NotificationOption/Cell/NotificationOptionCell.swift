//
//  NotificationOptionCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 22.06.21.
//

import UIKit

class NotificationOptionCell: TableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var checkMarkImageView: UIImageView!
    
    var cellSelected: Bool = false {
        didSet {
            checkMarkImageView.isHidden = !cellSelected
        }
    }
    
    //MARK: - Public API
    func configure(with option: NotificationOption) {
        titleLabel.text = option.title
        iconImageView.image = option.image
        checkMarkImageView.isHidden = true
        iconImageView.isHidden = option.image == nil
    }
}
