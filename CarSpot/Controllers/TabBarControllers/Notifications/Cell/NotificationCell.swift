//
//  NotificationCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 20.06.21.
//

import UIKit

class NotificationCell: TableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var coinLabel: UILabel!
    
    //MARK: - Public API
    func configure(with notification: Notification) {
        titleLabel.text = notification.title ?? ""
        descriptionLabel.text = notification.description ?? ""
        distanceLabel.text = notification.distance ?? ""
        durationLabel.text = notification.duration ?? ""
        
        if let coin = notification.coins {
            coinLabel.text = "\(coin) coin"
        } else {
            coinLabel.text = ""
        }
    }
}
