//
//  NotificationsRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

enum NotificationsSegue {
    case option
}

protocol NotificationsRoutable: Routable where SegueType == NotificationsSegue, SourceType == NotificationsViewController {

}

struct NotificationsRouter: NotificationsRoutable {
        
    func perform(_ segue: NotificationsSegue, from source: NotificationsViewController) {
        switch segue {
        case .option:
            let vc = NotificationOptionRouter.createNotificationOptionViewController()
            
            vc.didSelectOption = { option in
                source.change(option: option)
            }
            
            source.presentPanModal(vc)
        }
    }
}

extension NotificationsRouter {
    static func createNotificationsViewController() -> NotificationsViewController {
        let vc = NotificationsViewController.storyboardInstance
        vc.router = NotificationsRouter()
        vc.viewModel = NotificationsViewModel()
        
        return vc
    }
}
