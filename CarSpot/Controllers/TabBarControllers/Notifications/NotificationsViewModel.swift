//
//  NotificationsViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import Foundation

class NotificationsViewModel {
    
    //MARK: - Properties
    var notifications: [Notification] = []
    private var pageNumber = 0
    private let provider = Provider<NotificationEndPoint>()
    private let take = 20
    
    //MARK: - Public API
    func clean() {
        pageNumber = 0
    }
    
    func incrementPage() {
        pageNumber += take
    }
    
    func fetchSettings(completion: @escaping (CSError?, NotificationType?) -> Void) {
        provider.request(target: .settings,
                         type: NotificationType.self) { (type, error) in
            Thread.onMainThread {
                completion(error, type)
            }
        }
    }
    
    func fetchNotifications(completion: @escaping (NotificationsError?) -> Void) {
        let skip = pageNumber == 0 ? 0 : pageNumber + take
        provider.request(target: .notifications(skip, take),
                         type: [Notification].self,
                         path: .models) { notifications, error in
            Thread.onMainThread {
                if let error = error {
                    completion(.unknown(error.message))
                } else if let notifications = notifications {
                    if notifications.isEmpty {
                        completion(.empty)
                    } else {
                        if self.pageNumber == 0 {
                            self.notifications = notifications
                        } else {
                            self.notifications += notifications
                        }
                        completion(nil)
                    }
                } else {
                    completion(.empty)
                }
            }
        }
    }
    
    func delete(at index: Int, completion: @escaping (CSError?) -> Void) {
        provider.request(target: .delete(notifications[index].id),
                         type: Bool.self) { success, error in
            Thread.onMainThread {
                if let error = error {
                    completion(error)
                } else {
                    self.notifications.remove(at: index)
                    completion(nil)
                }
            }
        }
    }
    
    func off(with option: NotificationOption, completion: @escaping (CSError?) -> Void) {
        provider.request(target: .off(option),
                         type: Bool.self) { success, error in
            Thread.onMainThread {
                completion(error)
            }
        }
    }
}
