//
//  NotificationsError.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.07.21.
//

import Foundation

enum NotificationsError: Error {
    case empty
    case unknown(String)
}

extension NotificationsError: Errorable {
    var message: String {
        switch self {
        case .empty:
            return ""
        case .unknown(let message):
            return message
        }
    }
}

extension NotificationsError: Equatable { }
