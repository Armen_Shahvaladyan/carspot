//
//  OrdersViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import Foundation

class TransactionsViewModel {
    
    //MARK: - Properties
    var filter: TransactionFilter = TransactionFilter()
    var transactions: [Transaction] = []
    private let take = 20
    private var pageNumber = 0
    private let provider = Provider<TransactionEndPoint>()
    private let profileProvider = Provider<ProfileEndPoint>()
    
    //MARK: - Public API
    func clean() {
        pageNumber = 0
    }
    
    func incrementPage() {
        pageNumber += take
    }
    
    func fetchTransactions(completion: @escaping (TransactionsError?) -> Void) {
        let skip = pageNumber == 0 ? 0 : pageNumber + take
        provider.request(target: .transactions(filter, skip, take),
                         type: [Transaction].self,
                         path: .models) { transactions, error in
            if let error = error {
                completion(.unknown(error.message))
            } else if let transactions = transactions {
                if transactions.isEmpty {
                    completion(.empty)
                } else {
                    if self.pageNumber == 0 {
                        self.transactions = transactions
                    } else {
                        self.transactions += transactions
                    }
                    completion(nil)
                }
            } else {
                completion(.empty)
            }
        }
    }
    
    func fetchCoins(completion: @escaping (CSError?, Coin?) -> Void) {
        profileProvider.request(target: .coins,
                                type: Coin.self) { coin, error in
            completion(error, coin)
        }
    }
    
    func filterTransactions(completion: @escaping (TransactionsError?) -> Void) {
        pageNumber = 0
        provider.request(target: .transactions(filter, pageNumber, take),
                         type: [Transaction].self,
                         path: .models) { transactions, error in
            if let error = error {
                completion(.unknown(error.message))
            } else if let transactions = transactions {
                self.transactions = transactions
                completion(nil)
            } else {
                self.transactions = []
                completion(nil)
            }
        }
    }
}
