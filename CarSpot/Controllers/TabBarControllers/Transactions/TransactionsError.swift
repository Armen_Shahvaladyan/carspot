//
//  TransactionsError.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 30.07.21.
//

import Foundation

enum TransactionsError: Error {
    case empty
    case unknown(String)
}

extension TransactionsError: Errorable {
    var message: String {
        switch self {
        case .empty:
            return ""
        case .unknown(let message):
            return message
        }
    }
}

extension TransactionsError: Equatable { }
