//
//  OrdersRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

enum TransactionsSegue {
    case filter(TransactionFilter)
    case managements
    case topUp
    case plan
}

protocol TransactionsRoutable: Routable where SegueType == TransactionsSegue, SourceType == TransactionsViewController {

}

struct TransactionsRouter: TransactionsRoutable {
    
    func perform(_ segue: TransactionsSegue, from source: TransactionsViewController) {
        switch segue {
        case .filter(let filter):
            let vc = TransactionsFilterRouter.createTransactionsFilterViewController(with: filter)
            
            vc.didRest = {
                source.reset()
            }
            
            vc.didApply = { filter in
                source.filterTransactions(with: filter)
            }
            
            source.present(vc, animated: true)
        case .managements:
            print("managements")
        case .topUp:
            let vc = TopupRouter.createTopupViewController()
            source.presentPanModal(vc)
        case .plan:
            let vc = TrialPeriodRouter.createTrialPeriodViewController()
            let nc = NavigationController(rootViewController: vc)
            
            nc.modalPresentationStyle = .fullScreen
            nc.modalTransitionStyle = .crossDissolve
            source.present(nc, animated: true)
        }
    }
}

extension TransactionsRouter {
    static func createTransactionsViewController() -> TransactionsViewController {
        let vc = TransactionsViewController.storyboardInstance
        vc.router = TransactionsRouter()
        vc.viewModel = TransactionsViewModel()
        
        return vc
    }
}
