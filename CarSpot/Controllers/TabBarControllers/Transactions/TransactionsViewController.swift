//
//  OrdersViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit
import CRRefresh
import  SwiftMoment

class TransactionsViewController: MainTabBarViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var coinCountLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activeDateLabel: UILabel!
    @IBOutlet weak var orangeRoundView: UIView!
    @IBOutlet weak var whiteRoundView: UIView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var topUpButton: UIButton!
    
    //MARK: - Properties
    var router: TransactionsRouter!
    var viewModel: TransactionsViewModel!
    
    lazy var refreshControl: UIRefreshControl = {
        let rc = UIRefreshControl()
        rc.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return rc
    }()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        setupTableView()
        configureReset()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        viewModel.fetchCoins { [weak self] (error, coin) in
            guard let self = self else { return }
            if let error = error {
                self.showAlert(with: error)
            } else if let coin = coin {
                self.coinCountLabel.text = "\(coin.count.intValue) coin"
                if let activeDate = coin.activeDate,
                   let date = moment(activeDate)?.format("dd MMM YYYY") {
                    self.activeDateLabel.isHidden = false
                    self.activeDateLabel.text = "Active until: \(date)"
                } else {
                    self.activeDateLabel.isHidden = true
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        [orangeRoundView, whiteRoundView].forEach {
            $0?.layer.masksToBounds = true
            $0?.layer.cornerRadius = ($0?.bounds.height ?? 0) / 2
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        tabBarItem.title = "Transactions"
        tabBarItem.image = #imageLiteral(resourceName: "ic_orders").withRenderingMode(.alwaysTemplate)
    }

    //MARK: - Public API
    func filterTransactions(with filter: TransactionFilter) {
        self.viewModel.filter = filter
        tableView.cr.resetNoMore()
        configureReset()
        viewModel.filterTransactions { [weak self] error in
            guard let self = self else { return }
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.tableView.isHidden = false
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK: - Private API
    private func setup() {
        navigationItem.title = "Balance and transactions"
        
        let attributes: [NSAttributedString.Key: Any] = [.font: UIFont.SF(.display, weight: .regular, size: 16),
                                                         .foregroundColor: UIColor(white: 1, alpha: 0.5),
                                                         .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "Top up", attributes: attributes)
        topUpButton.setAttributedTitle(attributeString, for: .normal)
        
        showLoading(onView: self.view)
        viewModel.fetchTransactions { [weak self] error in
            guard let self = self else { return }
            Thread.onMainThread {
                self.hideLoading()
                if let error = error {
                    if error == .empty {
                        self.tableView.isHidden = true
                    } else {
                        self.showAlert(with: error)
                    }
                } else {
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.refreshControl = refreshControl
        
        tableView.cr.addFootRefresh { [weak self] in
            guard let self = self else { return }
            self.viewModel.incrementPage()
            self.viewModel.fetchTransactions { [weak self] error in
                guard let self = self else { return }
                if let error = error {
                    if error == .empty {
                        self.tableView.cr.noticeNoMoreData()
                        self.tableView.reloadData()
                    } else {
                        self.showAlert(with: error)
                    }
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.tableView.cr.endLoadingMore()
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func configureReset() {
        resetButton.isHidden = !viewModel.filter.hasFilter
        whiteRoundView.isHidden = !viewModel.filter.hasFilter
    }
    
    @objc private func refresh() {
        viewModel.clean()
        tableView.cr.resetNoMore()
        
        viewModel.fetchTransactions { [weak self] error in
            guard let self = self else { return }
            Thread.onMainThread {
                if self.refreshControl.isRefreshing {
                    self.refreshControl.endRefreshing()
                }
                if let error = error,
                   error != .empty {
                    self.showAlert(with: error)
                } else {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func openFilters() {
        router.perform(.filter(viewModel.filter), from: self)
    }
    
    @IBAction func openManagements() {
        router.perform(.managements, from: self)
    }
    
    @IBAction func reset() {
        viewModel.filter.reset()
        configureReset()
        filterTransactions(with: viewModel.filter)
    }
    
    @IBAction func topUp() {
        router.perform(.topUp, from: self)
    }
    
    @IBAction func plan() {
        router.perform(.plan, from: self)
    }
}

extension TransactionsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.transactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = TransactionCell.cell(table: tableView, indexPath: indexPath)
        cell.configure(with: viewModel.transactions[indexPath.row])
        return cell
    }
}

extension TransactionsViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .transactions
}
