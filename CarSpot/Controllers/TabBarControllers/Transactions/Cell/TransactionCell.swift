//
//  TransactionCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 24.06.21.
//

import UIKit
import  SwiftMoment

class TransactionCell: TableViewCell {

    //MARK: - IBOutlet
    @IBOutlet weak var coinLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    //MARK: - Public API
    func configure(with transaction: Transaction) {
        let type = transaction.coinsType
        nameLabel.text = type.name
        
        let sign = type == .earned ?  "+" : "-"
        coinLabel.text = "\(sign)\(transaction.coins) coin"
        coinLabel.textColor = type.color
                
        iconImageView.image = type.image
        iconImageView.tintColor = type.color
        
        let date = transaction.createdDate.convertToDate()
        if date.isEmpty {
            dateLabel.text = transaction.address
        } else {
            dateLabel.text = date  + " | " + transaction.address
        }
    }
}
