//
//  RangeSelectionViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.06.21.
//

import UIKit
import HorizonCalendar

final class RangeSelectionViewController: UIViewController {
    
    //MARK: - Properties
    lazy var calendarView = CalendarView(initialContent: makeContent())
    lazy var calendar = Calendar.current
    lazy var dayDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = calendar
        dateFormatter.locale = calendar.locale
        dateFormatter.dateFormat = DateFormatter.dateFormat(
            fromTemplate: "EEEE, MMM d, yyyy",
            options: 0,
            locale: calendar.locale ?? Locale.current)
        return dateFormatter
    }()
    
    var router: RangeSelectionRouter!
    var viewModel: RangeSelectionViewModel!
    
    var didSelectRange: ((CalendarSelection?) -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        setupNavigation()
    }
    
    //MARK: - Private API
    private func setup() {
        title = "Period"
        view.backgroundColor = .white
        view.addSubview(calendarView)
        
        calendarView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            calendarView.topAnchor.constraint(equalTo: view.topAnchor),
            calendarView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            calendarView.leadingAnchor.constraint(
                greaterThanOrEqualTo: view.layoutMarginsGuide.leadingAnchor),
            calendarView.trailingAnchor.constraint(
                lessThanOrEqualTo: view.layoutMarginsGuide.trailingAnchor),
            calendarView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            calendarView.widthAnchor.constraint(lessThanOrEqualToConstant: 375)
        ])
        
        calendarView.daySelectionHandler = { [weak self] day in
            guard let self = self else { return }
            
            switch self.viewModel.calendarSelection {
            case .singleDay(let selectedDay):
                if day > selectedDay {
                    self.viewModel.calendarSelection = .dayRange(selectedDay...day)
                } else {
                    self.viewModel.calendarSelection = .singleDay(day)
                }
            case .none, .dayRange:
                self.viewModel.calendarSelection = .singleDay(day)
            }
            
            self.calendarView.setContent(self.makeContent())
        }
    }
    
    private func setupNavigation() {
        let close = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_close"), style: .plain, target: self, action: #selector(close))
        navigationItem.leftBarButtonItem = close
        
        let apply = UIBarButtonItem(title: "Apply", style: .done, target: self, action: #selector(apply))
        apply.tintColor = .appOrange
        navigationItem.rightBarButtonItem = apply
    }
    
    private func makeContent() -> CalendarViewContent {
        let startDate = calendar.date(from: DateComponents(year: 2020, month: 01, day: 01))!
        let endDate = calendar.date(from: DateComponents(year: 2024, month: 12, day: 31))!
        
        let calendarSelection = self.viewModel.calendarSelection
        let dateRanges: Set<ClosedRange<Date>>
        if case .dayRange(let dayRange) = viewModel.calendarSelection,
            let lowerBound = calendar.date(from: dayRange.lowerBound.components),
            let upperBound = calendar.date(from: dayRange.upperBound.components) {
            dateRanges = [lowerBound...upperBound]
        } else {
            dateRanges = []
        }
        
        return CalendarViewContent(
            calendar: calendar,
            visibleDateRange: startDate...endDate,
            monthsLayout: .vertical(options: VerticalMonthsLayoutOptions()))
            
            .withInterMonthSpacing(40)
            .withVerticalDayMargin(8)
            .withHorizontalDayMargin(8)
            
            .withDayItemModelProvider { [weak self] day in
                let isSelectedStyle: Bool
                switch calendarSelection {
                case .singleDay(let selectedDay):
                    isSelectedStyle = day == selectedDay
                case .dayRange(let selectedDayRange):
                    isSelectedStyle = day == selectedDayRange.lowerBound || day == selectedDayRange.upperBound
                case .none:
                    isSelectedStyle = false
                }
                
                let dayAccessibilityText: String?
                if let date = self?.calendar.date(from: day.components) {
                    dayAccessibilityText = self?.dayDateFormatter.string(from: date)
                } else {
                    dayAccessibilityText = nil
                }
                
                return CalendarItemModel<DayView>(
                    invariantViewProperties: .init(isSelectedStyle: isSelectedStyle),
                    viewModel: .init(dayText: "\(day.day)", dayAccessibilityText: dayAccessibilityText))
            }
            
            .withDayRangeItemModelProvider(for: dateRanges) { dayRangeLayoutContext in
                CalendarItemModel<DayRangeIndicatorView>(
                    invariantViewProperties: .init(),
                    viewModel: .init(
                        framesOfDaysToHighlight: dayRangeLayoutContext.daysAndFrames.map { $0.frame }))
            }
    }
    
    @objc private func close() {
        router.perform(.dismiss, from: self)
    }
    
    @objc private func apply() {
        didSelectRange?(viewModel.calendarSelection)
        router.perform(.dismiss, from: self)
    }
}

