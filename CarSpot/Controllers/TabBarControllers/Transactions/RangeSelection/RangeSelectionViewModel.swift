//
//  RangeSelectionViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.06.21.
//

import HorizonCalendar

class RangeSelectionViewModel {
    
    //MARK: - Properties
    var calendarSelection: CalendarSelection?
    
    //MARK: - Lifecycle
    init(calendarSelection: CalendarSelection?) {
        self.calendarSelection = calendarSelection
    }
}

enum CalendarSelection {
    case singleDay(Day)
    case dayRange(DayRange)
}
