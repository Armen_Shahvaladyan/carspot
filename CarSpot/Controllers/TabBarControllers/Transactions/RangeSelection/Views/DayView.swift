//
//  DayView.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.06.21.
//

import HorizonCalendar
import UIKit

// MARK: - DayView

final class DayView: UIView {

  // MARK: Lifecycle

  init(invariantViewProperties: InvariantViewProperties) {
    dayLabel = UILabel()
    dayLabel.font = invariantViewProperties.isSelectedStyle ? invariantViewProperties.selectedFont : invariantViewProperties.font
    dayLabel.textAlignment = invariantViewProperties.textAlignment
    dayLabel.textColor = invariantViewProperties.isSelectedStyle ? invariantViewProperties.selectedColor : invariantViewProperties.textColor

    super.init(frame: .zero)

    addSubview(dayLabel)
    backgroundColor = invariantViewProperties.isSelectedStyle ? UIColor.appOrange.withAlphaComponent(0.4) : .clear
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: Internal

  var dayText: String {
    get { dayLabel.text ?? "" }
    set { dayLabel.text = newValue }
  }

  var dayAccessibilityText: String?

  var isHighlighted = false {
    didSet {
      updateHighlightIndicator()
    }
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    dayLabel.frame = bounds
    layer.cornerRadius = min(bounds.width, bounds.height) / 2
  }

  // MARK: Private

  private let dayLabel: UILabel

  private func updateHighlightIndicator() {
    backgroundColor = isHighlighted ? UIColor.black.withAlphaComponent(0.1) : .clear
  }

}

// MARK: UIAccessibility

extension DayView {

  override var isAccessibilityElement: Bool {
    get { true }
    set { }
  }

  override var accessibilityLabel: String? {
    get { dayAccessibilityText ?? dayText }
    set { }
  }

}

// MARK: CalendarItemViewRepresentable

extension DayView: CalendarItemViewRepresentable {

  struct InvariantViewProperties: Hashable {
    var font = UIFont.SF(.text, weight: .regular, size: 19)
    var selectedFont = UIFont.SF(.text, weight: .medium, size: 24)
    var textAlignment = NSTextAlignment.center
    var textColor: UIColor = .black
    var isSelectedStyle: Bool
    var selectedColor: UIColor = .appOrange
  }

  struct ViewModel: Equatable {
    let dayText: String
    let dayAccessibilityText: String?
  }

  static func makeView(
    withInvariantViewProperties invariantViewProperties: InvariantViewProperties)
    -> DayView
  {
    DayView(invariantViewProperties: invariantViewProperties)
  }

  static func setViewModel(_ viewModel: ViewModel, on view: DayView) {
    view.dayText = viewModel.dayText
    view.dayAccessibilityText = viewModel.dayAccessibilityText
  }

}
