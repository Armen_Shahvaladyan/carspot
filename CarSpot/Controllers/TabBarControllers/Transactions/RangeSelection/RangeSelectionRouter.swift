//
//  RangeSelectionRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.06.21.
//

import UIKit

enum RangeSelectionSegue {
    case dismiss
}

protocol RangeSelectionRoutable: Routable where SegueType == RangeSelectionSegue, SourceType == RangeSelectionViewController {

}

struct RangeSelectionRouter: RangeSelectionRoutable {
    
    func perform(_ segue: RangeSelectionSegue, from source: RangeSelectionViewController) {
        switch segue {
        case .dismiss:
            source.navigationController?.dismiss(animated: true)
        }
    }
}

extension RangeSelectionRouter {
    static func createRangeSelectionViewController(with calendarSelection: CalendarSelection?) -> RangeSelectionViewController {
        let vc = RangeSelectionViewController()
        vc.router = RangeSelectionRouter()
        vc.viewModel = RangeSelectionViewModel(calendarSelection: calendarSelection)
        
        return vc
    }
}
