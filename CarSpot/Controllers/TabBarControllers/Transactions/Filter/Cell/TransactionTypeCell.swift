//
//  TransactionTypeCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 24.06.21.
//

import UIKit

class TransactionTypeCell: CollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = 16
        containerView.layer.masksToBounds = true
    }
    
    //MARK: - Public API
    func configure(with type: CoinType, selected: Bool) {
        titleLabel.text = type.title
        titleLabel.textColor = selected ? .white : UIColor("2E2E33")
        containerView.backgroundColor = selected ? .appOrange : UIColor("F4F4F5")
    }
}
