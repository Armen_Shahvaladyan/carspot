//
//  TransactionsFilterViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 24.06.21.
//

import Foundation

class TransactionsFilterViewModel {
    
    //MARK: - Properties
    let coinTypes: [CoinType] = [.all, .earned, .spent]
    var calendarSelection: CalendarSelection?
    
    var filter: TransactionFilter!
    
    //MARK: - Lifecycle
    init(filter: TransactionFilter) {
        self.filter = filter
    }
    
    //MARK: - Public API
    func reset() {
        filter.reset()
    }
    
    func selectType(at index: Int) {
        filter.coinsType = coinTypes[index]
    }
}
