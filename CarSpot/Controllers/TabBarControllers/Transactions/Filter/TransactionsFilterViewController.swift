//
//  TransactionsFilterViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 24.06.21.
//

import UIKit
import HorizonCalendar
import SwiftMoment

class TransactionsFilterViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var rangeLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var rangeSlider: RangeSlider!
    @IBOutlet weak var periodView: SelectedViewWithIcon!
    @IBOutlet weak var applyButton: Button!
    
    //MARK: - Properties
    var router: TransactionsFilterRouter!
    var viewModel: TransactionsFilterViewModel!
    
    var didApply: ((TransactionFilter) -> Void)?
    var didRest: (() -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        UIView.animate(withDuration: 0.12) {
            self.view.backgroundColor = UIColor(white: 0, alpha: 0.4)
        }
    }
    
    //MARK: - Public API
    func selectPeriod(with calendarSelection: CalendarSelection?) {
        self.viewModel.calendarSelection = calendarSelection
       
        if let calendarSelection = calendarSelection {
            switch calendarSelection {
            case .dayRange(let range):
                self.viewModel.filter.period = calendarSelection
                let start = range.lowerBound
                let end = range.upperBound
                
                if let startDate = Calendar.current.date(from: start.components),
                   let endDate = Calendar.current.date(from: end.components) {
                    let startText = moment(startDate).format("d MMM, yyyy")
                    let endText = moment(endDate).format("d MMM, yyyy")
                    self.periodView.text = "\(startText) - \(endText)"
                } else {
                    self.periodView.text = ""
                    self.viewModel.filter.period = nil
                }
            default:
                self.periodView.text = ""
                self.viewModel.filter.period = nil
            }
        } else {
            self.periodView.text = ""
            self.viewModel.filter.period = nil
        }
        
        resetButton.isHidden = !viewModel.filter.hasFilter
        applyButton.buttonState = viewModel.filter.hasFilter ? .default : .disable
    }
    
    //MARK: - Private API
    private func setup() {
        periodView.delegate = self
        collectionView.delegate = self
        collectionView.dataSource = self
        containerView.layer.cornerRadius = 32
        containerView.layer.masksToBounds = true
        containerView.layer.maskedCorners = CACornerMask.topLeftTopRight
        
        configureViews()
    }
    
    private func dismiss() {
        UIView.animate(withDuration: 0.1) {
            self.view.backgroundColor = .clear
        } completion: { _ in
            self.dismiss(animated: true)
        }
    }
    
    private func configureViews() {
        resetButton.isHidden = !viewModel.filter.hasFilter
        let from = viewModel.filter.coinsFrom
        let to = viewModel.filter.coinsTo
        periodView.state = .default
        rangeSlider.lowerValue = from.doubleValue
        rangeSlider.upperValue = to.doubleValue
        rangeSlider.minimumValue = 10
        rangeSlider.maximumValue = 5000
        rangeLabel.text = "\(from) - \(to)"
        selectPeriod(with: viewModel.filter.period)
        applyButton.buttonState = viewModel.filter.hasFilter ? .default : .disable
    }
    
    //MARK: - IBActions
    @IBAction func changeRangeSliderValue(_ sender: RangeSlider) {
        resetButton.isHidden = false
        rangeLabel.text = "\(Int(sender.lowerValue)) - \(Int(sender.upperValue))"
        viewModel.filter.coinsFrom = sender.lowerValue.intValue
        viewModel.filter.coinsTo = sender.upperValue.intValue
        resetButton.isHidden = !viewModel.filter.hasFilter
        applyButton.buttonState = viewModel.filter.hasFilter ? .default : .disable
    }
    
    @IBAction func cancel() {
        dismiss()
    }
    
    @IBAction func apply(_ sender: Button) {
        didApply?(viewModel.filter)
        dismiss()
    }
    
    @IBAction func reset(_ sender: UIButton) {
        viewModel.reset()
        configureViews()
        collectionView.reloadData()
        didRest?()
    }
}

extension TransactionsFilterViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.coinTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = TransactionTypeCell.cell(collection: collectionView, indexPath: indexPath)
        
        let type = viewModel.coinTypes[indexPath.item]
        let selected = viewModel.filter.coinsType == type
        cell.configure(with: viewModel.coinTypes[indexPath.item], selected: selected)
        return cell
    }
}

extension TransactionsFilterViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.selectType(at: indexPath.item)
        resetButton.isHidden = !viewModel.filter.hasFilter
        applyButton.buttonState = viewModel.filter.hasFilter ? .default : .disable
        collectionView.reloadData()
    }
}

extension TransactionsFilterViewController: SelectedViewWithIconDelegate {
    func selectedViewWithIcon(_ view: SelectedViewWithIcon, didSelect text: String) {
        router.perform(.calendar(viewModel.calendarSelection), from: self)
    }
}

extension TransactionsFilterViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .transactions
}
