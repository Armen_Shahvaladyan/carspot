//
//  TransactionsFilterFilterRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 24.06.21.
//

import UIKit

enum TransactionsFilterSegue {
    case calendar(CalendarSelection?)
}

protocol TransactionsFilterRoutable: Routable where SegueType == TransactionsFilterSegue, SourceType == TransactionsFilterViewController {

}

struct TransactionsFilterRouter: TransactionsFilterRoutable {
        
    func perform(_ segue: TransactionsFilterSegue, from source: TransactionsFilterViewController) {
        switch segue {
        case .calendar(let selection):
            let vc = RangeSelectionRouter.createRangeSelectionViewController(with: selection)
            let nc = NavigationController(rootViewController: vc)
            nc.modalPresentationStyle = .fullScreen
           
            vc.didSelectRange = { calendarSelection in
                source.selectPeriod(with: calendarSelection)
            }
            
            source.present(nc, animated: true)
        }
    }
}

extension TransactionsFilterRouter {
    static func createTransactionsFilterViewController(with filter: TransactionFilter) -> TransactionsFilterViewController {
        let vc = TransactionsFilterViewController.storyboardInstance
        vc.router = TransactionsFilterRouter()
        vc.viewModel = TransactionsFilterViewModel(filter: filter)
        vc.modalPresentationStyle = .custom
        return vc
    }
}
