//
//  RewardsError.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 16.08.21.
//

import Foundation

enum RewardsError: Error {
    case empty
    case unknown(String)
}

extension RewardsError: Errorable {
    var message: String {
        switch self {
        case .empty:
            return ""
        case .unknown(let message):
            return message
        }
    }
}

extension RewardsError: Equatable { }
