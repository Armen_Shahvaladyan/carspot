//
//  InfoViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.06.21.
//

import Foundation

class InfoViewModel {
    var infos: [RewardInfo] = []
    
    //MARK: - Properties
    private let provider = Provider<GlobalEndPoint>()
    
    //MARK: - Public API
    func fetchRewardInfos(completion: @escaping (CSError?) -> Void) {
        provider.request(target: .rewards,
                         type: [RewardInfo].self) { infos, error in
            if let infos = infos {
                self.infos = infos
            }
            completion(error)
        }
    }
}
