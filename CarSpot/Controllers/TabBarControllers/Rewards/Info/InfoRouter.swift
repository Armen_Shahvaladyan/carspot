//
//  InfoRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.06.21.
//

import UIKit

enum InfoSegue {
    
}

protocol InfoRoutable: Routable where SegueType == InfoSegue, SourceType == InfoViewController {

}

struct InfoRouter: InfoRoutable {
    
    func perform(_ segue: InfoSegue, from source: InfoViewController) {
        
    }
}

extension InfoRouter {
    static func createInfoViewController() -> InfoViewController {
        let vc = InfoViewController.storyboardInstance
        vc.router = InfoRouter()
        vc.viewModel = InfoViewModel()
        
        return vc
    }
}
