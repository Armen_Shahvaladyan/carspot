//
//  InfoViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.06.21.
//

import UIKit

class InfoViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var router: InfoRouter!
    var viewModel: InfoViewModel!
    
    private var selectedIndexPath: IndexPath?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    //MARK: - Private API
    private func setup() {
        title = "How you earn"
        tableView.delegate = self
        tableView.dataSource = self
        
        self.showLoading(onView: self.view)
        viewModel.fetchRewardInfos { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.tableView.reloadData()
            }
        }
    }
}

extension InfoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.infos.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = InfoCell.cell(table: tableView, indexPath: indexPath)
        cell.configure(with: viewModel.infos[indexPath.row])
        cell.cellSelected = indexPath == selectedIndexPath
        return cell
    }
}

extension InfoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndexPath == indexPath {
            self.selectedIndexPath = nil
            tableView.reloadRows(at: [indexPath], with: .automatic)
            return
        }
        
        if let selectedIndexPath = selectedIndexPath {
            self.selectedIndexPath = nil
            tableView.reloadRows(at: [selectedIndexPath], with: .automatic)
        }
        
        self.selectedIndexPath = indexPath
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}

extension InfoViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .rewards
}
