//
//  InfoCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.06.21.
//

import UIKit

class InfoCell: TableViewCell {

    //MARK: - IBOutlet
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!

    var cellSelected: Bool = false {
        didSet {
            contentLabel.isHidden = !cellSelected
            arrowImageView.rotate(angle: cellSelected ? -(Double.pi / 2) : 2 * Double.pi)
        }
    }

    //MARK: - Public API
    func configure(with info: RewardInfo) {
        nameLabel.text = info.question
        contentLabel.text = info.answer
    }
}
