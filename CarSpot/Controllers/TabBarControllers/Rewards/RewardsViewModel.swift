//
//  CartViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import Foundation

class RewardsViewModel {
    //MARK: - Properties
    
    var rewards: [Reward] = []
    private let take = 20
    private var pageNumber = 0
    private let couponProvider = Provider<CouponEndPoint>()
    private let profileProvider = Provider<ProfileEndPoint>()
    
    //MARK: - Public API
    func clean() {
        pageNumber = 0
    }
    
    func incrementPage() {
        pageNumber += take
    }
    
    func fetchRewards(completion: @escaping (RewardsError?) -> Void) {
        let skip = pageNumber == 0 ? 0 : pageNumber + take
        couponProvider.request(target: .all(skip, take),
                         type: [Reward].self,
                         path: .models) { rewards, error in
            if let error = error {
                completion(.unknown(error.message))
            } else if let rewards = rewards {
                if rewards.isEmpty {
                    completion(.empty)
                } else {
                    if self.pageNumber == 0 {
                        self.rewards = rewards
                    } else {
                        self.rewards += rewards
                    }
                    completion(nil)
                }
            } else {
                completion(.empty)
            }
        }
    }
    
    func fetchCoins(completion: @escaping (TransactionsError?, Coin?) -> Void) {
        profileProvider.request(target: .coins,
                         type: Coin.self) { coin, error in
            Thread.onMainThread {
                if let error = error {
                    completion(.unknown(error.message), nil)
                } else {
                    completion(nil, coin)
                }
            }
        }
    }
}
