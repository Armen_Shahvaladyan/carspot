//
//  RewardsDetailViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.06.21.
//

import UIKit

class RewardsDetailViewController: BaseViewController {

    //MARK: - IBOutlets
    @IBOutlet var indicatorLeftViews: [UIView]!
    @IBOutlet var indicatorRightViews: [UIView]!
    @IBOutlet weak var blueThumbContainerView: UIView!
    @IBOutlet weak var goldThumbContainerView: UIView!
    @IBOutlet weak var platinumThumbContainerView: UIView!

    //MARK: - Prperties
    var router: RewardsDetailRouter!
    var viewModel: RewardsDetailViewModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    //MARK: - Private API
    private func setup() {
        navigationItem.title = "Level details"
                
        let blueThumbView = ThumbView.fromNib()
        blueThumbView.color = UIColor("30D2F2")
        blueThumbView.frame = blueThumbContainerView.bounds
        blueThumbContainerView.addSubview(blueThumbView)
        
        let goldThumbView = ThumbView.fromNib()
        goldThumbView.color = UIColor("E67E50")
        goldThumbView.frame = goldThumbContainerView.bounds
        goldThumbContainerView.addSubview(goldThumbView)
        
        let platinumThumbView = ThumbView.fromNib()
        platinumThumbView.color = UIColor("2E397B")
        platinumThumbView.frame = platinumThumbContainerView.bounds
        platinumThumbContainerView.addSubview(platinumThumbView)
        
        indicatorLeftViews.forEach {
            $0.layer.cornerRadius = 4
            $0.layer.masksToBounds = true
            $0.layer.maskedCorners = CACornerMask.leftSide
        }
        
        indicatorRightViews.forEach {
            $0.layer.cornerRadius = 4
            $0.layer.masksToBounds = true
            $0.layer.maskedCorners = CACornerMask.rightSide
        }
    }
}

extension RewardsDetailViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .rewards
}
