//
//  RewardsDetailDetailRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.06.21.
//

import Foundation

import UIKit

enum RewardsDetailSegue {
    case info
}

protocol RewardsDetailRoutable: Routable where SegueType == RewardsDetailSegue, SourceType == RewardsDetailViewController {

}

struct RewardsDetailRouter: RewardsDetailRoutable {
    
    func perform(_ segue: RewardsDetailSegue, from source: RewardsDetailViewController) {
        switch segue {
        case .info:
            let vc = InfoRouter.createInfoViewController()
            source.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension RewardsDetailRouter {
    static func createRewardsDetailViewController() -> RewardsDetailViewController {
        let vc = RewardsDetailViewController.storyboardInstance
        vc.router = RewardsDetailRouter()
        vc.viewModel = RewardsDetailViewModel()
        
        return vc
    }
}
