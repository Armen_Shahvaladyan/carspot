//
//  RewardsRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit

enum RewardsSegue {
    case info
    case detail
    case redeem(Reward)
}

protocol RewardsRoutable: Routable where SegueType == RewardsSegue, SourceType == RewardsViewController {

}

struct RewardsRouter: RewardsRoutable {
    
    func perform(_ segue: RewardsSegue, from source: RewardsViewController) {
        switch segue {
        case .info:
            let vc = InfoRouter.createInfoViewController()
            source.navigationController?.pushViewController(vc, animated: true)
        case .detail:
            let vc = RewardsDetailRouter.createRewardsDetailViewController()
            source.navigationController?.pushViewController(vc, animated: true)
        case .redeem(let reward):
            let vc = RedeemRouter.createRedeemViewController(with: reward)
            source.presentPanModal(vc)
        }
    }
}

extension RewardsRouter {
    static func createRewardsViewController() -> RewardsViewController {
        let vc = RewardsViewController.storyboardInstance
        vc.router = RewardsRouter()
        vc.viewModel = RewardsViewModel()
        
        return vc
    }
}
