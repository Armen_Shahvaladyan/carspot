//
//  CartViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 06.06.21.
//

import UIKit
import CRRefresh
import SwiftMoment

class RewardsViewController: MainTabBarViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var coinCountLabel: UILabel!
    @IBOutlet weak var activeDateLabel: UILabel!
    
    //MARK: - Properties
    var router: RewardsRouter!
    var viewModel: RewardsViewModel!
    
    private var attrs: [NSAttributedString.Key : Any] = [.font : UIFont.SF(.display, weight: .regular, size: 16),
                                                         .foregroundColor : UIColor("#2E2E33", alpha: 0.4),
                                                         .underlineStyle : 1]
    
    lazy var refreshControl: UIRefreshControl = {
        let rc = UIRefreshControl()
        rc.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return rc
    }()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        setupTableView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        tabBarItem.title = "Rewards"
        tabBarItem.image = #imageLiteral(resourceName: "ic_cart").withRenderingMode(.alwaysTemplate)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        showCoins()
    }
    
    //MARK: - Private API
    private func setup() {
        navigationItem.title = "Rewards"
        
        let info = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_info"), style: .plain, target: self, action: #selector(openInfo))
        navigationItem.rightBarButtonItem = info
        
        showLoading(onView: self.view)
        viewModel.fetchRewards { [weak self] error in
            guard let self = self else { return }
            Thread.onMainThread {
                self.hideLoading()
                if let error = error {
                    if error != .empty {
                        self.showAlert(with: error)
                    }
                } else {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.refreshControl = refreshControl
        
        tableView.cr.addFootRefresh { [weak self] in
            guard let self = self else { return }
            self.viewModel.incrementPage()
            self.viewModel.fetchRewards { [weak self] error in
                guard let self = self else { return }
                if let error = error {
                    if error == .empty {
                        self.tableView.cr.noticeNoMoreData()
                        self.tableView.reloadData()
                    } else {
                        self.showAlert(with: error)
                    }
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.tableView.cr.endLoadingMore()
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func showCoins() {
        viewModel.fetchCoins { [weak self] (error, coin) in
            guard let self = self else { return }
            if let error = error {
                self.showAlert(with: error)
            } else if let coin = coin {
                self.coinCountLabel.text = "\(coin.count.intValue) coin"
                if let activeDate = coin.activeDate,
                   let date = moment(activeDate)?.format("dd MMM YYYY") {
                    self.activeDateLabel.isHidden = false
                    self.activeDateLabel.text = "Active until: \(date)"
                } else {
                    self.activeDateLabel.isHidden = true
                }
            }
        }
    }
    
    @objc private func refresh() {
        viewModel.clean()
        tableView.cr.resetNoMore()
        
        viewModel.fetchRewards { [weak self] error in
            guard let self = self else { return }
            Thread.onMainThread {
                if self.refreshControl.isRefreshing {
                    self.refreshControl.endRefreshing()
                }
                if let error = error,
                   error != .empty {
                    self.showAlert(with: error)
                } else {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    @objc private func openInfo() {
        router.perform(.info, from: self)
    }
}

extension RewardsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.rewards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = RewardCell.cell(table: tableView, indexPath: indexPath)
        cell.configure(with: viewModel.rewards[indexPath.row])
        return cell
    }
}

extension RewardsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let reward = viewModel.rewards[indexPath.row]
        router.perform(.redeem(reward), from: self)
    }
}

extension RewardsViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .rewards
}
