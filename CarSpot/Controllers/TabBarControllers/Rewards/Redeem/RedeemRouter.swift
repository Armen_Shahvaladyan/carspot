//
//  RedeemRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.06.21.
//

import UIKit

enum RedeemSegue {
    case dismiss
}

protocol RedeemRoutable: Routable where SegueType == RedeemSegue, SourceType == RedeemViewController {

}

struct RedeemRouter: RedeemRoutable {
    
    func perform(_ segue: RedeemSegue, from source: RedeemViewController) {
        switch segue {
        case .dismiss:
            source.dismiss(animated: true)
        }
    }
}

extension RedeemRouter {
    static func createRedeemViewController(with reward: Reward) -> RedeemViewController {
        let vc = RedeemViewController.storyboardInstance
        vc.router = RedeemRouter()
        vc.viewModel = RedeemViewModel(reward: reward)
        
        return vc
    }
}
