//
//  RedeemViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.06.21.
//

import UIKit

class RedeemViewController: BaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pointValueLabel: UILabel!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    //MARK: - Properties
    var router: RedeemRouter!
    var viewModel: RedeemViewModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        imageContainerView.layer.cornerRadius = 13
        imageContainerView.layer.masksToBounds = true
    }
    
    //MARK: - Private API
    private func setup() {
        titleLabel.text = viewModel.reward.title
        subTitleLabel.text = viewModel.reward.description
        pointValueLabel.text = "\(viewModel.reward.priceCoins.removeZerosFromEnd()) pts"
        imageView.setImage(path: viewModel.reward.fullPath, #imageLiteral(resourceName: "ic_pin"))
    }
    
    //MARK: - IBActions
    @IBAction func buy(_ sender: Button) {
        sender.buttonState = .load
        viewModel.buy { [weak self] error in
            guard let self = self else { return }
            sender.buttonState = .default
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.router.perform(.dismiss, from: self)
            }
        }
    }
}

extension RedeemViewController: PanModalPresentable {
    var cornerRadius: CGFloat {
        return 32
    }

    var panScrollable: UIScrollView? {
        return scrollView
    }

    var longFormHeight: PanModalHeight {
        return .contentHeight(scrollView.contentSize.height)
    }

    var anchorModalToLongForm: Bool {
        return false
    }

    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .longForm = state
        else { return }

        panModalSetNeedsLayoutUpdate()
    }
}

extension RedeemViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .rewards
}
