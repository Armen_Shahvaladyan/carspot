//
//  RedeemViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.06.21.
//

import Foundation

class RedeemViewModel {
    
    //MARK: - Properties
    let reward: Reward
    private let couponProvider = Provider<CouponEndPoint>()
    
    //MARK: - Lifecycle
    init(reward: Reward) {
        self.reward = reward
    }
    
    //MARK: - Public API
    func buy(completion: @escaping (RewardsError?) -> Void) {
        couponProvider.request(target: .buy(reward.id),
                               type: Bool.self) { _, error in
            if let error = error {
                completion(.unknown(error.message))
            } else  {
                completion(nil)
            }
        }
    }
}
