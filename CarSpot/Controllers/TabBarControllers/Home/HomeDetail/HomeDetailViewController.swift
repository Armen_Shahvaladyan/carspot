//
//  HomeDetailViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 07.07.21.
//

import UIKit
import KeychainAccess

class HomeDetailViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var addressContainerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var findOrNextButton: Button!
    @IBOutlet weak var addressButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    
    //MARK: - Properties
    var router: HomeDetailRouter!
    var viewModel: HomeDetailViewModel!
    
    weak var homeViewController: HomeViewController?
    private var behavior: CollectionViewPeekingBehavior!
    
    var status: HomeDetailStatus = .find(false) {
        didSet {
            findOrNextButton.title = status.title
            switch status {
            case .suggest(let selected):
                addressButton.isUserInteractionEnabled = !selected
            case .find(let selected):
                addressButton.isUserInteractionEnabled = !selected
            }
        }
    }
        
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        configureCollection()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    
        addressContainerView.layer.cornerRadius = 16
        addressContainerView.layer.masksToBounds = true
    }
    
    //MARK: - Public API
    func update(offer: Offer) {
        status = .suggest(true)
        update(spots: offer.spots)
    }
    
    func update(spots: [Spot]) {
        viewModel.spots = spots
        collectionView.reloadData()
    }
    
    func updateAddress(_ address: String?, _ lat: Double?, _ lng: Double?) {
        addressLabel.text = address
        viewModel.lat = lat
        viewModel.lng = lng
        viewModel.address = address
    }
    
    func updateOffers() {
        collectionView.reloadData()
    }
    
    //MARK: - Private API
    private func setup() {
        view.backgroundColor = .clear
        status = .find(false)
    }
    
    private func configureCollection() {
        let peekWidth: CGFloat = 25
        let spacing: CGFloat = 0
        
        behavior = CollectionViewPeekingBehavior(cellSpacing: spacing,
                                                 cellPeekWidth: peekWidth)
        collectionView.configureForPeekingBehavior(behavior: behavior)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset.left = -15
    }
    
    @IBAction func findOrNext(_ sender: Button) {
        switch status {
        case .find(let selected):
            if selected {
                status = .find(false)
                homeViewController?.stopFindParking()
            } else {
                homeViewController?.findParking()
            }
        case .suggest(let selected):
            if selected {
                status = .suggest(false)
                homeViewController?.stopOffer()
            } else {
                guard let address = viewModel.address,
                      let lat = viewModel.lat,
                      let lng = viewModel.lng else { return }
                homeViewController?.addOffer(with: address, lat: lat, lng: lng)
            }
        }
    }
    
    @IBAction func openAddress(_ sender: UIButton) {
        homeViewController?.openAddressChooser()
    }
}

extension HomeDetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.spots.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch status {
        case .find:
            let cell = BookOfferCell.cell(collection: collectionView, indexPath: indexPath)
            let viewModel = viewModel.bookOfferCellViewModel(at: indexPath)
            cell.configure(with: viewModel)
            cell.delegate = self
            return cell
        case .suggest:
            let cell = AcceptOfferCell.cell(collection: collectionView, indexPath: indexPath)
            let viewModel = viewModel.acceptOfferCellViewModel(at: indexPath)
            cell.configure(with: viewModel)
            cell.delegate = self
            return cell
        }
    }
}

extension HomeDetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 318, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}

extension HomeDetailViewController: UICollectionViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        behavior.scrollViewWillEndDragging(scrollView, withVelocity: velocity, targetContentOffset: targetContentOffset)
    }
}

extension HomeDetailViewController: AcceptOfferCellDelegate {
    func acceptOfferCell(_ cell: AcceptOfferCell, didAccept spot: Spot) {
        homeViewController?.accept(spot: spot)
    }
}

extension HomeDetailViewController: BookOfferCellDelegate {
    func bookOfferCell(_ cell: BookOfferCell, didSend status: ParkingStatus) {
        homeViewController?.didReceive(status: status)
    }
    
    func bookOfferCell(_ cell: BookOfferCell, didSend error: CSError) {
        homeViewController?.didReceive(error: error)
    }
}

extension HomeDetailViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .home
}
