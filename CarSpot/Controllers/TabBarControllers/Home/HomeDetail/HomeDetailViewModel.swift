//
//  HomeDetailViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 07.07.21.
//

import Foundation

class HomeDetailViewModel {
    
    //MARK: - Properties
    var address: String?
    var lat: Double?
    var lng: Double?
    var spots: [Spot] = []
    
    //MARK: - Public API
    func acceptOfferCellViewModel(at indexPath: IndexPath) -> AcceptOfferCellViewModel {
        AcceptOfferCellViewModel(spot: spots[indexPath.item])
    }
    
    func bookOfferCellViewModel(at indexPath: IndexPath) -> BookOfferCellViewModel {
        BookOfferCellViewModel(spot: spots[indexPath.item])
    }
}

enum HomeDetailStatus {
    case find(Bool)
    case suggest(Bool)
    
    var title: String {
        switch self {
        case .find(let selected):
            return selected ? Action.stop.localized : Action.find.localized
        case .suggest(let selected):
            return selected ? Action.stop.localized : Action.next.localized
        }
    }
    
    var loaderTitle: String {
        switch self {
        case .find:
            return Home.findLoaderTitle.localized
        case .suggest:
            return Home.suggestLoaderTitle.localized
        }
    }
}
