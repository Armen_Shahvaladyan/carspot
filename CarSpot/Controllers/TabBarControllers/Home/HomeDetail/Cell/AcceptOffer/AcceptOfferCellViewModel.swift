//
//  AcceptOfferCellViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.08.21.
//

import Foundation

class AcceptOfferCellViewModel {
    
    //MARK: - Properties
    let spot: Spot
    
    //MARK: - Lifecycle
    init(spot: Spot) {
        self.spot = spot
    }
}
