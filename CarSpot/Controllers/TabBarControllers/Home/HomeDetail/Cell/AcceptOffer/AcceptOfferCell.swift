//
//  AcceptOfferCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 11.07.21.
//

import UIKit

protocol AcceptOfferCellDelegate: AnyObject {
    func acceptOfferCell(_ cell: AcceptOfferCell, didAccept spot: Spot)
}

class AcceptOfferCell: CollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var coinCountLabel: UILabel!
    @IBOutlet weak var acceptLabel: UILabel!
    @IBOutlet weak var acceptButton: Button!
    
    //MARK: - Properties
    weak var delegate: AcceptOfferCellDelegate?
    private var spot: Spot!
    
    //MARK: - Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageView.layer.cornerRadius = 14
        imageView.layer.masksToBounds = true
    }
    
    //MARK: - Public API
    func configure(with viewModel: AcceptOfferCellViewModel) {
        spot = viewModel.spot
        
        if let path = spot.picturePath {
            let fullPath = Environment.live.baseURL + path
            imageView.setImage(path: fullPath, #imageLiteral(resourceName: "ic_offer_image"))
        }

        nameLabel.text = spot.fullName
        rateLabel.text = spot.rate
        distanceLabel.text = spot.distanse
        durationLabel.text = spot.time
        coinCountLabel.text = "\(spot.amount.removeZerosFromEnd()) coin"
        acceptButton.isHidden = spot.isSuggested
    }
    
    //MARK: - IBActions
    @IBAction func accept() {
        delegate?.acceptOfferCell(self, didAccept: spot)
    }
}
