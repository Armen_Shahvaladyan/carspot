//
//  BookOfferCellViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 14.09.21.
//

import Foundation

class BookOfferCellViewModel {
    
    //MARK: - Properties
    let spot: Spot
    private let bookProvider = Provider<BookEndPoint>()
    
    //MARK: - Lifecycle
    init(spot: Spot) {
        self.spot = spot
    }
    
    //MARK: - Public API
    func book(completion: @escaping (ParkingStatus?, CSError?) -> Void) {
        bookProvider.request(target: .add(spot.id),
                                type: ParkingStatus.self) { (status, error) in
            completion(status, error)
        }
    }
}
