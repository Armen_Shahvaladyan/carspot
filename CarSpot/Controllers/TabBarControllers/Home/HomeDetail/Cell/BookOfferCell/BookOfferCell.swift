//
//  BookOfferCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 12.07.21.
//

import UIKit

protocol BookOfferCellDelegate: AnyObject {
    func bookOfferCell(_ cell: BookOfferCell, didSend status: ParkingStatus)
    func bookOfferCell(_ cell: BookOfferCell, didSend error: CSError)
}

class BookOfferCell: CollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var containerView: ShadowView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var coinCountLabel: UILabel!
    
    //MARK: - Properties
    weak var delegate: BookOfferCellDelegate?
    private var viewModel: BookOfferCellViewModel!
    
    //MARK: - Public API
    func configure(with viewModel: BookOfferCellViewModel) {
        self.viewModel = viewModel
        addressLabel.text = viewModel.spot.address
        distanceLabel.text = viewModel.spot.distanse
        timeLabel.text = viewModel.spot.time
        coinCountLabel.text = "\(viewModel.spot.amount.removeZerosFromEnd()) coin"
    }
    
    //MARK: - IBActions
    @IBAction func book(_ sender: Button) {
        sender.buttonState = .load
        viewModel.book { [weak self] (status, error) in
            guard let self = self else { return }
            sender.buttonState = .default
            if let error = error {
                self.delegate?.bookOfferCell(self, didSend: error)
            } else if let status = status {
                self.delegate?.bookOfferCell(self, didSend: status)
            }
        }
    }
}
