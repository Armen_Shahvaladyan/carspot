//
//  HomeDetailDetailRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 07.07.21.
//

import UIKit

enum HomeDetailSegue {
    
}

protocol HomeDetailRoutable: Routable where SegueType == HomeDetailSegue, SourceType == HomeDetailViewController {

}

struct HomeDetailRouter: HomeDetailRoutable {
    
    func perform(_ segue: HomeDetailSegue, from source: HomeDetailViewController) {
        
    }
}

extension HomeDetailRouter {
    static func createHomeDetailViewController(homeViewController: HomeViewController?) -> HomeDetailViewController {
        let vc = HomeDetailViewController.storyboardInstance
        vc.router = HomeDetailRouter()
        vc.viewModel = HomeDetailViewModel()
        vc.homeViewController = homeViewController
        
        return vc
    }
}
