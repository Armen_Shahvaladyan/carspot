//
//  HomeError.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 29.08.21.
//

import Foundation

enum HomeError: Error {
    case address
    case unknown(String)
    case locationDisable
    case locationDenied
}

extension HomeError: Errorable {
    var message: String {
        switch self {
        case .address:
            return "Please select address"
        case .locationDisable:
            return "Turn on Location Services to Allow \(Bundle.main.displayName ?? "CarSpot") Determinate Your Location"
        case .locationDenied:
            return "Please give access of location for the correct work of app"
        case .unknown(let message):
            return message
        }
    }
}

extension HomeError: Equatable { }
