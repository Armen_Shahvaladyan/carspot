//
//  SpotDetailsContainerViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 16.09.21.
//

import Foundation

class SpotDetailsContainerViewModel {
    
    //MARK: - Properties
    var status: ParkingStatus?
    private let locationProvider = Provider<LocationEndPoint>()
    
    //MARK: - Lifecycle
    init(status: ParkingStatus?) {
        self.status = status
    }
    
    //MARK: - Public API
    func shareLocation(lat: Double, lng: Double, completion: @escaping (CSError?) -> Void) {
        locationProvider.request(target: .share(lat, lng),
                                 type: Empty.self) { _, error in
            completion(error)
        }
    }
}
