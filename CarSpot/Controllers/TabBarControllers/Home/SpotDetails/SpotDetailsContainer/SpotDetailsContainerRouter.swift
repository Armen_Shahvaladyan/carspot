//
//  SpotDetailsContainerContainerRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 16.09.21.
//

import UIKit

enum SpotDetailsContainerSegue {
    case home
    case rate(ParkingStatus)
}

protocol SpotDetailsContainerRoutable: Routable where SegueType == SpotDetailsContainerSegue, SourceType == SpotDetailsContainerViewController {

}

struct SpotDetailsContainerRouter: SpotDetailsContainerRoutable {
    
    func perform(_ segue: SpotDetailsContainerSegue, from source: SpotDetailsContainerViewController) {
        switch segue {
        case .home:
            guard let window = source.view.window else { return }
            Launcher.perform(segue: .main, window: window)
        case .rate(let status):
            let vc = RateRouter.createRateViewController(with: status)
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .fullScreen
            source.present(vc, animated: true)
        }
    }
}

extension SpotDetailsContainerRouter {
    static func createSpotDetailsContainerViewController(with status: ParkingStatus?) -> SpotDetailsContainerViewController {
        let vc = SpotDetailsContainerViewController.storyboardInstance
        vc.router = SpotDetailsContainerRouter()
        vc.viewModel = SpotDetailsContainerViewModel(status: status)
        
        return vc
    }
}
