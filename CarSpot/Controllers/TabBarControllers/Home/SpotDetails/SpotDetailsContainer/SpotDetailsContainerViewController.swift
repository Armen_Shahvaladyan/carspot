//
//  SpotDetailsContainerViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 16.09.21.
//

import UIKit
import FloatingPanel
import GoogleMaps
import MapKit

class SpotDetailsContainerViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var mapView: MapView!
    
    //MARK: - Properties
    var router: SpotDetailsContainerRouter!
    var viewModel: SpotDetailsContainerViewModel!
    private let locationService = LocationService()
    
    lazy var fpc = FloatingPanelController()
    lazy var spotDetailsViewController = SpotDetailsRouter.createSpotDetailsViewController(with: viewModel.status)
    private let signalRService = SignalRService(url: URL(string: Environment.live.wssURL)!)
    private var shownMyLocation = false
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        updateMapView()
        setupFloatingPanel()
        setupSignalService()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        fpc.invalidateLayout()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Private API
    private func setup() {
        mapView.isMyLocationEnabled = false
        locationService.delegate = self
        locationService.distanceFilter = 0
        spotDetailsViewController.openRate = { [weak self] in
            guard let self = self,
                  let status = self.viewModel.status else { return }
            
            self.router.perform(.rate(status), from: self)
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector:#selector(appMovedToForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        checkLocationState()
    }
    
    private func updateMapView() {
        guard let status = viewModel.status,
              let lat = status.lat,
              let lng = status.lng,
              let spotType = status.spotType else { return }
        
        mapView.clear()
        
        let position = CLLocationCoordinate2D(latitude: lat,
                                              longitude: lng)
        mapView.camera = GMSCameraPosition(target: position,
                                           zoom: 15,
                                           bearing: 0,
                                           viewingAngle: 0)
        
        let marker = GMSMarker(position: position)
        let imageView: UIImageView
        switch spotType {
        case .find:
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 31, height: 51))
            imageView.image = UIImage(named: "ic_car")
        case .suggest:
            imageView = UIImageView(image: #imageLiteral(resourceName: "ic_spot_location"))
            imageView.tintColor = .appOrange
        }
        marker.iconView = imageView
        marker.map = mapView
    }
    
    private func setupFloatingPanel() {
        fpc.delegate = self
        let appearance = SurfaceAppearance()
        appearance.cornerRadius = 32
        fpc.contentMode = .fitToBounds
        fpc.surfaceView.appearance = appearance
        fpc.set(contentViewController: spotDetailsViewController)
        fpc.addPanel(toParent: self)
    }
    
    private func setupSignalService() {
        signalRService.handleBookReject = { [weak self] in
            guard let self = self else { return }
            let title = "Spot rejected"
            let message = "Your spot was rejected. You can rate and give a comment."
            self.closeSpotBook(with: title, message: message)
        }
        
        signalRService.handleBookFinish = { [weak self] in
            guard let self = self else { return }
            let title = "Book finish"
            let message = "Your spot was finish. You can rate and give a comment."
            self.closeSpotBook(with: title, message: message)
        }
        
        signalRService.handleShareLocation = {
            print("handleShareLocation")
        }
    }
    
    private func closeSpotBook(with title: String, message: String) {
        guard let status = viewModel.status else { return }
        self.showAlertForBook(with: title, message: message) {
            self.router.perform(.home, from: self)
        } yesAction: {
            self.router.perform(.rate(status), from: self)
        }
    }
    
    private func checkLocationState() {
        switch locationService.state {
        case .disabled:
            showAlertForLocation(with: HomeError.locationDisable) {
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url)
                }
            }
        case .notDetermined:
            let topVc = topMostViewController()
            if topVc is UIAlertController {
                topVc.dismiss(animated: true)
            }
            locationService.requestWhenInUseAuthorization()
        case .available:
            locationService.startUpdatingLocation()
        default:
            showAlertForLocation(with: HomeError.locationDenied) {
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url)
                }
            }
        }
    }
    
    @objc private func appMovedToForeground() {
        checkLocationState()
    }
    
    //MARK: - @IBActions
    @IBAction func navigateMap() {
        guard let status = viewModel.status,
                let location = status.location else { return }
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            if let placemarks = placemarks {
                if placemarks.count > 0 {
                    let placemark = MKPlacemark(placemark: placemarks[0])
                    let mapItem = MKMapItem(placemark: placemark)
                    let options = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
                    mapItem.openInMaps(launchOptions: options)
                }
            }
        }
    }
}

extension SpotDetailsContainerViewController: FloatingPanelControllerDelegate {
    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout {
        return PanelLayout()
    }
}

extension SpotDetailsContainerViewController: LocationServiceDelegate {
    func locationService(_ service: LocationService, didFailWithError error: Error) {
        print(error)
    }
    
    func locationService(_ service: LocationService, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        viewModel.shareLocation(lat: location.coordinate.latitude, lng: location.coordinate.longitude) { error in
            print(error?.message ?? "")
        }
    }
    
    func locationService(_ service: LocationService, didChange state: ServiceState) {
        checkLocationState()
    }
}

class PanelLayout: FloatingPanelLayout {
    var top = UIScreen.main.bounds.height - 420
    var initialState: FloatingPanelState { .tip }
    var position: FloatingPanelPosition { .bottom }
    var anchors: [FloatingPanelState : FloatingPanelLayoutAnchoring] {
        return [.full: FloatingPanelLayoutAnchor(absoluteInset: top, edge: .top, referenceGuide: .superview),
                .tip: FloatingPanelLayoutAnchor(absoluteInset: 138, edge: .bottom, referenceGuide: .safeArea)]
    }
}

extension SpotDetailsContainerViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .home
}
