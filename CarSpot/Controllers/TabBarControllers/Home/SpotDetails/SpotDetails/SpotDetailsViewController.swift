//
//  SpotDetailsViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 16.09.21.
//

import UIKit
import FloatingPanel

class SpotDetailsViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var rejectButton: Button!
    @IBOutlet weak var finishButton: Button!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var coinLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    
    //MARK: - Properties
    var router: SpotDetailsRouter!
    var viewModel: SpotDetailsViewModel!
        
    var openRate: (() -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    
        chatButton.layer.cornerRadius = 16
        chatButton.layer.masksToBounds = true
        
        imageView.layer.cornerRadius = 14
        imageView.layer.masksToBounds = true
    }
    
    //MARK: - Private API
    private func setup() {
        rejectButton.buttonState = .secondary
        
        if let status = viewModel.status,
           let amount = status.amount,
           let address = status.address,
           let distanse = status.distanse,
           let duration = status.time,
           let name = status.firstName,
           let lastName = status.lastName,
           let rate = status.rating {
            coinLabel.text = amount.removeZerosFromEnd() + " Coin"
            addressLabel.text = address
            distanceLabel.text = distanse
            durationLabel.text = duration
            fullNameLabel.text = name + " " + lastName
            rateLabel.text = rate.removeZerosFromEnd()
            
            if let path = status.picturePath {
                let fullPath = Environment.live.baseURL + path
                imageView.setImage(path: fullPath, #imageLiteral(resourceName: "ic_offer_image"))
            } else {
                imageView.image = #imageLiteral(resourceName: "ic_offer_image")
            }
        }
    }
    
    private func confirmPopUp() {
        let ac = UIAlertController(title: "Reject spot",
                                   message: "Are you sure want to reject spot",
                                   preferredStyle: .alert)
        
        let no = UIAlertAction(title: "No", style: .default) { _ in
            
        }
        
        let yes = UIAlertAction(title: "Yes", style: .default) { _ in
            self.reject()
        }
        
        ac.addAction(no)
        ac.addAction(yes)
        ac.preferredAction = yes
        self.present(ac, animated: true)
    }
    
    private func reject() {
        finishButton.isUserInteractionEnabled = false
        rejectButton.buttonState = .load
        viewModel.reject { [weak self] (success, error) in
            guard let self = self else { return }
            self.rejectButton.buttonState = .secondary
            self.finishButton.isUserInteractionEnabled = true
            if let error = error {
                self.showAlert(with: error)
            } else if success {
                self.openRate?()
            }
        }
    }
    
    //MARK: - @IBActions
    @IBAction func reject(_ sender: Button) {
        self.confirmPopUp()
    }
    
    @IBAction func finish(_ sender: Button) {
        rejectButton.isUserInteractionEnabled = false
        sender.buttonState = .load
        viewModel.finish { [weak self] (success, error) in
            guard let self = self else { return }
            sender.buttonState = .default
            self.finishButton.isUserInteractionEnabled = false
            if let error = error {
                self.showAlert(with: error)
            } else if success {
                self.openRate?()
            }
        }
    }
}

extension SpotDetailsViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .home
}
