//
//  SpotDetailsViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 16.09.21.
//

import Foundation

class SpotDetailsViewModel {
    
    //MARK: - Properties
    var status: ParkingStatus?
    
    private let bookProvider = Provider<BookEndPoint>()
    
    //MARK: - Lifecycle
    init(status: ParkingStatus?) {
        self.status = status
    }
    
    //MARK: - Public API
    func finish(completion: @escaping (Bool, CSError?) -> Void) {
        guard let status = status,
              let spotId = status.spotId else { return }
        bookProvider.request(target: .accept(spotId),
                             type: Bool.self) { (success, error) in
            completion(success ?? false, error)
        }
    }
    
    func reject(completion: @escaping (Bool, CSError?) -> Void) {
        guard let status = status,
              let spotId = status.spotId else { return }
        bookProvider.request(target: .reject(spotId),
                             type: Bool.self) { (success, error) in
            completion(success ?? false, error)
        }
    }
}
