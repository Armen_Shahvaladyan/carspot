//
//  SpotDetailsRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 16.09.21.
//

import UIKit

enum SpotDetailsSegue {

}

protocol SpotDetailsRoutable: Routable where SegueType == SpotDetailsSegue, SourceType == SpotDetailsViewController {

}

struct SpotDetailsRouter: SpotDetailsRoutable {
    
    func perform(_ segue: SpotDetailsSegue, from source: SpotDetailsViewController) {
        
    }
}

extension SpotDetailsRouter {
    static func createSpotDetailsViewController(with status: ParkingStatus?) -> SpotDetailsViewController {
        let vc = SpotDetailsViewController.storyboardInstance
        vc.router = SpotDetailsRouter()
        vc.viewModel = SpotDetailsViewModel(status: status)
        
        return vc
    }
}
