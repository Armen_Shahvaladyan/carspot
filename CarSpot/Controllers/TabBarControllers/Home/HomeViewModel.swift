//
//  HomeViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.06.21.
//

import Foundation

class HomeViewModel {
    
    //MARK: - Properties
    var lat: Double?
    var lng: Double?
    var address: String?
    var spots: [Spot]?
    var offer: Offer?
    let status: ParkingStatus?
    
    private let findProvider = Provider<FindParkiingEndPoint>()
    private let suggestProvider = Provider<SuggestParkingEndPoint>()
    private let profileProvider = Provider<ProfileEndPoint>()
    
    //MARK: - Lifecycle
    init(spots: [Spot]?, offer: Offer?, status: ParkingStatus?) {
        self.spots = spots
        self.offer = offer
        self.status = status
    }
    
    //MARK: - Public API
    func validate() -> HomeError? {
        if let _ = lat,
           let _ = lng,
           let address = address,
           !address.isEmpty {
            return nil
        }
        return .address
    }
    
    func stopOffer(completion: @escaping (CSError?) -> Void) {
        suggestProvider.request(target: .stop,
                                type: Bool.self) { (_, error) in
            completion(error)
        }
    }
    
    func findParking(completion: @escaping (CSError?) -> Void) {
        guard let lat = lat,
              let lng = lng,
              let address = address,
              !address.isEmpty else { return }
        findProvider.request(target: .find(lat, lng, address),
                             type: Empty.self) { (_, error) in
            completion(error)
        }
    }
    
    func stopFindParking(completion: @escaping (CSError?) -> Void) {
        findProvider.request(target: .stop,
                             type: Bool.self) { (_, error) in
            completion(error)
        }
    }
    
    func fetchCoins(completion: @escaping (CSError?, Coin?) -> Void) {
        profileProvider.request(target: .coins,
                                type: Coin.self) { coin, error in
            
            completion(nil, coin)
        }
    }
}
