//
//  HomeViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.06.21.
//

import UIKit
import GoogleMaps

class HomeViewController: MainTabBarViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var mapView: MapView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var detailContainerView: ShadowView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var detailHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderTitleLabel: ShimmerLabel!
    @IBOutlet weak var horizontalBar: PlainHorizontalProgressBar!
    
    //MARK: - Properties
    var router: HomeRouter!
    var viewModel: HomeViewModel!
    private let signalRService = SignalRService(url: URL(string: Environment.live.wssURL)!)
    private lazy var detailViewController = HomeDetailRouter.createHomeDetailViewController(homeViewController: self)
    
    private let locationService = LocationService()
    private var loaderTimer: Timer?
    
    private(set) var coinItem: UIBarButtonItem?
    private(set) var plusItem: UIBarButtonItem?
    
    var radarView: RadarView?
    private(set) var detailMaxHeight: CGFloat = 300
    private(set) var detailMinHeight: CGFloat = 154
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        setupSignalService()
        configureNavigatuion()
        configureRadarView()
        configureGradient()
        configureViewsWithState()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    
        loaderView.layer.cornerRadius = 32
        loaderView.layer.masksToBounds = true
        loaderView.layer.maskedCorners = CACornerMask.topLeftTopRight
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.fetchCoins { [weak self] error, coin in
            guard let self = self else { return }
            if let error = error {
                if let plusItem = self.plusItem {
                    self.navigationItem.rightBarButtonItem = plusItem
                }
                self.showAlert(with: error)
            } else if let coin = coin {
                if self.coinItem == nil {
                    self.coinItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                }
                self.coinItem?.title = "\(coin.count.removeZerosFromEnd()) coin"
                if let item = self.coinItem, let plusItem = self.plusItem {
                    self.navigationItem.rightBarButtonItems = [plusItem, item]
                }
            }
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        tabBarItem.title = "Home"
        tabBarItem.image = #imageLiteral(resourceName: "ic_home_location").withRenderingMode(.alwaysTemplate)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Public API
    func addOffer(with address: String, lat: Double, lng: Double) {
        router.perform(.createOffer(address, lat, lng), from: self)
    }
    
    func stopOffer() {
        radarView?.stopSpin()
        segmentedControl.isHidden = false
        mapView.enableDragging()
        self.closeSpots()
        self.updateMapView()
        viewModel.stopOffer { [weak self] error in
            guard let self = self else { return }
            if let error = error {
                self.showAlert(with: error)
            }
        }
    }
    
    func create(offer: Offer) {
        spinRadar()
        segmentedControl.isHidden = true
        detailViewController.update(offer: offer)
        offer.spots.isEmpty ? closeSpots() : openSpots()
        DispatchQueue.main.async {
            self.updateMapView()
            offer.spots.forEach { spot in
                let marker = self.spotMarker(with: spot.position)
                marker.map = self.mapView
            }
        }
    }
    
    func findParking() {
        if let error = viewModel.validate() {
            self.showAlert(with: error)
        } else {
            showLoader()
            detailViewController.status = .find(true)
            segmentedControl.isHidden = true
            spinRadar()
            viewModel.findParking { [weak self] error in
                guard let self = self else { return }
                if let error = error {
                    self.detailViewController.status = .find(false)
                    self.segmentedControl.isHidden = false
                    self.radarView?.stopSpin()
                    self.mapView.enableDragging()
                    self.showAlert(with: error)
                } else {
                    
                }
            }
        }
    }
    
    func stopFindParking() {
        radarView?.stopSpin()
        stopLoader()
        segmentedControl.isHidden = false
        mapView.enableDragging()
        viewModel.stopFindParking { [weak self] error in
            guard let self = self else { return }
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.updateMapView()
                self.closeSpots()
            }
        }
    }
    
    func openAddressChooser() {
        router.perform(.addressChooser, from: self)
    }
    
    func updateAddress(_ address: String?, _ lat: Double?, _ lng: Double?) {
        viewModel.lat = lat
        viewModel.lng = lng
        viewModel.address = address
        updateMapView()
        detailViewController.updateAddress(address, lat, lng)
    }
    
    func accept(spot: Spot) {
        router.perform(.accept(spot), from: self)
    }
    
    func updateOffers() {
        detailViewController.updateOffers()
    }
    
    func didReceive(status: ParkingStatus) {
        router.perform(.spotDetails(status), from: self)
    }
    
    func didReceive(error: CSError) {
        self.showAlert(with: error)
    }
    
    //MARK: - Private API
    private func setup() {
        loaderView.isHidden = true
        router.perform(.detail(detailViewController), from: self)
        
        detailMinHeight = UIDevice.current.hasSafeArea ? 154 : 120
        detailMaxHeight = UIDevice.current.hasSafeArea ? 300 : 270
        detailHeightConstraint.constant = detailMinHeight
        
        mapView.padding.bottom = 70
        mapView.isMyLocationEnabled = false
        mapView.delegate = self
        locationService.delegate = self
        checkLocationState()
        
        NotificationCenter.default.addObserver(self,
                                               selector:#selector(appMovedToForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
    }
    
    private func checkLocationState() {
        switch locationService.state {
        case .disabled:
            showAlertForLocation(with: HomeError.locationDisable) {
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url)
                }
            }
        case .notDetermined:
            let topVc = topMostViewController()
            if topVc is UIAlertController {
                topVc.dismiss(animated: true)
            }
            locationService.requestWhenInUseAuthorization()
        case .available:
            locationService.startUpdatingLocation()
        default:
            showAlertForLocation(with: HomeError.locationDenied) {
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url)
                }
            }
        }
    }
    
    private func setupSignalService() {
        signalRService.handleFindParking = { [weak self] spots in
            guard let self = self else { return }
            if case .suggest = self.detailViewController.status {
                spots.isEmpty ? self.closeSpots() : self.openSpots()
                self.updateMapView()
                self.detailViewController.update(spots: spots)
                
                spots.forEach { spot in
                    let marker = self.spotMarker(with: spot.position)
                    marker.map = self.mapView
                }
            }
        }
        
        signalRService.handleSuggestParking = { [weak self] spots in
            if case .find = self?.detailViewController.status {
                self?.showSuggest(spots)
            }
        }
        
        signalRService.handleBookSpot = { [weak self] status in
            if let status = status {
                guard let self = self else { return }
                self.router.perform(.spotDetails(status), from: self)
            }
        }
    }
    
    private func showSuggest(_ spots: [Spot]) {
        spots.isEmpty ? self.closeSpots() : self.openSpots()
        self.updateMapView()
        self.detailViewController.update(spots: spots)
        
        spots.forEach { spot in
            let marker = self.spotMarker(with: spot.position)
            marker.map = self.mapView
        }
    }
    
    private func configureNavigatuion() {
        navigationController?.hideHairline()
        
        addMenuItem()
        
        didOpenMenu = {
            (self.tabBarController as? TabBarController)?.showSideMenu()
        }
        
        plusItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_plus"), style: .plain, target: self, action: #selector(onSelectPlus))
        plusItem?.tintColor = .appOrange
        navigationItem.rightBarButtonItem = plusItem
    }
    
    private func configureGradient() {
        let gradientMaskLayer = CAGradientLayer()
        gradientMaskLayer.frame = gradientView.bounds
        let white = UIColor.white.cgColor
        gradientMaskLayer.colors = [white, white, white, UIColor.clear.cgColor]
        gradientMaskLayer.locations = [0, 0.1, 0.5, 1]
        gradientView.layer.mask = gradientMaskLayer
    }
    
    private func configureRadarView() {
        let size = max(mapView.bounds.height, mapView.bounds.width) + 70
        radarView = RadarView(frame: CGRect(x: 0, y: 0, width: size, height: size))
        var center = self.view.center
        let safeAreaTop = self.view.safeAreaTop > 44 ? 42 : self.view.safeAreaTop
        center.y -= safeAreaTop
        center.y -= UIDevice.current.hasSafeArea ? 0 : 20
        radarView?.center = center
        radarView?.layer.contentsScale = UIScreen.main.scale
        radarView?.alpha = 0.68
        radarView?.isHidden = true
        mapView.addSubview(radarView!)
    }
    
    private func configureViewsWithState() {
        guard let status = viewModel.status else { return }
        switch status.state {
        case .find:
            segmentedControl.selectedSegmentIndex = 0
            findDefaultData(with: status)
        case .suggest:
            guard let offer = viewModel.offer else { return }
            segmentedControl.selectedSegmentIndex = 1
            create(offer: offer)
        case .book:
            router.perform(.spotDetails(viewModel.status), from: self)
        default:
            print("sdsvd")
        }
    }
    
    private func findDefaultData(with status: ParkingStatus) {
        showLoader()
        detailViewController.status = .find(true)
        segmentedControl.isHidden = true
        spinRadar()
        updateAddress(status.address, status.lat, status.lng)
        showSuggest(viewModel.spots ?? [])
    }
    
    private func openSpots() {
        if detailHeightConstraint.constant == detailMinHeight {
            detailHeightConstraint.constant = detailMaxHeight
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    private func closeSpots() {
        if detailHeightConstraint.constant != detailMinHeight {
            detailHeightConstraint.constant = detailMinHeight
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    private func updateMapView() {
        guard let lat = viewModel.lat,
              let lng = viewModel.lng else { return }
        
        mapView.clear()
        
        let position = CLLocationCoordinate2D(latitude: lat,
                                              longitude: lng)
        mapView.camera = GMSCameraPosition(target: position,
                                           zoom: 15,
                                           bearing: 0,
                                           viewingAngle: 0)
        
        let marker = GMSMarker(position: position)
        let imgView = UIImageView(image: #imageLiteral(resourceName: "ic_radio"))
        imgView.tintColor = .appOrange
        marker.iconView = imgView
        marker.map = mapView
    }
    
    private func spotMarker(with position: CLLocationCoordinate2D) -> GMSMarker {
        let marker = GMSMarker(position: position)
        let imgView = UIImageView(image: #imageLiteral(resourceName: "ic_spot_location"))
        imgView.tintColor = .appOrange
        marker.iconView = imgView
        
        return marker
    }
    
    private func spinRadar() {
        if let lat = viewModel.lat,
           let lng = viewModel.lng {
            
            let position = CLLocationCoordinate2D(latitude: lat,
                                                  longitude: lng)
            let camera = GMSCameraPosition(target: position,
                                           zoom: 15,
                                           bearing: 0,
                                           viewingAngle: 0)
            mapView.animate(to: camera)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.radarView?.spin()
            self.mapView.disableDragging()
        }
    }
    
    private func showLoader() {
        horizontalBar.progress = 0
        loaderView.isHidden = false
        detailContainerView.cornerRadius = 0
        loaderTitleLabel.text = detailViewController.status.loaderTitle
        var time = 0
        var progress: CGFloat = 0.001
        loaderTimer = Timer.scheduledTimer(withTimeInterval: 0.001, repeats: true) { timer in
            if time == 30000 {
                self.loaderTimer?.invalidate()
                self.loaderTimer = nil
            }
            
            self.horizontalBar.progress = progress
            time += 1
            progress += 0.0001
        }
    }
    
    private func stopLoader() {
        loaderView.isHidden = true
        horizontalBar.progress = 0
        loaderTimer?.invalidate()
        loaderTimer = nil
        detailContainerView.cornerRadius = 32
    }
    
    @objc private func onSelectPlus() {
        router.perform(.topUp, from: self)
    }
    
    @objc private func appMovedToForeground() {
        checkLocationState()
    }
    
    //MARK: - IBAction
    @IBAction func onChangeSegment(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            detailViewController.status = .find(false)
        } else if sender.selectedSegmentIndex == 1 {
            detailViewController.status = .suggest(false)
        }
    }
}

extension HomeViewController: LocationServiceDelegate {
    func locationService(_ service: LocationService, didFailWithError error: Error) {
        print(error)
    }
    
    func locationService(_ service: LocationService, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        mapView.animate(toLocation: location.coordinate)
        GooglePlacesRequestHelpers.getPlaceAddress(location: location.coordinate) { address in
            if self.viewModel.status?.state != .find {
                self.updateAddress(address, location.coordinate.latitude, location.coordinate.longitude)
            }
        }
        service.stopUpdatingLocation()
    }
    
    func locationService(_ service: LocationService, didChange state: ServiceState) {
        checkLocationState()
    }
}

extension HomeViewController: GMSMapViewDelegate {
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        locationService.startUpdatingLocation()
        return true
    }
}

extension HomeViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .home
}
