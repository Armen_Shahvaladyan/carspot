//
//  HomeRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 05.06.21.
//

import UIKit

enum HomeSegue {
    case map
    case topUp
    case addressChooser
    case accept(Spot)
    case detail(HomeDetailViewController)
    case createOffer(String, Double, Double)
    case spotDetails(ParkingStatus?)
}

protocol HomeRoutable: Routable where SegueType == HomeSegue, SourceType == HomeViewController {

}

struct HomeRouter: HomeRoutable {
    
    func perform(_ segue: HomeSegue, from source: HomeViewController) {
        switch segue {
        case .detail(let detailViewController):
            source.addChild(detailViewController)
            source.detailContainerView.addSubview(detailViewController.view)
            detailViewController.didMove(toParent: source)
            detailViewController.view.frame = source.detailContainerView.bounds
        case let .createOffer(address, lat, lng):
            let vc = CreateOfferRouter.createCreateOfferViewController(with: address, lat: lat, lng: lng)
            
            vc.didCreateOffer = { [weak source] offer in
                source?.create(offer: offer)
            }
            
            vc.didUpdateAddress = { [weak source] (address, lat, lng) in
                source?.updateAddress(address, lat, lng)
            }
            
            source.navigationController?.pushViewController(vc, animated: true)
        case .addressChooser:
            let vc = AddressChooserRouter.createAddressChooserViewController()
            
            vc.didSelectPlace = { (_, placeDetails) in
                source.updateAddress(placeDetails.name, placeDetails.coordinate?.latitude, placeDetails.coordinate?.longitude)
            }
            
            vc.didSelectMap = {
                self.perform(.map, from: source)
            }
            
            source.present(vc, animated: true)
        case .map:
            let vc = AddressChooserMapRouter.createAddressChooserMapViewController(onlyAddress: true)
            
            vc.didSelectAddress = { [weak source] (address, lat, lng) in
                source?.updateAddress(address, lat, lng)
            }
            
            source.navigationController?.pushViewController(vc, animated: true)
        case .accept(let spot):
            let vc = AcceptSpotRouter.createAcceptSpotViewController(with: spot)
            
            vc.didSuggest = {
                source.updateOffers()
            }
            
            source.presentPanModal(vc)
        case let .spotDetails(status):
            guard let window = source.view.window else { return }
            Launcher.perform(segue: .spotDetails(status), window: window)
        case .topUp:
            let vc = TopupRouter.createTopupViewController()
            source.presentPanModal(vc)
        }
    }
}

extension HomeRouter {
    static func createHomeViewController(with spots: [Spot]?,
                                         offer: Offer?,
                                         status: ParkingStatus?) -> HomeViewController {
        let vc = HomeViewController.storyboardInstance
        vc.router = HomeRouter()
        vc.viewModel = HomeViewModel(spots: spots, offer: offer, status: status)
        
        return vc
    }
}
