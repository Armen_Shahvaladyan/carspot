//
//  StreetOprionsViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 10.07.21.
//

import Foundation

class StreetOptionsViewModel {
    
    //MARK: - Properties
    let options: [StreetOption] = [.privateParking, .publicParking, .privateParking, .publicParking]
}

enum StreetOption: String {
    case publicParking = "Public Parking"
    case privateParking = "Private Parking"
    
    var value: Int {
        switch self {
        case .publicParking:
            return 1
        case .privateParking:
            return 2
        }
    }
}
