//
//  StreetOprionsViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 10.07.21.
//

import UIKit

class StreetOptionsViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var router: StreetOptionsRouter!
    var viewModel: StreetOptionsViewModel!
    
    var didSelectOption: ((StreetOption) -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    //MARK: - Private API
    private func setup() {
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension StreetOptionsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = StreetOptionCell.cell(table: tableView, indexPath: indexPath)
        cell.configure(with: viewModel.options[indexPath.row])
        return cell
    }
}

extension StreetOptionsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        didSelectOption?(viewModel.options[indexPath.row])
        router.perform(.dismiss, from: self)
    }
}

extension StreetOptionsViewController: PanModalPresentable {
    var cornerRadius: CGFloat {
        return 32
    }

    var panScrollable: UIScrollView? {
        return tableView
    }

    var longFormHeight: PanModalHeight {
        return .contentHeight(tableView.contentSize.height + 56)
    }
    
    var anchorModalToLongForm: Bool {
        return false
    }

    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .longForm = state
        else { return }

        panModalSetNeedsLayoutUpdate()
    }
}

extension StreetOptionsViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .home
}
