//
//  StreetOptionCell.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 10.07.21.
//

import UIKit

class StreetOptionCell: TableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - Public API
    func configure(with option: StreetOption) {
        titleLabel.text = option.rawValue
    }
}
