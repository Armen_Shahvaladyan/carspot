//
//  StreetOptionsRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 10.07.21.
//

import UIKit

enum StreetOptionsSegue {
    case dismiss
}

protocol StreetOptionsRoutable: Routable where SegueType == StreetOptionsSegue, SourceType == StreetOptionsViewController {

}

struct StreetOptionsRouter: StreetOptionsRoutable {
    
    func perform(_ segue: StreetOptionsSegue, from source: StreetOptionsViewController) {
        switch segue {
        case .dismiss:
            source.dismiss(animated: true)
        }
    }
}

extension StreetOptionsRouter {
    static func createStreetOptionsViewController() -> StreetOptionsViewController {
        let vc = StreetOptionsViewController.storyboardInstance
        vc.router = StreetOptionsRouter()
        vc.viewModel = StreetOptionsViewModel()
        
        return vc
    }
}
