//
//  RateRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.09.21.
//

import UIKit

enum RateSegue {
    
}

protocol RateRoutable: Routable where SegueType == RateSegue, SourceType == RateViewController {

}

struct RateRouter: RateRoutable {
    
    func perform(_ segue: RateSegue, from source: RateViewController) {
        
    }
}

extension RateRouter {
    static func createRateViewController(with status: ParkingStatus) -> RateViewController {
        let vc = RateViewController.storyboardInstance
        vc.router = RateRouter()
        vc.viewModel = RateViewModel(status: status)
        
        return vc
    }
}
