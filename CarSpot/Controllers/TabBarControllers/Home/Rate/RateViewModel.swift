//
//  RateViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.09.21.
//

import Foundation

class RateViewModel {
    
    //MARK: - Properties
    let status: ParkingStatus
    var selectedTip: String?
    private let provider = Provider<RateEndPoint>()
    
    //MARK: - Lifecycle
    init(status: ParkingStatus) {
        self.status = status
    }
    
    //MARK: - Public API
    func submit(with rate: Int, reason: String?, completion: @escaping (CSError?) -> Void) {
        if let selectedTip = selectedTip,
           let tip = Int(selectedTip),
           let saleSpotId = status.saleSpotId {
            provider.request(target: .tip(saleSpotId, tip),
                             type: Bool.self) { [weak self] (success, error) in
                if success == true {
                    self?.rate(with: rate, reason: reason) { success, error in
                        completion(error)
                    }
                } else {
                    completion(error)
                }
            }
        } else {
            self.rate(with: rate, reason: reason) { success, error in
                completion(error)
            }
        }
    }
    
    //MARK: - Private API
    private func rate(with value: Int, reason: String?, completion: @escaping (Bool, CSError?) -> Void) {
        if let saleSpotId = status.saleSpotId {
            provider.request(target: .add(saleSpotId, value, reason),
                             type: Bool.self) { (success, error) in
                completion(success ?? false, error)
            }
        } else {
            completion(false, CSError(key: "Atention!", message: "Please try later"))
        }
    }
}
