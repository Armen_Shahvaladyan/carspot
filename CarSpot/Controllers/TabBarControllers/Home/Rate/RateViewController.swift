//
//  RateViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 28.09.21.
//

import UIKit

class RateViewController: BaseViewController {

    //MARK: - @IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var ratingView: StarRatingView!
    @IBOutlet weak var commentTextView: UITextView!
    
    @IBOutlet weak var submitButton: Button!
    @IBOutlet weak var notNowButton: Button!
    @IBOutlet weak var tipOneButton: UIButton!
    @IBOutlet weak var tipTwoButton: UIButton!
    @IBOutlet weak var tipCustomTextField: TextField!
    
    //MARK: - Properties
    var router: RateRouter!
    var viewModel: RateViewModel!
    
    private let attributes: [NSAttributedString.Key: Any] = [.font: UIFont.SF(.display, weight: .regular, size: 14),
                                                             .foregroundColor: UIColor("DBDBDE"),
                                                             .underlineStyle: NSUnderlineStyle.single.rawValue]
    var ratingViews: [UIView] {
        [tipOneButton, tipTwoButton, tipCustomTextField]
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        imageView.layer.cornerRadius = 32
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor("#27377E").cgColor
        imageView.layer.masksToBounds = true
        
        ratingViews.forEach {
            $0.layer.cornerRadius = 16
            $0.layer.masksToBounds = true
        }
    }
    
    //MARK: - Private API
    private func setup() {
        notNowButton.buttonState = .secondary
        ratingView.value = 5
        let attributeString = NSMutableAttributedString(string: "Give a comment", attributes: attributes)
        commentButton.setAttributedTitle(attributeString, for: .normal)
        
        let inset = UIEdgeInsets(top: 15, left: 24, bottom: 15, right: 24)
        tipOneButton.contentEdgeInsets = inset
        tipTwoButton.contentEdgeInsets = inset
        ratingViews.forEach { $0.backgroundColor = UIColor("737380", alpha: 0.08) }
        tipCustomTextField.delegate = self
        
        if let path = viewModel.status.picturePath {
            let fullPath = Environment.live.baseURL + path
            imageView.setImage(path: fullPath, #imageLiteral(resourceName: "ic_empty_image"))
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        commentTextView.isEditable = false
    }
    
    private func resetTextField() {
        tipCustomTextField.text = "Custom"
        tipCustomTextField.textColor = UIColor("2E2E33")
        tipCustomTextField.backgroundColor = UIColor("737380", alpha: 0.08)
    }
    
    //MARK: - @IBActions
    @IBAction func submit(_ sender: Button) {
        showLoading(onView: self.view)
        viewModel.submit(with: Int(ratingView.value),
                         reason: commentTextView.text) { [weak self] error in
            guard let self = self else { return }
            self.hideLoading()
            if let error = error {
                self.showAlert(with: error)
            } else {
                guard let window = self.view.window else { return }
                Launcher.perform(segue: .main, window: window)
            }
        }
    }
    
    @IBAction func notNow(_ sender: Button) {
        guard let window = self.view.window else { return }
        Launcher.perform(segue: .main, window: window)
    }

    @IBAction func comment(_ sender: UIButton) {
        commentTextView.isEditable = true
        commentTextView.becomeFirstResponder()
    }
    
    @IBAction func onSelectTipOne(_ sender: UIButton) {
        ratingViews.forEach { $0.backgroundColor = UIColor("737380", alpha: 0.08) }
        tipTwoButton.setTitleColor(UIColor("2E2E33"), for: .normal)
        sender.backgroundColor = .appOrange
        sender.setTitleColor(.white, for: .normal)
        tipCustomTextField.textColor = UIColor("2E2E33")
        viewModel.selectedTip = sender.titleLabel?.text
    }
    
    @IBAction func onSelectTipTwo(_ sender: UIButton) {
        ratingViews.forEach { $0.backgroundColor = UIColor("737380", alpha: 0.08) }
        tipOneButton.setTitleColor(UIColor("2E2E33"), for: .normal)
        sender.backgroundColor = .appOrange
        sender.setTitleColor(.white, for: .normal)
        tipCustomTextField.textColor = UIColor("2E2E33")
        viewModel.selectedTip = sender.titleLabel?.text
    }
}

extension RateViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        ratingViews.forEach { $0.backgroundColor = UIColor("737380", alpha: 0.08) }
        textField.textColor = .white
        textField.backgroundColor = .appOrange
        
        tipOneButton.setTitleColor(UIColor("2E2E33"), for: .normal)
        tipTwoButton.setTitleColor(UIColor("2E2E33"), for: .normal)
        
        if textField.text == "Custom" {
            textField.text = ""
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text,
           text.isEmpty {
            resetTextField()
        } else {
            if textField.text?.hasPrefix("0") == true {
                resetTextField()
            }
            viewModel.selectedTip = textField.text
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let changedText = text.replacingCharacters(in: textRange, with: string)
            return changedText.count <= 5
        }
        return true
    }
}

extension RateViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .home
}
