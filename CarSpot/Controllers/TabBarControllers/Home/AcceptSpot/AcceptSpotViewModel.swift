//
//  AcceptSpotViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 31.08.21.
//

import Foundation

class AcceptSpotViewModel {
    
    //MARK: - Properties
    let spot: Spot
    private let suggestParkingProvider = Provider<SuggestParkingEndPoint>()
    private let findParkiingProvider = Provider<FindParkiingEndPoint>()
    
    //MARK: - Lifecycle
    init(spot: Spot) {
        self.spot = spot
    }
    
    //MARK: - Public API
    func accept(with amount: Double,
                completion: @escaping (CSError?) -> Void) {
        suggestParkingProvider.request(target: .start(spot, amount),
                                       type: Bool.self) { (_, error) in
            completion(error)
        }
    }
    
    func reject(completion: @escaping (CSError?) -> Void) {
        findParkiingProvider.request(target: .hide(spot.id),
                                     type: Bool.self) { (_, error) in
            completion(error)
        }
    }
}
