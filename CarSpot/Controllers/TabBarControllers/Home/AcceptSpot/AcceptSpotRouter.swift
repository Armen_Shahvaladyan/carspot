//
//  AcceptSpotRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 31.08.21.
//

import UIKit

enum AcceptSpotSegue {
    case dismiss
}

protocol AcceptSpotRoutable: Routable where SegueType == AcceptSpotSegue, SourceType == AcceptSpotViewController {

}

struct AcceptSpotRouter: AcceptSpotRoutable {
    
    func perform(_ segue: AcceptSpotSegue, from source: AcceptSpotViewController) {
        switch segue {
        case .dismiss:
            source.dismiss(animated: true)
        }
    }
}

extension AcceptSpotRouter {
    static func createAcceptSpotViewController(with spot: Spot) -> AcceptSpotViewController {
        let vc = AcceptSpotViewController.storyboardInstance
        vc.router = AcceptSpotRouter()
        vc.viewModel = AcceptSpotViewModel(spot: spot)
        
        return vc
    }
}
