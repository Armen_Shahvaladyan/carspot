//
//  AcceptSpotViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 31.08.21.
//

import UIKit
import GoogleMaps

class AcceptSpotViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var mapView: MapView!
    @IBOutlet weak var rejectButton: Button!
    @IBOutlet weak var suggestButton: Button!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var amountView: EditingView!
    
    //MARK: - Properties
    var router: AcceptSpotRouter!
    var viewModel: AcceptSpotViewModel!
    
    var didSuggest: (() -> Void)?
    
    //MARK: - Lifectcle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            
        let position = CLLocationCoordinate2D(latitude: viewModel.spot.lat,
                                              longitude: viewModel.spot.lng)
        mapView.camera = GMSCameraPosition(target: position,
                                           zoom: 15,
                                           bearing: 0,
                                           viewingAngle: 0)
        mapView.animate(toLocation: position)
        let marker = GMSMarker(position: position)
        let imgView = UIImageView(image: #imageLiteral(resourceName: "ic_radio.pdf"))
        imgView.tintColor = .appOrange
        marker.iconView = imgView
        marker.map = mapView
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        mapContainerView.layer.cornerRadius = 32
        mapContainerView.layer.masksToBounds = true
        mapContainerView.layer.maskedCorners = .bottomLeftBottomRight
    }
    
    //MARK: - Private API
    private func setup() {
        rejectButton.buttonState = .secondary
        mapView.isMyLocationEnabled = false
        mapView.myLocationButton = false
        mapView.disableDragging()
        amountView.keyboardType = .decimalPad
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    private func configureView() {
        addressLabel.text = viewModel.spot.address
        distanceLabel.text = viewModel.spot.distanse
        amountView.text = "\(viewModel.spot.amount.removeZerosFromEnd())"
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y -= keyboardSize.height
        }
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    //MARK: - IBActions
    @IBAction func reject(_ sender: Button) {
        sender.buttonState = .load
        suggestButton.isEnabled = false
        viewModel.reject { [weak self] error in
            guard let self = self else { return }
            sender.buttonState = .secondary
            self.suggestButton.isEnabled = true
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.router.perform(.dismiss, from: self)
            }
        }
    }
    
    @IBAction func suggest(_ sender: Button) {
        self.view.endEditing(true)
        guard var amountText = amountView.text else { return }
        amountText = amountText.replacingOccurrences(of: ",", with: ".")
        guard let amount = amountText.doubleValue else { return }
        sender.buttonState = .load
        rejectButton.isEnabled = false
        
        viewModel.accept(with: amount) { [weak self] error in
            guard let self = self else { return }
            sender.buttonState = .default
            self.rejectButton.isEnabled = true
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.viewModel.spot.amount = amount
                self.viewModel.spot.isSuggested = true
                self.didSuggest?()
                self.router.perform(.dismiss, from: self)
            }
        }
    }
}

extension AcceptSpotViewController: PanModalPresentable {
    var cornerRadius: CGFloat {
        return 32
    }

    var panScrollable: UIScrollView? {
        return scrollView
    }

    var shortFormHeight: PanModalHeight {
        return longFormHeight
    }

    var anchorModalToLongForm: Bool {
        return false
    }

    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .longForm = state
        else { return }

        panModalSetNeedsLayoutUpdate()
    }
}

extension AcceptSpotViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .home
}
