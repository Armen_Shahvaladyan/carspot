//
//  OfferDetailRouter.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 08.07.21.
//

import UIKit

enum CreateOfferSegue {
    case pop
    case street
    case addressChooser
    case map
}

protocol CreateOfferRoutable: Routable where SegueType == CreateOfferSegue, SourceType == CreateOfferViewController {

}

struct CreateOfferRouter: CreateOfferRoutable {
    
    func perform(_ segue: CreateOfferSegue, from source: CreateOfferViewController) {
        switch segue {
        case .pop:
            source.navigationController?.popViewController(animated: true)
        case .street:
            let vc = StreetOptionsRouter.createStreetOptionsViewController()
            
            vc.didSelectOption = { option in
                source.updateOption(option)
            }
            
            source.presentPanModal(vc)
        case .addressChooser:
            let vc = AddressChooserRouter.createAddressChooserViewController()
            
            vc.didSelectPlace = { (_, placeDetails) in
                guard let address = placeDetails.name,
                      let lat = placeDetails.coordinate?.latitude,
                      let lng = placeDetails.coordinate?.longitude else { return }
                source.updateAddress(address, lat, lng)
            }
            
            vc.didSelectMap = {
                self.perform(.map, from: source)
            }
            
            source.present(vc, animated: true)
        case .map:
            let vc = AddressChooserMapRouter.createAddressChooserMapViewController(onlyAddress: true)
            
            vc.didSelectAddress = { (address, lat, lng) in
                guard let address = address,
                      let lat = lat,
                      let lng = lng else { return }
                source.updateAddress(address, lat, lng)
            }
            
            source.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension CreateOfferRouter {
    static func createCreateOfferViewController(with address: String, lat: Double, lng: Double) -> CreateOfferViewController {
        let vc = CreateOfferViewController.storyboardInstance
        vc.router = CreateOfferRouter()
        vc.viewModel = CreateOfferViewModel(address: address, lat: lat, lng: lng)
        
        return vc
    }
}
