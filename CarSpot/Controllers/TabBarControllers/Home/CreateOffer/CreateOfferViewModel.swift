//
//  OfferDetailViewModel.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 08.07.21.
//

import Foundation

class CreateOfferViewModel {
    
    //MARK: - Properties
    private(set) var address: String
    private(set) var lat: Double
    private(set) var lng: Double
    private let provider = Provider<SuggestParkingEndPoint>()
    
    var amount: Double = 0
    var streetOption: StreetOption?
    
    //MARK: - Lifecycle
    init(address: String, lat: Double, lng: Double) {
        self.lat = lat
        self.lng = lng
        self.address = address
    }
    
    //MARK: - Public API
    func update(address: String, lat: Double, lng: Double) {
        self.lat = lat
        self.lng = lng
        self.address = address
    }
    
    func checkValidation() -> CreateOfferError? {
        if amount == 0 {
            return .amount
        } else if streetOption == nil {
            return .streetOption
        }
        
        return nil
    }
    
    func addOffer(completion: @escaping (CSError?, Offer?) -> Void) {
        guard let streetOption = streetOption else { return }
        provider.request(target: .add(lat, lng, address, amount, streetOption),
                         type: Offer.self) { (offerSpot, error) in
            completion(error, offerSpot)
        }
    }
}
