//
//  CreateOfferError.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 08.07.21.
//

import Foundation

enum CreateOfferError: Error {
    case amount
    case streetOption
    case unknown(String)
}

extension CreateOfferError: Errorable {
    var message: String {
        switch self {
        case .amount:
            return "Please fill amount"
        case .streetOption:
            return "Please fill street oprion"
        case .unknown(let message):
            return message
        }
    }
}

extension CreateOfferError: Equatable { }
