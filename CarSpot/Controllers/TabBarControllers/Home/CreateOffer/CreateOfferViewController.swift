//
//  OfferDetailViewController.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 08.07.21.
//

import UIKit
import  GoogleMaps

class CreateOfferViewController: BaseViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var mapView: MapView!
    @IBOutlet weak var addressView: TitleView!
    @IBOutlet weak var amountView: EditingView!
    @IBOutlet weak var streetOptionsView: SelectedViewWithIcon!
    @IBOutlet weak var freezeView: UIView!
    
    //MARK: - Properties
    var router: CreateOfferRouter!
    var viewModel: CreateOfferViewModel!
    
    var didCreateOffer: ((Offer) -> Void)?
    var didUpdateAddress: ((String, Double, Double) -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        setupMap()
        updatViews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        mapView.layer.cornerRadius = 32
        mapView.layer.masksToBounds = true
        mapView.layer.maskedCorners = CACornerMask.bottomLeftBottomRight
    }
    
    //MARK: - Public API
    func updateOption(_ option: StreetOption) {
        viewModel.streetOption = option
        streetOptionsView.text = option.rawValue
    }
    
    func updateAddress(_ address: String, _ lat: Double, _ lng: Double) {
        viewModel.update(address: address, lat: lat, lng: lng)
        updatViews()
    }
    
    //MARK: - Private API
    private func setup() {
        title = "Offer spot"
        
        freezeView.isHidden = true
        amountView.delegate = self
        streetOptionsView.delegate = self
        amountView.textField.keyboardType = .decimalPad
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        addressView.addGestureRecognizer(tapGesture)
    }
    
    private func setupMap() {
        mapView.isMyLocationEnabled = false
        mapView.settings.myLocationButton = false
        mapView.disableDragging()
    }
    
    private func updatViews() {
        mapView.clear()
        let position = CLLocationCoordinate2D(latitude: viewModel.lat,
                                              longitude: viewModel.lng)
        mapView.camera = GMSCameraPosition(target: position,
                                           zoom: 15,
                                           bearing: 0,
                                           viewingAngle: 0)
        mapView.animate(toLocation: position)
        let marker = GMSMarker(position: position)
        let imgView = UIImageView(image: #imageLiteral(resourceName: "ic_radio.pdf"))
        imgView.tintColor = .appOrange
        marker.iconView = imgView
        marker.map = mapView
        addressView.text = viewModel.address
    }
    
    @objc private func handleTap() {
        router.perform(.addressChooser, from: self)
    }
    
    @IBAction func createOffer(_ sender: Button) {
        if let error = viewModel.checkValidation() {
            self.showAlert(with: error)
        } else {
            freezeView.isHidden = false
            sender.buttonState = .load
            viewModel.addOffer { [weak self] (error, offerSpot) in
                guard let self = self else { return }
                sender.buttonState = .default
                self.freezeView.isHidden = true
                if let error = error {
                    self.showAlert(with: error)
                } else if let offerSpot = offerSpot {
                    self.didCreateOffer?(offerSpot)
                    self.didUpdateAddress?(self.viewModel.address, self.viewModel.lat, self.viewModel.lng)
                    self.router.perform(.pop, from: self)
                }
            }
        }
    }
}

extension CreateOfferViewController: SelectedViewWithIconDelegate {
    func selectedViewWithIcon(_ view: SelectedViewWithIcon, didSelect text: String) {
        router.perform(.street, from: self)
    }
}

extension CreateOfferViewController: EditingViewDelegate {
    func editingView(_ view: EditingView, didChange text: String) {
        if text.isEmpty {
            viewModel.amount = 0
        } else if let amount = Double(text) {
            viewModel.amount = amount
        }
    }
}

extension CreateOfferViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .home
}
