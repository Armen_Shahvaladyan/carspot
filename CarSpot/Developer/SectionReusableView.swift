//
//  SectionReusableView.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 27.07.21.
//

import UIKit

protocol SectionReusableView: SectionDequeueReusable { }

extension SectionReusableView {
    
    static func register(table: UITableView) {
        table.register(UINib(nibName: String(describing: self), bundle: nil), forHeaderFooterViewReuseIdentifier: String(describing: self))
    }
    
    static func register(collection: UICollectionView, kind: CollectionSectionType) {
        collection.register(UINib(nibName: String(describing: self), bundle: nil),
                            forSupplementaryViewOfKind: kind == .footer ? UICollectionView.elementKindSectionFooter : UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: self))
    }
}
