//
//  Shadowable.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 23.05.21.
//

import UIKit

protocol Shadowable {
    var cornerRadius: CGFloat { get set }
    var shadowRadius: CGFloat { get set }
    var shadowOpacity: Float { get set }
    var shadowOffset: CGSize { get set }
    var shadowColor: UIColor? { get set }
}

protocol Bordering {
    var borderWidth: CGFloat { get set }
    var borderColor: UIColor? { get set }
}
