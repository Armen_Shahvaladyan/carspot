//
//  SectionDequeueReusable.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 27.07.21.
//

import UIKit

protocol SectionDequeueReusable { }

extension SectionDequeueReusable {
    static func section(table: UITableView, indexPath: IndexPath) -> Self {
        table.dequeueReusableHeaderFooterView(withIdentifier: String(describing: self)) as! Self
    }
    
    static func section(collection: UICollectionView, indexPath: IndexPath, kind: CollectionSectionType) -> Self {
        return collection.dequeueReusableSupplementaryView(ofKind: kind == .footer ? UICollectionView.elementKindSectionFooter : UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: self), for: indexPath) as! Self
    }
}

enum CollectionSectionType {
    case header, footer
}
