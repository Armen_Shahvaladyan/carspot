//
//  NibFetchable.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 01.07.21.
//

import UIKit

protocol NibView {
    static func nib() -> UINib
}

extension NibView where Self: UIView {
    static func nib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }

    static func fromNib() -> Self {
        return nib().instantiate(withOwner: self, options: nil).first as! Self
    }
}
