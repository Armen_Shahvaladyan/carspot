//
//  ImagePicker.swift
//  CarSpot
//
//  Created by Armen Shahvaladyan on 10.06.21.
//

import UIKit

enum ImagePickerSourceType {
    case photoLibrary
    case camera
    
    var type: UIImagePickerController.SourceType? {
        switch self {
        case .photoLibrary:
            return .photoLibrary
        case .camera:
            return .camera
        }
    }
    
    var title: String {
        switch self {
        case .photoLibrary:
            return "Photo library"
        case .camera:
            return "Take photo"
        }
    }
}

protocol ImagePickerDelegate: AnyObject {
    func imagePicker(_ imagePicker: ImagePicker, didSelect image: UIImage?)
}

class ImagePicker: NSObject {
    
    //MARK: - Properties
    private let pickerController: UIImagePickerController
    private weak var presentationController: UIViewController?
    private weak var delegate: ImagePickerDelegate?
    
    private var lxFirstFilterFlagOne: Bool = false
    private var lxFirstFilterFlagTwo: Bool = false

    private var lxSecondFilterFlagOne: Bool = false
    private var lxSecondFilterFlagTwo: Bool = false
    
    //MARK: - Lifecycle
    init(presentationController: UIViewController, delegate: ImagePickerDelegate) {
        self.pickerController = UIImagePickerController()
        super.init()
        self.presentationController = presentationController
        self.delegate = delegate
        self.pickerController.modalPresentationStyle = .fullScreen
        self.pickerController.delegate = self
        self.pickerController.allowsEditing = true
        self.pickerController.mediaTypes = ["public.image"]
    }
    
    func open(with type: UIImagePickerController.SourceType) {
        self.pickerController.sourceType = type
        self.presentationController?.present(self.pickerController, animated: true)
    }
    
    //MARK: - Private API
    private func action(for sourceType: ImagePickerSourceType) -> UIAlertAction? {
        switch sourceType {
        case .camera, .photoLibrary:
            guard let type = sourceType.type,
                  UIImagePickerController.isSourceTypeAvailable(type) else { return nil }
            return UIAlertAction(title: sourceType.title, style: .default) { [unowned self] _ in
                self.pickerController.sourceType = type
                self.presentationController?.present(self.pickerController, animated: true)
            }
        }
    }
    
    public func present(from sourceView: UIView) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if let action = self.action(for: .camera) {
            alertController.addAction(action)
        }
       
        if let action = self.action(for: .photoLibrary) {
            alertController.addAction(action)
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverPresentationController = alertController.popoverPresentationController {
            popoverPresentationController.sourceView = sourceView
            popoverPresentationController.sourceRect = sourceView.bounds
        }
        
        self.presentationController?.present(alertController, animated: true)
    }
    
    private func pickerController(_ controller: UIImagePickerController, didSelect image: UIImage?) {
        self.delegate?.imagePicker(self, didSelect: image)
        controller.dismiss(animated: true, completion: nil)
    }
}

extension ImagePicker: UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard let image = info[.editedImage] as? UIImage else {
            return self.pickerController(picker, didSelect: nil)
        }
        self.pickerController(picker, didSelect: image)
    }
}

extension ImagePicker: UINavigationControllerDelegate {
    
}
